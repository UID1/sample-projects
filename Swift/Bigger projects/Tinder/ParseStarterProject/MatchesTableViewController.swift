//
//  MatchesTableViewController.swift
//  Tinder
//
//  Created by Iberius Pred on 06/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import Parse

class MatchesTableViewController: UITableViewController {
    @IBOutlet var matchesTableView: UITableView!

    
    var acceptedUsers = [String]()
    var currentUserId = String()
    var userMatches = [String]()
    var currentMessages = [String]()
    var userMessageStack = [String]()
    
    var uniqueMessageStack = [String:String]()
    
    var images = [UIImage]()
    
    /* Imagine that clicking on each entry would lead to a segue to a whole conversation, then the message query should be generalised to the following: 
     
     messageQuery.whereKey("sender", containedIn: [currentUser, userID])
     messageQuery.whereKey("recipient", containedIn: [currentUser, userID])
     
     That generalization would give messages both received and sent from each of the conversationalist. The results should be returned in chronological order. The array could be generalized to a group of users where the filter would be applied to a certain group. I would suggest the following struct:
     */
    
    struct conversationContainer {
        // if objectId = current() then the key-pair should be '{objectId : "You"}'
        var withUser = String()    // Specify with whom the conversation is with, could also be implicitly extracted from the dictionary
        var namesOfParticipatingUsers = [String:String]() // objectId:userName
        var timeStamp = [NSDate]() // Message timestamps
        var sender = [String]()    // Sender userId/objectId (in User class)
        var content = [String]()   // Message content
    }
    
    /* This struct should then be in an array representing the table rows. */
    /* We can define the following struct: */
    
    struct User {
        init(userObject: PFUser) {
            self.userObject = userObject
        }
        var userObject: PFUser
        var profileImage: UIImage? = nil
        var lastMessage = String() // Here we can replace it with conversationContainer. "withUser" won't be neccessary
    }
    
    var currentUser = User(userObject: PFUser())
    
    /* Now we define an array of user matches and let the function willSet and didSet guard the dataflow into each entry: */
    var userMatches2 = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        //persivalsFetchMatches()
        fetchMatches()
        
    }
    
    private func persivalsFetchMatches() {
        let query = PFUser.query()
        // Provide only those that have accepted current user: (same as me)
        query?.whereKey("accepted", contains: PFUser.current()?.objectId)
        // Provided only those that have been accepted by current user:
        query?.whereKey("objectId", containedIn: PFUser.current()?["accepted"] as! [String])
        
        query?.findObjectsInBackground(block: { (objects, error) in
            if let users = objects {
                for object in users {
                    if let user = object as? PFUser {
                        let imageFile = user["Photo"] as! PFFile
                        imageFile.getDataInBackground(block: { (data, error) in
                            if let imageData = data {
                                
                                let messageQuery = PFQuery(className: "Message")
                                messageQuery.whereKey("recipient", equalTo: (PFUser.current()?.objectId!)!)
                                messageQuery.whereKey("sender", equalTo: user.objectId!)
                                messageQuery.findObjectsInBackground(block: { (objects, error) in
                                    var messageText = "No message from this user."
                                    if let objects = objects {
                                        for message in objects {
                                            if let messageContent = message["content"] as? String {
                                                messageText = messageContent
                                            }
                                        }
                                    }
                                    self.currentMessages.append(messageText)
                                    self.images.append(UIImage(data: imageData)!)
                                    self.acceptedUsers.append(user.objectId!)
                                    self.matchesTableView.reloadData()
                                })
                            }
                        })
                    }
                }
            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return images.count // my base measure: userMatches.count
        //return userMatches2.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell") as! MatchesTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MatchesTableViewCell
        cell.profileImageView.image = images[indexPath.row]
        cell.userIdLabel.text = userMatches[indexPath.row]
        cell.messageLabel.text = uniqueMessageStack[userMatches[indexPath.row]] //currentMessages[indexPath.row]
        cell.awakeFromNib()
        /*
        cell.profileImageView.image = userMatches2[indexPath.row].profileImage
        cell.userIdLabel.text = userMatches2[indexPath.row].userObject.objectId
        cell.messageLabel.text = userMatches2[indexPath.row].lastMessage
        cell.awakeFromNib()*/
        
        return cell
    }

    private func fetchMatches() {
        if let getAccepts = PFUser.current()?["accepted"] as? [String] {
            acceptedUsers = getAccepts
        } else {
            print("Trouble getting the current user accept list")
        }
        let userMatchQuery = PFUser.query()
        userMatchQuery?.whereKey("objectId", containedIn: acceptedUsers)
        userMatchQuery?.whereKey("accepted", contains: PFUser.current()?.objectId)
        userMatchQuery?.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if let unwrapObjects = objects as? [PFUser] {
                    self.userMatches.removeAll()
                    self.images.removeAll()
                    self.currentMessages.removeAll()
                    for eachUser in unwrapObjects {
                        if let userID = eachUser.objectId, let profileImageFilePath = eachUser["Photo"] as? PFFile {
                            //self.userMatches.append(userID)
                            self.uniqueMessageStack[userID] = "Fetching last message ..."
                            self.currentMessages.append("Fetching last mess4ge ...")
                            profileImageFilePath.getDataInBackground(block: { (data, error) in
                                if error == nil {
                                    if let unwrappedData = data, let profileImage = UIImage(data: unwrappedData) {
                                        //self.images.append(profileImage)
                                        
                                        
                                        //self.fetchMessages(forUser: userID)
                                        let messageQuery = PFQuery(className: "Message")
                                        let currentUser = (PFUser.current()?.objectId!)!
                                        self.userMessageStack.removeAll()
                                        messageQuery.whereKey("recipient", equalTo: currentUser)
                                        messageQuery.whereKey("sender", equalTo: userID)
                                        messageQuery.order(byAscending: "createdAt")
                                        messageQuery.findObjectsInBackground { (objects, error) in
                                            var eachUserMessageStack = [String]()
                                            if let objects = objects { // we could downcast it with as? PFObject or perhaps [PFObject]
                                                //print("Found messages from \(userID): \(objects.count)") // Also zero messages are "found"
                                                for eachMessage in objects {
                                                    if let messageContent = eachMessage["content"] as? String {
                                                        self.userMessageStack.append(messageContent)
                                                        eachUserMessageStack.append(messageContent)
                                                        //print(messageContent)
                                                    }
                                                }
                                            }
                                            if eachUserMessageStack.count == 0 {
                                                self.userMessageStack.append("(no messages)")
                                                eachUserMessageStack.append("(no messages)")
                                            }
                                            self.uniqueMessageStack[userID] = eachUserMessageStack.last!
                                            self.userMatches.append(userID)
                                            self.images.append(profileImage)
                                            // append adds onto existing elements, not refreshing the
                                            self.currentMessages.append(self.userMessageStack.last!)
                                            self.matchesTableView.reloadData()

                                        }
                                    }
                                }
                            })
                            //let imageFile = eachUser["Photo"] as! PFFile
                            
                        }
                    }
                }
            } else {
                print("Problem fetching users")
            }
        })
    }
    
    private func fetchMatches2() {
        if let getAccepts = PFUser.current()?["accepted"] as? [String] {
            acceptedUsers = getAccepts
        } else {
            print("Trouble getting the current user accept list")
        }
        let userMatchQuery = PFUser.query()
        userMatchQuery?.whereKey("objectId", containedIn: acceptedUsers)
        userMatchQuery?.whereKey("accepted", contains: PFUser.current()?.objectId)
        userMatchQuery?.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if let unwrapObjects = objects as? [PFUser] {
                    self.userMatches2.removeAll()
                    for eachUser in unwrapObjects {
                        self.userMatches2.append(User(userObject: eachUser))
                        self.getProfileImage(user: self.userMatches2.last!)
                        
                    }
                }
            } else {
                print("Problem fetching users")
            }
        })
    }
    
    private func getProfileImage(user: User) {
        if let profileImageFilePath = user.userObject["Photo"] as? PFFile {
            profileImageFilePath.getDataInBackground(block: { (data, error) in
                if error == nil {
                    if let unwrappedData = data, let profileImage = UIImage(data: unwrappedData) {
                        let endIndex = self.userMatches2.count - 1
                        if  endIndex >= 0 {
                            self.userMatches2[endIndex].profileImage = profileImage
                            //self.currentUser.profileImage = profileImage
                            self.getLastMessage(user: self.userMatches2[endIndex])
                        }
                    }
                }
            })
        }
    }
    
    private func getLastMessage (user: User) {
        let messageQuery = PFQuery(className: "Message")
        let currentUser = (PFUser.current()?.objectId!)!
        let userID = user.userObject.objectId!
        messageQuery.whereKey("recipient", equalTo: currentUser)
        messageQuery.whereKey("sender", equalTo: userID)
        messageQuery.order(byDescending: "createdAt")
        messageQuery.limit = 1
        messageQuery.findObjectsInBackground { (objects, error) in
            var eachUserMessageStack = [String]()
            if let objects = objects { // we could downcast it with as? PFObject or perhaps [PFObject]
                //print("Found messages from \(userID): \(objects.count)") // Also zero messages are "found"
                for eachMessage in objects {
                    if let messageContent = eachMessage["content"] as? String {
                        eachUserMessageStack.append(messageContent)
                    }
                }
            }
            if eachUserMessageStack.count == 0 {
                eachUserMessageStack.append("(no messages)")
            }
            //self.currentUser.lastMessage = eachUserMessageStack.first!
            let endIndex = self.userMatches2.count - 1
            if  endIndex >= 0 {
                self.userMatches2[endIndex].lastMessage = eachUserMessageStack.first!
            }
            self.matchesTableView.reloadData()
        }
    }
    
    private func fetchMessages(forUser: String) {
        let messageQuery = PFQuery(className: "Message")
        let currentUser = (PFUser.current()?.objectId!)!
        userMessageStack.removeAll()
        print("Current user: \(currentUser), other user: \(forUser)")
        messageQuery.whereKey("recipient", equalTo: currentUser)
        messageQuery.whereKey("sender", equalTo: forUser)
        messageQuery.order(byAscending: "createdAt")
        messageQuery.findObjectsInBackground { (objects, error) in
            if let objects = objects { // we could downcast it with as? PFObject or perhaps [PFObject]
                print("Found messages from \(forUser): \(objects.count)")
                for eachMessage in objects {
                        if let messageContent = eachMessage["content"] as? String {
                            self.userMessageStack.append(messageContent)
                            print("Inside query: Appended messages for user \(forUser); \(self.userMessageStack), number of messages appeded: \(self.userMessageStack.count)")
                            //print(messageContent)
                    }
                }
            }
            if self.userMessageStack.count == 0 {
               self.userMessageStack.append("(no messages)")
            }
        }
        print("Appended messages for user \(forUser); \(userMessageStack), number of messages appeded: \(userMessageStack.count)")

        
        // Don't know how to fetch messages:
        // How should the database be designed?
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
