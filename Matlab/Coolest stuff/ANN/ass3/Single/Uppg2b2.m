% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2b Solution 2
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

%TRN = importdata('train_data1.txt');
TRN = importdata('train_data_new.txt');
VLD = importdata('valid_data_new.txt');
TRNn = [(TRN(:,1)-mean(TRN(:,1)))/std(TRN(:,1)) ...
        (TRN(:,2)-mean(TRN(:,2)))/std(TRN(:,2)) TRN(:,3)];
	
TRNn = TRN;
P = length(TRN); % Number of patterns in the system
UP = 2; % Number of patterns to update with
NumIter = 1e6;

% Define the weight functions for the system
I = 1; % Number of outputs in the system
J = 3; % Number of nodes in the hidden layer
K = 2; % Number of inputs in the system

wjk = 2*(rand(J,K)-.5)*.2;
Wij = 2*(rand(I,J)-.5)*.2;


% Set predefined parameters
% Let's try a linearly decreasing η, say that
% η = 1 at i=1, and η = 10^-5 at i = 3*10^5
% where we see that the system converges. We
% can define an η_k and η_m such that
%  η = η_k/(i-η_m) the values of the parameters
% can be determined by solving η_k/(1-η_m) = 1
% and η_k/(3*10^5-η_m) = 10^-5
eta_0 = .1;
eta_1 = .01;
i_s = 1; % Start position of decline
i_e = 4*1e5; % End/target position
eta_s = .01; % Start value
eta_e = 1e-6; % End/target value
% Final decline parameters
eta_m = (eta_e*i_e-eta_s*i_s)/(eta_e-eta_s);
eta_k = eta_s*(i_s-eta_m);
theta_j = zeros(1,J);
theta_i = 0; % One output, one value
beta_0 = .5; % We set g(b) = tanh(beta*b) as defined on page 108


% Initialization of help variables
wb1 = waitbar(0,'Initializing ...');
movegui(wb1, 'southeast');
Ewinsize = 1e4; % Window size
Ewin1 = zeros(1,Ewinsize); % Time span for rolling avg of E
Ewin2 = zeros(1,Ewinsize);
Evec1 = zeros(1,floor(NumIter/Ewinsize)); % Vector of avg E:s
Evec2 = zeros(1,floor(NumIter/Ewinsize));
Cvvec1 = zeros(1,floor(NumIter/Ewinsize));
Cvvec2 = zeros(1,floor(NumIter/Ewinsize));
V_muj = zeros(P,J);

% We set up a trial pattern during training
SimSize = 1000;
simulationUpdateFrequency = 100;
stopRecording = 200000;
iFrame = 0;
% Monte-Carlo simulation
%INP = [rand(SimSize, 1)-.5 rand(SimSize,1)];
% Ordered grid
Xmin = -.5; Xmax = .5; Xstep = .01;
Ymin = 0;   Ymax = 1;  Ystep = .01;
MC1 = (Xmin:Xstep:Xmax)'*ones(1,length(Xmin:Xstep:Xmax));
MC2 = meshgrid((Ymin:Ystep:Ymax)',1:length(Ymin:Ystep:Ymax));
INP = [MC1(:) MC2(:)];
SimSize = length(INP);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [55, 36, 138]]/255;
FIGS.fig6 = figure('Color', [0 0 0]);
CInit = meshgrid(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax);
contourf(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, CInit, 50, 'LineStyle', 'none');
colormap(bone)
hold on
TST = [INP randi(2, ((Xmax-Xmin)/Xstep+1)*((Ymax-Ymin)/Ystep+1),1).*2-3];
contour(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, ...
             reshape(TST(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)', ...
			 [-2 0], '-', 'LineWidth', 3, 'color', FIGS.LineColors(2,:));
plot(TRN(find(TRN(:,3)==-1),1), TRN(find(TRN(:,3)==-1),2), 'o', ...
                            'markers', 10, 'color', FIGS.LineColors(4,:));
plot(TRN(find(TRN(:,3)==1),1), TRN(find(TRN(:,3)==1),2), 'o', ...
                            'markers', 10, 'color', FIGS.LineColors(8,:));
FIGS.plot3 = findall(FIGS.fig6,'Type','hggroup'); % This circumvents a Matlab bug
set(0,'units','pixels');
PixSS = get(0,'screensize');
FigX = 750; FigY = 600;
set(FIGS.fig6, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
FIGS.title1 = title('\fontsize{12}Simple single hidden layer perceptron during training');
FIGS.label1 = xlabel('Initializing ...');
% set(FIGS.plot3(2), 'LevelListMode', 'Auto');
FIGS.canvas1 = gca;
set(gca, 'Color', 'none');
set(gcf, 'Color', [0 0 0]);
% This sets our colors
set(FIGS.canvas1, 'Color', [0 0 0], 'XColor', [.85 .80 .65], 'YColor', [.85 .80 .65]);
set(FIGS.title1, 'Color', [.85 .80 .65]);

% Training/Learning process
for i=1:NumIter
   IdxIter = 1+floor(length(TRNn)*rand(UP,1));
   eta = (i <= 1e5)*eta_0+(i > 1e5)*(i <= 1.5*1e5)*eta_1+ ...
                 (i>1.5*1e5)*eta_k/(i-eta_m-1e5+1);
   % Forward propagation
   V_muj = tanh(beta_0*(TRNn(:,1:2)*wjk')-ones(300,1)*theta_j);
   O_imu = tanh(beta_0*(V_muj*Wij')-ones(length(V_muj),1)*theta_i);
   H = .5*sum((TRNn(:,3)-O_imu).^2);
   % Back propagation
   delta_imu = (TRNn(IdxIter,3)-O_imu(IdxIter)).*beta_0.*(1-O_imu(IdxIter).^2);
   delta_jmu = beta_0.*(1-V_muj(IdxIter,:).^2).*(Wij'*delta_imu')';
   % Stochastic updating of weights
   DeltaWij = eta*delta_imu'*V_muj(IdxIter,:);
   Deltawjk = eta*delta_jmu'*TRN(IdxIter,1:2);
   Deltatheta_i = -eta*sum(delta_imu);
   Deltatheta_j = -eta*sum(delta_jmu);
   Wij = Wij + DeltaWij;
   wjk = wjk + Deltawjk;
   theta_i = theta_i + Deltatheta_i;
   theta_j = theta_j + Deltatheta_j;
   % Energy and classification error calculations
   Ewin1(1+mod(i,Ewinsize)) = .5*sum((TRNn(:,3)-O_imu).^2);
   O_imuC = O_imu;
   V_muj = tanh(beta_0*(VLD(:,1:2)*wjk')-ones(length(VLD(:,1:2)),1)*theta_j);
   O_imu = tanh(beta_0*(V_muj*Wij')-ones(length(V_muj),1)*theta_i);
   Ewin2(1+mod(i,Ewinsize)) = .5*sum((VLD(:,3)-O_imu).^2);
   if mod(i,Ewinsize) == 0
	  Evec1(i/Ewinsize) = mean(Ewin1);
	  Evec2(i/Ewinsize) = mean(Ewin2);
	  Cvvec1(i/Ewinsize) = mean(abs(TRNn(:,3) - sign(O_imuC)));
  	  Cvvec2(i/Ewinsize) = mean(abs(VLD(:,3) - sign(O_imu)));
   end
   % Updating the decision boundary
   if mod(i,simulationUpdateFrequency) == 0
	  V_mujSF = 2*atan(beta_0*(INP*wjk')-ones(length(INP),1)*theta_j)/pi;
      O_imuSF = 2*atan(beta_0*(V_mujSF*Wij')-ones(length(V_mujSF),1)*theta_i)/pi;
      O_imuSSF = beta_0*(V_mujSF*Wij')-ones(length(V_mujSF),1)*theta_i;
	  V_mujMC = tanh(beta_0*(INP*wjk')-ones(length(INP),1)*theta_j);
      O_imuMC = tanh(beta_0*(V_mujMC*Wij')-ones(length(V_mujMC),1)*theta_i)/pi;

      TST = [INP(:,1) INP(:,2) sign(O_imuMC)];
	  % Ordinary decision mesh
	  CMesh = reshape(TST(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  % Softer mesh for a smoother contour plot
	  DMesh = reshape(O_imuMC, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  % Decision surface mesh
	  EMesh = reshape(O_imuSSF, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  set(FIGS.plot3(1), 'ZData', DMesh);
	  set(FIGS.plot3(2), 'ZData', EMesh, 'LevelList', ...
	                        linspace(min(min(EMesh)), max(max(EMesh)), 51));
	  set(FIGS.label1, 'String', sprintf('\\it C_v\\rm  = %f, \\it \\eta \\rm  = %f, Iteration step: %d', ...
	                  mean(abs(TRNn(:,3) - sign(O_imuC))), eta, i));
	  drawnow;
	  if i <= stopRecording
	     iFrame = iFrame +1;
		 % The default method of exporting figs fails to get the proper background
		 % color:
	     %hgexport(FIGS.fig6, sprintf('frame%07.f.png', iFrame), hgexport('factorystyle'), ...
		 %                'Format', 'png');
		 
		 % So I went for the external export_fig package available from GitHub:
		 export_fig(FIGS.fig6, sprintf('frame%07.f.png', iFrame), '-png', '-nocrop');
	  end
	  if mod(i,min(simulationUpdateFrequency,Ewinsize)) == 0
         SPC = toc;
         waitbar(i/NumIter, wb1, sprintf('Calculating at %.2f iterations/s', ...
		                    min(simulationUpdateFrequency,Ewinsize)/SPC));
         tic;
      end
   end
end
close(wb1);

% Plot the energy over iteration steps
FIGS.fig5 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(3, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(4, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Evolution of energy level over time')

FIGS.fig6 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(6, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Evolution of classification error over time')

% Provoke the system to return values given that the weights are set
% in a Monte-Carlo simulation

SimSize = 1000;
INP = [rand(SimSize, 1)-.5 rand(SimSize,1)];
V_muj = zeros(SimSize,J);
V_muj = tanh(beta_0*(INP*wjk')-ones(SimSize,1)*theta_j);
O_imu = tanh(beta_0*(V_muj*Wij')-theta_i);
TST = [INP(:,1) INP(:,2) sign(O_imu)];
FIGS.fig7 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')
