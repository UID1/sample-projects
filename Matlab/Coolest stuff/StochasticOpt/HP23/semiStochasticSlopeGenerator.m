function slopeValues = semiStochasticSlopeGenerator(alphaMax, filterFactor, flatness, ...
                                       numValues)
  warmUpElements = filterFactor * (filterFactor > 1);
  slopeVector = zeros(1, numValues + warmUpElements);
  slopeVector(1) = rand * alphaMax;
  for i = 2:numValues+warmUpElements
    a = 1 - exp(-slopeVector(i-1)/alphaMax);
    b = ((alphaMax - slopeVector(i-1))/alphaMax).^5;
    randValue = exp(-flatness*rand);
    slopeVector(i) = slopeVector(i-1) + alphaMax * (2 * b * randValue - a);
  end
  if filterFactor > 1
    for i = 1:numValues+warmUpElements
      slopeVector(i) = mean(slopeVector(max(1,i-filterFactor):i));
    end
  end
  slopeValues = slopeVector(1+filterFactor * (filterFactor > 1):end);
end