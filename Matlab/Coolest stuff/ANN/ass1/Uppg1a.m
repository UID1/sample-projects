% Artificial Neural Networks FFR135
% (c) Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1a Solution
%
%

tic

N = [10, 50, 100]; % Index i or j ranges over 1,..., N
p = [2, 10, 20];  % Index \nu or \mu ranges over 1, ..., p

% Parameter sets: N = [10, 50, 100]; p = [2, 10, 20];
clear PWS;

for(tstep=1:length(N))
   clear Zeta;
   Zeta = 2*(rand(N(tstep),p(tstep)) > .5) -1;

   % We construct W_ij according to the Hebb rule as defined in
   % page 16 in the lecture notes under section 1.4.2

   W_ij = 1/N(tstep)*Zeta*Zeta';
   W_ij = W_ij - diag(diag(W_ij));

   % Let's make a stability test

   S = Zeta(:,1);

   S_orig = S;
   S(end-1) = -S(end-1);
   %S(N-2) = -S(N-2);

   Error = sum(sum(-sign(W_ij*S) == S_orig))

   toc

   % We compute the cross talk term
   C_k = 0; 
   C_knu = zeros(N(tstep),p(tstep));
   %for(mu = 1:p)
   %	for(nu=1:p)
   %		for(j=1:N)
   %			for(k=1:N)
   %				C_k = C_k + Zeta(k,nu)*Zeta(k,mu)*Zeta(j,mu)*Zeta(j,nu)*(j~=k)*(mu~=nu);
   %				C_knu(k,nu) += Zeta(k,nu)*Zeta(k,mu)*Zeta(j,mu)*Zeta(j,nu)*(j~=k)*(mu~=nu);
   %			end
   %		end
   %	end
   %end
   for(k = 1:N(tstep))
      for(nu=1:p(tstep))
         for(j=1:N(tstep))
            for(mu=1:p(tstep))
               C_knu(k,nu) = C_knu(k,nu) + Zeta(k,nu)*Zeta(k,mu)*Zeta(j,mu)*Zeta(j,nu)*(mu~=nu);
            end
         end
      end
   end

   C_k = -C_k/N(tstep)
   C_knu = -C_knu/N(tstep);


   C_knuvec = reshape(C_knu, 1, prod(size(C_knu)));
   PDFgrain = 50; %length(C_knuvec);
   Px = linspace(min(min(C_knu)), max(max(C_knu)), PDFgrain);
   PC_knu = zeros(1,PDFgrain-1);
   for(i=2:PDFgrain)
      PC_knu(i-1) = sum(C_knuvec > Px(i-1) & C_knuvec <= Px(i))/length(C_knuvec);
   end
   P_Error = sum(C_knuvec > 1)/length(C_knuvec)
   PWS(tstep).C_knu = C_knu;
   
end
   %plot(C_knu(2:end), -log(PC_knu), 'o')

   %Statistisk analys av värden
FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; [230, 229, 214]; [115, 115, 115]; [176, 169, 165]; [211, 202, 183]]/255;
% 'Color', FIGS.LineColors(1, :)
average = 0;
%sigma = sqrt((p-1)*(N-1)/N^2);
%sigma = sqrt(p/N);
sigma = sqrt(.2);
prob = @(x)(1/sqrt(2*pi*sigma^2)*exp(-((x-average).^2)/(2*sigma^2)));
x = linspace(-2,2);
plot(x,-log(prob(x)),'LineWidth', 1, 'Color', FIGS.LineColors(1, :));
hold on;
xlabel('\fontsize{12}C_k^{\nu}');
ylabel('\fontsize{12}-log(P(C_k^{\nu}))');
for(tstep = 1:length(N))
   [PWS(tstep).y_Cv, PWS(tstep).x_Cv] = hist(PWS(tstep).C_knu(:),p(tstep)*6);
   % Numerical distribution
   plot(PWS(tstep).x_Cv,-log(PWS(tstep).y_Cv/trapz(PWS(tstep).x_Cv,PWS(tstep).y_Cv)),'o', 'Color', FIGS.LineColors(tstep+1, :))
end
legend('N(0,sqrt(2)))', 'N = 10, p = 2', 'N = 50, p = 10', 'N = 100, p = 20');
title('\fontsize{12}Distribution of cross-talk terms vs theoretical distribution');
%figure;
%PrCk = 1/sqrt(2*pi*p/N)*exp(-C_knu(:).^2/2*(p/N));
%plot(C_knu(:), -log(PrCk), 'o')
%hold on
%plot(x,-log(prob(x)),'r')
