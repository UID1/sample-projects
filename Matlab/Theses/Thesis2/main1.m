% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
%  The  matlab script 'main1.m'  uses a simulated  series  of stochastic
%  observation  values to  test and evaluate  different ways  of fitting
%  the AR model and the ARMA model. 
%
%  The methods tested for fitting AR are the least-square method and the
%  Yule-Walker method.
%  The methods for estimating ARMA that are tested are the Kalman method
%  and the method used by the Matlab package which is called the back-
%  cast method (see the article "New method for estimating ARMAX models"
%  by Torben Knudsen for more information").
%  Different information criteria and prediction error estimators
%  are evaluated.
%  A method for detrending and filtering out seasonality out of the data
%  is built using fourier methods. Also methods of how to detect these
%  components are introduced.
% 

samples1 = 1000;
TEST1.EE = randn(samples1,1);
TEST1.X = TEST1.EE + ...
        5*cos((1:samples1)'*2*pi/168) + ...
        5*cos((1:samples1)'*2*pi/24) + ...
        exp((1:samples1)'/samples1);


[TEST1.Y TEST1.m TEST1.s1 TEST1.s2] = boxjenkins1(TEST1.X);

% Some fourier analysis shows that the Box-Jenkins methods introduced in
% Brockwell-Davis (1986) don't filter out all periodicity. So we want to
% try and use a filter to  remove these peaks. The fdatool cannot handle
% floating   point  numbers  so  we  have to  use another  sample  rate.
% Multiplying  the sample rate with  100 000 scales  the location of the 
% peaks to 600 Hz  (between 500 and  700 Hz) and  4200 Hz (between 3 900 
% and 4 500 Hz).
%
% We could try to  design a discrete  casual filter  using a convolution
% with a window function or a very narrow exp(-(\xi^2-a)/b) function.
%
% There are the following ways  to design a  bandstop  filter in MATLAB.
% Assume that  the stop band is  Omega = [Omega_l Omega_u]. Then a band-
% stop filter can be designed the following way:
%
%   [num, den] = butter(n, Omega, 'stop'); % Butterworth filter
%   [num, den] = cheby1(n, Omega, 'stop'); % Type 1 Chebyshev
%           hd = fir1(N-1, Omega, 'stop'); % FIR1 filter
%
% The cutoff frequencies in Omega  are normalized  which means that they
% lie between 0 and 1 and are  defined as  F_cutoff/(Sr/2).  Where Sr is
% the sample rate in samples per time unit (e.g. second) and F_cutoff is
% the cutoff frequency in oscillations per time unit (e.g. second). The
% 'half' in the sample rate comes from the nyquist frequency.
%
% The filter can be analyzed with fvtool, one may want to convert the
% coefficients with zp2sos and dfilt.df2tsos.
%
% The filter is then applied by using filter(num, den, X_t).
%
% Another  way to  apply  the filter is  through  the  fdatool.  One can
% generate  matlab  code to  build  the  filter. An example of generated
% matlab code is the following:
%
% 
% /*-------BEGIN-----CODE---------------------------------------------*/
%
% Chebyshev Type II Bandstop filter designed using FDESIGN.BANDSTOP.
%
% All frequency values are in Hz.
% Fs = 100000;  % Sampling Frequency
% 
% Fpass1 = 500;         % First Passband Frequency
% Fstop1 = 550;         % First Stopband Frequency
% Fstop2 = 650;         % Second Stopband Frequency
% Fpass2 = 700;         % Second Passband Frequency
% Apass1 = 1;           % First Passband Ripple (dB)
% Astop  = 60;          % Stopband Attenuation (dB)
% Apass2 = 1;           % Second Passband Ripple (dB)
% match  = 'stopband';  % Band to match exactly
%
% % Construct an FDESIGN object and call its CHEBY2 method.
% h  = fdesign.bandstop(Fpass1, Fstop1, Fstop2, Fpass2, Apass1, Astop, ...
%                       Apass2, Fs);
% Hd = design(h, 'cheby2', 'MatchExactly', match);
%
%
% /*-------END-------CODE---------------------------------------------*/
%
%
% The  filter is  constructed by  h = fdesign(...) and  it can  then  be
% applied  with  filter(h, X_t).  It  can  also  be  analyzed  with hh =
% fvtool(Hd) and  set(hh, 'Analysis', 'freq') if the  frequency response
% is what one wants to analyze.

% So we build the two filters.  The frequencies  were retrieved by plot-
% ting  the spectrum  against a frequency scale  (that is one sample per
% time unit  and then multiplying  the scale by 1e5 in this case,  which
% put the peaks at 600 and 4200 respectively.

% h1 = fdesign.bandstop(500, 550, 650, 700, 1, 60, 1, 1e5)
% h2 = fdesign.bandstop(4000, 4100, 4300, 4400, 1, 60, 1, 1e5)

% And so we apply the filters

% TEST1.Y1 = filter(design(h2), filter(design(h1), TEST1.X));

% The design() function takes forever, so we have to resort to a simpler
% solution.

% Let's apply it manually

%[num1, den1] = cheby2(6, 3, [500 700]/5e4, 'stop');
%[num2, den2] = cheby2(6, 3, [4000 4400]/5e4, 'stop');

% Chebyshev2 doesn't work. Let's try some other filter

[num1, den1] = butter(2, [550 650]/5e4, 'stop');
[num2, den2] = butter(2, [4100 4300]/5e4, 'stop');

TEST1.Y2 = filter(num2, den2, filter(num1, den1, TEST1.X));

% The  filter  does its job but  is not  strong enough,  so we  apply it
% recursively  until  we have  filtered  out the  seasonality  from  the 
% series.

for i = 1:60
	TEST1.Y2 = filter(num2, den2, filter(num1, den1, TEST1.Y2));
	TEST1.Y2 = filter(num1, den1, TEST1.Y2);
end

% Set up the figures for plot

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;




% A basic fourier analysis and a plot is in place here
FIGS.fig1 = figure('Color',[1 1 1]);
plot(TEST1.X, 'LineWidth', 1.5, 'Color', FIGS.LineColors(3, :))
title('\fontsize{12}Simulation with trend and seasonality');
FIGS.fig2 = figure('Color',[1 1 1]);
TESTSPEC1 = fspectrum2(TEST1.X,1);
plot(TESTSPEC1.pscale, TESTSPEC1.spectrum, 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(3, :))
title('\fontsize{12}Period spectrum of the simulation');
FIGS.fig3 = figure('Color',[1 1 1]);
TESTSPEC2 = fspectrum2(TEST1.Y2,1);
plot(TESTSPEC2.fscale, TESTSPEC2.spectrum, 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(4, :))
title([ '\fontsize{12}Freq spectrum of simulation with ' ... 
        'seasonality removed']);
FIGS.fig4 = figure('Color',[1 1 1]);
TEST1_MA = est_ma(TEST1.Y2, 168);
plot(TEST1_MA.precise, 'LineWidth', 1.5, 'Color', FIGS.LineColors(3, :))
title('\fontsize{12}Estimated trend');
FIGS.fig5 = figure('Color',[1 1 1]);
plot(TEST1.Y2-TEST1_MA.rightprec, 'LineWidth', 1.5, ...
       'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Simulation with trend & seasonality removed')
FIGS.fig6 = figure('Color',[1 1 1]);
TESTSPEC3 = fspectrum2(TEST1.Y2-TEST1_MA.rightprec, 1);
plot(TESTSPEC3.fscale, TESTSPEC3.spectrum, 'LineWidth', 1.5, ...
       'Color', FIGS.LineColors(3, :));
title(['\fontsize{12}Freq. spectrum of filtered ' ...
       'and detrended simulation']);
FIGS.fig7 = figure('Color',[1 1 1]);
plot(TEST1.EE, 'LineWidth', 1.5, 'Color', FIGS.LineColors(5, :))
title('\fontsize{12}The original backgound signal of the simulation');
FIGS.fig8 = figure('Color',[1 1 1]);
autocorr(TEST1.Y2-TEST1_MA.rightprec)
title('\fontsize{12}Correlogram for the processed simulation');
FIGS.fig9 = figure('Color',[1 1 1]);
autocorr(TEST1.EE)
title('\fontsize{12}Correlogram for the background signal');
FIGS.fig10 = figure('Color',[1 1 1]);
TESTSPEC4 = fspectrum2(TEST1.EE, 1);
plot(TESTSPEC4.fscale, TESTSPEC4.spectrum, 'LineWidth', 1.5, ...
       'Color', FIGS.LineColors(7, :));
title('\fontsize{12}Freq. spectrum of the background signal');

% In the next step we estimate the AR and ARMA on the simulation.  We'll
% see  if we can capture the  seasonality in a proper way  in the models
% and we'll see how things look like when we work with the detrended and
% deseasonalized data.

% We  start with  some simple AR estimations  and see  what happens when
% removing  the  variations  described  by  the  coefficients  from  the
% observations.

ar_p = 10;

ARYW = ar_est(TEST1.X, ar_p, 1);
ARLS = ar_est(TEST1.X, ar_p, 2);

% We do the same on the detrended and deseasonalized data 

ARYW2 = ar_est(TEST1.Y2-TEST1_MA.rightprec, ar_p, 1);
ARLS2 = ar_est(TEST1.Y2-TEST1_MA.rightprec, ar_p, 2);


% The model parameters removed from the simulation

%ARYW.x = zeros(length(elpriser)-ar_p+1,1)';
%ARLS.x = ARYW.x;
%for i= 1:ar_p-1
%	ARYW.x = ARYW.x + ARYW.Phi(i)*TEST1.X(ar_p-i:end-i);
%	ARLS.x = ARLS.x + ARLS.Phi(i)*TEST1.X(ar_p-i:end-i);
%end

% Then we build functions for estimating information criteria, work with
% them to see which model gets the closest to the simulation.

TEST1IC_yw = get_ic(TEST1.X, ARYW.resid_var, ar_p);
TEST1IC_ls = get_ic(TEST1.X, ARLS.resid_var, ar_p);

TEST2IC_yw = get_ic(TEST1.Y2-TEST1_MA.rightprec, ARYW.resid_var, ar_p);
TEST2IC_ls = get_ic(TEST1.Y2-TEST1_MA.rightprec, ARLS.resid_var, ar_p);

% Now we do the same thing with ARMA.

% Here's what I've learned:  the internal Matlab command ARMAX() returns
% a so called  idpoly object.  Let m be an idpoly object  estimated with
% ARMAX,  then the coefficients can  be retrieved  by addressing m.a and
% m.c  respectively.  Also note  that  the coefficients are  in the form
% A(q)y(t) = C(q)e(t) which  is  why the  first coefficient is 1 and the
% sign is reversed  compared to the Kalman method.  The function  get(m)
% lists all properties of an idpoly 'm'.

% In  the process  I discovered the  Ljung-Box  statistic  and the Final
% Prediction Error.  The L-B statistic is a way to test the coefficients
% the FPE  is  another  variant of  Akaike's  Information  Criterion.  I
% suppose I could implement it in get_ic if it isn't already.

% Kalman estimation of ARMA

fprintf('Kalman filtering ARMA ..')
TEST1ARMA_kalman = Hevia_arma_mle(TEST1.X, 3, 3);
fprintf('.')
TEST2ARMA_kalman = Hevia_arma_mle(TEST1.Y2-TEST1_MA.rightprec, 3, 3);
fprintf(' done.\n')

% Estimation with internal Matlab ARMAX
fprintf('ARMAX estimation 1/2 ... \n')
[TEST1_ARMAX TEST1ARMAX.minac TEST1ARMAX.pbest TEST1ARMAX.qbest] = ...
             armabat(TEST1.X, 3, 3);
fprintf('ARMAX estimation 2/2 ...')
[TEST2_ARMAX TEST2ARMAX.minac TEST2ARMAX.pbest TEST2ARMAX.qbest] = ...
             armabat(TEST1.Y2-TEST1_MA.rightprec, 3, 3);

% Information criteria for the ARMA estimations

T1KalmanIC = get_ic(TEST1.X, TEST1ARMA_kalman.sigma^2, 20);
T2KalmanIC = get_ic(TEST1.Y2-TEST1_MA.rightprec, ...
                    TEST2ARMA_kalman.sigma^2, 20);

T1ARMAXIC = get_ic(TEST1.X, TEST1_ARMAX.NoiseVariance, 20);
T2ARMAXIC = get_ic(TEST1.Y2-TEST1_MA.rightprec, ...
                   TEST2_ARMAX.NoiseVariance, 20);

fprintf('Kalman vs ARMAX estimations on X(t):\n')

[- TEST1_ARMAX.A(2:end)' TEST1ARMA_kalman.ar -TEST1_ARMAX.C(2:end)' ...
   TEST1ARMA_kalman.ma]

fprintf('Kalman vs ARMAX estimations on X(t):\n')

[- TEST2_ARMAX.A(2:end)' TEST2ARMA_kalman.ar -TEST2_ARMAX.C(2:end)' ...
   TEST2ARMA_kalman.ma]


% Then we start working with the real data.

% So the goal of these different estimations  is to test the validity of
% the internal Matlab package for estimating ARMAX models.

% So first we start with a simple model such as the AR model and see how
% the information criteria and the prediction error works with it.

% Then we move on to ARMA and use a subset of these criteria to evaluate
% the Matlab package.  We introduce the Box-Ljung test so what we end up
% with is  the evaluated  Matlab package  and the test  to aply on real-
% world data.



% To be continued in main2.m
