import UIKit
import Parse

class MatchesTableViewController2: UITableViewController {
    @IBOutlet var matchesTableView: UITableView!
    
    var acceptedUsers = [String]()
    
    struct User {
        init(userObject: PFUser) {
            self.userObject = userObject
        }
        var userObject: PFUser
        var profileImage: UIImage?
        var lastMessage = String()
    }
    var userMatches2 = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchMatches2()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return userMatches2.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell") as! MatchesTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MatchesTableViewCell

        cell.profileImageView.image = userMatches2[indexPath.row].profileImage
        cell.userIdLabel.text = userMatches2[indexPath.row].userObject.objectId
        cell.messageLabel.text = userMatches2[indexPath.row].lastMessage
        cell.awakeFromNib()
        
        return cell
    }
    
    private func fetchMatches2() {
        if let getAccepts = PFUser.current()?["accepted"] as? [String] {
            acceptedUsers = getAccepts
        } else {
            print("Trouble getting the current user accept list")
        }
        let userMatchQuery = PFUser.query()
        userMatchQuery?.whereKey("objectId", containedIn: acceptedUsers)
        userMatchQuery?.whereKey("accepted", contains: PFUser.current()?.objectId)
        userMatchQuery?.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if let unwrapObjects = objects as? [PFUser] {
                    self.userMatches2.removeAll()
                    for eachUser in unwrapObjects {
                        self.userMatches2.append(User(userObject: eachUser))
                        // Continue to next subroutine
                        self.getProfileImage(user: self.userMatches2.last!, index: self.userMatches2.count-1)
                        
                    }
                }
            } else {
                print("Problem fetching users")
            }
        })
    }
    
    private func getProfileImage(user: User, index: Int) {
        if let profileImageFilePath = user.userObject["Photo"] as? PFFile {
            profileImageFilePath.getDataInBackground(block: { (data, error) in
                if error == nil {
                    if let unwrappedData = data, let profileImage = UIImage(data: unwrappedData) {
                        let endIndex = self.userMatches2.count - 1
                        if  endIndex >= index {
                            self.userMatches2[index].profileImage = profileImage
                            // Continue to final subroutine
                            self.getLastMessage(user: self.userMatches2[index], index: index)
                        }
                    }
                }
            })
        }
    }
    
    private func getLastMessage (user: User, index: Int) {
        let messageQuery = PFQuery(className: "Message")
        let currentUser = (PFUser.current()?.objectId!)!
        let userID = user.userObject.objectId!
        messageQuery.whereKey("recipient", equalTo: currentUser)
        messageQuery.whereKey("sender", equalTo: userID)
        messageQuery.order(byDescending: "createdAt")
        messageQuery.limit = 1
        messageQuery.findObjectsInBackground { (objects, error) in
            var eachUserMessageStack = [String]()
            if let objects = objects {
                for eachMessage in objects {
                    if let messageContent = eachMessage["content"] as? String {
                        eachUserMessageStack.append(messageContent)
                    }
                }
            }
            if eachUserMessageStack.count == 0 {
                eachUserMessageStack.append("(no messages)")
            }
            let endIndex = self.userMatches2.count - 1
            if  endIndex >= index {
                self.userMatches2[index].lastMessage = eachUserMessageStack.first!
            }
            self.matchesTableView.reloadData()
        }
    }
}
