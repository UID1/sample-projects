0064A870 sub_64A870      proc near               ; CODE XREF: sub_64A970+12p                                  
0064A870                                         ; sub_64A970+A7p                                             
0064A870                                                                                                       
0064A870 arg_0           = dword ptr  10h                                                                      
0064A870 arg_4           = dword ptr  14h                                                                      
0064A870 arg_8           = dword ptr  18h                                                                      
0064A870                                                                                                       
0064A870                 push    ebx                                                                           
0064A871                 push    esi                                                                           
0064A872                 push    edi             ; stdcall measures                                            
0064A873                 mov     esi, [esp+arg_4] ; esi => top of entered #L/K                                 
0064A877                 mov     ecx, [esi]      ; ecx = 2nd half of >#L/K                                     
0064A879                 mov     edx, [esi+4]    ; edx = 1st half of >#L/K                                     
0064A87C                 mov     eax, ecx                                                                      
0064A87E                 xor     eax, edx        ; eax = the two XORed halves of >#L/K                         
0064A880                 mov     ebx, 88888888h  ; ebx = 1000100010001000...b                                  
0064A885                 and     ebx, eax        ; Mask XOred value with ...1000b                              
0064A887                 xor     eax, ebx        ; XOR masked value into eax                                   
0064A889                 shr     ebx, 3          ; Shift the masked value 3 steps right                        
0064A88C                 shl     eax, 1          ; and shift eax one bit left                                  
0064A88E                 lea     ebx, [ebx+ebx*8] ; ebx = ebx+8*ebx                                            
0064A891                 xor     eax, ebx        ; Once again, XOR ebx into eax                                
0064A893                 mov     ebx, 88888888h  ; \                                                           
0064A898                 and     ebx, eax        ; |                                                           
0064A89A                 xor     eax, ebx        ; |                                                           
0064A89C                 shr     ebx, 3          ;  > We repeat the 7 lines above                              
0064A89F                 shl     eax, 1          ; |                                                           
0064A8A1                 lea     ebx, [ebx+ebx*8] ; |                                                          
0064A8A4                 xor     eax, ebx        ; /                                                           
0064A8A6                 mov     ebx, 88888888h  ; \                                                           
0064A8AB                 and     ebx, eax        ; |                                                           
0064A8AD                 xor     eax, ebx        ; |                                                           
0064A8AF                 shr     ebx, 3          ;  > again, we repeat                                         
0064A8B2                 shl     eax, 1          ; |                                                           
0064A8B4                 lea     ebx, [ebx+ebx*8] ; |                                                          
0064A8B7                 xor     eax, ebx        ; /                                                           
0064A8B9                 xor     ecx, eax        ; We XOR the obtained 3-step signature into >#L/K             
0064A8BB                 xor     edx, eax        ; ...                                                         
0064A8BD                 bt      edx, 1Ch        ; Copy the 28th bit into the Carry Flag                       
0064A8C1                 rcr     ecx, 1          ; Rotate through carry; this one 1 step right                 
0064A8C3                 rcl     edx, 4          ; and this one, 4 steps left                                  
0064A8C6                 bt      edx, 1Fh        ; Copy 31th bit into CF                                       
0064A8CA                 rcr     ecx, 4          ; and rotate right,                                           
0064A8CD                 rcl     edx, 1          ; rotate left through CF                                      
0064A8CF                 lea     ebx, ds:674D80h ; We get another lookup table                                 
0064A8D5                 mov     edi, [esp+arg_8] ; edi => #S/N + signature                                    
0064A8D9                 mov     eax, [edi]      ; eax = least significant word of #S/N + signature            
0064A8DB                 xor     eax, ecx        ; eax = eax XOR 1st scrambled half of >L/K                    
0064A8DD                 xlat                    ; mov al, [bx+al]                                             
0064A8DE                 rol     eax, 8          ; Rotate i byte left                                          
0064A8E1                 xlat                                                                                  
0064A8E2                 rol     eax, 8          ; .                                                           
0064A8E5                 xlat                    ; .                                                           
0064A8E6                 rol     eax, 8          ; .                                                           
0064A8E9                 xlat                                                                                  
0064A8EA                 rol     eax, 8          ; Now all of the bytes are translated                         
0064A8ED                 xor     ecx, eax        ; We XOR the obtained result into 1st half of >#L/K           
0064A8EF                 mov     eax, [edi+4]    ; eax = the rest of the #S/N                                  
0064A8F2                 xor     eax, edx        ; We now xor the 2nd half of the scrambles >#L/K              
0064A8F2                                         ; before we do another translation of eax                     
0064A8F4                 xlat                                                                                  
0064A8F5                 rol     eax, 8          ; .                                                           
0064A8F8                 xlat                    ; .                                                           
0064A8F9                 rol     eax, 8          ; .                                                           
0064A8FC                 xlat                                                                                  
0064A8FD                 rol     eax, 8                                                                        
0064A900                 xlat                                                                                  
0064A901                 rol     eax, 8          ; Translation of eax is finished                              
0064A904                 xor     edx, eax        ; We XOR the result into where the 2nd scrambled              
0064A904                                         ; half of >#L/K is located                                    
0064A906                 bt      edx, 0          ; \                                                           
0064A90A                 rcl     ecx, 4          ; |                                                           
0064A90D                 rcr     edx, 1          ; |                                                           
0064A90D                                         ;  > We shuffle the bits of the scrambled >#L/K around        
0064A90F                 bt      edx, 3          ; |                                                           
0064A913                 rcl     ecx, 1          ; |                                                           
0064A915                 rcr     edx, 4          ; /                                                           
0064A918                 mov     eax, ecx        ; \                                                           
0064A918                                         ;   We XOR the two halves of the >L/K into one dword          
0064A91A                 xor     eax, edx        ; /                                                           
0064A91C                 mov     ebx, 88888888h  ; \                                                           
0064A921                 and     ebx, eax        ; |                                                           
0064A923                 xor     eax, ebx        ; |                                                           
0064A925                 shr     ebx, 3          ; |                                                           
0064A928                 shl     eax, 1          ; |                                                           
0064A92A                 lea     ebx, [ebx+ebx*8] ; |                                                          
0064A92D                 xor     eax, ebx        ; |                                                           
0064A92F                 mov     ebx, 88888888h  ; |                                                           
0064A934                 and     ebx, eax        ; |                                                           
0064A936                 xor     eax, ebx        ;  > The same 3-step mask'n'shuffle procedure as above        
0064A938                 shr     ebx, 3          ; |                                                           
0064A93B                 shl     eax, 1          ; |                                                           
0064A93D                 lea     ebx, [ebx+ebx*8] ; |                                                          
0064A940                 xor     eax, ebx        ; |                                                           
0064A942                 mov     ebx, 88888888h  ; |                                                           
0064A947                 and     ebx, eax        ; |                                                           
0064A949                 xor     eax, ebx        ; |                                                           
0064A94B                 shr     ebx, 3          ; |                                                           
0064A94E                 shl     eax, 1          ; /                                                           
0064A950                 lea     ebx, [ebx+ebx*8] ; ebx = ebx + 8*ebx                                          
0064A953                 xor     eax, ebx        ; We XOR it into eax                                          
0064A955                 xor     ecx, eax        ; \                                                           
0064A955                                         ;   We XOR the value into >#L/K data                          
0064A957                 xor     edx, eax        ; /                                                           
0064A959                 mov     esi, [esp+arg_0] ; esi => some location of the subroutine 64AD00's stack frame
0064A95D                 mov     [esi], ecx      ; \                                                           
0064A95D                                         ;   We move the scrambled >#L/K into that stack frame         
0064A95F                 mov     [esi+4], edx    ; /                                                           
0064A962                 pop     edi             ; \                                                           
0064A963                 pop     esi             ;    "Stdcall"                                                
0064A964                 pop     ebx             ; /                                                           
0064A965                 retn                                                                                  
0064A965 sub_64A870      endp                                                                                  