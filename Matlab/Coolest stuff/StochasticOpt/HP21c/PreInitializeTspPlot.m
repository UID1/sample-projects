function tspFigure = PreInitializeTspPlot(cityLocation,range)

tspFigure = figure(1);
set(tspFigure,'DoubleBuffer','on');
axis([range(1),range(2),range(3),range(4)]);
title('\fontsize{16}Ant Colony Optimization', 'FontWeight','Normal');
%axis square;
grid on;

   