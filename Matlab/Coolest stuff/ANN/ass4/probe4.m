% This is similar to probe2 but prividing plots for assignment 3
% program

% for N = 1:Nmax
%    fprintf('Computing for N = %d ... ', N);
%    uppg3krnl
%    C_vNTRN(N) = gCvTRN(end,i_rwin);
%    C_vNVLD(N) = gCvVLD(end,i_rwin);
%    C_vNTrnMtx(N,:) = gCvTRN(end,:);
%    C_vNVldMtx(N,:) = gCvVLD(end,:);
%    ENTrnMtx(N,:) = gETRN(end,:);
%    ENVldMtx(N,:) = gEVLD(end,:);
%    fprintf('done.\n');
% end


FIGS.fig2 = figure('Color', [1 1 1]);
fill([1:length(ENTrnMtx') length(ENTrnMtx'):-1:1], ...
     [min(ENTrnMtx'/(.7*size(DATA,1))) fliplr(max(ENTrnMtx'/(.7*size(DATA,1))))], ...
	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
hold on
FIGS.plot6 = plot(mean(ENTrnMtx'/(.7*size(DATA,1)))', 'LineWidth', 1, 'Color', circshift(FIGS.LineColors(1,:),[0 -1]));
FIGS.plot7 = plot(mean(ENVldMtx'/(.3*size(DATA,1)))', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
fill([1:length(ENTrnMtx') length(ENTrnMtx'):-1:1], ...
     [max(ENTrnMtx'/(.7*size(DATA,1))) fliplr(max(ENVldMtx'/(.3*size(DATA,1))))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
fill([1:length(ENTrnMtx') length(ENTrnMtx'):-1:1], ...
     [min(ENTrnMtx'/(.7*size(DATA,1))) fliplr(min(ENVldMtx'/(.3*size(DATA,1))))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
xlabel('\fontsize{12}Gaussian nodes');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
xlim([1 20])
legend([FIGS.plot6 FIGS.plot7], 'Training data', 'Validation data');

FIGS.fig4 = figure('Color', [1 1 1]);
plot(mean(C_vNTrnMtx')', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
plot(min(C_vNTrnMtx')', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
plot(max(C_vNTrnMtx')', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));


FIGS.fig3 = figure('Color', [1 1 1]);
fill([1:length(C_vNTrnMtx') length(C_vNTrnMtx'):-1:1], ...
     [min(C_vNTrnMtx') fliplr(max(C_vNTrnMtx'))], ...
	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
hold on
FIGS.plot4 = plot(mean(C_vNTrnMtx')', 'LineWidth', 1, 'Color', circshift(FIGS.LineColors(1,:),[0 -1]));
FIGS.plot5 = plot(mean(C_vNVldMtx')', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
fill([1:length(C_vNTrnMtx') length(C_vNTrnMtx'):-1:1], ...
     [max(C_vNTrnMtx') fliplr(max(C_vNVldMtx'))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
fill([1:length(C_vNTrnMtx') length(C_vNTrnMtx'):-1:1], ...
     [min(C_vNVldMtx') fliplr(min(C_vNTrnMtx'))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
xlim([1 20])
ylim([0 1])
xlabel('\fontsize{12}Gaussian nodes');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
legend([FIGS.plot4 FIGS.plot5], 'Training data', 'Validation data');