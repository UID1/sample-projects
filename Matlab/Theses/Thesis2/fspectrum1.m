function fspec = fspectrum1(X_t, F_x)
% SYNOPSIS
%
% fspec = FSPECTRUM1(X_t, F_x)
%
% DESCRIPTION
%
%  This function returns the frequency spectrum of a series of observations
%
% INPUT
%
%   X_t            : The observed time-series
%   F_x            : The sample rate
%                    1 obs per day is 365.256 obs per year
%                    1 obs per hour is 8766.144 obs per year
%
% OUTPUT
%
%   fspec.fscale   : a frequency scale vector
%   fspec.spectrum : the frequency spctrum data
%
% NOTES
%
%  Designed for the final master thesis project in Physics to study
%  model estimation of electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011



% First we as a reference signal. Let's say that it goes from
% t = 0 to t = 10 seconds so the sample rate is length(X_t) per
% 10 seconds

t_ref = 0:10/length(X_t):10;  % Note that there is one element too many
refsig = 15*sin(10*t_ref(1:length(X_t))); % But that is fixed here

% Then we apply the Fast Fourier Transform

Y_t = fftshift(fft(X_t+refsig'));
Z_t = fftshift(fft(X_t));

% We create a vector going from -1 to +1 in even
% steps. We remove the last steps so that this
% vector has the same amount of elements as X_t.

s = -1:2/length(X_t):1-2/length(X_t);

% Now we adjust the frequency scale

refloc = -1 + floor(length(Y_t)/2) + find( ...
         max(Y_t(floor(length(Y_t)/2):length(Y_t))) == ...
	 Y_t(floor(length(Y_t)/2):length(Y_t)));

s = (-1:2*1/(length(X_t)):1)*10/s(refloc);
s = s(1:length(X_t));

f_zero = -21 + floor(length(X_t)/2) + find( ...
         min(abs(s(floor(length(X_t)/2)-20:floor(length(X_t)/2)+20))) == ...
	 s(floor(length(X_t)/2)-20:floor(length(X_t)/2)+20));

fspec.spectrum = abs(Z_t(f_zero:length(X_t)));
fspec.fscale = s(f_zero:length(X_t));
end

