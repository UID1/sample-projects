% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1a Solution
% by Robin Axelsson
%

a1=[8479, 4868, 3696, 2646, 169, 142];
b1=[11968, 5026, 1081, 1050, 691, 184];
c1=[8479, 4167, 2646, 1081, 881, 859, 701, 691, 184, 169, 142];

sigma_n = randperm(length(a1));
mu_m = randperm(length(b1));
lprcnt = 0;

CT = sort([cumsum(a1(sigma_n(1:(end-1)))) cumsum(b1(mu_m))]);
c_test1 = sort([CT(1) diff(CT)], 'descend'); % = "c_tilde"

H1 = sum(((c1-c_test1).^2./c1));
H=H1;
H_vec = H1;
TGL = -1; % Toggle flag for alternating config swaps
time = 0;
alpha_0 = 1e2;

fprintf('Energy level: ')
while H > 0 && H > eps(1)
   time = time + 1;
   beta_0 = alpha_0*time;

   TGL=-TGL;
   if TGL > 0
      SWP = randi(length(sigma_n), 1, 2);
	  sigma_n([SWP(2) SWP(1)]) = sigma_n([SWP(1) SWP(2)]);
   else
      SWP = randi(length(mu_m), 1, 2);
	  mu_m([SWP(2) SWP(1)]) = mu_m([SWP(1) SWP(2)]);
   end
   CT = sort([cumsum(a1(sigma_n(1:(end-1)))) cumsum(b1(mu_m))]);
   c_test2 = sort([CT(1) diff(CT)], 'descend');
   H2 = sum(((c1-c_test2).^2./c1));
   if H2 < H1
      H = H2;
	  sigma_opt = sigma_n;
	  mu_opt = mu_m;
	  c_test1 = c_test2;
   else
      if rand < exp(-beta_0*(H2-H1))
         H = H2;
		 c_test1 = c_test2;
      end
   end
   H1 = H;
   H_vec = [H_vec H];
   if mod(time, 20) == 0
      fprintf(1, repmat('\b',1,lprcnt));
      lprcnt = fprintf('%d, Time step: %i', H, time);
   end
end
fprintf('\n')

FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; [230, 229, 214]; [115, 115, 115]; [176, 169, 165]; [211, 202, 183]]/255;

plot(1:length(H_vec), H_vec, 'LineWidth', 2, 'Color', FIGS.LineColors(3, :));
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system H(S^{(k)})');
title('\fontsize{12}Evolution of energy level over time')