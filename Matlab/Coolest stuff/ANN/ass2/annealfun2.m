% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1a Solution
% by Robin Axelsson
%
function m = annealfun1(a, b, c, alpha_0, sigma_n, mu_m, timeupd)
   global DATA2

   lprcnt = 0;

   CT = sort([cumsum(a(sigma_n(1:(end-1)))) cumsum(b(mu_m))]);
   c_test1 = sort([CT(1) diff(CT)], 'descend'); % = "c_tilde"

   H1 = sum(((c-c_test1).^2./c));
   H=H1;
   TGL = -1; % Toggle flag for alternating config swaps
   time = 0;
   mu_opt = mu_m;
   sigma_opt = sigma_n;

   %fprintf('Energy level: ')   // Uncomment this if the progress should be presented on one line
   while H > 0 && H > eps(1)
      time = time + 1;

      TGL=-TGL;
      if TGL > 0
         SWP = randi(length(sigma_n), 1, 2);
         sigma_n([SWP(2) SWP(1)]) = sigma_n([SWP(1) SWP(2)]);
      else
         SWP = randi(length(mu_m), 1, 2);
	     mu_m([SWP(2) SWP(1)]) = mu_m([SWP(1) SWP(2)]);
      end
      CT = sort([cumsum(a(sigma_n(1:(end-1)))) cumsum(b(mu_m))]);
      c_test2 = sort([CT(1) diff(CT)], 'descend');
      H2 = sum(((c-c_test2).^2./c));
      if H2 < H1
         H = H2;
	     sigma_opt = sigma_n;
	     mu_opt = mu_m;
	     c_test1 = c_test2;
      else
         if rand < exp(-alpha_0*time*(H2-H1))
            H = H2;
		    c_test1 = c_test2;
         end
      end
      H1 = H;
      if mod(time, timeupd) == 0
         %fprintf(1, repmat('\b',1,lprcnt)); // These two lines are for one-line progress reporting
         %lprcnt = fprintf('%d, Temp prob: %.4f, Time step: %i', H, 100*exp(-beta_0*(H2-H1)), time);
		 fprintf('Energy level: %d, Temp prob: %.4f, Time step: %i\r', H, 100*exp(-alpha_0*time*(H2-H1)), time);
		 DATA2.time = time;
         DATA2.H = H;
         DATA2.mu = mu_opt;
         DATA2.sigma = sigma_opt;
         DATA2.c = c_test1;
      end
   end
   DATA2.time = time;
   DATA2.H = H;
   DATA2.mu = mu_opt;
   DATA2.sigma = sigma_opt;
   DATA2.c = c_test1;
   fprintf('\n')
end
