//
//  DriverTableViewController.swift
//  Uber
//
//  Created by Iberius Pred on 10/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import Parse

class DriverTableViewController: UITableViewController {

    var locationManager = CLLocationManager()
    var requestUsernames =  [String]()
    var requestUserIds = [String]()
    var requestLocations = [CLLocationCoordinate2D?]()
    var userLocation = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    @IBAction func userLogout(_ sender: Any) {
        performSegue(withIdentifier: "driverLogoutSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "driverLogoutSegue" {
            locationManager.stopUpdatingLocation()
            PFUser.logOut()
            // Remove that ugly navigation bar at the top of the (login) screen. It certainly is not ubiquitous even thoughit wants to
            self.navigationController?.navigationBar.isHidden = true
        } else if segue.identifier == "showRiderLocationViewController" {
            print("Found locations before seque: %d, selected row: %d", requestLocations.count, tableView.indexPathForSelectedRow!.row)
            if let selectedTableRow = tableView.indexPathForSelectedRow?.row, selectedTableRow < requestLocations.count {
                if let selectedRequestLocation = requestLocations[selectedTableRow] {
                    print("Row \(selectedTableRow) was selected.")
                    if let destination = segue.destination as? RiderLocationViewController {
                        destination.requestLocation = selectedRequestLocation
                        destination.requestUserName = requestUsernames[selectedTableRow]
                        destination.requestUserId = requestUserIds[selectedTableRow]
                    } else {
                        createAlert(title: "Parse Error", message: "Unable to get the location of the requested user")
                    }
                } else {
                    print("Index out of range, index: \(selectedTableRow), availableElements: \(requestLocations.count)")
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return requestUsernames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        // Configure the cell...
        
        // Find distance between the location of the driver and the locations of the driver requests issued by riders
        let driverCLLocation = CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)
        var cellUserName = "???"
        var distanceLabel = " - ?? km away"
        if indexPath.row < requestLocations.count, indexPath.row < requestUsernames.count, let requestLocation = requestLocations[indexPath.row] {
            let riderLocation = CLLocation(latitude: requestLocation.latitude, longitude: requestLocation.longitude)
            let distance = driverCLLocation.distance(from: riderLocation) / 1000
            distanceLabel = String(format: " - %.2f km away", distance)
            cellUserName = requestUsernames[indexPath.row]
        }
        cell.textLabel?.text = cellUserName + distanceLabel
        return cell
    }
    
    internal func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DriverTableViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = manager.location?.coordinate {
            userLocation = location
            let driverLocationQuery = PFQuery(className: "DriverLocation")
            driverLocationQuery.whereKey("username", equalTo: (PFUser.current()?.username)!)
            driverLocationQuery.findObjectsInBackground(block: { (objects, error) in
                if let driverLocations = objects {
                    for driverLocation in driverLocations {
                        driverLocation.deleteInBackground()
                    }
                    let driverLocation = PFObject(className: "DriverLocation")
                    driverLocation["username"] = PFUser.current()?.username
                    driverLocation["location"] = PFGeoPoint(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                    driverLocation.saveInBackground()
                }
            })
            let query = PFQuery(className: "RiderRequest")
            userLocation = location
            query.whereKey("location", nearGeoPoint: PFGeoPoint(latitude: location.latitude, longitude: location.longitude), withinKilometers: 150)
            query.limit = 10
            query.findObjectsInBackground(block: { (objects, error) in
                if let riderRequests = objects {
                    // Since the data aquisition is asynchronous, it's A LOT better to clear the
                    // contents of the cells once the new data has been successfully acquired.
                    // When having the clear statements outside, you can even get duplicates
                    // as there is no sync between the clear and the data write of the bkg˜ process.
                    self.requestUsernames.removeAll()
                    self.requestLocations.removeAll()
                    self.requestUserIds.removeAll()
                    for riderRequest in riderRequests {
                        if let userName = riderRequest["userName"] as? String, let userId = riderRequest.objectId, riderRequest["driverResponded"] == nil {
                            self.requestUsernames.append(userName)
                            self.requestUserIds.append(userId)
                            
                        }
                        if let userLocation = riderRequest["location"] as? PFGeoPoint {
                            self.requestLocations.append(CLLocationCoordinate2D(latitude: userLocation.latitude, longitude: userLocation.longitude))
                        } else {
                            self.requestLocations.append(nil)
                        }
                    }
                    self.tableView.reloadData()
                }
            })
        }
    }
}
