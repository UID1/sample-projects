    //
//  MatchesTableViewCell.swift
//  Tinder
//
//  Created by Iberius Pred on 07/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import Parse

class MatchesTableViewCell: UITableViewCell {

    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    
    @IBAction func send(_ sender: Any) {
        let messageToSend = PFObject(className: "Message")
        messageToSend["sender"] = PFUser.current()?.objectId!
        messageToSend["recipient"] = userIdLabel.text
        messageToSend["content"] = messageTextField.text
        messageToSend.saveInBackground { (success, error) in
            if error != nil {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Submission failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Submission failure", message: "Please try again later.")
                }
            } else {
                print("User sent text message \(self.messageTextField.text) to \(self.userIdLabel.text).")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            //self.dismiss(animated: true, completion: nil)
        }))
        //self.present(alert, animated: true, completion: nil)
    }

}
