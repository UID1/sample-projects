clear all;

GAParams.populationSize = 30;
GAParams.numberOfGenes = 40;
GAParams.crossoverProbability = .8;
GAParams.mutationProbability = .025;
GAParams.variableRange = 5.0;
GAParams.numberOfGenerations = 100;
GAParams.numberOfVariablesToDecode = 2;

seriesLength = 100;

GAParams.tournamentSize = 2;
GAParams.tournamentSelectionParameter = .7;
testSeries1 = zeros(seriesLength, 3);
fprintf('Generating series 1 of 8 ...');
for i=1:seriesLength
  [testSeries1(i, 1:2) testSeries1(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.tournamentSize = 2;
GAParams.tournamentSelectionParameter = .9;
testSeries2 = zeros(seriesLength, 3);
fprintf('Generating series 2 of 8 ...');
for i=1:seriesLength
  [testSeries2(i, 1:2) testSeries2(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.tournamentSize = 5;
GAParams.tournamentSelectionParameter = .7;
testSeries3 = zeros(seriesLength, 3);
fprintf('Generating series 3 of 8 ...');
for i=1:seriesLength
  [testSeries3(i, 1:2) testSeries3(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.tournamentSize = 5;
GAParams.tournamentSelectionParameter = .9;
testSeries4 = zeros(seriesLength, 3);
fprintf('Generating series 4 of 8 ...');
for i=1:seriesLength
  [testSeries4(i, 1:2) testSeries4(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.tournamentSize = 2;
GAParams.tournamentSelectionParameter = .75;
GAParams.crossoverProbability = .0;
testSeries5 = zeros(seriesLength, 3);
fprintf('Generating series 5 of 8 ...');
for i=1:seriesLength
  [testSeries5(i, 1:2) testSeries5(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.crossoverProbability = .25;
testSeries6 = zeros(seriesLength, 3);
fprintf('Generating series 6 of 8 ...');
for i=1:seriesLength
  [testSeries6(i, 1:2) testSeries6(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.crossoverProbability = .5;
testSeries7 = zeros(seriesLength, 3);
fprintf('Generating series 7 of 8 ...');
for i=1:seriesLength
  [testSeries7(i, 1:2) testSeries7(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');

GAParams.crossoverProbability = 1;
testSeries8 = zeros(seriesLength, 3);
fprintf('Generating series 8 of 8 ...');
for i=1:seriesLength
  [testSeries8(i, 1:2) testSeries8(i, 3)] = ...
                    OptimizationFunction(GAParams);
end
fprintf('done.\n');


fprintf('\nSummary:\n\n');
fprintf('            ');
fprintf('E[x_1] | E[x_2]  |  E[z]  | SD(x_1)| SD(x_2)| SD(z)\n');
fprintf('Nt=2, pt=.7 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries1(:,1)), mean(testSeries1(:,2)), mean(testSeries1(:,3)), ...
        std(testSeries1(:,1)), std(testSeries1(:,2)), std(testSeries1(:,3)));
fprintf('Nt=2, pt=.9 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries2(:,1)), mean(testSeries2(:,2)), mean(testSeries2(:,3)), ...
        std(testSeries2(:,1)), std(testSeries2(:,2)), std(testSeries2(:,3)));
fprintf('Nt=5, pt=.7 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries3(:,1)), mean(testSeries3(:,2)), mean(testSeries3(:,3)), ...
        std(testSeries3(:,1)), std(testSeries3(:,2)), std(testSeries3(:,3)));
fprintf('Nt=5, pt=.9 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries4(:,1)), mean(testSeries4(:,2)), mean(testSeries4(:,3)), ...
        std(testSeries4(:,1)), std(testSeries4(:,2)), std(testSeries4(:,3)));
fprintf('    p_co=.8 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries5(:,1)), mean(testSeries5(:,2)), mean(testSeries5(:,3)), ...
        std(testSeries5(:,1)), std(testSeries5(:,2)), std(testSeries5(:,3)));
fprintf('   p_co=.25 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries6(:,1)), mean(testSeries6(:,2)), mean(testSeries6(:,3)), ...
        std(testSeries6(:,1)), std(testSeries6(:,2)), std(testSeries6(:,3)));
fprintf('    p_co=.5 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries7(:,1)), mean(testSeries7(:,2)), mean(testSeries7(:,3)), ...
        std(testSeries7(:,1)), std(testSeries7(:,2)), std(testSeries7(:,3)));
fprintf('     p_co=1 ');
fprintf('%1.4f | %1.4f | %1.4f | %1.4f | %1.4f | %1.4f |\n', ...
        mean(testSeries8(:,1)), mean(testSeries8(:,2)), mean(testSeries8(:,3)), ...
        std(testSeries8(:,1)), std(testSeries8(:,2)), std(testSeries8(:,3)));
