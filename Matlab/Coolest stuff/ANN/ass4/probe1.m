% Just some code to probe without a whole rerun
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [198, 154, 101]]/255;

FIGS.fig1 = figure('Color', [1 1 1]);
Xmin = min(DATA(:,2)); Xmax = max(DATA(:,2));
Xstep = (max(DATA(:,2))-min(DATA(:,2)))/100;
Ymin = min(DATA(:,3)); Ymax = max(DATA(:,3));
Ystep = (max(DATA(:,3))-min(DATA(:,3)))/100;
Qx = (Xmin:Xstep:Xmax)'*ones(1,length(Xmin:Xstep:Xmax));
Qy = meshgrid((Ymin:Ystep:Ymax)',1:length(Ymin:Ystep:Ymax));
gridVec = [Qx(:) Qy(:)];
CInit = meshgrid(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax);
contourf(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, CInit, 50, 'LineStyle', 'none');
colormap(bone)
hold on
gridOut = [gridVec randi(2, ((Xmax-Xmin)/Xstep+1)*((Ymax-Ymin)/Ystep+1),1).*2-3];
contour(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, ...
             reshape(gridOut(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)', ...
			 [-2 0], '-', 'LineWidth', 3, 'color', FIGS.LineColors(2,:));
plot(DATA(find(DATA(:,1)==-1),2), DATA(find(DATA(:,1)==-1),3), 'o', 'LineWidth', 1, ... 
                            'markers', 10, 'color', FIGS.LineColors(4,:))
plot(DATA(find(DATA(:,1)==1),2), DATA(find(DATA(:,1)==1),3), '+', 'LineWidth', 1.3, ...
                            'markers', 10, 'color', FIGS.LineColors(6,:));
FIGS.plot3 = findall(FIGS.fig1,'Type','hggroup'); % This circumvents a Matlab bug
set(0,'units','pixels');
PixSS = get(0,'screensize');
FigX = 750; FigY = 600;
set(FIGS.fig1, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
FIGS.title1 = title('\fontsize{12}Locations of Gaussian nodes and decision boundary');
%FIGS.label1 = xlabel('Initializing ...');
O_gd = ones(length(gridVec),1);
O_sf = ones(length(gridVec),1);
for i = 1:length(gridVec)
   g_i = exp(-sum((gridVec(i,:)'*ones(1,N)-w_kiwin).^2)/2) ...
		 / sum(exp(-sum((gridVec(i,:)'*ones(1,N)-w_kiwin).^2)/2));
   O_gd(i) = tanh(beta_0*(g_i*W_iwin-theta));
   O_sf(i) = beta_0*(g_i*W_iwin-theta);
end
gridOut = [gridVec(:,1) gridVec(:,2) sign(O_gd)];
CMesh = reshape(gridOut(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
DMesh = reshape(O_gd, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
EMesh = reshape(O_sf, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  set(FIGS.plot3(1), 'ZData', DMesh);
	  set(FIGS.plot3(2), 'ZData', EMesh, 'LevelList', ...
	                        linspace(min(min(EMesh)), max(max(EMesh)), 51));
drawnow;
FIGS.MarkerColors = [[12 95 45]; [35 226 75]]/255;
plot(w_ki(1,:), w_ki(2,:), 'v', 'MarkerSize', 12, 'MarkerEdgeColor', ...
     FIGS.MarkerColors(2,:), 'MarkerFaceColor', FIGS.MarkerColors(1,:), ...
	 'LineWidth', 1.5)