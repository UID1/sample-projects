function newVelocities = updateVelocities(Particles, c1, c2, deltat, vmax, inertia)
  deltavij1 = zeros(length(Particles.xij),2);
  deltavij2 = deltavij1;
  deltavij1(:,1) = Particles.xijPB(1) - Particles.xij(:,1);
  deltavij1(:,2) = Particles.xijPB(2) - Particles.xij(:,2);
  deltavij2(:,1) = Particles.xijSB(1) - Particles.xij(:,1);
  deltavij2(:,2) = Particles.xijSB(2) - Particles.xij(:,2);
  deltavij = (c1*rand*deltavij1 + c2*rand*deltavij2)/deltat;
  velocityUpdates = inertia * Particles.vij + deltavij;
  velocities = sqrt(sum(velocityUpdates.^2,2));
  isWithinBoundaries = [velocities velocities] < vmax;
  newVelocities = isWithinBoundaries.*velocityUpdates;
end