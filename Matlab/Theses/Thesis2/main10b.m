% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% In this script  we evaluate the results from  the calculations done in
% main10.m

%
% /----------- : BEGIN : Preparations --------------------------------/
%
%
global f_empir;
global x_empir;
global data1;
global tmpdaily;

if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
tmpdaily = mean(reshape(tmp2010.national, 24, 365));
if ~any(strcmp(who,'ELSPOTd2010'))
   load ELSPOTd2010.mat;
end

ELSPEC1 = fspectrum3(ELSPOTd2010.sys, 1);
ELTREND = est_ma(ELSPOTd2010.sys, 50);
ELSPECdt = fspectrum3(ELSPOTd2010.sys - ELTREND.removable, 1);

[filt.n1, filt.d1] = butter(2, [1600 1700]/5e4, 'stop');
[filt.n2, filt.d2] = butter(2, [16500 17500]/5e4, 'stop');
[filt.n3, filt.d3] = butter(2, [34000 35000]/5e4, 'stop');

ELSPOTdt = ELSPOTd2010.sys - ELTREND.removable;

ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n2, filt.d2, ELSPOTdt));
ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n3, filt.d3, ELddes));
for i=1:2 % 
   ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n2, filt.d2, ELddes));
   ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n3, filt.d3, ELddes));
end
ELSPOTdsdt = ELddes;
ELSPOTds = ELddes+ELTREND.removable;
ELSPECdsdt = fspectrum3(ELSPOTdsdt,1);

load ARMAX3est(Mar-06_16-18.370).mat

%
% /----------- :  END  : Preparations --------------------------------/
%
%

% We set up the configuration for our plots

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

FIGS.fig1 = figure('Color',[1 1 1]);
plot(ELSPEC1.pscale, ELSPEC1.spectrum, 'LineWidth', 1.5, ...
             'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Period spectrum for the daily spot prices');
xlabel('\fontsize{12}Period length (Days)')
xlim([1 50])

FIGS.fig2 = figure('Color',[1 1 1]);
surf(m1a.par.aicmtx(:,:,23))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on 1 year daily data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig3 = figure('Color',[1 1 1]);
surf(m1b.par.aicmtx(:,:,1))
view(-227.5, 30)
title(['\fontsize{12}AIC surface for transformed ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on 1 year daily data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)


FIGS.fig4 = figure('Color',[1 1 1]);
surf(m2a.par.aicmtx)
view(-227.5, 30)
title(['\fontsize{12}AIC surface for ARMA(\itp\rm,\itq\rm)' ...
       ' on 1 year daily data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig5 = figure('Color',[1 1 1]);
surf(m2b.par.aicmtx)
view(-227.5, 30)
title(['\fontsize{12}AIC surface for transformed ARMA(\itp\rm,\itq\rm)' ...
       ' on 1 year daily data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

data1 = m1b.data;

FIGS.fig6 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m1a.data, ...
                     tmpdaily', ...
                     365, 1, 0));
[f_empir x_empir] = ksdensity(ELSPOTdsdt, 'kernel', 'box');

plot(x_model, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_empir, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMAX(7, 24, 23)', '\fontsize{12}Empirical', ...
       'Location', 'SouthEast')

FIGS.fig7 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m1b.data, ...
                     tmpdaily', ...
                     3650, 1, 0));
[f_empir x_empir] = ksdensity(ELSPOTdsdt, 'kernel', 'box');

% We need to adjust the coefficients


%xy_min = fminunc(@ksminimize, [100 100]);
[f_model x_model] = ksdensity(armaxsim(m1b.data, ...
                     tmpdaily', ...
                     3650, 1, 0)*79+119);


plot(x_empir-105, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_empir, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMAX(4, 23, 1)/1000', '\fontsize{12}Empirical', ...
       'Location', 'SouthEast')


FIGS.fig8 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m2a.data, ...
                     'nil', ...
                     365, 1, 0));
[f_empir x_empir] = ksdensity(ELSPOTdsdt, 'kernel', 'box');

plot(x_model, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_empir, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMA(8, 8)', '\fontsize{12}Empirical', ...
       'Location', 'SouthEast')

FIGS.fig9 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m2b.data, ...
                     'nil', ...
                     3650, 1, 0));
[f_empir x_empir] = ksdensity(ELSPOTdsdt, 'kernel', 'box');

plot(x_empir-100, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_empir, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMA(2, 5)/1000', '\fontsize{12}Empirical', ...
       'Location', 'SouthEast')


FIGS.fig10 = figure('Color',[1 1 1]);
plot(armaxsim(m1a.data, tmpdaily', 365, 1, 1), ...
     'Color', FIGS.LineColors(2,:))
hold on
plot(ELSPOTdsdt, 'Color', FIGS.LineColors(4,:), ...
     'LineWidth', 2)
set(gca, 'XTick', 1:90:365)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July' ...
		    'October', 'December'})
xlim([1 365])
title(['\fontsize{12} ARMAX(7, 24, 23)'])

FIGS.fig11 = figure('Color',[1 1 1]);
plot(armaxsim(m1b.data, tmpdaily', 365, 1, 1)*79, ...
     'Color', FIGS.LineColors(2,:))
hold on
plot(ELSPOTdsdt, 'Color', FIGS.LineColors(4,:), ...
     'LineWidth', 2)
set(gca, 'XTick', 1:90:365)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July' ...
		    'October', 'December'})
xlim([1 365])
title(['\fontsize{12}ARMAX(4, 23, 1)/1000'])

% This simulation is non-stationary
%
%FIGS.fig12 = figure('Color',[1 1 1]);
%plot(armaxsim(m2a.data, tmpdaily', 365, 1, 1), ...
%     'Color', FIGS.LineColors(4,:))
%hold on
%plot(ELSPOTdsdt, 'Color', FIGS.LineColors(2,:))
%set(gca, 'XTick', 1:90:365)
%set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
%                    'April', 'July' ...
%		    'October', 'December'})
%xlim([1 365])
%title(['\fontsize{12}Simulated estimations (bright) vs ' ...
%       'detrended/deseasonalized data (dark)'])


FIGS.fig12 = figure('Color',[1 1 1]);
plot(armaxsim(m2b.data, tmpdaily', 365, 1, 1)*79, ...
     'Color', FIGS.LineColors(2,:))
hold on
plot(ELSPOTdsdt, 'Color', FIGS.LineColors(4,:), ...
     'LineWidth', 2)
set(gca, 'XTick', 1:90:365)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July' ...
		    'October', 'December'})
xlim([1 365])
title(['\fontsize{12}ARMA(2, 5)/1000 '])





