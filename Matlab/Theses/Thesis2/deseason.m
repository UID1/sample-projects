%
%
%  Deseasonalization and detrending of stochastic data
%
%
%

% Assume that the processes X_t has a trend component m_t
% and a seasonal component s_t with periodicity d and that 
% we are studying n observations, i.e.
%
%         X_t = m_t + s_t + Y_t       (1)
%
% where Y_t is some random variable that is independent of
% m_t and s_t.
%

% In electricity prices we have periodicites 24 hours, 7
% days and 12 months. When removing a periodicity we
% start with the longest and then go for the shorter
% periodicities.

% Since we are looking at the data for one year of hourly
% prices we look upon the 12 year periodicity as a trend
% and only consider the 168-hour and 24-hour periodicies
% to begin with.

% The ElSpot hourly prices is loaded into the system

fid1 = fopen('elspoth2010.bin', 'r');
ELSPOTh2010 = fread(fid1, 8760, 'double');
fclose(fid1);

% The trend is estimated under the assumption of the
% presence of a given periodicity, starting with the
% longest periodicity d= 168h, the q-value is then
% d/2, i.e. q = 84

d1 = 168;
q1 = 84;
m1_vect = [.5 ones(1, d1-1) .5]';
m1 = ones(1,length(ELSPOTh2010)-d1);
for i = q1+1:length(ELSPOTh2010)-q1
	m1(i-q1) = ELSPOTh2010(i-q1:i+q1)'*m1_vect/d1;
end

m_h2010 = [m1(1)*ones(1,q1) m1 m1(end)*ones(1,q1)]';
m1_h2010 = m_h2010;

% In the next step we remove the 168 hour periodicity
% from the series

% This actually involves two steps. First we try to
% find indices that lie between q and n - q that
% satisfies the equation k + j*d for all possible
% positive integers j. Then we take the average of w(k)
% of the observed spot prices with those indices. We do
% this for all k = 1, ..., d. We simply perform the
% procedure that is stated in the theory, i.e. w(k) is
% the mean of
%
%     {X(k+j*d) - m(k+j*d): q < k+j*d < n-q}
%
% the trick is to make sure that the j's don't exceed
% the q and n-q indices which Matlab takes care of
% when stating k:l:m vectors.

w1_h2010 = ones(1,d1);
s1_base = ones(1,d1);
for k = 1:d1
	indices = q1+k:d1:length(ELSPOTh2010)-q1;
	w1_h2010(k) = mean(ELSPOTh2010(indices) - ...
	                m_h2010(indices));
end

% Then we estimate the periodicity s from those w(k)'s

for k = 1:d1
	s1_base(k) = w1_h2010(k)-mean(w1_h2010);
end

% We know that s_t is a periodic function so we
% use the modulus operator to create a d-periodic
% vector for all the observed spot prices

s1_h2010 = s1_base(1+mod(0:length(ELSPOTh2010)-1,d1))';

% Now, we want to remove the 24-hour periodicity
% from the spot prices. Before we begin we strip the 
% previously estimated periodicity from the spot price
% data


ELSPOTh2010d2 = ELSPOTh2010 - s1_h2010;

% We start by removing the 24 hour trend component
% from the series.

d2 = 24;
q2 = 12;
m2_vect = [.5 ones(1, d2-1) .5]';
m2 = ones(1,length(ELSPOTh2010d2)-d2);
for i = q2+1:length(ELSPOTh2010d2)-q2
	m2(i-q2) = ELSPOTh2010d2(i-q2:i+q2)'*m2_vect/d2;
end

m_h2010 = [m2(1)*ones(1,q2) m2 m2(end)*ones(1,q2)]';
m2_h2010 = m_h2010;

% Now we remove the 24-hour periodicity from the
% series. Since the steps have been explained before
% we proceed without further affirmations.

w2_h2010 = ones(1,d2);
s2_base = ones(1,d2);

for k = 1:d2
	indices = q2+k:d2:length(ELSPOTh2010d2)-q2;
	w2_h2010(k) = mean(ELSPOTh2010d2(indices) - ...
	                m_h2010(indices));
end
for k = 1:d2
	s2_base(k) = w2_h2010(k)-mean(w2_h2010);
end
s2_h2010 = s2_base(1+mod(0:length(ELSPOTh2010)-1,d2))';

% Then we re-estimate the trend with the periodicity(ies)
% removed according to best practices of detrending and
% deseasonalizing statistical data.

m2 = ones(1,length(ELSPOTh2010)-d2);
for i = q2+1:length(ELSPOTh2010)-q2
	m2(i-q2) = (ELSPOTh2010(i-q2:i+q2) - ...
	            s1_h2010(i-q2:i+q2) - ...
		    s2_h2010(i-q2:i+q2))'*m2_vect/d2;
end

m_h2010 = [m2(1)*ones(1,q2) m2 m2(end)*ones(1,q2)]';


% If we go back to equation (1) we are looking for
% Y_t which is the observed spot prices with the
% trend and periodicity removed from it. The periodicities
% are simply superimposed

Y_ELSPOTh2010 = ELSPOTh2010 - m_h2010 - s1_h2010 - s2_h2010;

