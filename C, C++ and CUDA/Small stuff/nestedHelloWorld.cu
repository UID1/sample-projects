#include <stdio.h>
#include <cuda_runtime.h>
#include <iostream>
#include <windows.h>


/*
 * A simple example of nested kernel launches from the GPU. Each thread displays
 * its information when execution begins, and also diagnostics when the next
 * lowest nesting layer completes.
 */

typedef long long int64;
typedef unsigned long long uint64;

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

/*
   Lessons learned here; the CUDA compiler does not support the C++ language,
   nor does it support host specific function calls. That means that parts of
   the code that gets compiled by CUDA, doesn't support C++, i.e. functions
   with "__<xxx>__" qualifiers cannot contain C++ code. Also system/host
   specific function calls are not allowed to be here either.
*/
 
 
__global__ void nestedHelloWorld (int64 const iSize, int64 iDepth)
{
   /*colors_t colors;*/
   int64 tid = threadIdx.x;
   /*colors.setFg(hGray);*/
   printf("Recursion = ");
   /*colors.setFg(dCyan);*/ printf("%d", iDepth);
   /*colors.setFg(hGray);*/ printf(": Hello World from thread ");
   /*colors.setFg(dCyan);*/ printf("%d", tid); /*colors.setFg(hGray);*/
   printf(" block "); /*colors.setFg(dCyan); */printf("%d", blockIdx.x);
   /*colors.setFg(hGray);*/ printf("\n");
   
   /* Condition to stop recursive execution */
   if(iSize == 1) return;
   
   /* Reduce block size to half */
   int64 nthreads = iSize >> 1;
   
   /* Thread 0 launches child grid recursively */
   if(tid == 0 && nthreads > 0)
   {
      nestedHelloWorld<<<1, nthreads>>>(nthreads, ++iDepth);
      /*colors.setFg(hGray);*/ printf(" -------> nested execution depth: ");
      /*colors.setFg(dCyan);*/ printf("%d", iDepth); /*colors.setFg(hGray);*/
      printf("\n");
   }
}

int main(int64 argc, char**argv)
{
   int64 size = 8;
   int64 blockSize = 8; /* Initial block size */
   int64 iGrid = 1;
   colors_t colors;
   if(argc > 1)
   {
      iGrid = atoi(argv[1]);
      size = iGrid*blockSize;
   }
   dim3 block (blockSize);
   dim3 grid ((size+block.x-1)/block.x);
   
   colors.setFg(dYellow); std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " Execution Configuration"; colors.setFg(White);
   std::cout << ":"; colors.setFg(dGray); std::cout << " grid ";
   colors.setFg(dCyan); std::cout << grid.x; colors.setFg(dGray);
   std::cout << " block "; colors.setFg(dCyan); std::cout << block.x;
   colors.setFg(hGray); std::cout << std::endl;
   
   nestedHelloWorld<<<grid, block>>>(block.x, 0);
   
   CHECK(cudaGetLastError());
   CHECK(cudaDeviceReset());
   return 0;
      
}
