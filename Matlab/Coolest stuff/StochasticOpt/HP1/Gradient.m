%
%  Course: Stochastic optimization
%  Assignment: Hand-In 1.1
%  Author: Robin Axelsson
%  Term: Fall 2016
%

function gradientValue = Gradient(x, mu)
  penaltyFactor = 4 * x(1) * mu * (1 - x(1)^2 - x(2)^2);
  delFpX1 = 2 * (x(1) - 1) - x(1) * penaltyFactor;
  delFpX2 = 4 * (x(2) - 2) - x(2) * penaltyFactor;
  
  gradientValue = [delFpX1 delFpX2];