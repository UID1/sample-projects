% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2c Solution 1
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

%TRN = importdata('train_data1.txt');
TRN = importdata('train_data_new.txt');
VLD = importdata('valid_data_new.txt');
TRNn = [(TRN(:,1)-mean(TRN(:,1)))/std(TRN(:,1)) ...
        (TRN(:,2)-mean(TRN(:,2)))/std(TRN(:,2)) TRN(:,3)];
	
TRNn = TRN;
P = length(TRN); % Number of patterns in the system
NumIter = 1e6;

% Define the weight functions for the system
I = 1; % Number of outputs in the system
J = 3; % Number of nodes in the hidden layer
K = 2; % Number of inputs in the system

i0 = 2; % Number of inputs in the system
i1 = 3; % Number of nodes in hidden layer 1
i2 = 2; % Number of nodes in hidden layer 2
i3 = 1; % Number of inputs in the system

wjk = 2*(rand(J,K)-.5)*.2;
Wij = 2*(rand(I,J)-.5)*.2;

w_i0i1 = 2*(rand(i0,i1)-.5)*.2;
w_i1i2 = 2*(rand(i1,i2)-.5)*.2;
w_i2i3 = 2*(rand(i2,i3)-.5)*.2;

% Set predefined parameters
theta_i1 = zeros(1,i1);
theta_i2 = zeros(1,i2);
theta_i3 = zeros(1,i3);
eta = .01;
beta_0 = .5; % We set g(b) = tanh(beta*b) as defined on page 108


% Initialization of help variables
wb1 = waitbar(0,'Initializing ...');
Ewinsize = 1e4; % Window size
Ewin1 = zeros(1,Ewinsize); % Time span for rolling avg of E
Ewin2 = zeros(1,Ewinsize);
Evec1 = zeros(1,floor(NumIter/Ewinsize)); % Vector of avg E:s
Evec2 = zeros(1,floor(NumIter/Ewinsize));
Cvvec1 = zeros(1,floor(NumIter/Ewinsize));
Cvvec2 = zeros(1,floor(NumIter/Ewinsize));
V_muj = zeros(P,J);

% Training/Learning process
for i=1:NumIter
   % Forward propagation
   V_mui0 = TRNn(:,1:2);
   V_mui1 = tanh(beta_0*(w_i0i1'*V_mui0')'-ones(length(V_mui0),1)*theta_i1);
   V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(length(V_mui1),1)*theta_i2);
   V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(length(V_mui2),1)*theta_i3); % = O_mui3 - system outtoputtu
   % Backward propagation
   delta_mui3 = beta_0*(1-V_mui3.^2).*(TRNn(:,3)-V_mui3);
   delta_mui2 = beta_0*(1-(V_mui2).^2).*(delta_mui3*w_i2i3');
   delta_mui1 = beta_0*(1-(V_mui1).^2).*(delta_mui2*w_i1i2');

   % Batch update of the system
   Deltaw_i2i3 = eta*V_mui2'*delta_mui3;
   Deltaw_i1i2 = eta*V_mui1'*delta_mui2;
   Deltaw_i0i1 = eta*V_mui0'*delta_mui1;
   Deltatheta_i3 = -eta*sum(delta_mui3);
   Deltatheta_i2 = -eta*sum(delta_mui2);
   Deltatheta_i1 = -eta*sum(delta_mui1);

   w_i2i3 = w_i2i3 + Deltaw_i2i3;
   w_i1i2 = w_i1i2 + Deltaw_i1i2;
   w_i0i1 = w_i0i1 + Deltaw_i0i1;

   theta_i3 = theta_i3 + Deltatheta_i3;
   theta_i2 = theta_i2 + Deltatheta_i2;
   theta_i1 = theta_i1 + Deltatheta_i1;

   % Energy calculations
   H = .5*sum((TRNn(:,3)-V_mui3).^2);
   Ewin1(1+mod(i,Ewinsize)) = .5*sum((TRNn(:,3)-V_mui3).^2);
   V_muiC = V_mui3;
   V_mui0 = VLD(:,1:2);
   V_mui1 = tanh(beta_0*(w_i0i1'*V_mui0')'-ones(length(V_mui0),1)*theta_i1);
   V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(length(V_mui1),1)*theta_i2);
   V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(length(V_mui2),1)*theta_i3); % = O_mui3 - system outtoputtu
   Ewin2(1+mod(i,Ewinsize)) = .5*sum((VLD(:,3)-V_mui3).^2);

   if mod(i,Ewinsize) == 0
      waitbar(i/NumIter, wb1);
	  Evec1(i/Ewinsize) = mean(Ewin1);
	  Evec2(i/Ewinsize) = mean(Ewin2);
	  Cvvec1(i/Ewinsize) = mean(abs(TRNn(:,3) - sign(V_muiC)));
	  Cvvec2(i/Ewinsize) = mean(abs(VLD(:,3) - sign(V_mui3)));

      SPC = toc;
      waitbar(i/NumIter, wb1, sprintf('Calculating at %.2f iterations/s', Ewinsize/SPC));
	  tic;
   end
end
close(wb1);

% Visualize the problem at hand
FIGS.fig4 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]]/255;

plot(TRN(find(TRN(:,3)==-1),1), TRN(find(TRN(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TRN(find(TRN(:,3)==1),1), TRN(find(TRN(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Locations of our training data points')

% Plot the energy over iteration steps
FIGS.fig5 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(3, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(4, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Evolution of energy level over time')

FIGS.fig6 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(6, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Evolution of classification error over time')


% Calculate the classification error as defined in the assignment

%C_v = mean(abs(TRNn(:,3) - sign(V_mui3)));

% Provoke the system to return values given that the weights are set
% in a Monte-Carlo simulation

SimSize = 1000;
INP = [rand(SimSize, 1)-.5 rand(SimSize,1)];
%V_mui1 = zeros(SimSize,J);
V_mui1 = tanh(beta_0*(w_i0i1'*INP')'-ones(SimSize,1)*theta_i1);
V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(SimSize,1)*theta_i2);
V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(SimSize,1)*theta_i3); 
TST = [INP(:,1) INP(:,2) sign(V_mui3)];
FIGS.fig7 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')
