#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <ctime>
#include <locale>
#include <cstdlib>
#include <Windows.h>

#define QS_MAX_LEVELS 300
typedef long long int64;
typedef unsigned long long uint64;
#define MAX_ZERO_SOLUTIONS 1000000

/* CUDA grid configuration */

#define GDIMX 4//8
#define GDIMY 4//8
#define GDIMZ 1
#define BDIMX 4//8
#define BDIMY 4//8
#define BDIMZ 1
#define NUM_THREADS 256 //4096

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
      {                                                                       \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
      }                                                                       \
}                                                                             \

struct scEnzConfig
{
	int *vEnzA;  // Vector of elements of enzyme A
	int *vEnzB;  // and B respectively
	int *vDigest; // Vector of resulting double digest of A and B
	int iSizeA; // Vector sizes
	int iSizeB;
	int iSizeDigest;
};

struct simConfig
{
	float fAlpha;
	float fBeta;
	int64 i64Seed;
	int64 i64Interval;
};

struct scKernelStatus
{
	float currentOptH;
	int currentOptCfgA;
	int currentOptCfgB;
	int numIterations;
	int threadIdx;
	int intCache;
	int64 int64Cache;
	float floatCache;
	bool isAlive;
};

struct scGlobalStatus
{
	/* A struct of arrays SOA */
	int zeroConfA[MAX_ZERO_SOLUTIONS];
	int zeroConfB[MAX_ZERO_SOLUTIONS];
	int numZeroes;
	int64 globalNumIterations;
	int64 currentIdx;
	int64 activeThreadIdx;
	bool stopSignal;
	bool allThreadsDead;
	float currentEnergy;
	int currentGlobalOptCfgA;
	int currentGlobalOptCfgB;
};

__global__ void testSetup(scEnzConfig *inpCfg, simConfig *inpSimCfg,
	scKernelStatus *currentKernelStatus,
	volatile scGlobalStatus *currentGlobalStatus,
	volatile int *bufferReady)
{
	printf("Thread available here ... ");
	printf("ThreadID %d,%d,%d ", threadIdx.x, threadIdx.y, threadIdx.z);
	printf("BlockID %d,%d,%d ", blockIdx.x, blockIdx.y, blockIdx.z);
	printf("BlockDim %d,%d,%d ", blockDim.x, blockDim.y, blockDim.z);
	printf("GridDim %d,%d,%d ", gridDim.x, gridDim.y, gridDim.z);
}

int main(int argc, char** argv)
{
	int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	CHECK(cudaSetDevice(dev));

   simConfig *simConfig1 = new simConfig;
	simConfig1->fAlpha = 1E-4;
	simConfig1->fBeta = 1E-6;
	simConfig1->i64Interval = 10 ^ 6;
	simConfig1->i64Seed = static_cast<int64>(123456);
   
  	simConfig *dSimConfig1;
	CHECK(cudaMallocManaged((simConfig **)&dSimConfig1, sizeof(simConfig)));
	CHECK(cudaMemcpy(dSimConfig1, simConfig1,
		sizeof(simConfig), cudaMemcpyHostToDevice));

	const int enzymeA1Size(6), enzymeB1Size(6), digestedProtein1Size(11);
	const int enzymeA2Size(10), enzymeB2Size(11), digestedProtein2Size(20);
	const int A1[enzymeA1Size] = { 8479, 4868, 3696, 2646, 169, 142 };
	const int B1[enzymeB1Size] = { 11968, 5026, 1081, 1050, 691, 184 };
	const int C1[digestedProtein1Size] = { 8479, 4167, 2646, 1081, 881,
		859, 701, 691, 184, 169, 142 };
	const int A2[enzymeA2Size] = { 9979, 9348, 8022, 4020, 2693,
		                           1892, 1714, 1371, 510, 451 };
	const int B2[enzymeB2Size] = { 9492, 8453, 7749, 7365, 2292,
		                           2180, 1023, 959, 278, 124, 85 };
	const int C2[digestedProtein2Size] = { 7042, 5608, 5464, 4371,
		3884, 3121, 1901, 1768, 1590, 959, 899, 707, 702, 510, 451,
		412, 278, 124, 124, 85 };

	scEnzConfig *enzConfig1 = new scEnzConfig;
	scEnzConfig *enzConfig2 = new scEnzConfig;
	enzConfig1->iSizeA = enzymeA1Size;
	enzConfig1->iSizeB = enzymeB1Size;
	enzConfig1->iSizeDigest = digestedProtein1Size;
	enzConfig1->vEnzA = new int[enzymeA1Size];
	enzConfig1->vEnzB = new int[enzymeB1Size];
	enzConfig1->vDigest = new int[digestedProtein1Size];
	for (int i = 0; i < enzymeA1Size; ++i)
		enzConfig1->vEnzA[i] = A1[i];
	for (int i = 0; i < enzymeB1Size; ++i)
		enzConfig1->vEnzB[i] = B1[i];
	for (int i = 0; i < digestedProtein1Size; ++i)
		enzConfig1->vDigest[i] = C1[i];
	enzConfig2->iSizeA = enzymeA2Size;
	enzConfig2->iSizeB = enzymeB2Size;
	enzConfig2->iSizeDigest = digestedProtein2Size;
	enzConfig2->vEnzA = new int[enzymeA2Size];
	enzConfig2->vEnzB = new int[enzymeB2Size];
	enzConfig2->vDigest = new int[digestedProtein2Size];
	for (int i = 0; i < enzymeA2Size; ++i)
		enzConfig2->vEnzA[i] = A2[i];
	for (int i = 0; i < enzymeB2Size; ++i)
		enzConfig2->vEnzB[i] = B2[i];
	for (int i = 0; i < digestedProtein2Size; ++i)
		enzConfig2->vDigest[i] = C2[i];

	/* Let's transfer the enzyme parameters to the device */
	scEnzConfig *dEnzConfig1, *dEnzConfig2;
	CHECK(cudaMalloc((scEnzConfig **)&dEnzConfig1, sizeof(scEnzConfig)));
	CHECK(cudaMalloc((scEnzConfig **)&dEnzConfig2, sizeof(scEnzConfig)));
	CHECK(cudaMemcpy(dEnzConfig1, enzConfig1, 
		sizeof(scEnzConfig), cudaMemcpyHostToDevice));
	CHECK(cudaMemcpy(dEnzConfig2, enzConfig2, 
		sizeof(scEnzConfig), cudaMemcpyHostToDevice));
	
	/* Setting up global status parameters in managed memory */
	volatile scGlobalStatus *globalStatus1;
	CHECK(cudaMallocManaged((scGlobalStatus **)&globalStatus1,
		sizeof(scGlobalStatus)));
	globalStatus1->numZeroes = 0;
	globalStatus1->activeThreadIdx = 0;
	globalStatus1->stopSignal = false;
	globalStatus1->allThreadsDead = false;
	globalStatus1->globalNumIterations = 0;

	/* Set up grid and block for execution */
	dim3 grid1(GDIMX, GDIMY, GDIMZ);
	dim3 block1(BDIMX, BDIMY, BDIMZ);
	/* Setup of kernel status */
	//scKernelStatus kernelStatus1[NUM_THREADS];
	scKernelStatus *kernelStatus1;
	int kernelStatusSpace = NUM_THREADS*(sizeof(struct scKernelStatus) +
		2 * sizeof(float) + 5 * sizeof(int) + sizeof(int64) + sizeof(bool));
	/* No need for it to be globally accessible */
	CHECK(cudaMalloc((scKernelStatus **)&kernelStatus1,
		kernelStatusSpace));
	/* Launch kernel */
	volatile int *bufferReady = 0;

	testSetup <<< grid1, block1 >>> (dEnzConfig1, dSimConfig1,
		kernelStatus1, globalStatus1, bufferReady);

   CHECK(cudaFree(dEnzConfig1));
	CHECK(cudaFree(dEnzConfig2));
	//CHECK(cudaFree(globalStatus1));
	CHECK(cudaFree(dSimConfig1));
	CHECK(cudaFree(kernelStatus1));
	delete[] simConfig1;
	delete[] enzConfig1;
	delete[] enzConfig2;
}
