function mutatedChromosome = Mutate(chromosome, mutationProbability)
  nGenes = length(chromosome);
  mutatedChromosome = chromosome;
  mutationVector = rand(1, nGenes) < mutationProbability;
  indicesToMutate = find(mutationVector);
  mutatedChromosome(indicesToMutate) = ~chromosome(indicesToMutate);
end