% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2b Solution 2
% by Robin Axelsson
%

Cvav = zeros(10,1);
Cvat = zeros(10,1);
Cvcv = zeros(10,1);
Cvct = zeros(10,1);
Cvev = zeros(10,1);
Cvet = zeros(10,1);

Eat = zeros(10,1);
Eav = zeros(10,1);
Ect = zeros(10,1);
Ecv = zeros(10,1);
Eet = zeros(10,1);
Eev = zeros(10,1);

fprintf('Running network 1 simulations .')
for ii=1:10
   close all
   Uppg2a2
   Cvat(ii) = Cvvec1(end);
   Cvav(ii) = Cvvec2(end);
   Eat(ii) = Evec1(end);
   Eav(ii) = Evec2(end);
   fprintf('.')
end
fprintf(' done.\n')
dlmwrite('Results2a.txt', [Cvav; NaN; Cvat; NaN; Eat; NaN; Eav])
fprintf('Running network 2 simulations .')
for ii=1:10;
   close all
   Uppg2b2
   Cvcv(ii) = Cvvec1(end);
   Cvct(ii) = Cvvec2(end);
   Ect(ii) = Evec1(end);
   Ecv(ii) = Evec2(end);

   fprintf('.')
end
fprintf(' done.\n')
dlmwrite('Results2c.txt', [Cvcv; NaN; Cvct; NaN; Ect; NaN; Ecv])
fprintf('Running network 3 simulations .')
for ii=1:10;
   close all
   Uppg2c2
   Cvev(ii) = Cvvec1(end);
   Cvet(ii) = Cvvec2(end);
   Eet(ii) = Evec1(end);
   Eev(ii) = Evec2(end);

   fprintf('.')
end
fprintf(' done.\n')
dlmwrite('Results2e.txt', [Cvev; NaN; Cvet; NaN; Eet; NaN; Eev])

close all;
FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]]/255;
plot(1:length(Cvat), Cvat, 'LineWidth', 3, ...
              'Color', FIGS.LineColors(6, :));
hold on
plot(1:length(Cvav), Cvav, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}10 runs of network 2a')

FIGS.fig2 = figure('Color', [1 1 1]);
plot(1:length(Eat), Eat, 'LineWidth', 3, ...
              'Color', FIGS.LineColors(6, :));
hold on
plot(1:length(Eav), Eav, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}10 runs of network 2a')

FIGS.fig3 = figure('Color', [1 1 1]);
plot(1:length(Ect), Ect, 'LineWidth', 3, ...
              'Color', FIGS.LineColors(6, :));
hold on
plot(1:length(Ecv), Ecv, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}10 runs of network 2c')

FIGS.fig4 = figure('Color', [1 1 1]);
plot(1:length(Cvet), Cvet, 'LineWidth', 3, ...
              'Color', FIGS.LineColors(6, :));
hold on
plot(1:length(Cvev), Cvev, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}10 runs of network 2e')
