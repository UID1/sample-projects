//
//  SwipeViewController.swift
//  Tinder
//
//  Created by Iberius Pred on 05/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import Parse

class SwipeViewController: TinderBaseViewController {

    var userIDList = [String]()
    var userNameList = [String]()
    var position = 0
    
    var displayedUserID = ""
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBAction func debugData(_ sender: Any) {
        print(userIDList)
        print(userNameList)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("Current user ID: \(PFUser.current()?.objectId), Name: \(PFUser.current()?.username), Password: \(PFUser.current()?.password), isWoman:  \(PFUser.current()?["isFemale"] as? Bool), wantsWomen: \(PFUser.current()?["interestedInWomen"] as? Bool)")
        queryMatchingUsers()
        print(userIDList)
        print(userNameList)
        if userIDList.count > 0 {
            loadProfileImageForUser(id: userIDList.first!)
        } else {
            print("No matching users!")
        }
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(swipeImage(gestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(gesture)
        //PFUser.logOut()
        
        PFGeoPoint.geoPointForCurrentLocation { (geopoint, error) in
            if let geopoint = geopoint {
                PFUser.current()?["Location"] = geopoint
                PFUser.current()?.saveInBackground()
                
            }
            
        }
        
    }

    // How do we download the profile image from a user with a userID
    private func loadProfileImageForUser(id: String) {
        //let imageQuery = PFQuery(className: "Users")
        let imageQuery = PFUser.query()
        imageQuery?.whereKey("objectId", equalTo: id)
        imageQuery?.findObjectsInBackground { (objects, error) in
            if error == nil {
                if let queryResults = objects {
                    for queryResult in queryResults {
                        if let selectedUserPhoto = queryResult["Photo"] as? PFFile {
                            selectedUserPhoto.getDataInBackground(block: { (data, error) in
                                if error == nil {
                                    if let imageData = data, let downloadedImage = UIImage(data: imageData) {
                                        self.profileImage.image = downloadedImage
                                    }
                                } else {
                                    if let errorContainer = error as? NSError {
                                        self.createAlert(title: "Image fetch failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                                    } else {
                                        self.createAlert(title: "Image fetch failure", message: "Please try again later.")
                                    }
                                }
                            })
                            //self.profileImage.image = UIImage(data: selectedUser["Photo"] as! Data)
                        }
                    }
                } else {
                    self.createAlert(title: "Update failure", message: "Failed to fetch the users. Perhaps try again later")
                }
            } else {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Update failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Update failure", message: "Please try again later.")
                }
            }
        }
    }
    @objc private func swipeImage(gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: view)
        let image = gestureRecognizer.view!
        image.center = CGPoint(x: self.view.bounds.width / 2 + translation.x, y: self.view.bounds.height / 2 + translation.y)
        let xFromCenter = image.center.x - self.view.bounds.width / 2
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200)
        let scale = min(20 / abs(xFromCenter), 1)
        var stretchandrotation = rotation.scaledBy(x: scale, y: scale)
        image.transform = stretchandrotation
        // The gesture recognizer is ".ended" it means that the user released the drag with his finger
        if gestureRecognizer.state == UIGestureRecognizerState.ended {
            var acceptedOrRejected = ""
            if image.center.x < 100 {
                acceptedOrRejected = "rejected"
            } else if image.center.x > self.view.bounds.width - 100 {
                acceptedOrRejected = "accepted"
            }
            if position < userIDList.count-1 {
                position += 1
            } else {
                position = 0
            }
            displayedUserID = userIDList[position]
            if acceptedOrRejected != "" {
                PFUser.current()?.addUniqueObjects(from: [displayedUserID], forKey: acceptedOrRejected)
                PFUser.current()?.saveInBackground(block: { (success, error) in
                    self.loadProfileImageForUser(id: self.userIDList[self.position])
                })
            }
            
            rotation = CGAffineTransform(rotationAngle: 0)
            stretchandrotation = rotation.scaledBy(x: 1, y: 1)
            image.transform = stretchandrotation
            image.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func queryMatchingUsers() {
        setFreezeView(activated: true)
        //let userQuery = PFQuery(className: "Users") // This doesn't work, why? // ACL was set to write only
        let userQuery = PFUser.query()
        if let CUIsInterestedInWomen = PFUser.current()?["interestedInWomen"] as? Bool, let CUIsWoman = PFUser.current()?["isFemale"] as? Bool, let CUID = PFUser.current()?.objectId! {
            print("Registered user ID: \(CUID), isFemale: \(CUIsWoman), wantsFemale: \(CUIsInterestedInWomen)")
            userQuery?.whereKey("isFemale", equalTo: CUIsInterestedInWomen)
            userQuery?.whereKey("interestedInWomen", equalTo: CUIsWoman)
            userQuery?.whereKey("objectId", notEqualTo: CUID)
        } else {
            createAlert(title: "Query Failure", message: "Unable to fetch data from logged in user")
            return
        }
        // Try finding users near a radius
        if let cuGeopoint = PFUser.current()?["location"] as? PFGeoPoint {
            let southWest = cuGeopoint - PFGeoPoint(latitude: 1,longitude: 1)
            let northEast = cuGeopoint + PFGeoPoint(latitude: 1,longitude: 1)
            userQuery?.whereKey("location", withinGeoBoxFromSouthwest: southWest, toNortheast: northEast)
            
        }
        
        //print("Finding matching users")
        userQuery?.findObjectsInBackground { (objects, error) in
            self.setFreezeView(activated: false)
            //print("Found objects in background")
            if error == nil {
                //print("No error")
                self.userIDList.removeAll()
                self.userNameList.removeAll()
                //print(objects)
                if let queryResults = objects {
                    //print("Results found: \(queryResults)")
                    for queryResult in queryResults {
                        //print("In for-loop")
                        if let selectedUser = queryResult as PFObject? {
                            //self.userList[selectedUser.objectId!] = selectedUser.value(forKey: "username") as? String
                            self.userIDList.append(selectedUser.objectId!)
                            //print("Welp, \(selectedUser.objectId!)")
                            self.userNameList.append((selectedUser.value(forKey: "username") as? String)!)
                            //print("(\(self.userNameList.count), \(self.userIDList.count)) users with (Name, ID)")
                        }
                    }
                } else {
                    self.createAlert(title: "Update failure", message: "Failed to fetch the users. Perhaps try again later")
                }
            } else {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Update failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Update failure", message: "Please try again later.")
                }

            }
        }
    }
    private func updateImage() {
        let query = PFUser.query()
        query?.whereKey("isFemale", equalTo: (PFUser.current()?["interestedInWomen"])!)
        query?.whereKey("interestedInWomen", equalTo: (PFUser.current()?["isFemale"])!)
        
        // Excludes already swiped users from the query
        var ignoredUsers = [""]
        if let acceptedUsers = PFUser.current()?["accepted"] {
            ignoredUsers += acceptedUsers as! Array
        }
        if let rejectedUsers = PFUser.current()?["rejected"] {
            ignoredUsers += rejectedUsers as! Array
        }
        query?.whereKey("objectId", notContainedIn: ignoredUsers)
        
        // Filter users by their geographical location
        if let cuGeopoint = PFUser.current()?["location"] as? PFGeoPoint {
            let southWest = cuGeopoint - PFGeoPoint(latitude: 1,longitude: 1)
            let northEast = cuGeopoint + PFGeoPoint(latitude: 1,longitude: 1)
            query?.whereKey("location", withinGeoBoxFromSouthwest: southWest, toNortheast: northEast)
            
        }
        
        query?.limit = 1
        query?.findObjectsInBackground(block: { (objects, error) in
            if let users = objects {
                for object in users {
                    if let user = object as? PFUser {
                        let imageFile = user["Photo"] as! PFFile
                        imageFile.getDataInBackground(block: { (data, error) in
                            if let imageData = data {
                                self.profileImage.image = UIImage(data: imageData)
                            }
                        })
                    }
                }
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logoutSegue" {
            PFUser.logOut()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PFGeoPoint {
    static func + (left: PFGeoPoint, right: PFGeoPoint) -> PFGeoPoint {
        return PFGeoPoint(latitude: left.latitude + right.latitude, longitude: left.longitude + right.longitude)
    }
    static func - (left: PFGeoPoint, right: PFGeoPoint) -> PFGeoPoint {
        return PFGeoPoint(latitude: left.latitude - right.latitude, longitude: left.longitude - right.longitude)
    }

}
