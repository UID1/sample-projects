function resultingAcceleration = getAcceleration(Params, SlopeParams)
  gravAcc = Params.gravitationConstant * ...
           sin(SlopeFun(Params.position, SlopeParams)*pi/180);
  engineAcc = Params.gearFactor / Params.vehicleMass;
  if Params.brakeTemp > Params.maxBrakeTemp - 100
    heatRestrictor = 1;
  else
    heatRestrictor = exp(-(Params.brakeTemp - (Params.maxBrakeTemp - 100))/ 100);
  end

  brakeAcc = heatRestrictor * Params.brakePressure * ...
             Params.gravitationConstant / 20;
  resultingAcceleration = gravAcc - brakeAcc - engineAcc;
end