.data:00674400                 dd      449Dh,    0FA8Ch,    0EBEFh,    0CE65h ; These big-endian 16bit values, being separated by 
.data:00674400                 dd     0AF33h,    0EE44h,     30F4h,     6C84h ; zero-valued 16-bit words, are somehow used during 
.data:00674400                 dd     0B3E9h,     74EFh,    0C4F3h,    0EC96h ; the License Key evaluation. The size of the const 
.data:00674400                 dd      7896h,      7FDh,    0DE5Bh,    0F7DDh ; table including zeroes is 128bytes comprising 32  
.data:00674400                 dd       0E4h,     85C8h,    0E3DBh,     84F2h ; values                                            
.data:00674400                 dd     0BCC8h,    0C612h,     1A22h,     59A9h                                                     
.data:00674400                 dd      51CFh,     8353h,     25EDh,    0A6D6h                                                     
.data:00674400                 dd     0FB12h,    0C2FDh,    0B8DEh,     4BF9h                                                     
.data:00674480                 db  76h, 63h, 0Dh, 6Dh, 70h,0B3h, 3Ch, 4Bh ; This is the beginning of a lookup table               
.data:00674480                 db  75h, 2Eh, 3Ah,0A1h, 43h,0A9h, 8Ah, 0Dh                                                         
.data:00674480                 db  99h, 0Bh, 56h, 91h,0A6h,0D6h,0F8h, 8Bh                                                         
.data:00674480                 db  25h, 96h, 0Bh,0C1h, 45h, 61h,0F1h, 33h                                                         
.data:00674480                 db 0D5h, 49h,0D8h,0D1h,0ECh,0ABh,0CAh, 2Eh                                                         
.data:00674480                 db 0A3h, 89h, 7Ch,0B1h, 33h, 1Eh, 83h, 71h                                                         
.data:00674480                 db  3Ah, 5Eh,0C2h, 1Eh,0E5h, 0Eh,0F4h,0EAh                                                         
.data:00674480                 db  86h,   7, 3Ch, 0Eh, 4Dh, 25h,   7,0BAh                                                         
.data:00674480                 db  95h,0E4h, 1Dh, 19h, 56h, 9Ah, 15h,0DAh                                                         
.data:00674480                 db 0ACh,0C5h, 35h, 1Ah, 4Bh,0E1h, 87h, 43h                                                         
.data:00674480                 db  9Eh, 1Ah, 93h, 15h, 31h, 85h, 7Ch,0E6h                                                         
.data:00674480                 db 0B5h, 45h, 19h,0CEh,0AAh, 9Dh, 71h, 1Dh                                                         
.data:00674480                 db 0A5h,0C9h, 63h,0D4h,0B6h, 31h,0DCh,0BCh                                                         
.data:00674480                 db  49h, 1Bh,0AEh,0F2h, 8Dh,0C6h, 6Dh, 5Eh                                                         
.data:00674480                 db  8Ch, 4Dh, 1Bh,0C7h, 70h,0A2h, 76h,0D9h                                                         
.data:00674480                 db 0C3h, 8Eh,0E9h, 92h, 61h, 75h,0B9h, 35h                                                         
.data:00674480                 db  46h, 62h, 91h, 3Eh, 8Ah,0BAh, 17h, 6Ch                                                         
.data:00674480                 db  51h, 99h, 23h,0A3h,0C5h, 53h,0ABh, 96h                                                         
.data:00674480                 db  29h, 17h, 59h,0E9h, 72h,0CEh, 26h, 6Eh                                                         
.data:00674480                 db  69h,0F2h,0B1h, 65h, 8Bh,0DAh, 66h, 55h                                                         
.data:00674480                 db  89h, 79h, 4Ah, 16h,0A9h,0E4h, 7Ah, 6Ah                                                         
.data:00674480                 db 0C9h, 23h,0A1h,0C6h,0E1h, 83h,0D9h, 13h                                                         
.data:00674480                 db  26h,0E6h,0A6h, 4Eh,0C7h, 16h,0B3h,0D4h                                                         
.data:00674480                 db  47h, 29h,0DCh,0C3h, 13h, 3Eh, 86h, 5Ah                                                         
.data:00674480                 db  9Dh, 93h,0F4h,0D6h,0AEh,0A5h, 46h,0B9h                                                         
.data:00674480                 db 0ECh, 87h, 55h, 59h, 2Bh, 6Ah, 9Eh, 2Dh                                                         
.data:00674480                 db  85h,0E5h,0B6h, 39h, 27h, 95h, 36h,0A2h                                                         
.data:00674480                 db  79h, 51h,0C1h,0BCh, 7Ah,0EAh, 53h, 9Ah                                                         
.data:00674480                 db  36h, 8Dh, 2Dh, 62h,0ACh, 5Ah, 72h, 2Bh                                                         
.data:00674480                 db  4Ah, 92h, 39h, 6Ch,0CAh,0B5h,0F8h,0F1h                                                         
.data:00674480                 db 0C2h, 69h,0D1h, 27h, 0Fh,0D5h,0AAh, 8Eh                                                         
.data:00674480                 db  66h, 6Eh,0D8h, 0Fh, 8Ch, 4Eh, 65h, 47h                                                         
.data:00674580                 db 0DFh,0BFh, 11h, 8Eh, 7Ah,   2,   0,   0 ; Another binary table                                  
.data:00674580                 db  11h, 12h, 4Bh,   1, 30h,0E8h,   0,   0                                                         
.data:00674580                 db  36h, 30h,0AFh,0AFh, 7Fh,0D3h,   0,   0                                                         
.data:00674580                 db 0E3h,   3,0B8h, 23h, 9Ch,0A7h,   0,   0                                                         
.data:00674580                 db 0B6h, 67h, 44h,0F3h,0E3h, 2Bh,   0,   0                                                         
.data:00674580                 db 0F2h, 51h,0CCh,0EFh,0B8h,0F4h,   0,   0                                                         
.data:00674580                 db 0DCh, 0Bh, 31h,0BFh,0EAh, 1Dh,   0,   0                                                         
.data:00674580                 db 0D0h,0B5h,0A0h, 66h,0B3h, 5Fh,   0,   0                                                         
.data:00674580                 db 0B1h,0E2h,0DDh,0D8h, 71h, 14h,   0,   0                                                         
.data:00674580                 db  53h, 40h,0A4h, 40h,0EAh, 79h,   0,   0                                                         
.data:00674580                 db  74h, 7Fh, 24h, 21h, 9Dh, 84h,   0,   0                                                         
.data:00674580                 db  6Ah, 69h,0F1h, 7Dh,0D3h, 9Eh,   0,   0                                                         
.data:00674580                 db  49h,0DAh, 9Eh, 73h, 95h, 1Bh,   0,   0                                                         
.data:00674580                 db  6Fh, 5Dh,0D2h,0BAh, 8Fh,0EDh,   0,   0                                                         
.data:00674580                 db  99h,0ECh,0E2h, 5Ch,0A5h, 73h,   0,   0                                                         
.data:00674580                 db  31h, 29h, 49h, 3Eh,0B3h,0B3h,   0,   0                                                         
.data:00674580                 db  9Dh,0FDh, 9Dh,0F0h,0ADh,0ECh,   0,   0                                                         
.data:00674580                 db  72h, 21h, 21h, 0Bh, 9Eh, 4Eh,   0,   0                                                         
.data:00674580                 db  40h, 54h, 70h, 0Ch,0E2h, 38h,   0,   0                                                         
.data:00674580                 db  11h,0B1h, 65h, 88h, 8Fh,0F7h,   0,   0                                                         
.data:00674580                 db  1Ch, 63h, 21h,0DAh, 13h, 9Ch,   0,   0                                                         
.data:00674580                 db  74h,0F1h, 45h,0EFh, 2Dh, 72h,   0,   0                                                         
.data:00674580                 db  80h, 7Ah, 90h,0DDh, 62h, 88h,   0,   0                                                         
.data:00674580                 db 0F0h,0B3h,0A6h,0F0h, 34h, 8Fh,   0,   0                                                         
.data:00674580                 db  5Fh, 0Dh, 90h,0FDh,0F6h, 23h,   0,   0                                                         
.data:00674580                 db  7Ah, 57h, 50h,0D4h, 5Eh,0FFh,   0,   0                                                         
.data:00674580                 db  59h, 4Dh, 22h,   4, 9Bh, 9Eh,   0,   0                                                         
.data:00674580                 db  46h, 54h, 0Fh,0C3h, 47h,0CCh,   0,   0                                                         
.data:00674580                 db 0C1h,0A0h, 53h, 27h,0EBh,0ACh,   0,   0                                                         
.data:00674580                 db  76h, 16h, 1Eh,0D3h, 29h, 82h,   0,   0                                                         
.data:00674580                 db  3Ah,0CFh,0FBh,   8,0A8h, 9Ch,   0,   0                                                         
.data:00674580                 db 0DEh, 1Bh, 6Ch, 4Ah,0B8h, 38h,   0,   0                                                         
.data:00674580                 db  59h,   5, 44h, 2Dh, 9Fh, 94h,   0,   0                                                         
.data:00674580                 db  4Fh, 44h, 55h, 45h,0DFh, 6Ch,   0,   0                                                         
.data:00674580                 db  63h,0FCh, 7Fh,   9,0DFh,0EBh,   0,   0                                                         
.data:00674580                 db  66h, 22h, 87h, 77h, 19h,0F1h,   0,   0                                                         
.data:00674580                 db 0D2h,0AEh,0F7h,0A6h,0F4h, 2Fh,   0,   0                                                         
.data:00674580                 db 0E6h, 34h,0D0h, 32h, 26h,0D6h,   0,   0                                                         
.data:00674580                 db 0F5h, 7Dh, 6Ch,0CAh,0AEh, 30h,   0,   0                                                         
.data:00674580                 db 0A8h,0BDh,0D2h,0C2h,0CDh,0CFh,   0,   0                                                         
.data:00674580                 db  3Ah,   5, 2Ah, 24h,   4,0C1h,   0,   0                                                         
.data:00674580                 db 0D1h, 60h, 55h, 66h, 16h,0A3h,   0,   0                                                         
.data:00674580                 db  7Fh, 27h, 26h,0FCh, 3Dh, 7Ch,   0,   0                                                         
.data:00674580                 db 0B8h, 41h, 50h, 5Ch,0E3h, 0Fh,   0,   0                                                         
.data:00674580                 db 0ABh,0C6h,0B7h,0AFh,0EDh,0F8h,   0,   0                                                         
.data:00674580                 db 0DAh, 2Bh, 63h, 7Ch,0E3h,0E7h,   0,   0                                                         
.data:00674580                 db  14h, 31h,0B2h, 2Ah, 4Ah, 77h,   0,   0                                                         
.data:00674580                 db 0C5h,0ECh, 4Bh, 6Dh, 3Bh,0DFh,   0,   0                                                         
.data:00674580                 db  94h, 21h, 0Fh, 32h,0BCh, 62h,   0,   0                                                         
.data:00674580                 db  7Bh, 5Eh, 73h, 23h, 96h, 0Dh,   0,   0                                                         
.data:00674580                 db 0B9h, 86h,   5, 21h, 8Bh, 42h,   0,   0                                                         
.data:00674580                 db  52h, 29h, 15h,0E2h,0E2h,0EAh,   0,   0                                                         
.data:00674580                 db  13h, 8Ah,0F8h, 90h, 77h, 48h,   0,   0                                                         
.data:00674580                 db  72h,0FFh, 47h,0C1h, 85h,0CCh,   0,   0                                                         
.data:00674580                 db 0B9h, 53h, 43h,   5,0E7h, 79h,   0,   0                                                         
.data:00674580                 db  63h,0ECh,0FDh,0E2h,0D1h,0F7h,   0,   0                                                         
.data:00674580                 db  16h,0ABh,0CBh, 72h, 81h,0E5h,   0,   0                                                         
.data:00674580                 db 0E3h,0C4h, 57h, 24h,0FDh,0EDh,   0,   0                                                         
.data:00674580                 db  43h,0FAh,0DCh,0A3h,0ABh, 69h,   0,   0                                                         
.data:00674580                 db  4Ch,0E2h, 60h, 51h, 90h, 3Dh,   0,   0                                                         
.data:00674580                 db  9Eh,0DCh, 2Ch,0ADh, 75h, 29h,   0,   0                                                         
.data:00674580                 db  77h, 90h, 4Ch, 14h, 87h, 88h,   0,   0                                                         
.data:00674580                 db  28h,0E4h,0CDh, 16h,0C6h, 20h,   0,   0                                                         
.data:00674580                 db  5Eh,0D7h,0E8h, 6Fh, 1Bh, 3Dh,   0,   0                                                         
.data:00674780                 db 0E4h,0DBh,   0,0F3h,0ACh, 77h,   0,   0 ; Another binary table that now is 256 bytes wide and   
.data:00674780                 db  30h, 87h, 2Bh,0A8h,0CEh, 6Bh,   0,   0 ; not 512 as usual. This may be due to the fact that    
.data:00674780                 db 0E6h,0D9h,   5, 22h, 3Fh,0B3h,   0,   0 ; we now are dealing with a 12 digit hexadecimal number 
.data:00674780                 db  26h, 69h,0EBh,0DEh, 9Eh,0BCh,   0,   0 ; and not 16 as we were last time. And that might       
.data:00674780                 db    9, 5Dh, 91h, 10h, 1Dh, 2Bh,   0,   0 ; explain why we have zeroes at the last two columns    
.data:00674780                 db 0EBh, 34h, 17h, 9Fh, 85h, 82h,   0,   0                                                         
.data:00674780                 db  13h, 47h,0E0h,0F9h, 0Dh,0CEh,   0,   0                                                         
.data:00674780                 db  0Fh,0DEh,0D5h, 83h,   4, 5Bh,   0,   0                                                         
.data:00674780                 db  60h,0CAh, 4Ch,0C9h, 98h,0F3h,   0,   0                                                         
.data:00674780                 db  3Bh, 0Eh, 79h, 40h, 85h,0BCh,   0,   0                                                         
.data:00674780                 db  3Ch,0B6h,0EEh,0F1h,0A2h, 5Bh,   0,   0                                                         
.data:00674780                 db  0Fh, 8Ah,0E5h, 78h,0EAh, 10h,   0,   0                                                         
.data:00674780                 db  43h, 53h,0C9h,   3, 45h, 36h,   0,   0                                                         
.data:00674780                 db 0BFh,0A2h, 8Dh,0FCh,0B7h,0C6h,   0,   0                                                         
.data:00674780                 db  8Ch, 76h,0F0h,0D8h,0A5h, 9Dh,   0,   0                                                         
.data:00674780                 db  0Fh, 34h,0E4h, 2Ch, 42h, 3Eh,   0,   0                                                         
.data:00674780                 db 0E8h,0D8h, 93h,0D9h,0D0h,0C2h,   0,   0                                                         
.data:00674780                 db  85h, 6Ah, 4Ah,0ADh, 61h, 0Fh,   0,   0                                                         
.data:00674780                 db  3Bh,0DAh,0A3h,0E0h, 76h,0C3h,   0,   0                                                         
.data:00674780                 db 0DEh,0A2h, 56h, 32h, 9Eh, 53h,   0,   0                                                         
.data:00674780                 db 0BFh,0D5h, 9Bh,0E9h, 6Fh, 43h,   0,   0                                                         
.data:00674780                 db 0C9h, 5Eh,0EFh,0D7h, 30h,0C0h,   0,   0                                                         
.data:00674780                 db 0DDh, 56h,0D6h,0EDh,0D7h,   7,   0,   0                                                         
.data:00674780                 db  68h,0FFh, 9Eh, 8Fh, 3Dh,0FDh,   0,   0                                                         
.data:00674780                 db  93h,0A8h,0EEh, 8Ah,   3,0C0h,   0,   0                                                         
.data:00674780                 db  77h, 44h, 99h,0E5h,0A3h,0BBh,   0,   0                                                         
.data:00674780                 db 0A3h, 9Fh,0A7h,0EDh,0CCh, 83h,   0,   0                                                         
.data:00674780                 db 0FDh,0B7h, 83h, 8Ch,0A5h, 17h,   0,   0                                                         
.data:00674780                 db 0B8h,0F1h, 34h, 5Eh, 32h,0BDh,   0,   0                                                         
.data:00674780                 db  7Ch, 28h, 47h,0E7h,0FEh,0EAh,   0,   0                                                         
.data:00674780                 db 0D2h,0EDh, 79h, 57h, 25h,   6,   0,   0                                                         
.data:00674780                 db 0AFh, 85h, 29h, 29h, 33h,0E9h,   0,   0                                                         
.data:00674880                 db  76h, 63h, 0Dh, 6Dh, 70h,0B3h, 3Ch, 4Bh ; Here's the start of a 256bytes wide                   
.data:00674880                 db  75h, 2Eh, 3Ah,0A1h, 43h,0A9h, 8Ah, 0Dh ; lookup table (for use through xlat)                   
.data:00674880                 db  99h, 0Bh, 56h, 91h,0A6h,0D6h,0F8h, 8Bh ; being used in the subroutine@64AA30                   
.data:00674880                 db  25h, 96h, 0Bh,0C1h, 45h, 61h,0F1h, 33h                                                         
.data:00674880                 db 0D5h, 49h,0D8h,0D1h,0ECh,0ABh,0CAh, 2Eh                                                         
.data:00674880                 db 0A3h, 89h, 7Ch,0B1h, 33h, 1Eh, 83h, 71h                                                         
.data:00674880                 db  3Ah, 5Eh,0C2h, 1Eh,0E5h, 0Eh,0F4h,0EAh                                                         
.data:00674880                 db  86h,   7, 3Ch, 0Eh, 4Dh, 25h,   7,0BAh                                                         
.data:00674880                 db  95h,0E4h, 1Dh, 19h, 56h, 9Ah, 15h,0DAh                                                         
.data:00674880                 db 0ACh,0C5h, 35h, 1Ah, 4Bh,0E1h, 87h, 43h                                                         
.data:00674880                 db  9Eh, 1Ah, 93h, 15h, 31h, 85h, 7Ch,0E6h                                                         
.data:00674880                 db 0B5h, 45h, 19h,0CEh,0AAh, 9Dh, 71h, 1Dh                                                         
.data:00674880                 db 0A5h,0C9h, 63h,0D4h,0B6h, 31h,0DCh,0BCh                                                         
.data:00674880                 db  49h, 1Bh,0AEh,0F2h, 8Dh,0C6h, 6Dh, 5Eh                                                         
.data:00674880                 db  8Ch, 4Dh, 1Bh,0C7h, 70h,0A2h, 76h,0D9h                                                         
.data:00674880                 db 0C3h, 8Eh,0E9h, 92h, 61h, 75h,0B9h, 35h                                                         
.data:00674880                 db  46h, 62h, 91h, 3Eh, 8Ah,0BAh, 17h, 6Ch                                                         
.data:00674880                 db  51h, 99h, 23h,0A3h,0C5h, 53h,0ABh, 96h                                                         
.data:00674880                 db  29h, 17h, 59h,0E9h, 72h,0CEh, 26h, 6Eh                                                         
.data:00674880                 db  69h,0F2h,0B1h, 65h, 8Bh,0DAh, 66h, 55h                                                         
.data:00674880                 db  89h, 79h, 4Ah, 16h,0A9h,0E4h, 7Ah, 6Ah                                                         
.data:00674880                 db 0C9h, 23h,0A1h,0C6h,0E1h, 83h,0D9h, 13h                                                         
.data:00674880                 db  26h,0E6h,0A6h, 4Eh,0C7h, 16h,0B3h,0D4h                                                         
.data:00674880                 db  47h, 29h,0DCh,0C3h, 13h, 3Eh, 86h, 5Ah                                                         
.data:00674880                 db  9Dh, 93h,0F4h,0D6h,0AEh,0A5h, 46h,0B9h                                                         
.data:00674880                 db 0ECh, 87h, 55h, 59h, 2Bh, 6Ah, 9Eh, 2Dh                                                         
.data:00674880                 db  85h,0E5h,0B6h, 39h, 27h, 95h, 36h,0A2h                                                         
.data:00674880                 db  79h, 51h,0C1h,0BCh, 7Ah,0EAh, 53h, 9Ah                                                         
.data:00674880                 db  36h, 8Dh, 2Dh, 62h,0ACh, 5Ah, 72h, 2Bh                                                         
.data:00674880                 db  4Ah, 92h, 39h, 6Ch,0CAh,0B5h,0F8h,0F1h                                                         
.data:00674880                 db 0C2h, 69h,0D1h, 27h, 0Fh,0D5h,0AAh, 8Eh                                                         
.data:00674880                 db  66h, 6Eh,0D8h, 0Fh, 8Ch, 4Eh, 65h, 47h                                                         
.data:00674980                 db  40h, 75h,0C9h,0CDh, 52h,0EAh, 0Bh, 39h ; A 512 bytes long binary table used to process         
.data:00674980                 db 0C4h,0D3h, 20h, 77h,0F3h, 2Eh,   6, 9Eh ; the by subroutine@64A870 scrambled >#L/K              
.data:00674980                 db  84h, 52h, 81h,0EEh, 7Dh, 86h, 35h, 25h                                                         
.data:00674980                 db  26h,0BDh,0FEh, 2Fh,0C0h, 5Bh, 8Ah,   6                                                         
.data:00674980                 db  9Ah, 70h,   8,0FCh, 6Fh, 36h, 9Fh,0D2h                                                         
.data:00674980                 db 0A3h,   3, 60h,0DFh, 66h, 85h, 42h,0F8h                                                         
.data:00674980                 db  17h, 2Dh, 19h, 3Eh, 83h, 19h,   1, 9Ah                                                         
.data:00674980                 db  97h, 5Eh, 3Ch, 25h, 69h,0A5h, 3Bh,0E6h                                                         
.data:00674980                 db  49h, 5Eh, 0Ch, 35h, 42h,0F4h, 7Ah,0C4h                                                         
.data:00674980                 db  9Eh, 27h,0FFh, 19h,0A1h,0CBh, 9Ah,0BEh                                                         
.data:00674980                 db 0A3h, 54h, 13h,0E1h, 42h,0A3h,0A5h,0FEh                                                         
.data:00674980                 db 0A4h,0E0h,0FCh,0C6h, 39h, 87h, 0Dh,0CCh                                                         
.data:00674980                 db 0E5h,0CBh,0B5h, 6Ch,0DAh, 90h,0FCh, 81h                                                         
.data:00674980                 db 0BFh, 70h,0E0h, 32h, 72h, 41h, 98h, 96h                                                         
.data:00674980                 db  0Bh,0D8h,0A0h,0CBh,0A8h,   0,0DEh, 49h                                                         
.data:00674980                 db 0A5h, 82h, 53h,0E1h,0D7h, 3Eh, 87h, 3Eh                                                         
.data:00674980                 db 0CAh, 70h,0FEh,0A3h, 4Ch, 75h, 9Bh, 31h                                                         
.data:00674980                 db  7Dh, 52h, 48h,0CFh, 3Ah, 85h, 2Eh,0DFh                                                         
.data:00674980                 db 0BEh, 7Eh, 3Eh,0F6h,   8, 92h, 9Ch,0C5h                                                         
.data:00674980                 db 0ECh, 9Bh,0D7h, 7Ch,0FAh, 58h,0D7h, 49h                                                         
.data:00674980                 db  80h,0B2h, 94h,0C0h, 85h,0D8h,0C0h, 73h                                                         
.data:00674980                 db 0FEh, 46h,0CDh,0E5h,0BCh, 99h,0E1h, 98h                                                         
.data:00674980                 db  8Fh, 52h, 8Bh, 0Ah,0B7h,0B6h,0B4h, 92h                                                         
.data:00674980                 db 0F0h, 12h, 83h,0D7h, 4Ch,   9,0F9h, 55h                                                         
.data:00674980                 db  2Eh,0EDh, 74h,0BFh,0ACh, 1Ch,0CCh, 37h                                                         
.data:00674980                 db 0FDh, 71h, 82h, 7Ah, 5Ah, 9Eh, 6Bh, 4Ah                                                         
.data:00674980                 db 0BBh,0C3h, 57h,0BBh,0EBh,0C1h, 4Ch, 2Ah                                                         
.data:00674980                 db 0A4h, 7Dh,0EFh,   3,0F5h, 48h, 40h, 7Bh                                                         
.data:00674980                 db  1Dh,0E9h,0B8h, 7Dh,0A0h, 9Eh,0ECh, 70h                                                         
.data:00674980                 db  31h, 9Bh, 3Bh, 85h, 3Dh,0E5h, 27h, 48h                                                         
.data:00674980                 db 0B9h, 5Bh, 3Bh, 5Ch, 1Eh,0FCh,0AAh, 2Dh                                                         
.data:00674980                 db  45h, 18h,0FFh, 68h,   9,0DDh,0F2h, 4Fh                                                         
.data:00674980                 db 0E1h, 0Dh, 6Fh, 72h,0B3h, 66h, 0Fh,0B6h                                                         
.data:00674980                 db 0F4h,   6,0CFh, 11h, 96h, 21h,0D4h,0A2h                                                         
.data:00674980                 db  43h, 1Dh, 6Bh,0CBh, 16h,0EFh, 2Eh,0A2h                                                         
.data:00674980                 db  64h, 76h, 72h, 14h,0ABh,0A8h, 48h, 4Bh                                                         
.data:00674980                 db 0BAh, 11h,0A2h,   4,0ECh, 96h,0B6h, 1Ch                                                         
.data:00674980                 db  5Fh, 69h, 91h, 19h, 5Dh, 5Fh,0A7h, 40h                                                         
.data:00674980                 db  31h, 85h, 13h,0AEh, 63h,   7, 11h, 4Ch                                                         
.data:00674980                 db  74h, 47h, 81h, 54h, 74h, 29h, 97h, 37h                                                         
.data:00674980                 db    9,0C5h,0C0h, 8Fh, 93h, 15h,0ABh, 3Eh                                                         
.data:00674980                 db  8Ch, 36h, 39h,0BBh,0F5h, 0Ah, 66h,0BFh                                                         
.data:00674980                 db 0FAh, 21h, 7Ch,0C2h,   9,0B2h, 17h,0EDh                                                         
.data:00674980                 db  0Fh,0DBh,0AFh,0C3h, 46h,0C8h, 47h,0A7h                                                         
.data:00674980                 db  9Bh, 6Ah, 3Ch, 58h,0DDh,   0, 5Eh, 70h                                                         
.data:00674980                 db 0C7h, 59h, 80h, 8Eh,0C1h,   0,0F9h, 71h                                                         
.data:00674980                 db 0ACh, 48h,0C2h, 98h,0A6h, 65h, 76h,0A4h                                                         
.data:00674980                 db 0D1h, 5Ch,0E5h,0D6h, 58h, 81h,0B4h,0B4h                                                         
.data:00674980                 db  89h, 0Eh, 7Fh,0A8h,0ADh,0A6h, 8Fh,0B0h                                                         
.data:00674980                 db  4Fh,0F1h, 55h,   6,0B7h,0FBh,   0, 71h                                                         
.data:00674980                 db 0C6h,0E0h, 66h,0EBh,0B1h,0E1h,0CAh,0EFh                                                         
.data:00674980                 db 0C0h,0D3h,0F0h,0BAh,0E0h,   4, 9Ah,0C8h                                                         
.data:00674980                 db 0C7h, 3Ah,0A8h,0FBh, 82h, 93h, 75h, 4Bh                                                         
.data:00674980                 db  39h, 11h, 5Ch, 84h, 87h, 26h,0C5h,0DFh                                                         
.data:00674980                 db  5Ch, 1Ah,   1, 3Eh,0A4h,0A0h,0D7h,0E8h                                                         
.data:00674980                 db  72h,0FDh,0BDh,0B6h, 27h, 0Ch,0ECh, 58h                                                         
.data:00674980                 db  25h, 89h, 9Eh, 49h, 92h,0EAh,0C0h, 7Fh                                                         
.data:00674980                 db  16h,0ABh,0CBh, 72h, 81h,0E5h, 8Dh,0EBh                                                         
.data:00674980                 db  97h, 4Ch,0E1h, 34h,0B0h,0A4h, 0Ah, 6Bh                                                         
.data:00674980                 db 0F4h, 3Fh, 33h,0BFh, 73h, 94h, 8Bh,0D8h                                                         
.data:00674980                 db  3Eh,0C7h, 42h, 91h,0EAh,0DBh,   6,0B7h                                                         
.data:00674980                 db  54h, 8Ch, 4Eh, 8Eh,0E5h, 7Ah,0AEh,0D8h                                                         
.data:00674980                 db  5Ah, 8Fh, 31h,0E8h, 2Ah, 7Bh,   3,0F3h                                                         
.data:00674980                 db 0E4h, 42h,0DBh, 38h, 24h,0D1h, 94h, 15h                                                         
.data:00674B80                 db 0C6h,0B1h,0C2h, 56h,0A1h, 1Dh,0A2h, 30h ; Another 512 bytes long binary table used for scrambled
.data:00674B80                 db 0E4h, 71h,0A3h, 6Ch, 26h, 3Ah,0F5h, 39h ; >#L/K processing                                      
.data:00674B80                 db 0D8h, 45h,0C9h, 44h, 94h, 1Fh, 7Ch,0ECh                                                         
.data:00674B80                 db  1Dh, 91h, 52h, 18h, 3Dh,0D1h, 5Ch, 7Dh                                                         
.data:00674B80                 db 0BDh, 0Dh, 88h,0ADh, 41h,0F5h, 73h, 44h                                                         
.data:00674B80                 db 0A2h, 7Fh, 52h,0CAh, 29h, 0Ah,0A6h, 0Ch                                                         
.data:00674B80                 db 0C5h,0B2h,0A6h,0F8h, 85h,0E5h, 12h,0D9h                                                         
.data:00674B80                 db  3Ah, 10h, 19h,0E0h, 13h, 1Fh,0B7h,0EDh                                                         
.data:00674B80                 db 0DEh, 68h,0E6h,0EAh, 89h,0C5h,   6,0F9h                                                         
.data:00674B80                 db  7Fh, 58h,0ACh, 0Ah, 95h,0A2h,0F7h, 33h                                                         
.data:00674B80                 db  52h, 61h,0CDh, 30h, 13h, 70h,0EFh,0A7h                                                         
.data:00674B80                 db  13h,0DFh, 8Fh,0D6h,0B6h, 74h,0DBh,0E8h                                                         
.data:00674B80                 db  79h, 63h,0CCh,0D8h,0A7h, 1Ah, 4Ah, 53h                                                         
.data:00674B80                 db 0A6h, 93h, 2Eh,0E3h,0FEh,0DCh, 98h,0A1h                                                         
.data:00674B80                 db  19h,0BAh, 1Eh, 86h,   2, 7Fh,0F0h,0B8h                                                         
.data:00674B80                 db  76h,0C3h,0DBh, 70h,0A6h,0EBh,0A3h,   9                                                         
.data:00674B80                 db  44h, 9Bh, 28h, 6Bh, 22h, 4Fh, 99h, 7Eh                                                         
.data:00674B80                 db 0B2h,0C9h,0D2h,0D9h, 63h,0ADh,0DDh,0F5h                                                         
.data:00674B80                 db 0F7h, 47h,0C9h, 96h, 93h,0E0h,0A6h, 83h                                                         
.data:00674B80                 db  7Eh,0A9h, 57h, 99h,0B9h,0DEh,0E1h, 0Ch                                                         
.data:00674B80                 db 0B5h, 4Dh, 59h, 14h, 48h,0D4h, 8Fh, 8Bh                                                         
.data:00674B80                 db  49h, 42h, 58h,   3, 7Ch, 46h,0F6h, 60h                                                         
.data:00674B80                 db  2Fh,0F7h, 88h,0C3h, 4Eh,0E8h,0AEh, 4Fh                                                         
.data:00674B80                 db  90h, 7Fh,0F1h, 3Eh,0C4h, 24h,0D9h, 72h                                                         
.data:00674B80                 db  1Dh,0ABh,0D2h, 43h, 29h, 3Fh,   2, 22h                                                         
.data:00674B80                 db 0D1h, 12h,0F7h,0F7h, 38h, 41h,0CEh,0DBh                                                         
.data:00674B80                 db  4Fh, 75h, 61h, 0Fh, 5Eh, 6Ah,0AFh,0BBh                                                         
.data:00674B80                 db  58h,0FDh,   0, 12h,0BCh,0B1h, 15h,0ADh                                                         
.data:00674B80                 db  33h, 28h, 28h,0D1h,0A6h, 0Fh, 2Ah, 12h                                                         
.data:00674B80                 db  6Fh, 91h, 98h,0F3h, 4Eh,0C7h,0D8h,0B4h                                                         
.data:00674B80                 db  14h, 42h, 1Fh, 52h,0E3h, 9Fh,0B6h,0B4h                                                         
.data:00674B80                 db  6Ch, 6Ah, 6Ch,0DCh, 89h,0B0h, 75h, 79h                                                         
.data:00674B80                 db 0B4h,0A5h,0A3h,0B9h,   1, 35h, 42h,0D4h                                                         
.data:00674B80                 db 0BCh,   1,0C4h, 96h,0B7h, 2Bh,0E2h,0E8h                                                         
.data:00674B80                 db  64h,0C2h,0C6h,0D5h,0E0h,0E4h, 9Eh, 25h                                                         
.data:00674B80                 db  7Fh,0CDh, 37h,0A1h, 48h, 53h,0CCh, 88h                                                         
.data:00674B80                 db  10h, 52h,0A5h,0DEh, 96h, 93h,0F1h,0CAh                                                         
.data:00674B80                 db  87h, 37h, 38h, 9Ah,0CFh,0DCh, 6Ch,   0                                                         
.data:00674B80                 db  93h, 97h, 99h, 31h, 9Fh, 88h, 76h, 18h                                                         
.data:00674B80                 db 0AFh, 10h, 9Ah, 2Eh,   1, 85h, 59h,0E7h                                                         
.data:00674B80                 db 0D5h, 98h,   4,0DFh, 45h,0F0h,0B6h,0F2h                                                         
.data:00674B80                 db  9Eh,0EDh,0EAh, 81h, 76h, 7Fh,0AAh,   2                                                         
.data:00674B80                 db  38h,0C8h, 70h,0C1h, 16h, 78h, 0Ah, 94h                                                         
.data:00674B80                 db  86h, 93h,0D7h, 80h, 1Bh, 73h,0FAh, 8Fh                                                         
.data:00674B80                 db  58h, 36h, 0Bh, 6Dh, 6Eh,0F8h,0F4h, 72h                                                         
.data:00674B80                 db  7Eh,0F6h,0FDh, 3Ch, 5Ch, 1Dh, 3Ah,0E0h                                                         
.data:00674B80                 db  63h,0EAh, 13h,0A1h,0E7h, 71h,0FBh,0F5h                                                         
.data:00674B80                 db  67h, 13h, 95h,   2, 6Dh,0DEh,0FCh, 81h                                                         
.data:00674B80                 db  1Bh, 1Fh,0FCh, 82h, 9Bh,0C3h, 1Ch, 33h                                                         
.data:00674B80                 db 0DBh, 38h, 7Eh, 52h, 3Ah, 0Ch, 69h,0DCh                                                         
.data:00674B80                 db  32h,0E4h,0B8h,0E1h,0B0h,0A1h,0DFh, 91h                                                         
.data:00674B80                 db  86h, 7Fh, 60h, 29h, 1Dh,0AFh, 3Ah,0B0h                                                         
.data:00674B80                 db  7Fh, 70h, 46h,0DCh, 1Bh, 4Ah, 59h,0BBh                                                         
.data:00674B80                 db  2Ah,   8, 41h, 78h,0D0h, 93h,0E0h, 90h                                                         
.data:00674B80                 db  41h,   0,0E0h,0E6h, 3Dh,0C7h,   5, 71h                                                         
.data:00674B80                 db 0C3h, 6Ah,0DFh, 8Ch, 38h,0C8h, 9Ch,0B0h                                                         
.data:00674B80                 db    3,   1, 35h, 88h, 11h, 41h, 59h, 6Bh                                                         
.data:00674B80                 db 0C4h,0C0h,0A6h,0FCh,0E5h, 7Ch,0BAh,0BBh                                                         
.data:00674B80                 db  47h, 8Dh, 31h, 11h,0DFh,0D3h,0DEh, 55h                                                         
.data:00674B80                 db  64h, 4Fh,0A6h,0A6h,0D7h, 5Ah, 21h, 9Eh                                                         
.data:00674B80                 db  63h,0F4h,0E2h,0A1h, 4Ch, 54h, 21h, 7Eh                                                         
.data:00674B80                 db  6Fh, 7Ah,0D6h, 57h,0F8h, 1Ah,0AAh, 3Eh                                                         
.data:00674B80                 db  8Dh, 42h, 41h,0B8h, 8Ah,0F1h,0F9h,0E3h                                                         
.data:00674B80                 db  44h, 54h, 14h,0E6h, 77h, 67h,0F3h, 37h                                                         
.data:00674D80                 db  76h, 63h, 0Dh, 6Dh, 70h,0B3h, 3Ch, 4Bh ; This is the beginning of a lookup table being used    
.data:00674D80                 db  75h, 2Eh, 3Ah,0A1h, 43h,0A9h, 8Ah, 0Dh ; in the first step of the #L/K scrambling              
.data:00674D80                 db  99h, 0Bh, 56h, 91h,0A6h,0D6h,0F8h, 8Bh                                                         
.data:00674D80                 db  25h, 96h, 0Bh,0C1h, 45h, 61h,0F1h, 33h                                                         
.data:00674D80                 db 0D5h, 49h,0D8h,0D1h,0ECh,0ABh,0CAh, 2Eh                                                         
.data:00674D80                 db 0A3h, 89h, 7Ch,0B1h, 33h, 1Eh, 83h, 71h                                                         
.data:00674D80                 db  3Ah, 5Eh,0C2h, 1Eh,0E5h, 0Eh,0F4h,0EAh                                                         
.data:00674D80                 db  86h,   7, 3Ch, 0Eh, 4Dh, 25h,   7,0BAh                                                         
.data:00674D80                 db  95h,0E4h, 1Dh, 19h, 56h, 9Ah, 15h,0DAh                                                         
.data:00674D80                 db 0ACh,0C5h, 35h, 1Ah, 4Bh,0E1h, 87h, 43h                                                         
.data:00674D80                 db  9Eh, 1Ah, 93h, 15h, 31h, 85h, 7Ch,0E6h                                                         
.data:00674D80                 db 0B5h, 45h, 19h,0CEh,0AAh, 9Dh, 71h, 1Dh                                                         
.data:00674D80                 db 0A5h,0C9h, 63h,0D4h,0B6h, 31h,0DCh,0BCh                                                         
.data:00674D80                 db  49h, 1Bh,0AEh,0F2h, 8Dh,0C6h, 6Dh, 5Eh                                                         
.data:00674D80                 db  8Ch, 4Dh, 1Bh,0C7h, 70h,0A2h, 76h,0D9h                                                         
.data:00674D80                 db 0C3h, 8Eh,0E9h, 92h, 61h, 75h,0B9h, 35h                                                         
.data:00674D80                 db  46h, 62h, 91h, 3Eh, 8Ah,0BAh, 17h, 6Ch                                                         
.data:00674D80                 db  51h, 99h, 23h,0A3h,0C5h, 53h,0ABh, 96h                                                         
.data:00674D80                 db  29h, 17h, 59h,0E9h, 72h,0CEh, 26h, 6Eh                                                         
.data:00674D80                 db  69h,0F2h,0B1h, 65h, 8Bh,0DAh, 66h, 55h                                                         
.data:00674D80                 db  89h, 79h, 4Ah, 16h,0A9h,0E4h, 7Ah, 6Ah                                                         
.data:00674D80                 db 0C9h, 23h,0A1h,0C6h,0E1h, 83h,0D9h, 13h                                                         
.data:00674D80                 db  26h,0E6h,0A6h, 4Eh,0C7h, 16h,0B3h,0D4h                                                         
.data:00674D80                 db  47h, 29h,0DCh,0C3h, 13h, 3Eh, 86h, 5Ah                                                         
.data:00674D80                 db  9Dh, 93h,0F4h,0D6h,0AEh,0A5h, 46h,0B9h                                                         
.data:00674D80                 db 0ECh, 87h, 55h, 59h, 2Bh, 6Ah, 9Eh, 2Dh                                                         
.data:00674D80                 db  85h,0E5h,0B6h, 39h, 27h, 95h, 36h,0A2h                                                         
.data:00674D80                 db  79h, 51h,0C1h,0BCh, 7Ah,0EAh, 53h, 9Ah                                                         
.data:00674D80                 db  36h, 8Dh, 2Dh, 62h,0ACh, 5Ah, 72h, 2Bh                                                         
.data:00674D80                 db  4Ah, 92h, 39h, 6Ch,0CAh,0B5h,0F8h,0F1h                                                         
.data:00674D80                 db 0C2h, 69h,0D1h, 27h, 0Fh,0D5h,0AAh, 8Eh                                                         
.data:00674D80                 db  66h, 6Eh,0D8h, 0Fh, 8Ch, 4Eh, 65h, 47h                                                         