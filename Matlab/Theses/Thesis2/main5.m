% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% The  Matlab script  'main2.m' uses  the  internal  matlab package  and
% function ARMAX  to fit ARMA  models into the  electricity  price data.
% The model estimations are evaluated  using a subset of the information
% criteria  and prediction  error  estimators  introduced  in 'main1.m'.
% Trend and seasonalities are  evaluated and  filtered out of  the price
% data. The spikes in  the data are  also analyzed  and methods to over-
% come the hurdles that are caused by these spikes are investigated.
%
% 'main5.m' is a continuation of main3.m  and main4.m and it is intended
% to further analyze the computations made in main3.m and main4.m.

% Load data needed for computations

load eldata1.mat
load armaxdata003.mat

% In case we didn't to run main3.m
if ~any(strcmp(who,'tmp2010'))
   % Get the temperature data for year 2010
   tmp2010 = temp_proc(8760, 'cubic');
end

% Compute the two winners from the two batches
% Winner 1
ELDATA = log(ELh2010_des1);
A1        = [ELDATA(3:end-1) ELDATA(2:end-2) ...
             ELDATA(1:end-3)];
el_acoeff = (A1'*A1)\A1'*tmp2010.national(4:end);
ELDATA1t = ELDATA(4:end) - A1*el_acoeff;
ELTREND1t = est_ma(ELDATA1t, 500);
[m5a par5a] = armabat2(ELDATA(4:end)-ELTREND1t.removable, ...
			tmp2010.national(4:end), ...
			84, 18, 31, 2, 'Winner1')

% Winner 2
A1        = [ELh2010_des1(3:end-1) ELh2010_des1(2:end-2) ...
             ELh2010_des1(1:end-3)];
el_acoeff = (A1'*A1)\A1'*tmp2010.national(4:end);
ELh2010_des1t = ELh2010_des1(4:end) - A1*el_acoeff;
ELTREND2010_2t = est_ma(ELh2010_des1t, 500);
[m5b par5b] = armabat2(ELh2010_des1(2004:3003) - ...
                        ELTREND2010_2t.removable(2001:3000), ...
			tmp2010.national(2004:3003), ...
			80, 19, 27, 2, 'Winner2')

% Now we want to make simulations out of our estimations. The parameters
% are readily available  so we have  to compute them manually.  I cannot
% find any particular method in the Matlab documentation. We have
%
%   y(t) = a_1*y(t-1)+a_2*y(t-2) + ... + a_p*y(t-p) +
%            b_1*u(t-d) + b_2*u(t-d-1) + ... + B_r*u(t-d-r) +
%            e(t) + c_1*e(t-1) + ... + c_q*e(t-q)
%
% where  u is an  exogeneous variable,  d is the delay in  discrete time
% steps. This should preferably be done in a function.  I just found out
% that d  is inherent in the vector.  So the b vector  should be applied
% directly on the exogeneous vector u  just like the c vector is applied
% directly  on the e vector.  The delay  is manifested  as the first few
% elements of b are zero.

% The simulations can be done with armaxsim(). Instructions can be given
% with 'help armaxsim'.

% Simulations  reveal that none of the estimations  are stable.  A quick
% review shows that the AR part of the model contribute to instabilities
% for p-values greater than 1.  A suitable next step would be to try log
% temperatures  and see what that does to the model.  The other thing to
% do  is to try  without the presence of temperature variations  and see
% how that turns out. 
%
% To be continued in 'main6.m' ...

