% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2a Solution
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

% This program works better with the old data
TRN = importdata('train_data1.txt');
VLD = importdata('valid_data_new.txt');
%TRN = importdata('train_data_new.txt');

TRNn = [(TRN(:,1)-mean(TRN(:,1)))/std(TRN(:,1)) ...
        (TRN(:,2)-mean(TRN(:,2)))/std(TRN(:,2)) TRN(:,3)];
	

%TRNn = TRN; %For the new data	
T = length(TRN);
NumIter = 1e6;

% Define the weight functions for the system
w = 2*(rand(1,2)-.5)*.2;

% Set predefined parameters
eta = .01;
theta = 0; % One output, one value
beta_0 = .5; % We set g(b) = tanh(beta*b) as defined on page 108

% Initialization of help variables
wb1 = waitbar(0,'Initializing ...');
Ewinsize = 1e4; % Window size
Ewin1 = zeros(1,Ewinsize); % Time span for rolling avg of E
Ewin2 = zeros(1,Ewinsize);
Evec1 = zeros(1,floor(NumIter/Ewinsize)); % Vector of avg E:s
Evec2 = zeros(1,floor(NumIter/Ewinsize));
Cvvec1 = zeros(1,floor(NumIter/Ewinsize));
Cvvec2 = zeros(1,floor(NumIter/Ewinsize));

% Training/Learning process
for i=1:NumIter
   O_mu = tanh(beta_0*(TRNn(:,1:2)*w'-theta));
   derivO_mu = beta_0*(1-O_mu.^2);
   deltaw_ik = eta*sum(meshgrid((TRNn(:,3)-O_mu).*derivO_mu,1:2)'.*TRN(:,1:2));
   w = w + deltaw_ik;
   deltatheta = -eta*sum((TRNn(:,3)-O_mu).*derivO_mu);
   theta = theta + deltatheta;
   % Energy and classification error calculations
   Ewin1(1+mod(i,Ewinsize)) = .5*sum((TRNn(:,3)-O_mu(:)).^2);
   O_muC = O_mu;
   O_mu = tanh(beta_0*(VLD(:,1:2)*w'-theta));
   Ewin2(1+mod(i,Ewinsize)) = .5*sum((VLD(:,3)-O_mu(:)).^2);
   if mod(i,Ewinsize) == 0
      waitbar(i/NumIter, wb1);
	  Evec1(i/Ewinsize) = mean(Ewin1);
	  Evec2(i/Ewinsize) = mean(Ewin2);
	  Cvvec1(i/Ewinsize) = mean(abs(TRNn(:,3) - sign(O_muC)));
	  Cvvec2(i/Ewinsize) = mean(abs(VLD(:,3) - sign(O_mu)));

      SPC = toc;
      waitbar(i/NumIter, wb1, sprintf('Calculating at %.2f iterations/s', Ewinsize/SPC));
	  tic;
   end
end
close(wb1);

% Visualize the problem at hand
FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]]/255;

plot(TRN(find(TRN(:,3)==-1),1), TRN(find(TRN(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TRN(find(TRN(:,3)==1),1), TRN(find(TRN(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Locations of our training data points')

% Plot the energy over iteration steps
FIGS.fig2 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(3, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(4, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Evolution of energy level over time')

FIGS.fig3 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(6, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Evolution of classification error over time')


% Calculate the classification error as defined in the assignment

%C_v = mean(abs(TRNn(:,3) - sign(O_mu)));

% Lets figure out how well our system works with a Monte-Carlo
% simulation

INP = [rand(1000, 1)-.5 rand(1000,1)];
TST = [INP(:,1) INP(:,2) sign(w(1)*INP(:,1)+w(2)*INP(:,2)-theta)];
FIGS.fig4 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')
