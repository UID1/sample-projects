#include <iostream>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/


int main(int argc, char **argv)
{
   int64 k1(0), k2(0);
   int64 dataSize1(1000000);
   int64 dataSize2(10000000);
   int64 numSubOperations1(10);
   int64 numSubOperations2(5);
   colors_t colors;
  	LARGE_INTEGER startTime, lapTime, countFreq;
	QueryPerformanceFrequency((LARGE_INTEGER *) &countFreq);
	
   colors.setFg(hGray);   std::cout << "Data size 1:";
   colors.setFg(dMagenta);std::cout << dataSize1;
   colors.setFg(White);   std::cout << ".";
   colors.setFg(hGray);   std::cout << std::endl;
   colors.setFg(hGray);   std::cout << "Data size 2:";
   colors.setFg(dMagenta);std::cout << dataSize2;
   colors.setFg(White);   std::cout << ".";
   colors.setFg(hGray);   std::cout << std::endl;
   QueryPerformanceCounter((LARGE_INTEGER *) &startTime);
   for(int64 i=0;i < max(dataSize1, dataSize2); ++i)
   {
      if(i<dataSize1) k1 += 5;
      if(i<dataSize2) k2 += 3;
   }
   QueryPerformanceCounter((LARGE_INTEGER *) &lapTime);
   colors.setFg(hGray);std::cout << "Nested for-loop took :";
   colors.setFg(dCyan);std::cout << 1000 * (lapTime.QuadPart - startTime.QuadPart) /
      static_cast<double>(countFreq.QuadPart);
   colors.setFg(White);std::cout << " ms";
   colors.setFg(hGray);std::cout << "." << std::endl;
   
   QueryPerformanceCounter((LARGE_INTEGER *) &startTime);
   for(int64 i=0;i < dataSize2; ++i)
   {
      if(i<dataSize1) k1 += 5;
      k2 += 3;
   }
   QueryPerformanceCounter((LARGE_INTEGER *) &lapTime);
   colors.setFg(hGray);std::cout << "Nested and optimized for-loop took :";
   colors.setFg(dCyan);std::cout << 1000 * (lapTime.QuadPart - startTime.QuadPart) /
      static_cast<double>(countFreq.QuadPart);
   colors.setFg(White); std::cout << " ms";
   colors.setFg(hGray);std::cout << "." << std::endl;
   
   QueryPerformanceCounter((LARGE_INTEGER *) &startTime);
   for(int64 i=0;i < dataSize1; ++i)
      k1 += 5;
   for(int64 i=0;i < dataSize2; ++i)
      k2 += 3;
   QueryPerformanceCounter((LARGE_INTEGER *) &lapTime);
   colors.setFg(hGray);std::cout << "Standard dual for-loop took :";
   colors.setFg(dCyan);std::cout << 1000 * (lapTime.QuadPart - startTime.QuadPart) /
      static_cast<double>(countFreq.QuadPart);
   colors.setFg(White); std::cout << " ms";
   colors.setFg(hGray);std::cout << "." << std::endl;

   QueryPerformanceCounter((LARGE_INTEGER *) &startTime);
   for(int64 i=0;i < max(dataSize1, dataSize2); ++i)
   {
      if(i<dataSize1) for(int j=0; j<numSubOperations1; ++j) k1 += 7 + rand();
      if(i<dataSize2) for(int j=0; j<numSubOperations2; ++j) k2 += 3 + rand();
   }
   QueryPerformanceCounter((LARGE_INTEGER *) &lapTime);
   colors.setFg(hGray);std::cout << "Complex nested for-loop took :";
   colors.setFg(dCyan);std::cout << 1000 * (lapTime.QuadPart - startTime.QuadPart) /
      static_cast<double>(countFreq.QuadPart);
   colors.setFg(White);std::cout << " ms";
   colors.setFg(hGray);std::cout << "." << std::endl;
   
   QueryPerformanceCounter((LARGE_INTEGER *) &startTime);
   for(int64 i=0;i < dataSize2; ++i)
   {
      if(i<dataSize1) for(int j=0; j<numSubOperations1; ++j) k1 += 7 + rand();
      for(int j=0; j<numSubOperations2; ++j) k2 += 3 + rand();
   }
   QueryPerformanceCounter((LARGE_INTEGER *) &lapTime);
   colors.setFg(hGray);std::cout << "Complex nested and optimized for-loop took :";
   colors.setFg(dCyan);std::cout << 1000 * (lapTime.QuadPart - startTime.QuadPart) /
      static_cast<double>(countFreq.QuadPart);
   colors.setFg(White); std::cout << " ms";
   colors.setFg(hGray);std::cout << "." << std::endl;
   
   QueryPerformanceCounter((LARGE_INTEGER *) &startTime);
   for(int64 i=0;i < dataSize1; ++i)
      for(int j=0; j<numSubOperations1; ++j) k1 += 7 + rand();
   for(int64 i=0;i < dataSize2; ++i)
      for(int j=0; j<numSubOperations2; ++j) k2 += 3 + rand();
   QueryPerformanceCounter((LARGE_INTEGER *) &lapTime);
   colors.setFg(hGray);std::cout << "Complex standard dual for-loop took :";
   colors.setFg(dCyan);std::cout << 1000 * (lapTime.QuadPart - startTime.QuadPart) /
      static_cast<double>(countFreq.QuadPart);
   colors.setFg(White); std::cout << " ms";
   colors.setFg(hGray);std::cout << "." << std::endl;
}
