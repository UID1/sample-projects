%
% Stokastisk modellering av elpriser f�r utformning av elprisderivat
%
% R, Axelsson
% Chalmers CTH, G�teborg
% H�sten, 2011
%


% Try some sample information data

N = 1000;
elpriser = randn(1,1000);

p = 10;
ar_p = 0:10; % A vector of lags in ascending order up to p

% We construct a vector with E[X_i*X_{i-k}] for values of k up to p

m_k = zeros(1,length(ar_p));
for i = 1:length(ar_p)
	m_k(i) = mean(elpriser(1+ar_p(i):end).*elpriser(1:end-ar_p(i)));
end

% From the m_k vector we build a vector r_k consisting of the autocorrelations
% the mean is not zero but it is the same for both variables in the correlation
% function since both variables are from the same series and therefore the autocorrelation
% calculations reduce to r_k = E[X_i*X_{i-k}] - E[X_i]^2. We should be careful here because
% the formula is no longer unbiased for sample correlation as it is not adjusted with N-1
% for N observations.

r_k = (m_k-mean(elpriser)^2)/var(elpriser);

% In the following for-loop we build the matrix of Yule-Walker equations
% To be exact, we build the upper-right-half triangle of the matrix which
% is symmetric

R_p = zeros(length(ar_p)-1);
for i = 1:(length(ar_p)-1)
	R_p(i,:) = [zeros(1, i) r_k(2:end-i)];
end

% We fill the rest of the matrix by adding it with its transpose and a diagonal
% of ones as the correlation r_0 (zero means zero lag) between the series and 
% itself is one.
R_p = R_p + R_p' + eye(length(ar_p)-1);


% Here we calculate the coefficients (the subscript "yw" means that is is the Yule-Walker
% estimations we are refering to.

Phi_yw = R_p\r_k(2:end)';

% Least square estimation

A_ls = zeros(length(elpriser)-length(ar_p)+1, length(ar_p)-1);
for i = 1:(length(ar_p)-1);
	A_ls(:,i)=elpriser(length(ar_p)-i:end-i)';
end
b_ls=elpriser(length(ar_p):end)';
Phi_ls = (A_ls'*A_ls)\(A_ls'*b_ls);

% Caluculating residuals and estimating
% goodness of fit

E_yw = b_ls-A_ls*Phi_yw;
E_ls = b_ls-A_ls*Phi_ls;
Var_yw = (E_yw'*E_yw)/length(E_yw);
Var_ls = (E_ls'*E_ls)/length(E_ls);

% Information Criteria for estimations

% We try using 5 different information criteria for testing for
% goodness of fit. The first one is Akaike Information Criterion (AIC),
% the second one is the Bayesian Information Criterion (BIC) which is
% also known as the Schwarz Information Criterion, the third criterion is
% the Hannan-Quinn Information Criterion, the fourth is the Final
% Prediction Error (FPE) and the last one is the extended Bayesian
% Information Criterion (EBIC).

% Criteria for the Yule-Walker estimations

AIC_yw  = N*log(Var_yw)+2*p
BIC_yw  = log(Var_yw)+p*log(N)/N;
HQIC_yw = log(Var_yw)+2*p*log(log(N))/N;
FPE_yw  = Var_yw*(N+p)/(N-p);
EBIC_yw = (N-p)*log(N*Var_yw/(N-p))+N*(1+log((2*pi)*.5))+p*log(p^-1*norm(elpriser)^2-N*Var_yw);

% Criteria for the Least-Square estimations
AIC_ls  = N*log(Var_ls)+2*p
BIC_ls  = log(Var_ls)+p*log(N)/N;
HQIC_ls = log(Var_ls)+2*p*log(log(N))/N;
FPE_ls  = Var_ls*(N+p)/(N-p);
EBIC_ls = (N-p)*log(N*Var_ls/(N-p))+N*(1+log((2*pi)*.5))+p*log(p^-1*norm(elpriser)^2-N*Var_ls);


% Error estimations
X_ls = zeros(length(elpriser)-length(ar_p)+1,1)';
X_yw = X_ls;
for i=1:length(ar_p)-1
	X_ls = X_ls + Phi_ls(i)*elpriser((length(ar_p)-i):end-i);
	X_yw = X_yw + Phi_yw(i)*elpriser((length(ar_p)-i):end-i);
end
SSE_ls = (X_ls-elpriser(length(ar_p):end))*(X_ls-elpriser(length(ar_p):end))'/(length(elpriser)-length(ar_p)-1);
SSE_yw = (X_yw-elpriser(length(ar_p):end))*(X_yw-elpriser(length(ar_p):end))'/(length(elpriser)-length(ar_p)-1);


