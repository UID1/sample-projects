%
%
%  Laboration i mekanik 2 2013
%  Stekroppssimulering av
%  asteroiden Toutatis
%
%

global I_p

w_0 = [20, 32, 98]*pi/180/24; % Vi r�knar i radianer per timme
m = 7.35*10^13;

% Kropp 1 - tv� sf�rer

r_1 = 1.5e3; % Sf�rradie 1 i meter
r_2 = 2.5e3; % Sf�rradie 2 i meter
V_1 = 4/3*pi*r_1^3; % Volym hos sf�r 1
V_2 = 4/3*pi*r_2^3; % Volym hos sf�r 2
V_tot = V_1 + V_2; % Kroppens totala volym
rho_s = m/V_tot; % Kroppens densitet
m_1 = rho_s*V_1; % Massa hos sf�r 1 i kg
m_2 = rho_s*V_2; % Massa hos sf�r 2 i kg
q_2 = r_2; % Avst. fr. ytterkant till m.c. hos sf�r 1
q_1 = 2*r_2+r_1; % Avst. fr. ytterkant till sf�r 2 masscent.
r_mc = (m_1*q_1+q_2*m_2)/m; % Kroppens mc sett fr. ytterkant
I_1 = 2/5*m_1*r_1^2; % Tr�ghetsmom. hos sf�r 1 i kg*m^2
I_2 = 2/5*m_2*r_2^2; % Tr�ghetsmom. sf�r 2
d_1 = 2*r_2+r_1-r_mc; % Avst�nd 1 f�r Steiners sats
d_2 = r_mc - r_2; % Avst�nd 2 f�r Steiners sats
I_0 = I_1+I_2+m_1*d_1^2+m_2*d_2^2; % Deltr�ghetsmom.
I_zeta = I_1+I_2; % Huvudtr�ghetsmom.
k_0 = I_0/m; %Vi dividerar bort massan s� att vi kan variera
k_zeta = I_zeta/m; % den i senare ber�kningar

I_sym = m*[k_0 k_0 k_zeta];

%I_sym = [32873/285 32873/285 842/15];

I_p = I_sym;

options = odeset('RelTol',1e-12,'AbsTol',[1e-10 1e-10 1e-11]);
% R�relse under 10 dygn
[T_s,w_s] = ode45(@wdot,[0 240],w_0,options);

%Kropp 2 - assymetrisk kropp

I_ass = 2/5*m*([4.5 2.5 1]*(2500^2+1500^2));

I_p = I_ass;
%R�relse under 10 dygn
options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4 1e-5]);
[T_ass,w_ass] = ode45(@wdot,[0 240],w_0,options);

H_ass = zeros(1,length(w_ass));
H_s = zeros(1,length(w_s));
Hv_ass = zeros(3,length(w_ass));
Hv_s = zeros(3,length(w_s));
K_s = zeros(1,length(w_s));
K_ass = zeros(1,length(w_ass));
for i = 1:length(w_ass)
    H_ass(i) = norm(diag(I_ass)*w_ass(i,:)');
    Hv_ass(:,i) = diag(I_ass)*w_ass(i,:)';
    K_ass(i) = Hv_ass(:,i)'*w_ass(i,:)';
end
for i = 1:length(w_s)
    H_s(i) = norm(diag(I_ass)*w_s(i,:)');
    Hv_s(:,i) = diag(I_sym)*w_s(i,:)';
    K_s(i) = Hv_s(:,i)'*w_s(i,:)';
end

Hv_ass = Hv_ass';
Hv_s = Hv_s';
FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

FIGS.fig1 = figure('Color', [1 1 1]);
plot3(w_ass(:,1),w_ass(:,2), w_ass(:,3), 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Rotation hos assymmetrisk kropp')
xlabel('\fontsize{12}\it\omega_{\xi}\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\omega_{\eta}\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\omega_{\zeta}\rm', 'interpreter', 'tex')
FIGS.fig2 = figure('Color', [1 1 1]);
plot3(w_s(:,1),w_s(:,2), w_s(:,3), 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Rotation hos symmetrisk kropp')
xlabel('\fontsize{12}\it\omega_{\xi}\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\omega_{\eta}\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\omega_{\zeta}\rm', 'interpreter', 'tex')
FIGS.fig3 = figure('Color', [1 1 1]);
plot3(w_ass(:,1)/norm(w_ass(:,1)),w_ass(:,2)/norm(w_ass(:,2)), ...
      w_ass(:,3)/norm(w_ass(:,3)), 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(1, :))
hold on
plot3(Hv_ass(:,1)/norm(Hv_ass(:,1)), Hv_ass(:,2)/norm(Hv_ass(:,2)), ...
      Hv_ass(:,1)/norm(Hv_ass(:,1)), 'LineWidth', 2, ...
      'Color', FIGS.LineColors(7, :))
title('\fontsize{12}Storhetsriktningar hos assymetrisk kropp');
FIGS.leg3 = legend(['$\it \hat{\omega}\rm$'], ...
       ['$\it \hat{H}\rm$']);
set(FIGS.leg3,'interpreter', 'latex', 'fontsize', 12, ...
              'Location', 'NorthEast');
xlabel('\fontsize{12}\it\xi\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\eta\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\zeta\rm', 'interpreter', 'tex')
xlim([-.2 .2]);
ylim([-.2 .2]);
zlim([-.2 .2]);
FIGS.fig4 = figure('Color', [1 1 1]);
plot3(w_s(:,1)/norm(w_s(:,1)),w_s(:,2)/norm(w_s(:,2)), ...
      w_s(:,3)/norm(w_s(:,3)),'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(1, :))
hold on
plot3(Hv_s(:,1)/norm(Hv_s(:,1)), Hv_s(:,2)/norm(Hv_s(:,2)), ...
      Hv_s(:,1)/norm(Hv_s(:,1)), 'LineWidth', 2, ...
      'Color', FIGS.LineColors(7, :))
title('\fontsize{12}Storhetsriktningar hos symmetrisk kropp')
FIGS.leg4 = legend(['$\it \hat{\omega}\rm$'], ...
       ['$\it \hat{H}\rm$']);
set(FIGS.leg4, 'interpreter', 'latex', 'fontsize', 12, ...
               'Location', 'NorthEast');
xlabel('\fontsize{12}\it\xi\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\eta\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\zeta\rm', 'interpreter', 'tex')
xlim([-.22 .22]);
ylim([-.22 .22]);
zlim([-.22 .22]);

