function mutatedChromosome = MultiMutate(chromosome, mutationProbability)
  nGenes = length(chromosome);
  mutatedChromosome = chromosome;
  possibleNumberofMutations = 3:10;
  generatedRandomNumberOfMutations = possibleNumberofMutations( ...
                         randi(length(possibleNumberofMutations), 1));
  for i = 1:generatedRandomNumberOfMutations
    if rand < mutationProbability;
      swap = randi(nGenes, 1, 2);
      mutatedChromosome([swap(1) swap(2)]) = ...
             mutatedChromosome([swap(2) swap(1)]);
    end
  end
end