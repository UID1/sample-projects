%1DIsing.m

% Simulering av 1D Isingmodellen med periodiska randvillkor (Metropolisalgoritmen).

% Programmet plottar medelenergi, specifikt v�rme och magnetisering som
% funktion av "temperaturen" kT f�r en Isingkedja med N spinn med antagandet
% att termisk j�mvikt uppn�tts efter c:a K steg.

clear

global N J kT I muB K

clf;

home;


% INDATA
N = input('Antalet spinn N: ');
J = input('Spinnkoppling J: ');
muB = input('magnetf�lt muB: ');
I = input('Antalet iterationer I: ');
% Hur m�nga iterationer vi vill g�ra p� modellen innan vi b�rjar summera energierna
K = input('Starta sampling, K: ');

%STARTKONFIGURATION

%Vi vill slumpa fram en vektor (matris av dimension 1) av l�ngden N
%som inneh�ller tillst�nden -1 resp 1

% Vi ser till att slumpgeneratorn befinner sig i ett nytt tillst�nd vid
% varje k�rning genom att basera tillst�ndet p� avl�sning av tid och datum
rand('state', sum(100*clock)) 
%s = ones(N,1); % Ferromagnetisk kall start
s =1-2*round(rand(N,1)); % Varm start


%ber�kna energin och magnetisering f�r start tillst�ndet

new_energy=0;
magnetization=0;
for i = 1:N
new_energy=new_energy-J*s(i)*s(mod(i,N)+1)-muB*s(i);   %periodic BC: N+1<=>1
magnetization=magnetization+s(i);
end

for p=1:20  % Stega fram temperaturvariabeln "kT"
kT = (21.-p)*.25;
Temperatur = kT % Temperaturflagga


% Initialisera energi och spin summor

sumenergy = 0; 
squaresumenergy = 0;
sumspin = 0;



for t=1:I  % Stega fram i tiden

old_energy=new_energy;

site = floor(rand.*N + 1);  % V�lj slumpvis ut ett spinn
s(site) = (-1).*s(site); % Flippa det valda spinnet --> ny konfiguration

% Nya spinnkonfigurationens energi och magnetisering
new_energy = old_energy -2*J.*s(site)*(s(mod(site,N)+1)+s(mod(site-2,N)+1))-2*muB*s(site); 
magnetization=magnetization+2*s(site);

if (new_energy >= old_energy) & (exp((old_energy-new_energy)./(kT)) <= rand)
 
 
s(site) = (-1).*s(site); 
new_energy=old_energy;
magnetization=magnetization+2*s(site);
% F�rkasta den nya spinnkonfigurationen
% (dvs. flippa tillbaks det slumpm�ssigt
% valda spinnet!) om dess energi �r st�rre
% �n den "gamla" konfigurationens energi OCH
% den relativa Boltzmannvikten �r mindre �n
% ett slumpm�ssigt valt tal i intervallet [0,1].

end 

if (t>=K)  %r�kna med det nya eller gamla tillst�ndet

sumenergy = sumenergy + new_energy;
squaresumenergy = squaresumenergy + (new_energy)^2.;
sumspin = sumspin + magnetization;

end



end %t-loop




%ensembelmedelv�rden vid given temp

x(p) = kT;  
Medelenergi(p) = sumenergy/((I-K+1)*N);
Specifiktvarme(p) = (squaresumenergy/(I-K+1)-(sumenergy/(I-K+1))^2.)/(N*(kT)^2.);
Magnetisering(p) = sumspin/(N*(I-K+1));

end  %p-loop, temp

% PLOT

subplot(1,3,1); plot(x,Medelenergi); hold on;
title('Medelenergi'), ylabel('<E>/N'), xlabel('kT')
subplot(1,3,2); plot(x,Specifiktvarme,'r'); hold on
title('Specifikt v�rme'), ylabel('C/Nk'), xlabel('kT')
subplot(1,3,3); plot(x,abs(Magnetisering),'g'); hold on
title('Magnetisering'), ylabel('M/N'), xlabel('kT')


% Plot av exakta resultat f�r medelenergi och specifikt v�rme som j�mf�relse
% (Plishke-Bergersen, "Equilibrium Statistical Physics", sid 80 - 86)

if (muB==0)
subplot(1,3,1); kT = .05:.001:5; plot(kT, -J.*tanh(J./kT),'k'); hold on
subplot(1,3,2); plot(kT, (J./kT).^2./(cosh(J./kT)).^2,'k'); hold on
end 
subplot(1,3,3); kT = .05:.2:5; 
plot(kT, sinh(muB./kT)./((sinh(muB./kT)).^2+exp(-4*J./kT)).^.5,'ok'); hold on
