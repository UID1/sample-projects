% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% The  Matlab script  'main2.m' uses  the  internal  matlab package  and
% function ARMAX  to fit ARMA  models into the  electricity  price data.
% The model estimations are evaluated  using a subset of the information
% criteria  and prediction  error  estimators  introduced  in 'main1.m'.
% Trend and seasonalities are  evaluated and  filtered out of  the price
% data. The spikes in  the data are  also analyzed  and methods to over-
% come the hurdles that are caused by these spikes are investigated.

% We set up the configuration for our plots

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

% +----------------+
% |     Step 1     |
% +----------------+


% We begin by opening the file
fid1 = fopen('elspoth2010.bin', 'r');
ELSPOTh2010 = fread(fid1, 8760, 'double');
fclose(fid1);

% And we analyze the period spectrum of this series
ELSPECh2010 = fspectrum3(ELSPOTh2010, 1);
FIGS.fig1 = figure('Color',[1 1 1]);
plot(ELSPECh2010.pscale, ELSPECh2010.spectrum, 'LineWidth', 1.5, ...
             'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Period spectrum for the spot prices');
xlabel('\fontsize{12}Period length (Hours)')
xlim([0 500])

% A quick analysis of the spectrum indeed shows that we have 24 hour and
% 168 hour  periodicities in  the  series.  But it also  appears to have
% other periodicities that cannot  be readily explained. We have period-
% icities at 4.77, 7.952, 11.93, 23.9 84.47, 170.6, 2175.

% I draw the  conclusion that 11.93  and 23.9 are related to the 24 hour
% periodicity and  84.47 and 170.6  are related to  the 168 hour period-
% icity.  The 4.77, 7.952 are  a little harder to  explain, perhaps they
% come from  8 hour working shifts.  The most  difficult  periodicity to
% explain �s the  2175 hour or 90 day periodicity.  It seems like  there
% is something that occurs roughly every 3 months.  We try to remove the
% trend component from the series and see how that affects the spectrum.

ELTREND2010 = est_ma(ELSPOTh2010, 500);
ELSPECh2010_2 = fspectrum3(ELSPOTh2010-ELTREND2010.removable, 1);
FIGS.fig2 = figure('Color',[1 1 1]);
plot(ELSPECh2010_2.pscale, ELSPECh2010_2.spectrum, ...
         'Color', FIGS.LineColors(3, :))
title('\fontsize{12}Period spectrum for the detrended spot prices');

% In the next step we try  to filter out  seasonalities from the series.
% We begin by  plotting the frequency  spectrum to  locate the peaks by:
% plot(X.fscale*1e5, X.fspectrum)  by  the Nyquist rate  the peaks  will
% then be located by magnitudes of 5e4.  We begin by defining the 24 and
% 168 hour filters. we use the Butterworth filters:

[num1, den1] = butter(2, [550 650]/5e4, 'stop');
[num2, den2] = butter(2, [4100 4300]/5e4, 'stop');
[num3, den3] = butter(2, [8350 8420]/5e4, 'stop');
[num4, den4] = butter(2, [1150 1250]/5e4, 'stop');

% And so we apply these filters on the observations. We apply the filter
% recursively  a  number  of  times  until the periodicities are removed
% satisfactorily.

ELh2010_des1 = filter(num2, den2, filter(num1, den1, ELSPOTh2010));
ELh2010_des1 = filter(num3, den3, filter(num4, den4, ELh2010_des1));

for i = 1:4
	ELh2010_des1 = filter(num2, den2, ...
	               filter(num1, den1, ELh2010_des1));
	ELh2010_des1 = filter(num1, den1,  ELh2010_des1);
	ELh2010_des1 = filter(num3, den3, ...
	               filter(num4, den4, ELh2010_des1));

end

% Let's fix the first 300 elements
fix_lt = find(ELh2010_des1(1:300) < ELSPOTh2010(1:300));
ELh2010_des1(fix_lt) = ELSPOTh2010(fix_lt);

FIGS.fig2 = figure('Color',[1 1 1]);
plot(ELh2010_des1, 'Color', [135 103 235]/255)
hold on
plot(ELSPOTh2010, 'Color', [239 112 154]/255)
legend('Deseasonalized data', 'Original data')
title('\fontsize{12}Hourly spot prices for 2010')
ylabel('\fontsize{12}SEK/MWh')
set(gca, 'XTick', 0:3*730:8760)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July', ...
                    'October', 'December'})

% Let's check how successful the filtration was:
ELTREND2010_2 = est_ma(ELh2010_des1, 500);
ELSPECh2010_3 = fspectrum3(ELh2010_des1-ELTREND2010_2.removable, 1);
FIGS.fig3 = figure('Color',[1 1 1]);
plot(ELSPECh2010_3.pscale, ELSPECh2010_3.spectrum)
title('Period spectrum for the detrended and filtered spot prices');

% Something weird  has happened  with  the first 300  elements after the
% filtration. Either we throw these observations away or we replace them
% with the original series for values lower than the original series.
% It is also suggestible to remove values that exceeds 1 200 - 1 500 and
% replace them with a that number.
% In the next step we could try analysing the logaritm functions.


% +----------------+
% |     Step 2     |
% +----------------+


% We start by applying the threshold

ELMAX = 1500;
ELSPOTh2010_capped = ELSPOTh2010;
ELSPOTh2010_capped(find(ELSPOTh2010 > 1500)) = 1500;

% And we analyze the period spectrum of this series
ELSPECh2010_4 = fspectrum3(ELSPOTh2010_capped, 1);
plot(ELSPECh2010_4.pscale, ELSPECh2010_4.spectrum)
title('Period spectrum for the capped spot prices');
% Peaks at: 2175, 870, 621, 543, 362.5, 290, 170.6, 84.47
%           33.46, 23.84, 11.93, 7.945, 4.772 hours period lenghts.
% The peaks are essentially the same as when not capping the data.
figure
plot(ELSPECh2010_4.fscale*1e5, ELSPECh2010_4.spectrum)
title('Frequency spectrum for the capped spot prices');

ELTREND2010_3 = est_ma(ELSPOTh2010_capped, 500);
figure
plot(ELTREND2010_3.precise)
title('Trend for the capped ELSPOT data');

% Since the peaks are essentially the same we use 'essentially' the same
% filters as well

ELh2010_des2 = filter(num2, den2, ...
               filter(num1, den1, ELSPOTh2010_capped));
ELh2010_des2 = filter(num3, den3, ...
               filter(num4, den4, ELh2010_des2));
for i = 1:4
	ELh2010_des2 = filter(num2, den2, ...
	               filter(num1, den1, ELh2010_des1));
	ELh2010_des2 = filter(num1, den1,  ELh2010_des1);
	ELh2010_des2 = filter(num3, den3, ...
	               filter(num4, den4, ELh2010_des1));

end
% Let's fix the first 300 elements
fix_lt = find(ELh2010_des2(1:300) < ELSPOTh2010(1:300));
ELh2010_des2(fix_lt) = ELSPOTh2010(fix_lt);

ELSPECh2010_5 = fspectrum3(ELSPOTh2010_capped, 1);
figure
plot(ELSPECh2010_5.pscale, ELSPECh2010_5.spectrum)
title('Period spectrum for the filtered and capped spot prices');
ELTREND2010_4 = est_ma(ELh2010_des2, 500);
ELSPECh2010_6 = fspectrum3(ELh2010_des2-ELTREND2010_4.removable, 1);
figure
plot(ELSPECh2010_6.pscale, ELSPECh2010_6.spectrum)
title(['Period spectrum for the capped, ' ...
       'filtered & detrended spot prices']);

% Even here  something happened with  the 300 first  elements during the
% filtration process.  We correct it by replacing the 300 first elements
% lower than the original.

Repl_idcs = find(ELh2010_des2(1:300) < ELSPOTh2010_capped(1:300));
ELh2010_des2(Repl_idcs) = ELSPOTh2010_capped(Repl_idcs);

% Now we try to fit an ARMA model to this series.
[ELh1m ELh1.minac ELh1.pbest ELh1.qbest] = ...
      armabat(ELh2010_des2-ELTREND2010_4.removable, 15, 15);


% I'm unable to find a proper fit  for the first 200 AR coefficients and
% the first 40 MA coefficients. I get Ljung-Box P-values : 
% K=241 P-v=0.000e+000,   K=1118 P-v=0.000e+000,   K=1995 P-v=9.871e-001
% K=2872 P-v=1.000e+000, K=3749 P-v=1.000e+000
% (p,q)=(200,40) aic=6.68823  fpe=801.68
%
% We need to do something else.  Perhaps taking logarithm and/or shorter
% periods to  fit the model (3 months for example).  We could be tighter
% with  the threshold  and we could also  apply some kind of a  low-pass
% filter.
%
% We should also try the 2009 and 2011 electricity price data.  We could
% also try the daily electricity price data.

% I have now managed  to get my hands on temperature data.  The tempera-
% ture data  is every three hours and some values are missing.  Also the
% temperatures  are local  so I had  to calculate a national average.  I
% managed to  make Matlab interpolate  the missing values  and  make the
% data hourly so that it matches  the frequency of the electricity price
% data. The implementation of this is in the temp_proc.m file.

% If tmptrs = temp_proc(8760, mtd); Then one can fit ARMAX in Matlab by
%
%  armax(iddata(ELSPOTh2010, tmptrs.national), [p q r 1])
%
% where p, q is as before and r  is the last r values of the temperature
% data that affects the electricity prices. The '1' denotes the delay in
% number of discrete time steps  before the temperature data affects the
% electricity  price movements.  I determine that there is no delay so I
% set it to 1 which is the minimal value  to set when there is no delay.
% When I come to think of it, a delay of a few hours wouldn't hurt as it
% takes a few hours  to cool down  a house  or heat it up.  The variable
% 'mtd' is the method used in the interpolation process,  the default is
% 'cubic'/'pchip' but it should be explicitly defined. Other methods are
% 'linear',  'nearest'  and 'spline'  as  used by  the  Matlab  function
% interp1().


% Let's try the  (1:20, 1:20, 1:3, 2) on  original data,  filtered data,
% and filtered and deseasonalized data:
%
%  ELSPOTh2010                                     _.: 1a :._
%  ELSPOTh2010_des1                                _.: 1b :._
%  ELSPOTh2010_des1-ELTREND2010_2.removable        _.: 1c :._
%
%  Repeat  the  procedure  on  element  1:2190  (3 months data)  (2a-2c)
%  We pick the model with the lowest AIC, then we try different caps but
%  but with the same parameters. When we have found a cap, we fix it and
%  re-evaluate  the best ARMA-parameters above.  Also try to do the same
%  without the temperature data included (3a-c and 4a-c).
% 
% To be continued in main3.m

