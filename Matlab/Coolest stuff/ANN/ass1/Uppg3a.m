% Artificial Neural Networks FFR135
% (c) Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 3a Solution
%
%

% alpha = p / N

% N_correct = ... = N/2 * (1+m_1)  according to page 47
% N_correct -> N as beta -> inf
% The temperature T is related to T through; beta = 1/ (k_B*T)
% For simplicity we can set k_B=1 and then we have T = beta^-1

% Choose beta^-1 from 0.1 to 0.5 in .1 steps.
% For each step

clear beta_0;

beta_0 = (.1:.1:.8).^-1;
alpha_0 = [.025 .05 .1 .125 .15 .2];

N = 1000;
p = N*alpha_0;
T = 10000;

% Dim 1; beta Dim 2: alpha Dim 3: rolling average of m_1

% Traveling mean = (mean(m_1(1:i-1))*(i-1)+m_1)/i
% or simply just = cumsum(m_1)./(1:length(m_1))

h3 = waitbar(0,'Beraknar ...');

for a_c = 1:length(p);
   for b_c = 1:length(beta_0);
      clear Zeta;
      clear m_1;
      Zeta = 2*(rand(N,p(a_c)) > .5) -1;

      W_ij = 1/N*Zeta*Zeta';
      W_ij = W_ij - diag(diag(W_ij));

      S = Zeta(:,1);

      g = @(x)(1/(1+exp(-2*beta_0(b_c)*x)));

      for t = 1:T;
         istep = round(N*rand);
         istep = (istep == 0) + (istep ~= 0)*istep;
         b_i = W_ij*S;
         S(istep) = (rand <= g(b_i(istep))) - (rand > g(b_i(istep)));
         m_1(t) = 1/N*Zeta(:,1)'*S;
      end
      M_1ab(a_c, b_c) = mean(m_1(ceil(T/2):end));
	  MQ95_1abl(a_c, b_c) = quantile(m_1(ceil(T/2):end), .025);
	  MQ95_1abu(a_c, b_c) = quantile(m_1(ceil(T/2):end), .975);
	  waitbar((a_c-1)/length(p)+b_c/(length(p)*length(beta_0)), h3);
   end
end
close(h3);
% Upper 95% limit = mean(x) + (SE*1.96) => SEU = (U95L - mean(x))/1.96
% Lower 95% limit = mean(x) - (SE*1.96) => SEL = (mean(x) - U95L)/1.96
disp('Mean, Standard Error')
[M_1ab (M_1ab-MQ95_1abl)/1.96]

FIGS.fig5 = figure('Color', [1 1 1]);
surf(alpha_0, beta_0, M_1ab');
xlabel('\fontsize{12}\alpha');
ylabel('\fontsize{12}\beta');
zlabel('\fontsize{12}m_1');
title('\fontsize{12}Walking mean magnetization')