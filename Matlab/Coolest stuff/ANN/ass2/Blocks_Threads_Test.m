function[returnValues] = Blocks_Threads_Test(blocks,threads)

%1  Create Cuda Kernel Object

k = parallel.gpu.CUDAKernel('BlocksThreads.ptx', 'BlocksThreads.cu','BlocksThreads');

%2 Set Object properties

k.GridSize = [blocks];
k.ThreadBlockSize = [threads];

%3 Set Argument Variables

gpu_Values = gpuArray(ones(4,blocks*threads));

[data] = feval(k, gpu_Values);


returnValues = gather(data);