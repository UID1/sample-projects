% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1a.1 Solution
% by Robin Axelsson
%

% This program analyses iteration times over
% a larger number of independent runs. It also
% tests for different alphas to see how different
% alpha values may affect the speed at which a
% solution might be found.

a1=[8479, 4868, 3696, 2646, 169, 142];
b1=[11968, 5026, 1081, 1050, 691, 184];
c1=[8479, 4167, 2646, 1081, 881, 859, 701, 691, 184, 169, 142];
alpha_0 = [1e-2]; % tested alpha values
numruns = 50; % Number of runs for each session
sigma_opts = zeros(length(a1),1);
mu_opts = zeros(length(b1),1);

%Let's try for all combinations
tic
wb6 = waitbar(0,'Running neural simulation');
ATR = perms(1:6);
HMATR = zeros(length(ATR),length(ATR));
for i = 1:length(ATR)
   for j = 1:length(ATR)
      sigma_n = ATR(i,:)';
	  mu_m = ATR(j,:)';
      CT = sort([cumsum(a1(sigma_n(1:(end-1)))) cumsum(b1(mu_m))]);
      c_test = sort([CT(1) diff(CT)], 'descend'); % = "c_tilde"
      H = sum(((c1-c_test).^2./c1));
	  HMATR(i,j) = H;
	  if H <= eps(1)
	     sigma_opts = [sigma_opts sigma_n];
		 mu_opts = [mu_opts mu_m];
	  end
	end
	waitbar(i/length(ATR), wb6);
end
sigma_opts(:,1) = [];
mu_opts(:,1) = [];
close(wb6);
toc
