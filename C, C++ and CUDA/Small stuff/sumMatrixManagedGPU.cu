#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <time.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

#define BDIMX 32
#define BDIMY 32

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/


#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

template <typename iox1>
void initialData(iox1 *ip, const int size)
{
   for(int i=0; i < size;++i)
      ip[i] = static_cast<iox1>(rand()/static_cast<iox1>(RAND_MAX));
      // Old implementation: (iox1)(rand() & 0xFF)/10.0f;
}

template<typename iox1>
void sumMatrixOnHost(iox1 *A, iox1 *B, iox1 *C, const int nx, const int ny)
{
   iox1 *ia = A;
   iox1 *ib = B;
   iox1 *ic = C;
   
   for(int iy=0; iy<ny;++iy)
   {
      for(int ix=0; ix<nx; ++ix)
         ic[ix] = ia[ix]+ib[ix];
      ia += nx;
      ib += nx;
      ic += nx;
   }
   return;
}

template <typename iox1>
__global__ void sumMatrixGPU(iox1 *MatA, iox1 *MatB, iox1 *MatC, int nx,
                             int ny)
{
    unsigned int ix = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int iy = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int idx = iy * nx + ix;
    if (ix < nx && iy < ny)
        MatC[idx] = MatA[idx] + MatB[idx];
}



void checkResult(float *hostRef, float *gpuRef, const int N)
{
   double epsilon = 1.0e-8;
   for(int i=0; i<N; ++i)
      if(abs(hostRef[i]-gpuRef[i]) > epsilon)
      {
         colors_t colors;
         colors.setFg(hGray);
         std::cout << "Host ";
         colors.setFg(dCyan);
         std::cout << hostRef[i];
         colors.setFg(hGray);
         std::cout << " GPU ";
         colors.setFg(dCyan);
         std::cout << gpuRef[i];
         colors.setFg(hGray);
         std::cout << ": ";
         colors.setFg(hRed);
         std::cout << "Arrays do not match!";
         colors.setFg(hGray);
         std::cout << std::endl << std::endl;
         colors.setColors(colors.initial_colors);
         break;
      }
}


int main(int argc, char **argv)
{
   colors_t colors;
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));
   
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", for matrix summation"; colors.setFg(hGray);
   std::cout << std::endl;

   /* Set up array size */
   int64 nx, ny ;
   int iShift = 12;
   
   if(argc>1) iShift = atoi(argv[1]);
   nx = ny = 1<<iShift;
   int64 nxy = nx*ny;
   int64 nBytes = nxy*sizeof(float);
   
   colors.setFg(hGray); std::cout << "Matrix size nx, ny: ";
   colors.setFg(dCyan); std::cout << nx;
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << ny;
   colors.setFg(hGray); std::cout << "." <<std::endl;

   /* Malloc host memory */
   float *A, *B, *hostRef, *gpuRef;
   CHECK(cudaMallocManaged((void **)&A, nBytes));
   CHECK(cudaMallocManaged((void **)&B, nBytes));
   CHECK(cudaMallocManaged((void **)&gpuRef, nBytes));
   CHECK(cudaMallocManaged((void **)&hostRef, nBytes));
   

   /* Initialize data at the host side */
   double iStart = cpuSecond();
   initialData(A, nxy);
   initialData(B, nxy);
   double iElaps = cpuSecond() - iStart;

   colors.setFg(hGray); std::cout << "Matrix initialization completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;
   
   memset(hostRef, 0, nBytes);
   memset(gpuRef,  0, nBytes);
   
   /* Add matrix on the host side to check results */ 
   iStart = cpuSecond();
   sumMatrixOnHost(A, B, hostRef, nx, ny);
   iElaps = cpuSecond() - iStart;

   colors.setFg(hGray); std::cout << "sumMatrixOnHost";
   colors.setFg(White); std::cout << "()";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   /* Configuration of execution */
   int blockx(32), blocky(32);
   dim3 block (blockx, blocky);
   dim3 grid ((nx+block.x-1)/block.x, (ny+block.y-1)/block.y);
   
   /* 
      Warmup, with unified memory all pages will migrate from host
      to device
   */
   sumMatrixGPU<<<grid, block>>>(A, B, gpuRef, 1, 1);
   
   iStart = cpuSecond();
   sumMatrixGPU<<<grid, block>>>(A, B, gpuRef, nx, ny);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "sumMatrixGPU ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(dCyan); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(dCyan); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(dCyan); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << "()";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaDeviceSynchronize());
   
   /* Check  results */
   checkResult(hostRef, gpuRef, nxy);
   
   /* Free host and device memory */
   CHECK(cudaFree(A));
   CHECK(cudaFree(B));
   CHECK(cudaFree(hostRef));
   CHECK(cudaFree(gpuRef));
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}
