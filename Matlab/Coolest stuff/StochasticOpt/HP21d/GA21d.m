% Initialization procedure of data
cityLocation = LoadCityLocations;
nCities = size(cityLocation,1);
path = GetNearestNeighbourPath(cityLocation);                
tspFigure = InitializeTspPlot(cityLocation,[0 20 0 20]); 
connection = InitializeConnections(cityLocation); 
plot(cityLocation(:,1), cityLocation(:,2), 'o', 'MarkerSize', 4, ...
     'MarkerFaceColor', [0 .4 .65], 'MarkerEdgeColor', [0 .25 .5]);

% Setting up parameters for genetic algorithm
GAParams.populationSize = 6000;
GAParams.numberOfGenes = 40;
GAParams.crossoverProbability = .8;
GAParams.mutationProbability = .95;
GAParams.variableRange = 5.0;
GAParams.numberOfGenerations = 200;
GAParams.numberOfConnectionsToPermute = nCities;
GAParams.tournamentSize = 2;
GAParams.tournamentSelectionParameter = .7;

% Initialize plots
PlotPath(connection, cityLocation, path);
fitnessFigureHandle = figure;
hold on;
set(fitnessFigureHandle, 'Position', [50,50,500,200]);
set(fitnessFigureHandle, 'DoubleBuffer',        'on');
axis([-GAParams.numberOfGenerations 0 0 500]);
bestPlotHandle = plot(-GAParams.numberOfGenerations:-1, ...
                      zeros(1,GAParams.numberOfGenerations));
yTextHandle = ylabel(sprintf('Best path: %4.3f', 0.0));
xTextHandle = xlabel('Initializing ...');
hold off;
drawnow;

% Initialize with nearest neighbourhood selection for the population
wb1 = waitbar(0,'Generating nearest neighbour paths');
population = zeros(GAParams.populationSize, ... 
                   GAParams.numberOfConnectionsToPermute);
for iPath = 1:GAParams.populationSize
  population(iPath, :) = GetNearestNeighbourPath(cityLocation);
  waitbar(iPath/GAParams.numberOfGenerations, wb1);
end
close(wb1);
fitness = zeros(GAParams.populationSize, 1);

% Apply genetic algorithm
currentGeneration = 0;
while true
  currentGeneration = currentGeneration + 1;
  maximumFitness = .0; % Assumes non-negative fitness values!
  bestPath = randperm(nCities); % [0 0]
  bestIndividualIndex = 0;
  % Tournament selection
  for i = 1:GAParams.populationSize
    chromosome = population(i,:);
    fitness(i) = EvaluateIndividual(chromosome, cityLocation);
    if (fitness(i) > maximumFitness)
      maximumFitness = fitness(i);
      bestIndividualIndex = i;
      bestPath = chromosome;
    end
  end
  tempPopulation = population;
  
  % Update plots
  PlotPath(connection, cityLocation, population(bestIndividualIndex,:));
  plotvector = get(bestPlotHandle, 'YData');
  bestPathLength = GetPathLength(bestPath,cityLocation);
  plotvector = [plotvector(2:end) bestPathLength];
  set(bestPlotHandle, 'YData', plotvector);
  set(yTextHandle, 'String', sprintf('Best path: %4.3f', ...
                                    bestPathLength));
  set(xTextHandle, 'String', sprintf('Generation %d', currentGeneration));
  drawnow;
  
  % Set first individual to the best and mutate the rest
  tempPopulation(1,:) = population(bestIndividualIndex,:);
  for i = 2:GAParams.populationSize
    originalChromosome = tempPopulation(i,:);
    mutatedChromosome = MultiMutate(originalChromosome, ...
                                    GAParams.mutationProbability);
    tempPopulation(i,:) = mutatedChromosome;
  end
  population = tempPopulation;    
end

disp('Best path');
disp(bestPath');
disp('Path length');
disp(GetPathLength(bestPath, cityLocation));