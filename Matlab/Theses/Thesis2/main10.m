% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% See the help information for prior main<x>.m scripts  for a background
% story. In main10.m we once again start from scratch as in main8.m but
% now with daily data. Then we proceed to fit ARMA(p,q) and ARMAX(p,q,r)
% models like we did in main9.m

if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
tmpdaily = mean(reshape(tmp2010.national, 24, 365));
if ~any(strcmp(who,'ELSPOTd2010'))
   load ELSPOTd2010.mat;
end

ELSPEC1 = fspectrum3(ELSPOTd2010.sys, 1);
figure
plot(ELSPEC1.pscale, ELSPEC1.spectrum)
title('Period spectrum for the daily spot prices');

% The spectrum doesn't tell the difference between hours and daty so now
% the period spectrum shows period lengths in days instead of hours.
% The largest peak is at 75 days that joins with a peak at 60 days and 100
% days. Perhaps removing the 50-day trend of the observations could take
% care of that. Then there are two smaller peaks at 2.9 days (y = 3942)
% and 5.9 days (y = 5007).

figure
hold on
for i=1:100
   ELTREND = est_ma(ELSPOTd2010.sys, i);
   plot(ELTREND.removable, 'color', [230-2*i 0 230-i]/255, ...
         'LineWidth', 1)
end

ELTREND = est_ma(ELSPOTd2010.sys, 50);
ELSPECdt = fspectrum3(ELSPOTd2010.sys - ELTREND.removable, 1);

figure
plot(ELSPECdt.fscale*1e5, ELSPECdt.spectrum)
title('Frequency spectrum for the detrended spot prices');

% Even with the trend removed, there is still a 60 day seasonality to be
% removed. Peaks in descending order: 1667, 17000, 34500.

[filt.n1, filt.d1] = butter(2, [1600 1700]/5e4, 'stop');
[filt.n2, filt.d2] = butter(2, [16500 17500]/5e4, 'stop');
[filt.n3, filt.d3] = butter(2, [34000 35000]/5e4, 'stop');

ELSPOTdt = ELSPOTd2010.sys - ELTREND.removable;

% I choose not to filter everything as some part of the variations come
% from the temperature variations.

ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n2, filt.d2, ELSPOTdt));
ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n3, filt.d3, ELddes));
for i=1:2 % 
   ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n2, filt.d2, ELddes));
   ELddes = filter(filt.n1, filt.d1, ...
                       filter(filt.n3, filt.d3, ELddes));
end
ELSPOTdsdt = ELddes;
ELSPOTds = ELddes+ELTREND.removable;
ELSPECdsdt = fspectrum3(ELSPOTdsdt,1)

figure
plot(ELSPECdsdt.pscale, ELSPECdsdt.spectrum)


[m1a.data m1a.par] = armabat4(ELSPOTdsdt, tmpdaily', ...
                               1:25, 1:25, 1:25, 1, '1a')
[m1b.data m1b.par] = armabat4(ELSPOTdsdt/1e3, tmpdaily', ...
                               1:25, 1:25, 1:25, 1, '1b')

[m2a.data m2a.par] = armabat4(ELSPOTdsdt, 'nil', ...
                               1:25, 1:25, 0, 1, '2a')
[m2b.data m2b.par] = armabat4(ELSPOTdsdt/1e3, 'nil', ...
                               1:25, 1:25, 0, 1, '2b')

savefile = sprintf('ARMAX3est(%s).mat', ...
                     datestr(now, 'mmm-dd_HH-MM.FFF'));
save(savefile, 'm1a', 'm1b', 'm2a', 'm2b')

% There is  an impenetrable but transparent screen  between our world of
% mathematical descriptions and the real world. We can look through this
% window  and compare  certain aspects of  the physical system  with its
% mathematical description, but we can never establish any exact connec-
% tion beteen them.
%            - Lennart Ljung, System Identification Theory for the User.

