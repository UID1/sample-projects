//
//  ViewController.swift
//  Core Data Demo
//
//  Created by Iberius Pred on 28/02/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Create a "context" entitiy to use to interact with the database
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        // Creates a new entry and properties for that entry.
        /*
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
        // Haaplo, Raapla, Ursula
        newUser.setValue("Klukki", forKey: "username")
        newUser.setValue("myPassword", forKey: "password")
        newUser.setValue(6, forKey: "age") */
        // Then we try to save it to hte database
        do {
            try context.save()
            print("Entry was successfully saved")
        } catch {
            print("There was an error")
        }
        // Fetch entries from the database
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "username == %@", "Dooley")
        // "%@" means any object
        request.returnsObjectsAsFaults = false
        
        
        // We submit the request and if we get results, we loop them through, and if we don't then we
        // report that no results were found.
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if let username = result.value(forKey: "username") as? String/*, let userAge = result.value(forKey: "age") as? Int16*/ {
                        //result.setValue("Dooley", forKey: "username")
                        /*
                        context.delete(result)
                        
                        do {
                            try context.save()
                        } catch {
                            print("Delete failed for \(result.value(forKey: "username")).")
                        }*/
                        print(username)
                    }
                }
            } else {
                print("No results")
            }
        } catch {
            print("Couldn't fetch results")
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

