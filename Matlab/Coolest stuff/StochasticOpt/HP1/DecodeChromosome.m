function x = DecodeChromosome(chromosome, n, variableRange)
  nPartial = length(chromosome) / n;
  x = zeros(1,n);
  for i = 1:n
    indexRangeK = 1+(i-1)*nPartial:i*nPartial;
    x(i) = 2.^-(1:nPartial) * chromosome(indexRangeK)';
  end
  x = -variableRange + 2 * variableRange * x / (1 - 2^(-nPartial));
end