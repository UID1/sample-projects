function altitudeValues = getSlopeProfile(slopeVector, deltaX)
  numElements = length(slopeVector);
  altitudeValues = zeros(1, numElements);
  for i = numElements:-1:2
    altitudeValues(i-1) = altitudeValues(i) + ...
                            tan(slopeVector(i) * pi / 180) * deltaX;
  end
end