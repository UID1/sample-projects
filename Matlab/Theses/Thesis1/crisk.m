%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelsh�gskolan, G�teborg
% April, 2011
%

% We define the market hazard function as an exponentially decreasing function

gamma_h = mean(mean(rand(10))); % Just some arbitrary value for gamma
Q_market_fun = @(t, gamma) (exp(-gamma.*t));

% The intensity volatility parameters are (as in Brigo, Pallavicini 2008)

y_0 = .0165;
kappa = .4;
mu_t = .026;
nu = .14;

% The CIR-based intensity model has a closed form expression for the
% default probability which can be found in Alexander's Lecture notes
% from lecture 3 in his Credit risk modelling course (slide 32; eq 33-36) 

gamma_CIR = @(alpha, sigma)(sqrt(alpha.^2+2.*sigma.^2));
CIR_A = @(T, gamma, alpha, mu, nu)(log(2.*gamma.*exp((gamma+alpha).*T.*.5)./((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma)).*2.*alpha.*mu./nu.^2);
CIR_B = @(T, gamma, alpha)(2.*(exp(gamma.*T)-1)./ ((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma));

P_CIR = @(t, kappa, nu, mu, y_0)(exp(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)-CIR_B(t, gamma_CIR(kappa, nu), kappa).*y_0));

% From this we can shape the Psi function as in expression (3.6) in the paper (Brigo,
% Pallavicini 2008). As can be seen Psi has a closed form expression.

beta_CIR = [y_0, kappa, mu_t, nu];

Psi = @(t, beta, gamma_h)(log(P_CIR(t, beta(2), beta(4), beta(3), beta(1))./Q_market_fun(t, gamma_h)));

% In the next step we work with a CIR process that should be integrated.
% First we decide upon a set of observation times over which this process
% is observed and define a vector of time increments dt

t_CIR = 0:.0005:.01;
dt_CIR = diff(t_CIR');
n_CIR = length(t_CIR);

% We initialize a vector that starts with the initial value y_0
y_t = [y_0; nan*dt_CIR];

% We assume that y_t follows the CIR process 
% dy_t = kappa*(mu_t-y_t)dt + nu*sqrt(mu_t)*dW

d = 4*kappa*mu_t/nu^2;
e = exp(-kappa*dt_CIR);
c = nu^2.*(1-e)/(4*kappa);
Y_t = zeros(1,length(y_t));

% We have to use a for-loop since it is a recursive algorithm

for i = 1:(n_CIR-1)
    l = y_t(i)*e(i)/c(i); 
    y_t(i+1) = c(i)*ncx2rnd(d,l);
    Y_t(i+1) = Y_t(i)+dt_CIR(i)*(y_t(i)+y_t(i+1))/2;
end

Q_model = mean(exp(-Psi(t_CIR, beta_CIR, gamma_h)-Y_t))


