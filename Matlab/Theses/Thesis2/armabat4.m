function [mbest,bestpar]=armabat4(x,y,pvec,qvec,rvec,delay,msg)
% SYNOPSIS
%
%  [mbest, bestpar] = ARMABAT4(x, y, pvec, qvec, rvec, delay, msg)
%
% DESCRIPTION
%
%  Computes  the ARMA(p,q)  or  ARMAX(p,q,r) models for (p,q,r) in pvec,
%  qvec  and rvec,  and returns the best  fitting  model  according to a
%  computed distance between the cumulative distribution a simulation of
%  each model and the empirical cumulative distribution.
%
% INPUT
%
%   x                    : input data
%   y                    : exogeneous data supposedly affecting x. Set
%                          y = 'nil' for pure ARMA(p,q)
%   delay                : the delay before the exogeneous data y begins
%                          to affect x specified in number of time-
%                          steps. Minimum delay is '1'.
%   pvec                 : vector of p's; set pvec=[0] for no AR
%   qvec                 : vector of q's; set qvec=[0] for no MA
%   qvec                 : vector of q's; set qvec=[0] and/or y = 'nil'
%                          for no exogeneous influence.
%   msg                  : a message or tag to use to identify the run
%                          process and monitor progress when running
%                          multiple batches
%
% OUTPUT
%
%   mbest                : matlab model structure for the best one found
%                          using the matlab aic the model parameters can
%                          be retreived from mbest
%   bestpar.minaic       : value of the aic at the min
%   bestpar.p_best       : best value of p found
%   bestpar.q_best       : best value of q found
%   bestpar.r_best       : best value of r found
%   bestpar.(p|q|r)_good : vectors of simulatable p, q, and r values
%
% NOTES
%
%  Designed for the final master thesis project in Physics to study
%  model estimation of electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011


np=length(pvec);
nq=length(qvec);
nr=length(rvec);
simdist = 1e99*ones(np,nq,nr);
aicsave =  -99*ones(np,nq,nr);
fpesave =  -99*ones(np,nq,nr);
[f_emp x_emp] = ksdensity(x);
delay = max(1, delay);
mindst = 1e99;
minaic=1e+6;
p_good = [];
q_good = [];
r_good = [];
p_best = 00;
q_best = 00;
r_best = 00;
disp(sprintf('Starting computation job : %s', msg))
fprintf('Trial (1,1,1) of (%d,%d,%d) on %s : ', length(pvec), ...
          length(qvec), length(rvec), msg)
goodone =0;
prev = [1 1 1];
for pp=1:np 
    p=pvec(pp);
    for qq=1:nq
        q=qvec(qq);
        for rr = 1:nr
            r = rvec(rr);
            if p+q+r ~=0 % ~= is 'not' equal, or '!='
                if strcmp(y, 'nil')
                    m = armax(x,[p q]);
                else
                    m = armax(iddata(x,y),[p q r delay]);
                end
		if ~goodone % fprintf cleanup if there is no newline
                    for i = 1:length(sprintf( ...
	                         'Trial (%d,%d,%d) of (%d,%d,%d) on %s : ', ...
                                  prev(1), prev(2), prev(3), length(pvec), ...
                                  length(qvec), length(rvec), msg))
                         fprintf('\b')
                    end
                end
		goodone = 0;        % These are to make sure ^^fprintf^^
		prev = [pp qq rr];  % cleans up correctly at next run
                fprintf('Trial (%d,%d,%d) of (%d,%d,%d) on %s : ', pp, ...
                               qq, rr, length(pvec), length(qvec), ...
                               length(rvec), msg)
                sim1 = armaxsim(m, y, length(x), 1, 0); 
                aicsave(pp,qq,rr)=aic(m);
                fpesave(pp,qq,rr)=fpe(m);
                if max(sim1) < 1e20
                    goodone = 1;
                    disp(sprintf( ...
                         'Found a good one! AIC = %g, FPE = %g', ...
                         aicsave(pp,qq,rr), fpesave(pp,qq,rr)))
                    p_good = [p_good p];
                    q_good = [q_good q];
                    r_good = [r_good r];
                    if aicsave(pp,qq,rr) < minaic
                        minaic=aicsave(pp,qq,rr);
                    end
	            [f_sim,x_sim] = ksdensity(sim1);
                    simdist(pp,qq,rr) = trapz(x_emp, abs( ...
                                               cumsum(f_emp) - ...
                                               cumsum(f_sim)));
                    if simdist(pp,qq,rr) < mindst
                       p_best = p;
                       q_best = q;
                       r_best = r;
                       mbest = m;
                       mindst = simdist(pp,qq,rr);
                    end
                end
            end
        end
    end
end
bestpar.aicmtx = aicsave;
bestpar.fepmtx = fpesave;
mtxa = simdist;
midx = find(simdist == 1e99);
mtxa(midx) = 0;
bestpar.simdstmtx2 = mtxa;
mtxa(midx) = max(max(max(mtxa))) + 10;
bestpar.simdstmtx = mtxa;
bestpar.p_good = p_good;
bestpar.q_good = q_good;
bestpar.r_good = r_good;
bestpar.p_best = p_best;
bestpar.q_best = q_best;
bestpar.r_best = r_best;
bestpar.minaic = minaic;
end % armabat4.m

