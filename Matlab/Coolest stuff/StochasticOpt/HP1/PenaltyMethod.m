%
%  Course: Stochastic optimization
%  Assignment: Hand-In 1.1
%  Author: Robin Axelsson
%  Term: Fall 2016
%


muSequence = [10.^(0:3) 905];
T = 1e-6;
eta = 1e-5;

x_0 = [1 2];

fprintf('Results:\n\n');
fprintf('   mu    |   x_1^*  |  x_2^*  \n');
fprintf('---------+----------+---------\n');

for i = 1:length(muSequence)
  xOpt = GradientDescent(x_0, muSequence(i), eta, T);
  fprintf('%5d    | %8.3f | %8.3f\n', muSequence(i), xOpt(1), xOpt(2));
end
fprintf('---------+----------+---------\n');
