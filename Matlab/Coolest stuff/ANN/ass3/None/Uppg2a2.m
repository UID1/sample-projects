% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2a Solution 2
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

% This program also works better with the old data
% if using the new data, it should be left unnormalized

%TRN = importdata('train_data1.txt');
TRN = importdata('train_data_new.txt');
VLD = importdata('valid_data_new.txt');
TRNn = [(TRN(:,1)-mean(TRN(:,1)))/std(TRN(:,1)) ...
        (TRN(:,2)-mean(TRN(:,2)))/std(TRN(:,2)) TRN(:,3)];
	
TRNn = TRN;
T = length(TRN);
NumIter = 1e6;

% Define the weight functions for the system
w = 2*(rand(1,2)-.5)*.2;

% Set predefined parameters
eta_0 = .1;
eta_1 = .01;
i_s = 1; % Start position of decline
i_e = 3*1e4; % End/target position
eta_s = .01; % Start value
eta_e = 1e-6; % End/target value
% Final decline parameters
eta_m = (eta_e*i_e-eta_s*i_s)/(eta_e-eta_s);
eta_k = eta_s*(i_s-eta_m);
eta = .01;
theta = 0; % One output, one value
beta_0 = .5; % We set g(b) = tanh(beta*b) as defined on page 108

% Initialization of help variables
wb1 = waitbar(0,'Initializing ...');
movegui(wb1, 'southeast');
Ewinsize = 1e4; % Window size
Ewin1 = zeros(1,Ewinsize); % Time span for rolling avg of E
Ewin2 = zeros(1,Ewinsize);
Evec1 = zeros(1,floor(NumIter/Ewinsize)); % Vector of avg E:s
Evec2 = zeros(1,floor(NumIter/Ewinsize));
Cvvec1 = zeros(1,floor(NumIter/Ewinsize));
Cvvec2 = zeros(1,floor(NumIter/Ewinsize));

% We set up a trial pattern during training
SimSize = 1000;
simulationUpdateFrequency = 100;
stopRecording = 50000;
iFrame = 0;
Xmin = -.5; Xmax = .5; Xstep = .01;
Ymin = 0;   Ymax = 1;  Ystep = .01;
MC1 = (Xmin:Xstep:Xmax)'*ones(1,length(Xmin:Xstep:Xmax));
MC2 = meshgrid((Ymin:Ystep:Ymax)',1:length(Ymin:Ystep:Ymax));
INP = [MC1(:) MC2(:)];
SimSize = length(INP);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [55, 36, 138]]/255;
FIGS.fig6 = figure('Color', [0 0 0]);
CInit = meshgrid(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax);
contourf(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, CInit, 50, 'LineStyle', 'none');
colormap(bone)
hold on
TST = [INP randi(2, ((Xmax-Xmin)/Xstep+1)*((Ymax-Ymin)/Ystep+1),1).*2-3];
contour(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, ...
             reshape(TST(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)', ...
			 [-2 0], '-', 'LineWidth', 3, 'color', FIGS.LineColors(2,:));
plot(TRN(find(TRN(:,3)==-1),1), TRN(find(TRN(:,3)==-1),2), 'o', ...
                            'markers', 10, 'color', FIGS.LineColors(4,:))
plot(TRN(find(TRN(:,3)==1),1), TRN(find(TRN(:,3)==1),2), 'o', ...
                            'markers', 10, 'color', FIGS.LineColors(8,:))
FIGS.plot3 = findall(FIGS.fig6,'Type','hggroup'); % This circumvents a Matlab bug
set(0,'units','pixels');
PixSS = get(0,'screensize');
FigX = 750; FigY = 600;
set(FIGS.fig6, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
FIGS.title1 = title('\fontsize{12}Simple perceptron during training');
FIGS.label1 = xlabel('Initializing ...');
% set(FIGS.plot3(2), 'LevelListMode', 'Auto');
FIGS.canvas1 = gca;
set(gca, 'Color', 'none');  % Some parameters to fiddle with a Matlab bug
set(gcf, 'Color', [0 0 0]); % that interferes with figure exports
% This sets our colors
set(FIGS.canvas1, 'Color', [0 0 0], 'XColor', [.85 .80 .65], 'YColor', [.85 .80 .65]);
set(FIGS.title1, 'Color', [.85 .80 .65]);

% Training/Learning process
for i=1:NumIter
   IdxIter = 1+floor(length(TRNn)*rand());
   eta = (i <= 2*1e4)*eta_0+(i > 2*1e4)*(i <= 3*1e4)*eta_1+ ...
              (i>3*1e4)*eta_k/(i-eta_m-3*1e4+1);

   O_mu = tanh(beta_0*(TRNn(:,1:2)*w'-theta));
   derivO_mu = beta_0*(1-O_mu(IdxIter).^2);
   deltaw_ik = eta*(((TRNn(IdxIter,3)-O_mu(IdxIter))*[1 1]).*derivO_mu.*TRN(IdxIter,1:2));
   w = w + deltaw_ik;
   deltatheta = -eta*sum((TRNn(IdxIter,3)-O_mu(IdxIter)).*derivO_mu);
   theta = theta + deltatheta;
   % Energy and classification error calculations
   Ewin1(1+mod(i,Ewinsize)) = .5*sum((TRNn(:,3)-O_mu(:)).^2);
   O_muC = O_mu;
   O_mu = tanh(beta_0*(VLD(:,1:2)*w'-theta));
   Ewin2(1+mod(i,Ewinsize)) = .5*sum((VLD(:,3)-O_mu(:)).^2);
   if mod(i,Ewinsize) == 0
      waitbar(i/NumIter, wb1);
	  Evec1(i/Ewinsize) = mean(Ewin1);
	  Evec2(i/Ewinsize) = mean(Ewin2);
	  Cvvec1(i/Ewinsize) = mean(abs(TRNn(:,3) - sign(O_muC)));
	  Cvvec2(i/Ewinsize) = mean(abs(VLD(:,3) - sign(O_mu)));

      SPC = toc;
      waitbar(i/NumIter, wb1, sprintf('Calculating at %.2f iterations/s', Ewinsize/SPC));
	  tic;
   end
   if mod(i,simulationUpdateFrequency) == 0
	  O_muMC = tanh(beta_0*(INP*w'-theta));
	  O_muSF = 2*atan(beta_0*(INP*w'-theta))/pi;
	  O_muSSF = beta_0*(INP*w'-theta);
	  
      TST = [INP(:,1) INP(:,2) sign(O_muMC)];
	  CMesh = reshape(TST(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  DMesh = reshape(O_muMC, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  EMesh = reshape(O_muSF, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  set(FIGS.plot3(1), 'ZData', DMesh);
	  set(FIGS.plot3(2), 'ZData', EMesh, 'LevelList', ...
	                        linspace(min(min(EMesh)), max(max(EMesh)), 51));
	  set(FIGS.label1, 'String', sprintf('\\it C_v\\rm  = %f, \\it \\eta \\rm  = %f, Iteration step: %d', ...
	                  mean(abs(TRNn(:,3) - sign(O_muC))), eta, i));
	  drawnow;   
      if i <= stopRecording
          iFrame = iFrame +1;
          %hgexport(FIGS.fig6, sprintf('frame%07.f.png', iFrame), hgexport('factorystyle'), 'Format', 'png');
	      export_fig(FIGS.fig6, sprintf('frame%07.f.png', iFrame), '-png', '-nocrop');
      end
   end
   if mod(i,min(simulationUpdateFrequency,Ewinsize)) == 0
      SPC = toc;
      waitbar(i/NumIter, wb1, sprintf('Calculating at %.2f iterations/s', ...
		                    min(simulationUpdateFrequency,Ewinsize)/SPC));
      tic;
   end
end
close(wb1);

% Plot the energy over iteration steps
FIGS.fig2 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(3, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(4, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Evolution of energy level over time')

FIGS.fig3 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(6, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Evolution of classification error over time')

% Lets figure out how well our system works with a Monte-Carlo
% simulation

INP = [rand(1000, 1)-.5 rand(1000,1)];
TST = [INP(:,1) INP(:,2) sign(w(1)*INP(:,1)+w(2)*INP(:,2)-theta)];
FIGS.fig4 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')
