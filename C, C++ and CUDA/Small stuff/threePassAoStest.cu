#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <ctime>
#include <locale>
#include <cstdlib>
#include <Windows.h>

#define QS_MAX_LEVELS 300
typedef long long int64;
typedef unsigned long long uint64;
#define MAX_ZERO_SOLUTIONS 1000000

/* CUDA grid configuration */

#define GDIMX 2//8
#define GDIMY 2//8
#define GDIMZ 1
#define BDIMX 2//8
#define BDIMY 2//8
#define BDIMZ 1
#define NUM_THREADS 16 //4096

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
      {                                                                       \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
      }                                                                       \
}                                                                             \

struct scKernelStatus
{
	float currentOptH;
	int currentOptCfgA;
	int currentOptCfgB;
	int numIterations;
	int threadIdx;
	int intCache;
	int64 int64Cache;
	float floatCache;
	bool boolCache;
	bool isAlive;
};

__device__ int getGlobalThreadIdx()
{
	int blockId = blockIdx.x
		+ gridDim.x * blockIdx.y
		+ gridDim.x * gridDim.y * blockIdx.z;
	int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z)
		+ (threadIdx.z * (blockDim.x * blockDim.y))
		+ (threadIdx.y * blockDim.x)
		+ threadIdx.x;

	return threadId;
}

__device__ int generateLehmerCode(const int *vCfg, int iSize)
{
	int *iMask = new int[iSize];
	for (int i = 0; i < iSize; ++i)
		iMask[i] = 1;
	int *lehmerVector = new int[iSize];
	int lehmerValue(0), iFactorial(1);
	for (int i = 0; i < iSize; ++i)
	{
		iMask[vCfg[i]] = 0;
		lehmerVector[i] = 0;
		for (int j = 0; j<=vCfg[i]; ++j) lehmerVector[i] += iMask[j];
	}
	for (int i = 1; i<=iSize; ++i) {
		iFactorial *= i;
		lehmerValue += lehmerVector[iSize - i] * iFactorial;
	}
   
	delete[] iMask;
	delete[] lehmerVector;
	return lehmerValue;
}

__device__ void permuteFromLehmerCode(int *vPermutation,
	const int iSize, const int iLehmerValue)
{
	int iFactorial(1), iRestValue(iLehmerValue);
	for (int i = 1; i <= iSize; ++i)
		iFactorial *= i;
	int *lehmerVector = new int[iSize];
   for(int i  = 0; i < iSize; ++i)
   {
      lehmerVector[i] = iRestValue / iFactorial;
      iRestValue -= lehmerVector[i]*iFactorial;
      iFactorial /= iSize-i;
   }
	int *iMask = new int[iSize];
	for (int i = 0; i < iSize; ++i)
		iMask[i] = 1;
	for (int i = 0; i < iSize; ++i)
	{
		int cumIElements = 0;
		int cumSumIMask = 0;
		while ((cumSumIMask < lehmerVector[i]+1) &&
				(cumIElements<iSize))
			cumSumIMask += iMask[cumIElements++];
      --cumIElements;
		vPermutation[i] = cumIElements;
		iMask[cumIElements] = 0;
	}

	delete[] iMask;
	delete[] lehmerVector;
}

__global__ void initializeKernel(scKernelStatus *currentKernelStatus, int size, int64 seed)
{
/* Initialize variables for generating permutation */
	int swapIdx(0), swapper(0);
   int threadId = getGlobalThreadIdx();
	curandStateMRG32k3a swpElemState;
	curand_init(seed, threadId, 0, &swpElemState);
   
   
	/* Initialize vectors to be used for permutation */
	int sizeA(size);
	int *cfgA = new int[sizeA];
   int *lehmerA = new int[sizeA];
	for (int i = 0; i < sizeA; ++i) cfgA[i] = i;
	/* Conduct the permutation */
	for (int i = 0; i < sizeA-1; ++i)
	{
		swapIdx = curand(&swpElemState) % (sizeA - 1 - i);
		swapper = cfgA[i];
		cfgA[i] = cfgA[i + swapIdx];
		cfgA[i + swapIdx] = swapper;
	}
   
	currentKernelStatus[threadId].currentOptCfgA
			= generateLehmerCode(cfgA, sizeA);

  	currentKernelStatus[threadId].numIterations = 0;
	currentKernelStatus[threadId].currentOptH = 1.0E6;
	currentKernelStatus[threadId].threadIdx = threadId;
	currentKernelStatus[threadId].isAlive = true;

   delete[] cfgA, lehmerA;
}

__global__ void annealKernel(scEnzConfig *inpCfg, scKernelStatus *currentKernelStatus)
{
   int threadId = getGlobalThreadIdx();
   if (currentKernelStatus[threadId].isAlive)
   {
      int64 compuTimer(currentKernelStatus[threadId].numIterations);
      int sizeA(6);
      int *cfgA = new int[sizeA];
      permuteFromLehmerCode(cfgA, sizeA,
                     currentKernelStatus[threadId].currentOptCfgA);
  	   printf("AnnKinitData: Tid %d, CfgA %d, Iter %d.\n", threadId,
		            currentKernelStatus[threadId].currentOptCfgA,
                  compuTimer);
      for(int i = 0; i < 500; ++i)
      {
         currentKernelStatus[threadId].numIterations += 1;
         compuTimer++;
      }
      printf("AnnKNewData: Tid %d, CfgA %d , Iter %d.\n", threadId, 
                   currentKernelStatus[threadId].currentOptCfgA, compuTimer);
      currentKernelStatus[threadId].currentOptH -= 12;
   }
}

__global__ void globalUpdate(scKernelStatus *currentKernelStatus)
{
   int threadId = getGlobalThreadIdx();
   printf("GlobUpdKernData: Tid %d, CfgA %d, Iter %d, OptH %g\n",
      threadId, currentKernelStatus[threadId].currentOptCfgA,
      currentKernelStatus[threadId].numIterations,
      currentKernelStatus[threadId].currentOptH);
}
int main (int argc, char** argv)
{
   int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	CHECK(cudaSetDevice(dev));

   scKernelStatus *kernelStatus1;
   CHECK(cudaMalloc((scKernelStatus **)&kernelStatus1, NUM_THREADS*sizeof(scKernelStatus)));

   dim3 grid(GDIMX, GDIMY, GDIMZ);
   dim3 block(BDIMX, BDIMY, BDIMZ);

   initializeKernel<<<grid, block>>> (kernelStatus1, 6, static_cast<int64>(12345));   
   CHECK(cudaDeviceSynchronize());
   for (int i = 0; i < 11; ++i)
   {
      annealKernel<<<grid, block>>> (kernelStatus1);
      CHECK(cudaDeviceSynchronize());
      globalUpdate<<<grid, block>>> (kernelStatus1);
   }
   CHECK(cudaDeviceSynchronize());
   CHECK(cudaFree(kernelStatus1));
}
