%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%

% Maturities and rates for observed market swaps

MKTSWP.Mt   = [0 5     10    15    20    25    30   ];
MKTSWP.R = [0 3.249 4.074 4.463 4.675 4.775 4.811]*.01;

% Risk profiles with their intensities and probabilities

% In this risk profile we assume that the first period starts at t = 0. This is
% a qualified assumption since the survival probability is 1 at that time. Alter-
% natively we can assume a later time which then would be a matter of adding a
% discount factor. The year differences are specified in the column vector 'dT'. 
% The first column in LR (Low Risk), MR (Medium Risk) and HR (Human Resources)
% contains the intensities and the second column contains the probabilities.

% Since the probabilities already are computed we can use them directly without
% further computations.

LR.lambda = [  .0036 .0036 .0065 .0099 .0111 .0177 .0177 .0177 .0177 .0177]';
LR.prob   = [ 1      .9964 .9834 .9638 .9424 .8931 .8164 .7463 .6822 .6236]';
LR.dT     = [ 0     1     2     2     2     3     5     5     5     5     ]';

MR.lambda = [  .0202 .0202 .0231 .0266 .0278 .0349 .0349 .0349 .0349 .0349]';
MR.prob   = [ 1      .9796 .9348 .8857 .8371 .7527 .6305 .5280 .4423 .3705]';
MR.dT     = [ 0     1     2     2     2     3     5     5     5     5     ]';

HR.lambda = [  .0534 .0534 .0564 .0600 .0614 .0696 .0696 .0696 .0696 .0696]';
HR.prob   = [ 1      .9470 .8447 .7478 .6603 .5342 .3753 .2636 .1851 .1301]';
HR.dT     = [ 0     1     2     2     2     3     5     5     5     5     ]';

sigmaBS = .17;


DPPLR = dpcalcfun(LR, MKTSWP, sigmaBS);
DPPMR = dpcalcfun(MR, MKTSWP, sigmaBS);
DPPHR = dpcalcfun(HR, MKTSWP, sigmaBS);

fprintf('My calculations of a payer swap using the Low, Medium and High risk\nscenarios\n\n')
fprintf('Calculations of anticipated payer swaps using pure table values\n(columns: Maturities, LR, MR, HR):\n')

[MKTSWP.Mt(2:end)' DPPLR.antpure DPPMR.antpure DPPHR.antpure]

fprintf('Calculations of anticipated payer swaps using fully interpolated interest\nrates and manually calculated default probabilities:\n')

[MKTSWP.Mt(2:end)' DPPLR.antinterp DPPMR.antinterp DPPHR.antinterp]

fprintf('The same calculations but on postponed accruals:\n')

[MKTSWP.Mt(2:end)' DPPLR.postinterp DPPMR.postinterp DPPHR.postinterp]

fprintf('Calculations of anticipated payer swaps assuming piecewise constant interest\nrates and manually calculated default probabilities:\n')

[MKTSWP.Mt(2:end)' DPPLR.antstep DPPMR.antstep DPPHR.antstep]

fprintf('The same calculations but on postponed accruals:\n')

[MKTSWP.Mt(2:end)' DPPLR.poststep DPPMR.poststep DPPHR.poststep]

x_i = 0:.1:1;
DR.prob = LR.prob;
DR.dT = LR.dT;
DPMESH = zeros(length(MKTSWP.Mt)-1,length(x_i));
for i = 1:length(x_i)
    DR.lambda = (HR.lambda - LR.lambda)*x_i(i) + LR.lambda;
	DPPDR = dpcalcfun(DR, MKTSWP, sigmaBS);
	DPMESH(:,i) = DPPDR.antinterp;
end


% Just a mockup of how the mesh plotter works:
% mesh(1:3, MKTSWP.Mt(2:end), [DPPLR.antinterp DPPMR.antinterp DPPHR.antinterp ]*10000);

FIGS.fig1 = figure('Color',[1 1 1]);
surf(x_i, MKTSWP.Mt(2:end), DPMESH*1e4);
surface(x_i, MKTSWP.Mt(2:end), DPMESH*1e4);
alpha .5
shading interp
material metal
lighting phong
camlight('right')

% Note that the big font size is chosen so that the image can be scaled down
% so as to compensate for the resolution limitations in the pixel based
% z-buffer plot (only z-buffer can render 3D images with lighting effects
% and it cannot make vector based images, at least not in Matlab 2010a).

zlabel('\fontsize{16}Premium (Basis points)')
ylabel('\fontsize{16}Maturity(years)')
xlabel('\fontsize{16}Risk profile')
title('\fontsize{16}Default premium for different risk profiles')

set(gca,'XTick',[0 .5 1])
set(gca,'XTickLabel',{'low', 'medium', 'high'});

% (1) A comparative graph to show how postponed vs anticipated affect the CVA
% and (2) a comparative graph to show interpolated vs stepwise interest rate
% would ba a good thing to add.
% (3) The three risk profiles for anticipated and postponed would be good to add
%

