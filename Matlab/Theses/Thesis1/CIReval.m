%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelsh�gskolan, G�teborg
% April, 2011
%
% CIR evaluation test
%

y_0 = .0165;
kappa = .4;
mu_t = .026;
nu = .15;

gamma_CIR = @(alpha, sigma)(sqrt(alpha.^2+2.*sigma.^2));
CIR_A = @(T, gamma, alpha, mu, nu)(log(2.*gamma.*exp((gamma+alpha).*T.*.5)./((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma)).*2.*alpha.*mu./nu.^2);
CIR_B = @(T, gamma, alpha)(2.*(exp(gamma.*T)-1)./ ((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma));

P_CIR = @(t, kappa, nu, mu, y_0)(exp(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)-CIR_B(t, gamma_CIR(kappa, nu), kappa)).*y_0);

