function mutatedChromosome = Mutate(chromosome, mutationProbability)
  nGenes = length(chromosome);
  mutatedChromosome = chromosome;
  if rand < mutationProbability;
    swap = randi(nGenes, 1, 2);
    mutatedChromosome([swap(1) swap(2)]) = ...
           mutatedChromosome([swap(2) swap(1)]);
  end
end