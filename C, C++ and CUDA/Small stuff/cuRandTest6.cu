#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <curand_kernel.h>
#include <time.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

#define BDIMX 32
#define BDIMY 32

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}


/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/

__global__ void generate_kernel(curandStateMRG32k3a *state,
                                int n, 
                                unsigned int *result)
{
    int id = threadIdx.x + blockIdx.x * 64;
    unsigned int count = 0;
    unsigned int x;
    /* Copy state to local memory for efficiency */
    curandStateMRG32k3a localState = state[id];
    /* Generate pseudo-random unsigned ints */
    for(int i = 0; i < n; i++) {
        x = curand(&localState);
        /* Check if low bit set */
        if(x & 1) {
            count++;
        }
    }
    /* Copy state back to global memory */
    state[id] = localState;
    /* Store results */
    result[id] += count;
}

/*
   The cuRAND MTGP32 random number generator doesn't seem to be possible to
   initialize from the device. If the Mersenne Twister is to be used, it must
   be initialized on the host and then parsed into the kernel.
   
   The default generator is the XORWOW generator.
   
   There is also a generator called Philox generator, and there is the Sobol'
   generator which generates quasirandom sequences.
   
   You also have the MRG32k3a generator which is a multiple recursive 32-bit
   generator of order 3.
   
   https://software.intel.com/en-us/node/590402
   
*/

__global__ void randInts1(int *output, unsigned int n, unsigned maxVal,
                          int seed, int sequence, int offset)
{
   curandStateMRG32k3a cState;
   curand_init(seed, sequence, offset, &cState);
   int localVal;
   
   //printf("CUDA kernel - using offset %d: ", offset);
   for(int i=0;i<n;++i)
   {
      localVal = curand(&cState) % maxVal;
      //printf(" %d ", localVal);
      output[i] = localVal;
   }
   //printf("\n");
}

__global__ void randFlts1(float *output, uint64 n, int seed, int sequence)
{
   curandState cState;
   curand_init(seed, sequence, 0, &cState);
   for(int i=0;i<n;++i)
      output[i] = curand_uniform(&cState);
}
__global__ void dummyKernel(int *output, unsigned int n)

{
printf("Test sequence 1: ");
   for(int i=0;i<n;++i)
   {
      printf(" %d ", i);
      output[i] = i;
   }
   printf("\n");

}

int main(int argc, char **argv)
{
   colors_t colors;
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));
   
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", for random number generation"; colors.setFg(hGray);
   std::cout << std::endl << std::endl;
   
   /* Set up our vectors and allocate memory for these vectors*/
   int numElem = 20;
   int offset0 = 0;
   int offset1 = 1;
   int sequence0 = 1;
   int sequence1 = 2;
   int sequence2 = 0;
   int currentSeed = static_cast<int>(cpuSecond());
   
   int *dVec1, *dVec2, *dVec3, *dVec4, *dVec5;
   float *dVec6;
   CHECK(cudaMalloc((int **)&dVec1, numElem*sizeof(int)));
   CHECK(cudaMalloc((int **)&dVec2, numElem*sizeof(int)));
   CHECK(cudaMalloc((int **)&dVec3, numElem*sizeof(int)));
   CHECK(cudaMalloc((int **)&dVec4, numElem*sizeof(int)));
   CHECK(cudaMalloc((int **)&dVec5, numElem*sizeof(int)));
   CHECK(cudaMalloc((float **)&dVec6, numElem*sizeof(float)));
   int *hVec1 = new int[numElem];
   int *hVec2 = new int[numElem];
   int *hVec3 = new int[numElem];
   int *hVec4 = new int[numElem];
   int *hVec5 = new int[numElem];
   float *hVec6 = new float[numElem];
   
   /* A couple of runs with the kernel */
   randInts1<<<1, 1>>> (dVec1, numElem, 20, 0, 0, offset0);
   randInts1<<<1, 1>>> (dVec2, numElem, 20, 0, 0, offset1);
   randInts1<<<1, 1>>> (dVec3, numElem, 20, sequence1, sequence0, offset1);
   randInts1<<<1, 1>>> (dVec4, numElem, 20, 0, sequence1, offset1);
   randInts1<<<1, 1>>> (dVec5, numElem, 20, currentSeed, sequence1, offset1);
   // randFlts1(float *output, uint64 n, int seed, int sequence)
   randFlts1<<<1, 1>>>(dVec6, (int64) numElem, 0, sequence2);
   //dummyKernel<<<1, 1>>> (dVec3, numElem);
   /* Transfer the data to host */
   CHECK(cudaMemcpy(hVec1, dVec1, numElem*sizeof(int), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(hVec2, dVec2, numElem*sizeof(int), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(hVec3, dVec3, numElem*sizeof(int), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(hVec4, dVec4, numElem*sizeof(int), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(hVec5, dVec5, numElem*sizeof(int), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(hVec6, dVec6, numElem*sizeof(float), cudaMemcpyDeviceToHost));
  
   colors.setFg(hGray); std::cout << "Offset    ";
   colors.setFg(hMagenta); std::cout << std::setw(4) << offset0;
   colors.setFg(White); std::cout << ": ";
   for(int i=0; i<numElem; ++i)
   {
      if(colors.getFg()==dCyan) colors.setFg(hCyan);
      else colors.setFg(dCyan);  
      std::cout << std::setw(2) << hVec1[i] << " ";
   }
   colors.setFg(hGray); std::cout << std::endl;
   
   colors.setFg(dGray); std::cout << "Offset    ";
   colors.setFg(dMagenta); std::cout << std::setw(4) << offset1;
   colors.setFg(White); std::cout << ": ";
   for(int i=0; i<numElem; ++i)
   {
      if(colors.getFg()==dCyan) colors.setFg(hCyan);
      else colors.setFg(dCyan);  
      std::cout << std::setw(2) << hVec2[i] << " ";
   }
   colors.setFg(hGray); std::cout << std::endl;

   colors.setFg(hGray); std::cout << "Sequence  ";
   colors.setFg(hMagenta); std::cout << std::setw(4) <<sequence0;
   colors.setFg(White); std::cout << ": ";
   for(int i=0; i<numElem; ++i)
   {
      if(colors.getFg()==dCyan) colors.setFg(hCyan);
      else colors.setFg(dCyan);  
      std::cout << std::setw(2) << hVec3[i] << " ";
   }
   colors.setFg(hGray); std::cout << std::endl;

   colors.setFg(dGray); std::cout << "Sequence  ";
   colors.setFg(dMagenta); std::cout << std::setw(4) << sequence1;
   colors.setFg(White); std::cout << ": ";
   for(int i=0; i<numElem; ++i)
   {
      if(colors.getFg()==dCyan) colors.setFg(hCyan);
      else colors.setFg(dCyan);  
      std::cout << std::setw(2) << hVec4[i] << " ";
   }
   colors.setFg(hGray); std::cout << std::endl;

   colors.setFg(hGray); std::cout << "Trurandom ";
   colors.setFg(hMagenta); std::cout << std::setw(4) << (currentSeed & 0xFFF);
   colors.setFg(White); std::cout << ": ";
   for(int i=0; i<numElem; ++i)
   {
      if(colors.getFg()==dCyan) colors.setFg(hCyan);
      else colors.setFg(dCyan);  
      std::cout << std::setw(2) << hVec5[i] << " ";
   }
   colors.setFg(hGray); std::cout << std::endl;

   colors.setFg(dGray); std::cout << "Uniform   ";
   colors.setFg(dMagenta); std::cout << std::setw(4) << sequence2;
   colors.setFg(White); std::cout << ": ";
   for(int i=0; i<numElem; ++i)
   {
      if(colors.getFg()==dCyan) colors.setFg(hCyan);
      else colors.setFg(dCyan);  
      std::cout << hVec6[i] << " ";
   }
   colors.setFg(hGray); std::cout << std::endl;

   
   //colors.setFg(hGray); std::cout << "Host - dummy ";
   //colors.setFg(White); std::cout << ": ";
   //for(int i=0; i<numElem; ++i)
   //{
   //   if(colors.getFg()==dCyan) colors.setFg(hCyan);
   //   else colors.setFg(dCyan);  
   //   std::cout << hVec3[i] << " ";
   //}
   //colors.setFg(hGray); std::cout << std::endl;

   
   /* Clean up and reset device */
   CHECK(cudaFree(dVec1));
   CHECK(cudaFree(dVec2));
   CHECK(cudaFree(dVec3));
   CHECK(cudaFree(dVec4));
   CHECK(cudaFree(dVec5));
   delete[] hVec1, hVec2, hVec3, hVec4, hVec5;
   
   CHECK(cudaDeviceReset());
}

/* Let's implement a proper struct
*/

struct scEnzConfig 
{
   int *vEnzA;
   int *vEnzB;
   int *vDigest;
   int iSizeA;
   int iSizeB;
   int iSizeDigest;
   float fAlpha;
   float fBeta;
};

scEnzConfig myConfig = {0, 0, 0, sizeA, sizeB, sizeDigest, alpha, beta};
myConfig.vEnzA = new int[sizeA];
myConfig.vEnzB = new int[sizeB];
myConfig.vDigest = new int[sizeDigest];

int sizeofMyStruct = sizeof(struct scEnzConfig +
            (myConfig.iSizeA + myConfig.iSizeB + myConfig.iSizeDigest)*sizeof(int)
             + 2*sizeof(float));


struct dyn_array { 
    int size;
    int data[];
};

struct dyn_array my_array = malloc(sizeof(struct dyn_array) + 100 * sizeof(int));

















