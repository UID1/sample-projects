function [xBest maximumFitness] = OptimizationFunction(Params)

  populationSize = Params.populationSize;
  numberOfGenes = Params.numberOfGenes;
  crossoverProbability = Params.crossoverProbability;
  mutationProbability = Params.mutationProbability;
  tournamentSize = Params.tournamentSize;
  tournamentSelectionParameter = Params.tournamentSelectionParameter;
  variableRange = Params.variableRange;
  numberOfGenerations = Params.numberOfGenerations;
  numberOfVariablesToDecode = Params.numberOfVariablesToDecode;
  fitness = zeros(populationSize, 1);
  decodedPopulation = zeros(populationSize, 2);
  population = InitializePopulation(populationSize, numberOfGenes);
  
  for iGeneration = 1:numberOfGenerations
  
      maximumFitness = .0; % Assumes non-negative fitness values!
      xBest = zeros(1,2); % [0 0]
      bestIndividualIndex = 0;
      
      for i = 1:populationSize
          chromosome = population(i,:);
          x = DecodeChromosome(chromosome, numberOfVariablesToDecode, variableRange);
          decodedPopulation(i,:) = x; % The new statement
          fitness(i) = EvaluateIndividual(x);
          if (fitness(i) > maximumFitness)
              maximumFitness = fitness(i);
              bestIndividualIndex = i;
              xBest = x;
          end
      end
  
      bestChromosome = population(bestIndividualIndex, :);
      tempPopulation = InsertBestIndividual(population, ...
                            bestChromosome, insertNumberOfCopies);
       
      for i = 1:2:populationSize
          i1 = TournamentSelect(fitness, tournamentSize, tournamentSelectionParameter);
          i2 = TournamentSelect(fitness, tournamentSize, tournamentSelectionParameter);
          chromosome1 = population(i1, :);
          chromosome2 = population(i2, :);
  
          r = rand;
          if(r < crossoverProbability)
              newChromosomePair = Cross(chromosome1, chromosome2);
              tempPopulation(i,:) = newChromosomePair(1,:);
              tempPopulation(i+1,:) = newChromosomePair(2,:);
          else
              tempPopulation(i,:) = chromosome1;
              tempPopulation(i+1,:) = chromosome2;
          end
      end % Loop over population
  
      for i = 1:populationSize
          originalChromosome = tempPopulation(i,:);
          mutatedChromosome = Mutate(originalChromosome, mutationProbability);
          tempPopulation(i,:) = mutatedChromosome;
      end
      
      tempPopulation(1,:) = population(bestIndividualIndex,:);
      population = tempPopulation;
  end
end