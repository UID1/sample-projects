%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%
% edited using vim (vim.org) over ssh (openssh.org)
%

% The basic strategy of calculating the Default Part (DP) is to treat the
% net present value of the swap agreement as a swaption. This means that
% the swap agreement as a European call option. The strike price is the
% fair rate that is established through equation (4.2) in Brigo, Masetti
% (2005) at the start of the agreement. The underlying "stock price" is
% then the fair price at time t after the beginning of agreement but before
% time of maturity. The maturity is then T_end-t which is the time period
% from time t to the end of the swap agreement.
%
% Assuming that the IRS agreement has the same development pattern as a
% regular stock followin a regular Wiener process may be a bit too over
% simplifying. Brigo, Mercurio (2008) presents a closed form expression
% for a call on a security that follows a CIR process which is defined in
% equation (3.26). I have also written an implementation of it in the
% criskclosed.m file.
%

t = 0:.5:5;
DP_t = zeros(1,length(t));

h = waitbar(0, 'Calculating default parts ...');
for i=1:length(t)
    waitbar(i/length(t));
    DP_t(i) = dpcalc(t(i), 0, 5, 20, .17);
end
close(h);
plot(t,DP_t)



