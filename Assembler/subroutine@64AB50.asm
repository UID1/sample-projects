0064AB50 ; Attributes: bp-based frame                                                                                          
0064AB50                                                                                                                       
0064AB50 sub_64AB50      proc near               ; CODE XREF: sub_64AD00+62p                                                  
0064AB50                                                                                                                       
0064AB50 var_8           = dword ptr -8                                                                                        
0064AB50 var_4           = dword ptr -4                                                                                        
0064AB50 arg_0           = dword ptr  8                                                                                        
0064AB50 arg_4           = dword ptr  0Ch                                                                                      
0064AB50                                                                                                                       
0064AB50                 push    ebp             ; \                                                                           
0064AB50                                         ;   Stdcall                                                                   
0064AB51                 mov     ebp, esp        ; /                                                                           
0064AB53                 sub     esp, 8                                                                                        
0064AB56                 mov     eax, [ebp+arg_4] ; eax => #S/N                                                                
0064AB59                 push    ebx             ; Push => SC#L/K                                                              
0064AB5A                 push    esi                                                                                           
0064AB5B                 push    edi             ; Push => #S/N                                                                
0064AB5C                 push    eax                                                                                           
0064AB5D                 push    ecx             ; Push SC>#L/K                                                                
0064AB5E                 lea     edx, [ebp+var_8] ; edx => &var_8                                                              
0064AB61                 push    edx                                                                                           
0064AB62                 call    sub_64AA30      ; sub_64AA30(& var_8, cx, arg_4) "shake'n'scramble"                           
0064AB62                                         ; &var_8 => B50000h = 11h ...                                                 
0064AB62                                         ; cx => SC >#L/K                                                              
0064AB62                                         ; arg_4 => #S/N                                                               
0064AB62                                         ; This routine produces the result into &var_8 (and &var_4)                   
0064AB67                 add     esp, 0Ch        ; Move down the Stack Pointer                                                 
0064AB6A                 xor     eax, eax        ; \                                                                           
0064AB6C                 xor     ebx, ebx        ; |                                                                           
0064AB6C                                         ;  > We clear eax-edx                                                         
0064AB6E                 xor     ecx, ecx        ; |                                                                           
0064AB70                 xor     edx, edx        ; /                                                                           
0064AB72                 mov     esi, [ebp+var_8] ; esi => another version of the SC >#L/K                                     
0064AB75                 lea     edi, ds:674580h                                                                               
0064AB7B                                                                                                                       
0064AB7B loc_64AB7B:                             ; CODE XREF: sub_64AB50+3Fj                                                  
0064AB7B                 shr     esi, 1          ; This loop shifts the bits out of esi that contains the                      
0064AB7B                                         ; 1st half of the scrambled >#L/K until it's empty and                        
0064AB7B                                         ; XOR some values of binary table into registers eax-edx                      
0064AB7B                                         ; as many times as there are binary ones in the 1/2:SC>#L/K                   
0064AB7D                 jnb     short loc_64AB8C                                                                              
0064AB7F                 xor     eax, [edi]                                                                                    
0064AB81                 xor     ebx, [edi+4]                                                                                  
0064AB84                 xor     ecx, [edi+8]                                                                                  
0064AB87                 xor     edx, [edi+0Ch]                                                                                
0064AB8A                 test    esi, esi                                                                                      
0064AB8C                                                                                                                       
0064AB8C loc_64AB8C:                             ; CODE XREF: sub_64AB50+2Dj                                                  
0064AB8C                 lea     edi, [edi+10h]                                                                                
0064AB8F                 jnz     short loc_64AB7B ; Jump as long as esi isn't empty                                            
0064AB91                 mov     esi, [ebp+var_4] ; Now, we get the other half of the scrambled >#L/K                          
0064AB94                 lea     edi, ds:674780h ; and get another binary table                                                
0064AB9A                                                                                                                       
0064AB9A loc_64AB9A:                             ; CODE XREF: sub_64AB50+5Ej                                                  
0064AB9A                 shr     esi, 1          ; This loop is doing exactly the same thing as the one above                  
0064AB9A                                         ; but with the other half of the scrambled >#L/K and a                        
0064AB9A                                         ; different binary table                                                      
0064AB9C                 jnb     short loc_64ABAB                                                                              
0064AB9E                 xor     eax, [edi]                                                                                    
0064ABA0                 xor     ebx, [edi+4]                                                                                  
0064ABA3                 xor     ecx, [edi+8]                                                                                  
0064ABA6                 xor     edx, [edi+0Ch]                                                                                
0064ABA9                 test    esi, esi                                                                                      
0064ABAB                                                                                                                       
0064ABAB loc_64ABAB:                             ; CODE XREF: sub_64AB50+4Cj                                                  
0064ABAB                 lea     edi, [edi+10h]                                                                                
0064ABAE                 jnz     short loc_64AB9A                                                                              
0064ABB0                 xor     esi, esi                                                                                      
0064ABB2                 xor     edi, edi                                                                                      
0064ABB4                                                                                                                       
0064ABB4 loc_64ABB4:                             ; CODE XREF: sub_64AB50+80j                                                  
0064ABB4                                         ; sub_64AB50+8Dj                                                             
0064ABB4                 bt      edx, 4          ; Copy 4th bit of edx into Carry Flag                                         
0064ABB4                                         ; This loop is repeated until ecx and                                         
0064ABB4                                         ; ecs equal zero                                                              
0064ABB8                 rcl     ecx, 4          ; Rotate ecx 4 steps left through the Carry Flag                              
0064ABBB                 jnb     short loc_64ABDF ; Jump if CF turns zero after rotation 0                                     
0064ABBD                 xor     esi, eax        ; XOR eax into esi and                                                        
0064ABBF                 xor     edi, ebx        ; XOR ebx into edi                                                            
0064ABC1                                                                                                                       
0064ABC1 loc_64ABC1:                             ; CODE XREF: sub_64AB50+91j                                                  
0064ABC1                                         ; sub_64AB50+95j                                                             
0064ABC1                 rcr     dx, 5           ; Rotate 5 steps right through CF                                             
0064ABC5                 bt      ebx, 0Bh        ; Copy 11th bit to CF (last time it was the 27th)                             
0064ABC9                 rcr     eax, 4          ; Shake                                                                       
0064ABCC                 rcl     bx, 5           ; and shuffle                                                                 
0064ABD0                 jnb     short loc_64ABB4 ; Jump back if CF=0                                                          
0064ABD2                 xor     eax, 400200Ch   ; otherwise XOR these two values into eax                                     
0064ABD7                 xor     ebx, 1000h      ; and ebx respectively                                                        
0064ABDD                 jmp     short loc_64ABB4                                                                              
0064ABDF ; ---------------------------------------------------------------------------                                         
0064ABDF                                                                                                                       
0064ABDF loc_64ABDF:                             ; CODE XREF: sub_64AB50+6Bj                                                  
0064ABDF                 test    edx, edx                                                                                      
0064ABE1                 jnz     short loc_64ABC1 ; jump as long as edx is nonzero                                             
0064ABE3                 test    ecx, ecx                                                                                      
0064ABE5                 jnz     short loc_64ABC1 ; jump as long as ecx is nonzero                                             
0064ABE5                                         ; We end up with eax=A2DB3CF4, ebx=0000FCAF                                   
0064ABE5                                         ; esi = FCE7A2F9 and edi = 00004439                                           
0064ABE7                 mov     [ebp+var_8], esi ; \                                                                          
0064ABE7                                         ;   Get the computed data in esi, edi which contains                          
0064ABE7                                         ;   a multiscrambled version of >#L/K                                         
0064ABEA                 mov     [ebp+var_4], edi ; /                                                                          
0064ABED                 mov     eax, [ebp+arg_4] ; \                                                                          
0064ABF0                 mov     edx, [ebp+arg_0] ; |                                                                          
0064ABF3                 push    eax             ; |                                                                           
0064ABF3                                         ;  > Parsing arguments into the call below (see call comment for further info)
0064ABF4                 lea     ecx, [ebp+var_8] ; |                                                                          
0064ABF7                 push    ecx             ; |                                                                           
0064ABF8                 push    edx             ; /                                                                           
0064ABF9                 call    sub_64AA30      ; ax = sub_64AA30(arg_0, & var_8, arg_4) shake'n'scramble                     
0064ABF9                                         ; arg_4 => #S/N                                                               
0064ABF9                                         ; var_8 (and var_4) = multiscrambled >#L/K                                    
0064ABF9                                         ; arg_0 => another Stack Frame where another pair of var_4                    
0064ABF9                                         ;    and var_8 contains the scrambled values generated by                     
0064ABF9                                         ;    this subroutine                                                          
0064ABFE                 add     esp, 0Ch                                                                                      
0064AC01                 pop     edi             ; \                                                                           
0064AC02                 pop     esi             ; |                                                                           
0064AC03                 pop     ebx             ;  > Stdcall                                                                  
0064AC04                 mov     esp, ebp        ; |                                                                           
0064AC06                 pop     ebp             ; /                                                                           
0064AC07                 retn                                                                                                  
0064AC07 sub_64AB50      endp                                                                                                  