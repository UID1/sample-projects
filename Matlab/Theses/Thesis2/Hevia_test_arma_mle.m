% test arma_mle function

% Simulate ARMA(p,q) process:
% y[t] = phi(1)*y[t-1] + ... + phi(p)*y[t-p] + 
%        + eps[t] + theta(1)eps[t-1] +...+theta(q)eps[t-q]
%

clear; close all;

% THEORETICAL ARMA MODEL (Must be stationary)
phi = [ 0.5  ];
theta = [ 0.4 .24 ];
p = length(phi);
q = length(theta);

sigma = 0.1;        % Standard deviation error term

T = 75;    % Number of periods
n = 20;     % Number of periods to drop

Nmontecarlo = 1000;  % Number of montecarlo experiments

ARcoef = zeros(Nmontecarlo,p);
MAcoef = zeros(Nmontecarlo,q);
SIGMA  = zeros(Nmontecarlo,1);

disp('Montecarlo iteration')
for j = 1:Nmontecarlo
    disp(j)
    y           = filter([1 theta],[1 -phi], sigma*randn(T+n,1) );
    y           = y(n+1:T+n);
    results     = arma_mle(y,p,q);   % ESTIMATE ARMA MODEL
    ARcoef(j,:) = results.ar;
    MAcoef(j,:) = results.ma;
    SIGMA(j)    = results.sigma;
end
    
    
% Plot histograms
for j=1:p
    figure
    hist(ARcoef(:,j),Nmontecarlo/20)
    title(['Histogram AR coefficient ',num2str(j)])
end
for j=1:q
    figure
    hist(MAcoef(:,j),Nmontecarlo/20)
    title(['Histogram MA coefficient ',num2str(j)])
end
figure
hist(SIGMA,Nmontecarlo/20)
title('Histogram Standard Deviation')

