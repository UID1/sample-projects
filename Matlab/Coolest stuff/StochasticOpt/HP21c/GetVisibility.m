function visibilities = GetVisibility(cityLocations)
  cLex = exp(cityLocations(:,1));
  cLey = exp(cityLocations(:,2));
  visibilities = (log(cLex * cLex'.^-1).^2 + log(cLey * cLey'.^-1).^2).^-.5;
  visibilities(logical(eye(length(cityLocations)))) = 0; % dij=0 <=> infinite visibility :(
end


% Alternative way of computing visibilities:
% function visibilities = GetVisibility(cityLocations)
%   n = length(cityLocations);
%   visibilities = zeros(n);
%   for i = 2:n
%     edgeij = cityLocations(i:end,:)-meshgrid(cityLocations(i-1,:), i:n);
%     edgeiDistances = sqrt(sum(edgeij.^2,2))';
%     edgeiVisibilities = edgeiDistances.^-1;
%     visibilities(i-1,i:end) = edgeiVisibilities;
%     visibilities(i:end, i-1) = edgeiVisibilities';
%   end
% end


