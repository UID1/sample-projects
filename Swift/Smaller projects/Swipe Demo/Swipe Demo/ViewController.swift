//
//  ViewController.swift
//  Swipe Demo
//
//  Created by Iberius Pred on 04/03/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // This creates a label without the help from the storyboard
        let label = UILabel(frame: CGRect(x: self.view.bounds.width / 2 - 100, y: self.view.bounds.height / 2 - 50, width: 200, height: 100))
        label.text = "Drag Me!"
        label.textAlignment = .center
        // This adds the label to the subview
        view.addSubview(label)
        
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(gesture)
        
    }
    
    @objc private func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: view)
        let label = gestureRecognizer.view!
        label.center = CGPoint(x: self.view.bounds.width / 2 + translation.x, y: self.view.bounds.height / 2 + translation.y)
        let xFromCenter = label.center.x - self.view.bounds.width / 2
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200)
        let scale = min(100 / abs(xFromCenter), 1)
        var stretchandrotation = rotation.scaledBy(x: scale, y: scale)
        label.transform = stretchandrotation
        // The gesture recognizer is ".ended" it means that the user released the drag with his finger
        if gestureRecognizer.state == UIGestureRecognizerState.ended {
            if label.center.x < 100 {
                print("Not chosen")
            } else if label.center.x > self.view.bounds.width - 100 {
                print("Chosen")
            }
            rotation = CGAffineTransform(rotationAngle: 0)
            stretchandrotation = rotation.scaledBy(x: 1, y: 1)
            label.transform = stretchandrotation
            label.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

