//
//  ViewController.swift
//  Maps
//
//  Created by Iberius Pred on 26/02/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Set up the view and location of the map
        let latitude: CLLocationDegrees = 40.7
        let longitude: CLLocationDegrees = -73.9
        let latDelta: CLLocationDegrees = 0.05
        let lonDelta: CLLocationDegrees = 0.05
        
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let region: MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        
        map.setRegion(region, animated: true)
        
        // Set up an annotation (A pin on the map with a simple text description)
        let annotation = MKPointAnnotation()
        annotation.title = "New York"
        annotation.subtitle = "One day I'll go here ..."
        annotation.coordinate = location
        
        map.addAnnotation(annotation)
        
        // Set up a long pressure recognizer that sets an annotation at the
        // coordinates of the pressed point.
        // The "gestureRecognizer:" argument makes the coordinates be parsed into the
        // function called ("longpress") to set up the annotation.
        let uilpgr = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.longpress(gestureRecognizer:)))
        uilpgr.minimumPressDuration = 2
        map.addGestureRecognizer(uilpgr)
    }
    @objc private func longpress(gestureRecognizer: UILongPressGestureRecognizer) {
        let touchPoint = gestureRecognizer.location(in: self.map)
        let coordinate = map.convert(touchPoint, toCoordinateFrom: self.map)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = "New Place"
        annotation.subtitle = "Maybe I'll go here too ..."
        map.addAnnotation(annotation)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

