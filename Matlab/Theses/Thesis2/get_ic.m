%
% This function calculates the information criteria for different models
%
%
%

function IC = get_is(X_t, Var_mod, p)

N = length(X_t);

IC.AIC  = N*log(Var_mod)+2*p;
IC.AIC2 = log(Var_mod)+2*p;
IC.BIC  = log(Var_mod)+p*log(N)/N;
IC.HQIC = log(Var_mod)+2*p*log(log(N))/N;
IC.FPE  = Var_mod*(N+p)/(N-p);
IC.EBIC = (N-p)*log(N*Var_mod/(N-p))+N*(1+log((2*pi)*.5))+p*log(p^-1*norm(X_t)^2-N*Var_mod);
end % get_ic
