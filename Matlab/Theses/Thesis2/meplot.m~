function [sme,out]=meplot(data,omit),
%Plots sample mean excesses over increasing thresholds
%
%USAGE: [sme,out]=meplot(data,omit)
%
% data: Data vector
% omit: Number of largest values to be omitted. Default is 3.
%
%  out: Increasing thresholds vector for reproducing the plot (x-axis data).
%  sme: Sample mean excesses over thresholds for reproducing the plot (y-axis data).
%
warning off
if nargin==1,
   omit=3;
end

out=sort(data);
n=length(out);

for i=1:n;
    sme(i)=mean(data(data>out(i)))-out(i);
end
out=out(1:end-omit);
sme=sme(1:end-omit)';
FIGS.fig1 = figure('Color',[1 1 1]);
plot(out,sme,'.', 'MarkerFaceColor', 'k')
xlabel('\fontsize{12}Threshold');
ylabel('\fontsize{12}Mean Excess');
warning on
