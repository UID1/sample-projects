/**
* Copyright (c) 2015-present, Parse, LLC.
* All rights reserved.
*
* This source code is licensed under the BSD-style license found in the
* LICENSE file in the root directory of this source tree. An additional grant
* of patent rights can be found in the PATENTS file in the same directory.
*/

import UIKit
import Parse

class UberLoginSignupViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var submitButtonTitle: UIButton!
    @IBOutlet weak var uberRoleSwitch: UISwitch!
    @IBOutlet weak var uberRoleStackView: UIStackView!
    @IBOutlet weak var changeStateButtonTitle: UIButton!
    @IBOutlet weak var stateMessageLabel: UILabel!
    @IBOutlet weak var changeStateButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var repeatPassConstraint: NSLayoutConstraint!
    @IBOutlet weak var uberRoleStackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var roleSwitchStackView: UIStackView!
    var isLogin = false
    var activityIndicator = UIActivityIndicatorView()
    var currentUserIsDriver = false
    
    @IBAction func changeSubmissionState(_ sender: Any) {
        self.view.endEditing(true)
        if isLogin {
            setSignUpState()
        } else {
            setLoginState()
        }
    }

    // Change the UI to a login interface
    private func setLoginState() {
        isLogin = true
        stateMessageLabel.text = "Don't have an account?"
        repeatPasswordTextField.isHidden = true
        changeStateButtonTitle.setTitle("Sign Up", for: [])
        submitButtonTitle.setTitle("Log in", for: [])
        self.view.layoutIfNeeded()
        self.roleSwitchStackView.isHidden = true
        self.uberRoleSwitch.isHidden = true
        UIView.animate(withDuration: 0.1, animations: {
            self.repeatPassConstraint.constant = 0
            self.uberRoleStackViewHeightConstraint.constant = 0
            self.changeStateButtonWidth.constant = 56
            self.view.layoutIfNeeded()
        })
    }
    // Change the UI to a signup interface and clean it up
    private func setSignUpState() {
        isLogin = false
        stateMessageLabel.text = "Already have an account?"
        repeatPasswordTextField.isHidden = false
        repeatPasswordTextField.text = ""
        changeStateButtonTitle.setTitle("Log In", for: [])
        submitButtonTitle.setTitle("Sign Up", for: [])
        self.view.layoutIfNeeded()
        self.roleSwitchStackView.isHidden = false
        UIView.animate(withDuration: 0.1, animations: {
            self.repeatPassConstraint.constant = 30
            self.uberRoleStackViewHeightConstraint.constant = 30
            self.uberRoleSwitch.isHidden = false
            self.changeStateButtonWidth.constant = 42
            self.view.layoutIfNeeded()
        })
    }
    // When activated, it sets the app in a 'busy' state
    private func setFreezeView(activated: Bool) {
        if activated {
            self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            self.activityIndicator.center = self.view.center
            self.activityIndicator.hidesWhenStopped = true
            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            view.addSubview(activityIndicator)
            self.activityIndicator.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
        } else {
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    // Delegate interface to deliver error messages to user
    internal func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    // Submit signup credentials to database
    private func userSignUp(username: String, password: String) {
        setFreezeView(activated: true)
        let userEntry = PFUser()
        let userACL = PFACL()
        userEntry.username = username
        userEntry.password = password
        userEntry["isDriver"] = uberRoleSwitch.isOn
        userACL.getPublicWriteAccess = true
        userACL.getPublicReadAccess = true
        userEntry.acl = userACL
        userEntry.signUpInBackground { (success, error) in
            self.setFreezeView(activated: false)
            if error != nil {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Submission failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Submission failure", message: "Please try again later.")
                }
            } else {
                print("User signed up")
                self.redirectUser()
                // Better to place the continuation here. Because the execution is asynchronous
                // The other way 'round woulf be to watch a struct with a didSet or willSet function
            }
        }
    }
    // Log user into database
    private func userLogIn(username: String, password: String) {
        self.setFreezeView(activated: true)
        PFUser.logInWithUsername(inBackground: username, password: password) { (user, error) in
            self.setFreezeView(activated: false)
            if error != nil {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Login failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Login failure", message: "Please try again later.")
                }
            } else {
                print("User logged in")
                // Better to place the continuation here. Because the execution is asynchronous
                // The other way 'round woulf be to watch a struct with a didSet or willSet function
                
                self.redirectUser()
            }
        }
    }
    // Make the keyoard close when not editing
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // Submit the login details to database
    @IBAction func submitCredentials(_ sender: Any) {
        self.view.endEditing(true)
        if let userName = userNameTextField.text, userName != "" {
            if let userPass = passwordTextField.text, userPass != "" {
                if isLogin {
                    userLogIn(username: userName, password: userPass)
                    //performSegue(withIdentifier: "goToUserDetails", sender: self)
                } else {
                    if let userRepeat = repeatPasswordTextField.text, userRepeat == userPass {
                        // Sign up user
                        userSignUp(username: userName, password: userPass)
                        redirectUser()
                    } else {
                        // Report wrong credentials
                        createAlert(title: "Signup Error", message: "Password mismatch")
                    }
                }
            } else {
                // Report invalid password entered
                createAlert(title: (isLogin ? "Login " : "Signup ") + "Error", message: "Invalid password entered")
            }
        } else {
            // Report invalid username
            createAlert(title: (isLogin ? "Login " : "Signup ") + "Error", message: "Invalid username")
        }
    }
    // Redirect the user to another view after submission
    private func redirectUser() {
        if PFUser.current()?.objectId != nil {
            print("Welcome \(PFUser.current()?.username!)!")
            if let isDriver = PFUser.current()?["isDriver"] as? Bool {
                print("User is driver: \(isDriver)")
                if isDriver {
                    performSegue(withIdentifier: "showDriverViewController", sender: self)
                } else {
                    performSegue(withIdentifier: "showRiderViewController", sender: self)
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if PFUser.current()?["isDriver"] != nil {
            redirectUser()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
