% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Assignment 4 - task 3
% by Robin Axelsson
%

FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [55, 36, 138]]/255;
Nmax = 20;
C_vNTRN = ones(Nmax, 1);
C_vNVLD = ones(Nmax, 1);
C_vNTrnMtx = ones(Nmax, 20);
C_vNVldMtx = ones(Nmax, 20);
ENTrnMtx = ones(Nmax, 20);
ENVldMtx = ones(Nmax, 20);


fprintf('Performance test of network with 1 - %d Gaussian nodes.\n', Nmax)
for N = 1:Nmax
   fprintf('Computing for N = %d ... ', N);
   uppg3krnl
   C_vNTRN(N) = gCvTRN(end,i_rwin);
   C_vNVLD(N) = gCvVLD(end,i_rwin);
   C_vNTrnMtx(N,:) = gCvTRN(end,:);
   C_vNVldMtx(N,:) = gCvVLD(end,:);
   ENTrnMtx(N,:) = gETRN(end,:);
   ENVldMtx(N,:) = gEVLD(end,:);
   fprintf('done.\n');
end

FIGS.fig1 = figure('Color', [1 1 1]);
plot(C_vNTRN, 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
xlabel('\fontsize{12}Number of Gaussian nodes');
ylabel('\fontsize{12}Avg. classification error  \itC_v\rm');
hold on;
plot(C_vNVLD, 'LineWidth', 2, 'Color', FIGS.LineColors(7, :));
legend('Training', 'Validation')
xlim([1 20])
ylim([0 1])


FIGS.fig2 = figure('Color', [1 1 1]);
fill([1:length(ENTrnMtx') length(ENTrnMtx'):-1:1], ...
     [min(ENTrnMtx/(.7*size(DATA,1))) fliplr(max(ENTrnMtx/(.7*size(DATA,1))))], ...
	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
hold on
FIGS.plot6 = plot(mean(ENTrnMtx'/(.7*size(DATA,1)))', 'LineWidth', 1, 'Color', circshift(FIGS.LineColors(1,:),[0 -1]));
FIGS.plot7 = plot(mean(ENVldMtx'/(.3*size(DATA,1)))', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
fill([1:length(ENTrnMtx') length(ENTrnMtx'):-1:1], ...
     [max(ENTrnMtx/(.7*size(DATA,1))) fliplr(max(ENVldMtx/(.3*size(DATA,1))))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
fill([1:length(ENTrnMtx') length(ENTrnMtx'):-1:1], ...
     [min(ENTrnMtx/(.7*size(DATA,1))) fliplr(min(ENVldMtx/(.3*size(DATA,1))))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
xlabel('\fontsize{12}Gaussian nodes');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
xlim([1 20])
legend([FIGS.plot6 FIGS.plot7], 'Training data', 'Validation data');


FIGS.fig3 = figure('Color', [1 1 1]);
fill([1:length(C_vNTrnMtx') length(C_vNTrnMtx'):-1:1], ...
     [min(C_vNTrnMtx) fliplr(max(C_vNTrnMtx))], ...
	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
hold on
FIGS.plot4 = plot(mean(C_vNTrnMtx)', 'LineWidth', 1, 'Color', circshift(FIGS.LineColors(1,:),[0 -1]));
FIGS.plot5 = plot(mean(C_vNVldMtx)', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
fill([1:length(C_vNTrnMtx') length(C_vNTrnMtx'):-1:1], ...
     [max(C_vNTrnMtx) fliplr(max(C_vNVldMtx))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
fill([1:length(C_vNTrnMtx') length(C_vNTrnMtx'):-1:1], ...
     [min(C_vNVldMtx) fliplr(min(C_vNTrnMtx))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
xlim([1 20])
ylim([0 1])
xlabel('\fontsize{12}Gaussian nodes');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
legend([FIGS.plot4 FIGS.plot5], 'Training data', 'Validation data');

save('BakMtrx.mat', 'C_vNTrnMtx', 'C_vNVldMtx', 'ENTrnMtx', 'ENVldMtx');

