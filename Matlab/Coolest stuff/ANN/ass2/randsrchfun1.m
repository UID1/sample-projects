% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1c Solution
% by Robin Axelsson
%
function m = randsrchfun1(a, b, c, timeupd)
   sigma_n = randperm(length(a));
   mu_m = randperm(length(b));
   lprcnt = 0;

   CT = sort([cumsum(a(sigma_n(1:(end-1)))) cumsum(b(mu_m))]);
   c_test1 = sort([CT(1) diff(CT)], 'descend'); % = "c_tilde"
   H1 = sum(((c-c_test1).^2./c));
   
   H=H1;
   m.H_vec = H1;
   time = 0;

   fprintf('Energy level: ')
   while H > 0 && H > eps(1)
      time = time + 1;
      sigma_n = randperm(length(a));
      mu_m = randperm(length(b));

      CT = sort([cumsum(a(sigma_n(1:(end-1)))) cumsum(b(mu_m))]);
      c_test2 = sort([CT(1) diff(CT)], 'descend');
      H2 = sum(((c-c_test2).^2./c));
      if H2 < H1
         H = H2;
	     sigma_opt = sigma_n;
	     mu_opt = mu_m;
	     c_test1 = c_test2;
      end
      H1 = H;
      m.H_vec = [m.H_vec H];
      if mod(time, timeupd) == 0
         fprintf(1, repmat('\b',1,lprcnt));
         lprcnt = fprintf('%d, Time step: %i', H, time);
      end
   end
   m.time = time;
   m.H = H;
   m.mu = mu_opt;
   m.sigma = sigma_opt;
   m.c = c_test1;
   fprintf('\n')
end
