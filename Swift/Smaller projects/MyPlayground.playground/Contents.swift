//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

print("Hello World")

print(str)

var myArray = [3.87, 7.1, 8.9]

myArray.remove(at: 1)

myArray

myArray.append(myArray.first! * myArray.last!)


var myMeal = [String: Decimal]()
myMeal.removeAll()
myMeal["pizza"] = 10.99
myMeal["ice cream"] = 4.99
myMeal["salad"] = 7.99
myMeal
myMeal["pizza"]

var myTotalCost: Decimal = 0
myTotalCost += myMeal["salad"]!
myTotalCost += myMeal["ice cream"]!
myTotalCost += myMeal["pizza"]!

print("The total cost of my meal is \(myTotalCost).")



let diceRoll = arc4random_uniform(6)

let someString = "Some text"

if (Int(someString) != nil) {
    print("We have a number!")
} else {
    print("String is NaN!")
}

let testOne = 35

testOne/6



class checkPrime {
    private var numberToCheck: Int?
    init () {
        numberToCheck = nil
    }
    
    public func setNumber(_ value: Int) {
        numberToCheck = value
    }
    
    public func getNumber() -> Int? {
        return numberToCheck
    }
    
    public func isPrime() -> Bool? {
        if let testValue = numberToCheck {
            for i in 2...(testValue/2) {
                if testValue % i == 0 {
                    return false
                }
            }
            return true
        }
        return nil
    }
    
}

let myChecker = checkPrime()

myChecker.setNumber(623)

myChecker.isPrime()













