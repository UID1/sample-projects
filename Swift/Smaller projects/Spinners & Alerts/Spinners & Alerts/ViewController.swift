//
//  ViewController.swift
//  Spinners & Alerts
//
//  Created by Iberius Pred on 02/03/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var activityIndicator = UIActivityIndicatorView() // A spinning wheel that shows in front of ...

    @IBAction func createAlert(_ sender: Any) {
        let alertController = UIAlertController(title: "Strangle your cat!", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            print("Button Yes pressed")
            self.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
            print("Button No pressed")
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func pauseApp(_ sender: Any) {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        // We may want to disable interactivity with the app while the spinner is spinning
        //UIApplication.shared.beginIgnoringInteractionEvents()
        // The line above isn't useful because a user interaction is needed to break it which
        // won't be possible since all interaction is ignored, i.e. we have a catch 22 situation
        // but in many cases this ignore function is commonly used in conjunction with spinners
        // to make the user wait for internal processes to finish.
    }
    @IBAction func restoreApp(_ sender: Any) {
        activityIndicator.stopAnimating()
        //UIApplication.shared.endIgnoringInteractionEvents()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

