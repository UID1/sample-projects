function tspFigure = InitializeTspPlot(cityLocation,range)

tspFigure = figure(1);
set(tspFigure,'DoubleBuffer','on');
axis([range(1),range(2),range(3),range(4)]);
title('\fontsize{12}Genetic Algorithm Optimization', 'FontWeight', 'Normal');
%axis square;
grid on;
city = [];
hold on;
for i = 1:length(cityLocation)
   text(cityLocation(i,1)+.2, cityLocation(i,2)-.2, sprintf('\\fontsize{8}%d', i));
end
   