#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <cstdio>
#include <iostream>
#include <windows.h>

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

int main(void)
{
   const int N = 18, M = 3;
   colors_t colors;
   thrust::device_vector<int> myvector(N);

   /* Let's try something elementary */
   
   thrust::transform(  thrust::make_counting_iterator(0),
                       thrust::make_counting_iterator(N),
                       thrust::make_constant_iterator(M),
                       myvector.begin(),
                       thrust::divides<int>() );

   for(int i=0; i<N; i++)
   {
      int val = myvector[i];
      colors.setFg(dMagenta); std::cout << i;
      colors.setFg(White); std::cout << ":";
      colors.setFg(dCyan); std::cout << val;
      colors.setFg(White); std::cout << ", ";
   }
   colors.setFg(hGray); std::cout << std::endl; 
   
   
   /* This example is very similar to Matlabs linspace
   */
   
   int NN = 20;

   float a     = 3.87f;
   float b     = 7.11f;

   float Dx    = (b-a)/(float)(NN-1);
   
   thrust::device_vector<float> myvector2(NN);

   thrust::transform(  thrust::make_counting_iterator(a/Dx),
                       thrust::make_counting_iterator((b+1.f)/Dx),
                       thrust::make_constant_iterator(Dx),
                       myvector2.begin(),
                       thrust::multiplies<float>());

   for(int i=0; i<NN; i++) {
      float val = myvector2[i];
      colors.setFg(dMagenta); std::cout << i;
      colors.setFg(White); std::cout << ":";
      colors.setFg(dCyan); std::cout << val;
      colors.setFg(White); std::cout << ", ";
   }
   return 0;
}