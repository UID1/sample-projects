function output = ksminimize(XY)

global f_empir;
global x_empir;
global data1;
global tmpdaily;

[f_y f_x] = ksdensity(armaxsim(data1, tmpdaily', 36500, 1, 0)* ...
            XY(1)+XY(2));
output = trapz(abs(cumsum(f_y)/sum(f_y) - cumsum(x_empir)/sum(x_empir))) + ...
         sum(abs(f_x-x_empir));

end

