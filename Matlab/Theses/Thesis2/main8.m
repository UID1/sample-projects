% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% See the help information for prior main<x>.m scripts  for a background
% story.  In main8.m  we start from scratch  after picking the subperiod
% with the lowest min-max span and re applying the filters and so on. We
% also  introduce  a more accurate method  for measuring  best fits that
% involves an automated simulation process.  It requires a lot of compu-
% tations but it does the job. It computes the empirical distribution of
% the observed data and computes the distribution of the simulated data,
% then it calculates the integral  of the difference between the cumula-
% tive distributions.  By systematically computing the distribution fun-
% ctions for the different models and integrating the difference between
% the cdf of each simulated model and the empirical cdf,  the model that
% yields the lowest absolute value is selected as the best model.

if ~any(strcmp(who,'ELSPOTh2010'))
   fid1 = fopen('elspoth2010.bin', 'r');
   ELSPOTh2010 = fread(fid1, 8760, 'double');
   fclose(fid1);
end
if ~any(strcmp(who,'tmp2010'))
   % Get the temperature data for year 2010
   % convert it to Kelvin and then take the logarithm.
   tmp2010 = temp_proc(8760, 'cubic');
   tmp2010.lognl = log(272.15+tmp2010.national);
end


% We are  going to pick  the 90 day  period  with the lowest  difference
% between the maximum and minimum electricity price.

deltaELmin = 1e7;
for k = 1:length(ELSPOTh2010)-90*24
	deltaEL = max(ELSPOTh2010(k:k+90*24)) - ...
	          min(ELSPOTh2010(k:k+90*24));
	if deltaEL < deltaELmin
		deltaELmin = deltaEL;
		kmin = k;
	end
end
min90 = (kmin:kmin+90*24);

% Some Fourier analysis
figure
ELSPEC1 = fspectrum3(ELSPOTh2010(min90), 1);
plot(ELSPEC1.pscale, ELSPEC1.spectrum)
title('Period spectrum for the spot prices');

ELTREND = est_ma(ELSPOTh2010(min90), 500);
ELSPECdt = fspectrum3(ELSPOTh2010(min90)-ELTREND.removable, 1);
figure
plot(ELSPECdt.pscale, ELSPECdt.spectrum)
title('Period spectrum for the detrended spot prices');

figure
plot(ELSPECdt.fscale*1e5, ELSPECdt.spectrum)
title('Frequency spectrum for the detrended spot prices');

% We have major seasonalities at 700, 23.6, 11, 73 and two minor at 175,
% and 84 hours period lengths.  In frequencies we have the largest major
% peak at  4238(23.6),  second largest at 8524(11.73),  the rest  of the
% peaks  are given  in descending  amplitude:  142.9 (700), 571.4 (175),
% 1190(84) where the numbers in parenthesis are the period lengths.

[filt.n1, filt.d1] = butter(2, [450 750]/5e4, 'stop');
[filt.n2, filt.d2] = butter(2, [4100 4300]/5e4, 'stop'); % Partial
[filt.n3, filt.d3] = butter(2, [8350 8420]/5e4, 'stop'); % Max partial
[filt.n4, filt.d4] = butter(2, [1150 1250]/5e4, 'stop');

% Fourier analysis of the temperatures
ELSPOTdt = ELSPOTh2010(min90)-ELTREND.removable;
TMPSPEC = fspectrum3(tmp2010.national(min90), 1);
figure
plot(TMPSPEC.pscale, TMPSPEC.spectrum);
title('Period spectrum of temperature variations');

% We have a major peak at 700,  23.6,  and smaller peaks at 350,  131.3,
% 116.7 and 11.73.
% This means  that we don't need to filter  the 700 periodicity and only
% filter the 11.73 periodicity in part or not at all.

% Ever wondered  how to convert  hourly to daily values?  This one-liner
% does just that:
%
% mean(reshape(<hourlydata>, 24, 365))
%
% smart and simple huh?

% I have retrieved some consumption and production data from the website
% and there is no  question that the  electricity consumption is varying
% with  temperature.  It is more obvious on the  daily data than  on the
% hourly as there are hourly variations. When looking at the consumption
% when the temperature  is around 20 degrees,  which is room temperature
% we have  a minimum electricity consumption  as no electricity  is used
% for heating real estate. This part of the electricity consumption must
% be independent of the temperature.  When looking into one area of Nor-
% way and comparing it  to temperature observations  from a place nearby
% we see that at least half of the consumption is independent of outdoor
% temperature on a cold winter day. 
% Also, a part of the electricity consumption  depends on  the amount of
% light there is during the day. During the winter half of the year
% 

% Next step: filter design. Then we have the data needed for further
% ARMAX estimations.

ELhdes = filter(filt.n1, filt.d1, ...
	               filter(filt.n2, filt.d2, ...
		                ELSPOTh2010(kmin-300:kmin+90*24)));
for i = 1:2
   ELhdes = filter(filt.n3, filt.d3, ...
                        filter(filt.n1, filt.d1, ...
                                ELhdes));
   ELhdes = filter(filt.n3, filt.d3, ELhdes);
end
ELSPECdes = fspectrum3(ELhdes, 1);
figure
plot(ELSPECdes.pscale, ELSPECdes.spectrum);
title('Period spectrum of filtered price data');
ELSPOTds = ELhdes(301:end);
ELTRENDds = est_ma(ELSPOTds, 500);
ELSPOTdsdt = ELhdes(301:end)-ELTRENDds.removable;
ELSPECdsdt = fspectrum3(ELSPOTdsdt, 1);
figure
plot(ELSPECdsdt.pscale, ELSPECdsdt.spectrum);
title('Filtered and detrended price data');


% So, now  we have the detrended  and deseasonalized  price data  as the
% variable ELSPOTdsdt. Let us try to fit an ARMAX model with it.

% I formulate  the temperature  as the difference  between average  room
% temperature (20) and outdoor temperature.

% /--------------- Long Calculation commented out ... -----------------/

%[m1a.data m1a.par] = armabat2(ELSPOTdsdt, 20 - ...
%                               tmp2010.national(min90), ...
%                               1:10, 1:10, 1:5, 2, '1a')

figure
plot(armaxsim(m1a.data, tmp2010.national(min90), ...
                length(min90), 1, 0)+ELTRENDds.removable)
title('Simulated electricity price data')

% The estimation
%
%    [m1a.data m1a.par] = armabat2(ELSPOTdsdt, 20 - ...
%                         tmp2010.national(min90), 2, 20, 13, 1, '1a')
%
% yielded a process  that could be simulated.  In some cases r=3 worked.
% Note the delay = 1 instead of the "usual" 2.  Let's try log prices for
% that period. 
% 

ELloghds = (ELSPOTds);
ELTRENDdslog = est_ma(ELloghds, 500);
ELloghdsdt = (ELloghds - ELTRENDdslog.removable);
ELloghdsdt = ELloghdsdt/1000;


% /--------------- Long Calculation commented out ... -----------------/

%[m2a.data m2a.par] = armabat2(ELloghdsdt, 20 - ...
%                               tmp2010.national(min90), ...
%                               1:10, 1:10, 1:5, 2, '1a')


% I tried to include 24, 48  and 96 coefficients of the price data.  The
% result of that is that the process becomes resonant in simulations for
% AR orders of q > 1 and it doesn't matter that I scale down the exogen-
% eous data. So the conclusion is that when using temperature as exogen-
% eous data, higher order AR processes are not applicable.
% 
% Let's try pure ARMA and then try to shape an empirical distribution.

% /--------------- Long Calculation commented out ... -----------------/

%[m3a.data m3a.par] = armabat3(ELSPOTdsdt, 1:20, 1:20, '3a')
%[m3b.data m3b.par] = armabat3(ELloghdsdt, 1:20, 1:20, '3b')

% Sweet spots for m3a when r = 3: q = 6, 9, 14, 15, 16, 

% Let's run an automated process of checking valid processes

pvect = [3 4];
qvect = 16:55;
p3a_good = [];
q3a_good = [];
p3b_good = [];
q3b_good = [];
p3a_best = 00;
p3b_best = 00;
q3a_best = 00;
q3b_best = 00;
mind3a = 1e99;
mind3b = 1e99;
[f_emp x_emp] = ksdensity(ELSPOTdsdt);
for pp = 1:length(pvect)
   p = pvect(pp);
   for qq = 1:length(qvect)
      q = qvect(qq);
      disp(sprintf('(trial (%d, %d) of (%d,%d)', pp, ...
                     qq, length(pvect), length(qvect)))
      [m3a.data m3a.par] = armabat3(ELSPOTdsdt, p, q, '3a')
      [m3b.data m3b.par] = armabat3(ELloghdsdt, p, q, '3b')
      sim3a = armaxsim(m3a.data, 'nil', length(min90), 1, 0); 
      if max(sim3a) < 1e20
         p3a_good = [p3a_good p];
	 q3a_good = [q3a_good q];
	 [f3a_sim,x3a_sim] = ksdensity(sim3a);
	 simdist3a = abs(trapz(cumsum(f_emp)-cumsum(f3a_sim),x_emp));
	 if simdist3a < mind3a
            p3a_best = p;
            q3a_best = q;
            mind3a = simdist3a;
	 end
      end
      sim3b = armaxsim(m3b.data, 'nil', length(min90), 1, 0); 
      if max(sim3b) < 1e20
         p3b_good = [p3b_good p];
	 q3b_good = [q3b_good q];
	 [f3b_sim,x3b_sim] = ksdensity(sim3b);
	 simdist3b = abs(trapz(cumsum(f_emp)-cumsum(f3b_sim),x_emp));
	 if simdist3b < mind3b
            p3b_best = p;
            q3b_best = q;
            mind3b = simdist3b;
	 end
      end
   end
end

% Let us create a empirical-simulation mesh
distmtx3a = zeros(length(p3a_good), length(q3a_good));
for pp = 1:length(p3a_good)
   p = p3a_good(pp);
   for qq = 1:length(q3a_good)
      q = q3a_good(qq);
      [m3a.data m3a.par] = armabat3(ELSPOTdsdt, p, q, '3a')
      sim3a = armaxsim(m3a.data, 'nil', length(min90), 1, 0);
      [f3a_sim,x3a_sim] = ksdensity(sim3a);
      distmtx3a(pp,qq) =abs(trapz(cumsum(f_emp)-cumsum(f3a_sim),x_emp));
   end
end
distmtx3b = zeros(length(p3b_good), length(q3b_good));
for pp = 1:length(p3b_good)
   p = p3b_good(pp);
   for qq = 1:length(q3b_good)
      q = q3b_good(qq);
      [m3b.data m3b.par] = armabat3(ELloghdsdt, p, q, '3b')
      sim3b = armaxsim(m3b.data, 'nil', length(min90), 1, 0);
      [f3b_sim,x3b_sim] = ksdensity(sim3b);
      distmtx3b(pp,qq) =abs(trapz(cumsum(f_emp)-cumsum(f3b_sim),x_emp));
   end
end

figure
surf(distmtx3a)
figure
surf(distmtx3b)

% Try to get ksdensity  to fit a more precise function  than the default
% normal distribution.  Try to use this measure  to get a proper process
% for the  ARMAX(p,q,r) = (1:24, 1:24, 1:24)  on the ELSPOTdtds process.

% Then start from scratch  and process the daily electricity price data,
% then use this measurement method  to find good fits  and then the best
% fit.

