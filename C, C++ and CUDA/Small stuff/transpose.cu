#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <time.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

#define BDIMX 32
#define BDIMY 32

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/


#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

template <typename iox1>
void initialData(iox1 *ip, const int size)
{
   for(int i=0; i < size;++i)
      ip[i] = static_cast<iox1>(rand()/static_cast<iox1>(RAND_MAX));
      // Old implementation: (iox1)(rand() & 0xFF)/10.0f;
}

template <typename iox1>
__global__ void warmup (iox1 *out, iox1 *in, const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y + threadIdx.y;
   if(ix<nx && iy<ny)
      out[iy*nx+ix] = in[iy*nx + ix];
}

template <typename iox1>
void checkResult(iox1 *hostRef, iox1 *gpuRef, const int N)
{
   const double epsilon = 1.0E-8;
   bool match = 1;
   colors_t colors;
   for(int i=0; i<N; ++i)
   {
      if(abs(hostRef[i] - gpuRef[i]) > epsilon)
      {
         match = 0;
         colors.setFg(hRed); std::cout << "Arrays don't match!";
         std::cout << std::endl;
         std::cout << "Diverging element " << i << ":";
         std::cout << "host value = " << hostRef[i];
         std::cout << ", device value = " << gpuRef[i];
         colors.setColors(colors.initial_colors);
         std::cout << std::endl;
         break;
      }

   }
   if(match)
   {
      colors.setFg(hGreen);
      std::cout << "Arrays match!" << std:: endl;
   }
}


template <typename iox1>
__global__ void copyRow(iox1 *out, iox1 *in, const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y + threadIdx.y;
   if(ix<nx && iy<ny)
      out[iy*nx+ix] = in[iy*nx + ix];
}

template <typename iox1>
__global__ void copyCol(iox1 *out, iox1 *in, const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y + threadIdx.y;
   if(ix<nx && iy<ny)
      out[ix*ny+iy] = in[ix*ny+iy];
}

template <typename iox1>
__global__ void transposeNaiveRow(iox1 *out, iox1 *in,
                                   const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y + threadIdx.y;
   if(ix<nx && iy<ny) out[ix*ny+iy] = in[iy*nx+ix];
}

template <typename iox1>
__global__ void transposeNaiveCol(iox1 *out, iox1 *in,
                                   const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y + threadIdx.y;
   if(ix<nx && iy<ny) out[iy*nx+ix] = in[ix*ny+iy];
}

template <typename iox1>
__global__ void transposeUnroll4Row (iox1 *out, iox1 *in,
                                       const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x*4 + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y   + threadIdx.y;
   
   unsigned int ti = iy*nx + ix;  // access in rows
   unsigned int to = ix*ny + iy;  // access in columns

   if(ix+3*blockDim.x<nx && iy < ny)
   {
      out[to]                   = in[ti];
      out[to + ny*blockDim.x]   = in[ti+blockDim.x];
      out[to + ny*2*blockDim.x] = in[ti+2*blockDim.x];
      out[to + ny*3*blockDim.x] = in[ti+3*blockDim.x];
   }
}

template <typename iox1>
__global__ void transposeUnroll4Col (iox1 *out, iox1 *in,
                                       const int nx, const int ny)
{
   unsigned int ix = blockDim.x*blockIdx.x*4 + threadIdx.x;
   unsigned int iy = blockDim.y*blockIdx.y   + threadIdx.y;
   
   unsigned int ti = iy*nx + ix;  // access in rows
   unsigned int to = ix*ny + iy;  // access in columns

   if(ix+3*blockDim.x<nx && iy < ny)
   {
      out[ti]                = in[to];
      out[ti +   blockDim.x] = in[to + ny*blockDim.x];
      out[ti + 2*blockDim.x] = in[to+2*ny*blockDim.x];
      out[ti + 3*blockDim.x] = in[to+3*ny*blockDim.x];
   }
}

template <typename iox1>
__global__ void transposeDiagonalRow(iox1 *out, iox1 *in,
                                     const int nx, const int ny)
{
   unsigned int blk_y = blockIdx.x;
   unsigned int blk_x = (blockIdx.x+blockIdx.y)%gridDim.x;
   unsigned int ix = blockDim.x*blk_x + threadIdx.x;
   unsigned int iy = blockDim.y*blk_y + threadIdx.y;
   if(ix<nx && iy < ny) out[ix*ny+iy] = in[iy*nx+ix];
}

template <typename iox1>
__global__ void transposeDiagonalCol(iox1 *out, iox1 *in,
                                     const int nx, const int ny)
{
   unsigned int blk_y = blockIdx.x;
   unsigned int blk_x = (blockIdx.x+blockIdx.y)%gridDim.x;
   unsigned int ix = blockDim.x*blk_x + threadIdx.x;
   unsigned int iy = blockDim.y*blk_y + threadIdx.y;
   if(ix<nx && iy < ny) out[iy*nx+ix] = in[ix*ny+iy];
}

template <typename iox1>
void transposeHost(iox1 *out, iox1 *in, const int nx, const int ny)
{
    for( int iy = 0; iy < ny; ++iy)
        for( int ix = 0; ix < nx; ++ix)
            out[ix * ny + iy] = in[iy * nx + ix];
}

int main(int argc, char **argv)
{
   colors_t colors;
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));
   
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", for transposing matrices"; colors.setFg(hGray);
   std::cout << std::endl;

   /* Set up array size 2048x2048 */
   int nx = 1<<13;
   int ny = 1<<13;
   
   /* Setting up kernel and block size */
   int iKernel(0), blockx(BDIMX), blocky(BDIMY);
   if(argc>1) iKernel = atoi(argv[1]);
   if(argc>2) blockx  = atoi(argv[2]);
   if(argc>3) blocky  = atoi(argv[3]);
   if(argc>4) nx      = atoi(argv[4]);
   if(argc>5) ny      = atoi(argv[5]);

   colors.setFg(hGray); std::cout << "with dimensions ";
   colors.setFg(dYellow); std::cout << "nx ";
   colors.setFg(White); std::cout << "x";
   colors.setFg(dYellow); std::cout << " ny";
   colors.setFg(hGray); std::cout << ": ";
   colors.setFg(dCyan); std::cout << nx;
   colors.setFg(White); std::cout << " x ";
   colors.setFg(dCyan); std::cout << ny;
   colors.setFg(hGray); std::cout << ", using kernel: ";
   colors.setFg(dCyan); std::cout << iKernel;
   colors.setFg(hGray); std::cout << "." <<  std::endl;

   uint64 nBytes = static_cast<int64>(nx)*ny*sizeof(double);
   std::cout << "Calculation: " << nBytes << std::endl;
   std::cout << "nx: " << nx << std::endl;
   std::cout << "ny: " << ny << std::endl;
   std::cout << "nx x ny: " << nx*ny << std::endl;
   std::cout << "Data size: "; colors.setFg(dCyan);
   std::cout << static_cast<float>
                (nBytes/((nBytes>(1<<30))?
                (1024.0f*1024.0f*1024.0f)
                ((nBytes>(1<<18))?
                (1024.0f*1024.0f):1024.0f)));
   colors.setFg(White);
   //std::cout << ((nBytes>(1<<18))?" M":" k") << "B";
   std::cout << ((nBytes>(1<<30))?" G":
                 ((nBytes>(1<<18))?" M":" k")) << "B";
   colors.setFg(hGray); std::cout << "." << std::endl;

   /* Configuration of execution */
   dim3 block (blockx, blocky);
   dim3 grid ((nx+block.x-1)/block.x, (ny+block.y-1)/block.y);
   
   /* Allocate memory on host */   
   /* This is the good old C way */
   double *h_A(NULL), *hostRef(NULL), *gpuRef(NULL);
   
   //h_A     = (double *)malloc(nBytes);
   //hostRef = (double *)malloc(nBytes);
   // gpuRef = (double *)malloc(nBytes);

   /* Let's try to instantiate them C++ style instead */
   try
   {
      h_A     = new double[nx*ny];
      hostRef = new double[nx*ny];
      gpuRef  = new double[nx*ny];
   }
   catch (std::bad_alloc& errH)
   {
      colors.setFg(hRed); std::cout << "Allocation failure: " << errH.what();
      colors.setFg(hGray); std::cout << std::endl;
   }
   //if((h_A==NULL)||(hostRef==NULL)||(gpuRef==NULL))
   //{
   //   colors.setFg(hRed); std::cout << "Unable to allocate memory!";
   //   colors.setFg(hGray); std::cout << std::endl;
   //   return -1;
   //}

   /* Initialize the host array */
   initialData(h_A, nx*ny);
   
   
   /* Allocate device memory */
   double *d_A, *d_C;
   CHECK(cudaMalloc((double **)&d_A, nBytes));
   CHECK(cudaMalloc((double **)&d_C, nBytes));
   
   /* Copy data from host to device */
   CHECK(cudaMemcpy(d_A, h_A, nBytes, cudaMemcpyHostToDevice));
   
   /* Warmup to eleminate startup overhead */
   double iStart = cpuSecond();
   warmup <<<grid, block>>> (d_C, d_A, nx, ny);
   CHECK(cudaDeviceSynchronize());
   double iElaps = cpuSecond() - iStart;
   
   colors.setFg(hGray); std::cout << "warmup         ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(dCyan); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(dCyan); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(dCyan); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << "()";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   /* Kernel pointer and descriptor */
   void (*kernel)(double *, double *, int, int);
   char *kernelName;
   
   /* Set up kernel */
   switch(iKernel)
   {
      case 0:
         kernel = &copyRow<double>;
         kernelName = "CopyRow        ";
         break;
      
      case 1:
         kernel = &copyCol<double>;
         kernelName = "CopyCol        ";
         break;

      case 2:
         kernel = &transposeNaiveRow<double>;
         kernelName = "NaiveCol       ";
         break;

      case 3:
         kernel = &transposeNaiveCol<double>;
         kernelName = "NaiveRow       ";
         break;
      
      case 4:
         kernel = &transposeUnroll4Row<double>;
         kernelName = "Unroll4Row     ";
         break;
      case 5:
         kernel = &transposeUnroll4Col<double>;
         kernelName = "Unroll4Col     ";
         break;

      case 6:
         kernel = &transposeDiagonalRow<double>;
         kernelName = "DiagonalRow    ";
         break;

      case 7:
         kernel = &transposeDiagonalCol<double>;
         kernelName = "DiagonalCol    ";
         break;
   }
   
   /* Run kernel */
   iStart = cpuSecond();
   kernel <<<grid, block>>> (d_C, d_A, nx, ny);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond()-iStart;
   
   
   /* Calculate effective bandwidth */
   double ibnd = 2*nx*ny*sizeof(double)/1e9/iElaps;

   colors.setFg(hGray); std::cout << kernelName;
   colors.setFg(hBlue); std::cout << "<<<";
   colors.setFg(White); std::cout << "(";
   colors.setFg(dCyan); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(dCyan); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(dCyan); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << "()";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms";
   colors.setFg(hGray); std::cout << " using an effective bandwidth of ";
   if(!std::isinf(ibnd)) {colors.setFg(dCyan); std::cout << ibnd;}
   else {colors.setFg(hRed); std::cout << "--";}
   colors.setFg(White); std::cout << " GB/s";
   colors.setFg(hGray); std::cout << "." << std::endl;
   
   /* Check kernel results */
   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   if(iKernel<2) checkResult(h_A, gpuRef, nx*ny);
   else 
   {
      transposeHost(hostRef, h_A, nx, ny);
      checkResult(hostRef, gpuRef, nx*ny);
   }
   
   /* Free host and device memory */
   CHECK(cudaFree(d_A));
   CHECK(cudaFree(d_C));
   //free(h_A);
   //free(hostRef);
   //free(gpuRef);
   /* Arrays instantiated C++ style should be deallocated the same style */
   delete[] h_A;
   delete[] hostRef;
   delete[] gpuRef;
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}
