%
% AR estimation given order p
%

function output = ar_est(y, p, method)

% Just to make sure that the input argument y always will be a column
% vector
SZ = size(y);
if SZ(2) > 1
	y=y';
end

if method == 1  % Yule-Walker method

	% We begin by constructing a correlation matrix R_p
	r_k = zeros(1, p);
	y_mean = mean(y);
	y_var = var(y);
	for i = 1:p
		r_k(i) = (mean(y(1+i:end).*y(1:end-i)) ...
		          - y_mean^2)/y_var;
	end
	X = zeros(length(y)-p+1, p);
	R_p = zeros(p);
	for i = 1:p
		if i < p
			R_p(:,i) = [zeros(1, i) r_k(1:end-i)];
		end
		% I also throw in the buildup routine for the
		% estimation matrix X to save computations
		X(:,i) = y(p-i+1:end-i+1)';
	end
	R_p = R_p + R_p' + eye(p);

	% The coefficients, residuals and the variance of these
	% residuals are estimated. The estimations are evaluated
	% using Bayesian Information Criterion (BIC)

	phi_y = R_p\r_k';
	y_resid = y(p:end)-(X*phi_y);
	y_var_resid = (y_resid'*y_resid)/length(y_resid);
	y_BIC = log(y_var_resid)+p*log(length(y))/length(y);

	output.BIC = y_BIC;
	output.resid = y_resid;
	output.phi = phi_y;
	output.resid_var = y_var_resid;

	% Let us estimate the errors 
	SSE = zeros(length(y)-p+1,1);
	for i = 1:p-1
		SSE = SSE + phi_y(i)*y(p-i:end-i);
	end
	output.SSE = SSE;

end
if method == 2 % Least-square method
	A_ls = zeros(length(y)-p+1, p-1);
	for i = 1:(p-1);
		A_ls(:,i)=y(p-i:end-i)';
	end
	b_ls=y(p:end);
	Phi_ls = (A_ls'*A_ls)\(A_ls'*b_ls);
	E_ls = b_ls-A_ls*Phi_ls;
	Var_ls = (E_ls'*E_ls)/length(E_ls);
	BIC_ls  = log(Var_ls)+p*log(length(y))/length(y);

	output.BIC = BIC_ls;
	output.resid = E_ls;
	output.phi = Phi_ls;
	output.resid_var = Var_ls;

	% Let us estimate the errors 
	SSE = zeros(length(y)-p+1,1);
	for i = 1:p-1
		SSE = SSE + Phi_ls(i)*y(p-i:end-i);
	end
	output.SSE = SSE;
end
if method == 3 % State-space modelling using Kalman-filter

end
	
end % ar_est


