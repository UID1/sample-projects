%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%

% +------------------------------------------------------------------------------------------------
% ####################################################################################
% #####################################################################
% /////////////////////////////////////////////////////                     |
% //////////////////////////                                               -+-
% ///////                                                                   |
%
%           Definitions
%

% Additional information:
%
%  t = 0 since PS(t) is not defined at other time values
function DPP = dpcalcfun(DEFPAR, MKTSWP, sigmaBS)

% The inparameters for this function are:
% 
%  DEFPAR.lambda  -  a vector of default intensities
%  DEFPAR.prob    -  a vector of default probabilities
%  DEFPAR.dT      -  a vector of interval lenghts for eact default intensity/probability
% 
%  sigmaBS        -  a sigma parameter for the B-S swaption valuation
%  
%  MKTSWP.R       -  a vector of observed swap spreads
%  MKTSWP.Mt      -  a vector of maturities for the observed swap spreads
%
%  It is assumed that the vectors have the same lenght in the DEFPAR parameter and in the
%  MKTSWP parameter.

% I want to find a way to construct a detection algorithm that matches the
% elements in LR, MR, and HR with the proper year. Perhaps find(pT == Mt(i)) to
% return those indices and then use them on LR, MR and HR where

pT = cumsum(DEFPAR.dT);

% The function for a fair strike price of a CDS agreement is
% (WARNING: For valid S_ab the parameters a,b must be a < b)

S_ab = @(P_0T,a,b, beta)((P_0T(a)-P_0T(b))./(beta*sum(P_0T(a+1:b))));

% This comes directly from equation (4.2).

% So we need to use the Black-Scholes model for swaptions as implemented by Brigo
% (eq 1.26 Brigo-Pallavicini)

d_1= @(K, F, v)((log(F./K)+v.^2/2)./v);
d_2= @(K, F, v)((log(F./K)-v.^2/2)./v);

Bl = @(K, F, v, w)(F.*w.*normcdf(w.*d_1(K,F,v)) - K.*w.*normcdf(w.*d_2(K,F,v)) );

% The cumulative density function for a piecewise constant intensity is
% 

F_tau = @(t, lambda)(1-exp(-lambda(:,1)'*(((t>lambda(:,3)).*(lambda(:,3)-lambda(:,2)))+((t<=lambda(:,3))-(t<=lambda(:,2)))*(t-lambda(:,2)'*((t<=lambda(:,3))-(t<=lambda(:,2)))))));

% where 
% lambda = a matrix containing the parameters of the intensity which is
%          assumed to be a step-function with respect to time. The first
%          column of this matrix contains the intensity values, the second
%          column contains the time points where each intensity value
%          starts and the third column contains the time points where
%          each of them stop. So the second and third column defines the
%          start and the end of each time step such that the entire
%          intensity function can be defined as a sum of Heaviside step
%          functions H(t), i.e.
%
%           lambda(t) = lambda(:,1)'*(H(t-lambda(:,2))-H(t-lambda(:,3)))
%                     = lambda(:,1)'*((t > lambda(:,2))-(t > lambda(:,3)))
%                     = lambda(:,1)'*((t <= lambda(:,3))-(t <= lambda(:,2)))
%
%         so the sums are expressed here as scalar products
%
% So in this case: lambda = [#R(:,1) pT [pT(2:end);100] ] where # is L, M or H
% and Q(\tau > t) =  1 - F_tau(t, [#R(:,1) pT [pT(2:end);100])

% The risk-free discount functions are only defined on discrete points in time. 
% If we want to find out the values between these points we need to interpolate:

% This defines a discount function that follows a piecewise constant interest rate

P0c_t0 = @(t, R0T, Mt)(exp(-[R0T(1);R0T]'*[0 diff(t <= Mt)]'.*t));
P0c_t = @(t, R0T, Mt)(exp(-[R0T(1);R0T]'*[0*(1:length(t))' diff(meshgrid(t,Mt) <= meshgrid(Mt,t)')']'.*t));

% This defines a discount function that uses an interest rate that follows an
% linearly interpolated continuous function. It is also adapted to accept time
% as a vector.

% (Original function for troubleshooting purposes: R0f_t = @(t, R0T, Mt)((diff([0;R0T])'./diff(Mt))*(max(min(t, Mt(2:end)),Mt(1:end-1))-Mt(1:end-1))');)

P0f_t = @(t, R0T, Mt)(exp(-(diff([0;R0T])'./diff(Mt))*(max(min(meshgrid(t, Mt(2:end))' , meshgrid(Mt(2:end),t)),meshgrid(Mt(1:end-1),t))-meshgrid(Mt(1:end-1),t))'.*t));

% This is the interest rate function (for verification). I assume that this is the
% interest rate with respect to maturity, i.e. that it is already integrated and
% I can apply it directly as a "fixed" rate with respect to the given time-period.

R0f_t = @(t, R0T, Mt)((diff([0;R0T])'./diff(Mt))*(max(min(meshgrid(t, Mt(2:end))' , meshgrid(Mt(2:end),t)),meshgrid(Mt(1:end-1),t))-meshgrid(Mt(1:end-1),t))');

% We also need to redefine the S_ab with respect to these interpolations

S_abf = @(t, R0T, Mt)((P0f_t(t(1), R0T, Mt)-P0f_t(t(end), R0T, Mt))./(mean(diff(t))*sum(P0f_t(t(2:end), R0T, Mt))));

% We also redefine S_ab for use with a piecewise constant interest rate function
S_abf2 = @(t, R0T, Mt)((P0c_t(t(1), R0T, Mt)-P0c_t(t(end), R0T, Mt))./(mean(diff(t))*sum(P0c_t(t(2:end), R0T, Mt))));

% Note that the mean(diff) is put there because I assume that the time points are
% 'monospaced'. If the time intervals are heterogeneous, all we have to do is to
% remove the mean() and the sum() and change the expression denominator to a scalar
% product, i.e. to 'diff(t)'*P0x_t(t(2:end))'.

% The following is for calculations straight off the table values and the table
% time-grid (5 years) using piecewise constant interest rates

ADPP.antpure = zeros(length(MKTSWP.Mt)-1, 1);

% We also want to try the same evaluations for the interpolated values

ADPP.antinterp  = zeros(length(MKTSWP.Mt)-1, 1);
ADPP.postinterp = ADPP.antinterp;

% Let's also try the same evaluations for the piecewise constant interest rate
% but with a finer time-grid

ADPP.antstep = zeros(length(MKTSWP.Mt)-1, 1);
ADPP.poststep = ADPP.antstep;

% +------------------------------------------------------------------------------------------------
% #####################################################################################
% #####################################################################
% /////////////////////////////////////////////////////                        |  \ /   /
% //////////////////////////                                                  -+-  X   / 
% ///////                                                                      |  / \ /   
%
%          Computations
%

% Computing the P(0, T_i) from the table values

P0T = zeros(1,length(MKTSWP.R))';
for j = 1:length(MKTSWP.R)
    P0T(j) = (1-5*MKTSWP.R(j)*sum(P0T))./(1+5*MKTSWP.R(j));
end

R0T = -log(P0T(2:end))./MKTSWP.Mt(2:end)';
ADPP.R0T = R0T;

% Here begins the big foor-loop that does all the necessary calculations. The variable
% k is for the different maturities of the swaps where k = 2 3 4 5 6 7 (length(Mt))
% which is from the table values. The for-loops below basically use equations (1.28)
% and (21.41) in Brigo-Pallavicini.

for k = 2:length(MKTSWP.Mt)
    
    % Calculations of anticipated values done pretty much straigh from the table values
    
    for i = 2:k
        SabC = S_ab(P0T(i-1:k), 1, k-i+2, 5);
	%SabC(find(isnan(SabC))) = 0;  % This is in case SabC returns NaN (when inp arg a = b)
        ADPP.antpure(k-1, :) = ADPP.antpure(k-1, :) + (DEFPAR.prob(find(MKTSWP.Mt(i-1)==pT),:) ...
		   - DEFPAR.prob(find(MKTSWP.Mt(i)==pT),:)).*Bl(MKTSWP.R(k),SabC,sigmaBS.* ...
		   MKTSWP.Mt(i-1).^.5,1).*5.*sum(P0T(1:k));
    end
    for i = 1:MKTSWP.Mt(k)
	% We now calculate the anticipated adjustments for the interpolated functions using
	% annual time-grid
	
        ADPP.antinterp(k-1) = ADPP.antinterp(k-1) + (F_tau(i, [DEFPAR.lambda pT [pT(2:end);100]]) - ...
           F_tau(i-1, [DEFPAR.lambda pT [pT(2:end);100]])).* ...
		   Bl(MKTSWP.R(k), S_abf(i-1:MKTSWP.Mt(k), R0T, MKTSWP.Mt), sigmaBS.*(i-1)^.5, 1).* ...
		   sum(P0f_t(i-1:MKTSWP.Mt(k), R0T, MKTSWP.Mt));

        % We now calculate the anticipated adjustments for the stepwise constant
        % interest rate functions but with an annual time-grid instead of the
        % "quintoannual" (5 years) time-grid that was used for the table-based
        % calculations above.

        ADPP.antstep(k-1) = ADPP.antstep(k-1) + (F_tau(i, [DEFPAR.lambda pT [pT(2:end);100]]) - ...
            F_tau(i-1, [DEFPAR.lambda pT [pT(2:end);100]])).* ...
			Bl(MKTSWP.R(k), S_abf2(i-1:MKTSWP.Mt(k), R0T, MKTSWP.Mt), sigmaBS.*(i-1)^.5, 1).* ...
			sum(P0f_t(i-1:MKTSWP.Mt(k), R0T, MKTSWP.Mt));
        if i < MKTSWP.Mt(k)
            % The postponed adjustments for the interpolated functions are calculated here

            ADPP.postinterp(k-1) = ADPP.postinterp(k-1) + (F_tau(i, [DEFPAR.lambda pT [pT(2:end);100]]) ...
			- F_tau(i-1, [DEFPAR.lambda pT [pT(2:end);100]])).* ...
			Bl(MKTSWP.R(k), S_abf(i:MKTSWP.Mt(k), R0T, MKTSWP.Mt), sigmaBS.*(i)^.5, 1).* ...
			sum(P0f_t(i:MKTSWP.Mt(k), R0T, MKTSWP.Mt));

            % The same adjustments are calculated but for the stepwise constant interest rate
            % function

            ADPP.poststep(k-1) = ADPP.poststep(k-1) + (F_tau(i, [DEFPAR.lambda pT [pT(2:end);100]]) - ...
               F_tau(i-1, [DEFPAR.lambda pT [pT(2:end);100]])).* ...
			   Bl(MKTSWP.R(k), S_abf2(i:MKTSWP.Mt(k), R0T, MKTSWP.Mt), sigmaBS.*(i)^.5, 1).* ...
			   sum(P0f_t(i:MKTSWP.Mt(k), R0T, MKTSWP.Mt));
        end
    end
end

DPP = ADPP;