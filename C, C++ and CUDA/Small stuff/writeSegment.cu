#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <time.h>
#include <windows.h>
#include <limits>

typedef long long int64;
typedef unsigned long long uint64;

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/


#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

template <typename iox1>
void initialData(iox1 *ip, const int size)
{
   for(int i=0; i < size;++i)
      ip[i] = (iox1)(rand() & 0xFF)/10.0f;
}

template <typename iox1>
void sumArraysOnHostRO (iox1 *A, iox1 *B, iox1 *C,
                      const int N, int offset)
{
   for(int idx=offset, k = 0; idx<N;++idx, ++k)
      C[k] = A[idx] + B[idx];
}

template <typename iox1>
void sumArraysOnHostWO (iox1 *A, iox1 *B, iox1 *C,
                      const int N, int offset)
{
   for(int idx=offset, k = 0; idx<N;++idx, ++k)
      C[idx] = A[k] + B[k];
}

template <typename iox1>
__global__ void warmup (iox1 *A, iox1 *B, iox1 *C, const int N, int offset)
{
   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
   unsigned int k = i + offset;
   if(k<N) C[k] = A[i]+B[i];
}

template <typename iox1>
__global__ void readOffset (iox1 *A, iox1 *B, iox1 *C, const int N,
                            int offset)
{
   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
   unsigned int k = i + offset;
   /* Here the offset is applied with 'k' onto the read operations of the
      data present in arrays 'A' and 'B'.
   */
   if(k<N) C[i] = A[k]+B[k];
}

template <typename iox1>
__global__ void writeOffset (iox1 *A, iox1 *B, iox1 *C, const int N,
                            int offset)
{
   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
   unsigned int k = i + offset;
   /* Here the offset is applied with 'k' onto the write operations
      to the array 'C'.
   */
   if(k<N) C[k] = A[i]+B[i];
}

template <typename iox1>
void checkResult(iox1 *hostRef, iox1 *gpuRef, const int N)
{
   const double epsilon = 1.0E-8;
   bool match = 1;
   colors_t colors;
   for(int i=0; i<N; ++i)
   {
      if(abs(hostRef[i] - gpuRef[i]) > epsilon)
      {
         match = 0;
         colors.setFg(hRed); std::cout << "Arrays don't match!";
         std::cout << std::endl;
         std::cout << "Diverging element " << i << ":";
         std::cout << "host value = " << hostRef[i];
         std::cout << ", device value = " << gpuRef[i];
         colors.setColors(colors.initial_colors);
         std::cout << std::endl;
         break;
      }

   }
   if(match)
      colors.setFg(hGreen);
      std::cout << "Arrays match!" << std:: endl;
   colors.setColors(colors.initial_colors);
}

template <typename iox1>
__global__ void testsummation (iox1 *A, uint64 N)
{
   double tmp(0);
   for(uint64 i = 0; i<N; ++i)
      tmp += A[i];
   A[0] = tmp;
   printf("GPU sum: %d\n", A[0]);
}

int main(int argc, char **argv)
{
   colors_t colors;
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));

   int iPower(25);
   uint64 nElem = 1<<iPower;
   uint64 nBytes = nElem * sizeof(float);
   bool isLarge ((iPower>18) ? true : false);
   
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", number of elements: "; colors.setFg(dCyan); std::cout << nElem;
   colors.setFg(hGray); std::cout << ", data size: "; colors.setFg(dCyan);
   std::cout << static_cast<float>
                (nBytes/(isLarge?(1024.0f*1024.0f):1024.0f));
   colors.setFg(White);
   std::cout << (isLarge?" M":" k") << "B";
   colors.setFg(hGray); std::cout << "." << std::endl;

   /* Set up offset for summary */
   int blockSize = 1024;
   int offset = 0;
   if(argc>1) offset     = atoi(argv[1]);
   if(argc>2) blockSize = atoi(argv[2]);
   
   /* Execution configuration */
   dim3 block (blockSize);
   dim3 grid ((nElem+block.x-1)/block.x);
   
   /* Host memory allocation */
   float *h_A     = (float *)malloc(nBytes);
   float *h_B     = (float *)malloc(nBytes);
   float *hostRef = (float *)malloc(nBytes);
   float *gpuRef  = (float *)malloc(nBytes);
   
   /* Host array initialization */
   initialData(h_A, nElem);
   memcpy(h_B, h_A, nBytes);
   
   /* Summary at host side */
   sumArraysOnHostWO(h_A, h_B, hostRef, nElem, offset);
   
   /* Data transfer from host to device */
   float *d_A, *d_B, *d_C;
   CHECK(cudaMalloc((float **)&d_A, nBytes));
   CHECK(cudaMalloc((float **)&d_B, nBytes));
   CHECK(cudaMalloc((float **)&d_C, nBytes));
   CHECK(cudaMemcpy(d_A, h_A, nBytes, cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(d_B, h_B, nBytes, cudaMemcpyHostToDevice));
   
   /* Evaluation of kernel 1 */
   double iStart = cpuSecond();
   warmup <<<grid, block>>> (d_A, d_B, d_C, nElem, offset);
   CHECK(cudaDeviceSynchronize());
   double iElaps = cpuSecond() - iStart;
   
   colors.setFg(hGray); std::cout << "warmup     ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "with offset "; colors.setFg(dCyan);
   std::cout << offset; colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<int>(1000*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;
   
   iStart = cpuSecond();
   writeOffset <<<grid, block>>> (d_A, d_B, d_C, nElem, offset);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   colors.setFg(hGray); std::cout << "readOffset ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "with offset "; colors.setFg(dCyan);
   std::cout << offset; colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<int>(1000*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;
   
   /* Some test code */
   uint64 tDS = 100000;
   double refVal(0), sumVal1(0), sumVal2(2*tDS), perturbVal(0), sumHAA(0);
   double *sumEl, *retVal;
   sumEl = (double *)malloc(tDS*sizeof(double));
   retVal = (double *)malloc(sizeof(double));
   retVal[0] = 0;
   for(int i = 0; i<tDS; ++i)
   {
      sumEl[i] = 1;
      refVal += 1;
      sumVal1 += sumEl[i];
      sumVal2 -= 1;
      perturbVal += 1 - std::numeric_limits<double>::epsilon();
   }
   
   double *h_AA, *d_AA;
   h_AA = (double *)malloc(tDS*sizeof(double));
   memset(h_AA, 1.0, tDS*sizeof(double));
   //for(int i = 0; i<tDS; ++i)
   //   h_AA[i] = 1.0;
   for(int i = 0; i<tDS; ++i)
      sumHAA += h_AA[i];
   CHECK(cudaMalloc((double **)&d_AA, tDS*sizeof(double)));
   CHECK(cudaMemcpy(d_AA, h_AA, tDS*sizeof(double),
                                      cudaMemcpyHostToDevice));
   dim3 block1(1);
   dim3 grid1(1);
   
   testsummation <<<block1, grid1>>> (d_AA, tDS);
   CHECK(cudaMemcpy(h_AA, d_AA, sizeof(double), cudaMemcpyDeviceToHost));
   
   
   colors.setFg(hGray);
   std::cout << "Reference value  : "; colors.setFg(dCyan);
   std::cout << refVal; colors.setFg(hGray); std::cout << std::endl;
   std::cout << "Difference 1     : "; colors.setFg(dCyan);
   std::cout << refVal-sumVal1;
   colors.setFg(hGray); std::cout << std::endl;
   std::cout << "Difference 2     : "; colors.setFg(dCyan);
   std::cout << refVal-sumVal2;
   colors.setFg(hGray); std::cout << std::endl;
   std::cout << "Perturbed diff   : "; colors.setFg(dCyan);
   std::cout << refVal-perturbVal;
   colors.setFg(hGray); std::cout << std::endl;
   std::cout << "h_AA(i)          : "; colors.setFg(dCyan);
   std::cout << h_AA[10];
   colors.setFg(hGray); std::cout << std::endl;
   std::cout << "Diff h_AA        : "; colors.setFg(dCyan);
   std::cout << refVal-sumHAA;
   colors.setFg(hGray); std::cout << std::endl;

   std::cout << "GPU Difference 1 : "; colors.setFg(dCyan);
   std::cout << refVal-h_AA[0];
   colors.setFg(hGray); std::cout << std::endl;

   
   /* Copy kernel result back to host side and verify device results */
   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   checkResult(hostRef, gpuRef, nElem - offset);
   
   /* Free host and device memory */
   CHECK(cudaFree(d_A));
   CHECK(cudaFree(d_AA));
   CHECK(cudaFree(d_B));
   CHECK(cudaFree(d_C));
   free(h_A);
   free(h_AA);
   free(h_B);
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   colors.setColors(colors.initial_colors);
   return EXIT_SUCCESS;
}
