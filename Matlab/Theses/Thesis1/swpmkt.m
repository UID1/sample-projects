%
%  This generates a chart showing the defvelopment of the derivatives market
%
%


%DateLabels = ['Jun.1998';'Dec.1998';'Jun.1999';'Dec.1999';'Jun.2000';'Dec.2000';'Jun.2001';'Dec.2001';'Jun.2002';'Dec.2002';'Jun.2003';'Dec.2003';'Jun.2004';'Dec.2004';'Jun.2005';'Dec.2005';'Jun.2006';'Dec.2006';'Jun.2007';'Dec.2007';'Jun.2008';'Dec.2008';'Jun.2009';'Dec.2009';'Jun.2010';'Dec.2010'];

DateLabels = {'Jun.1998';'Dec.1998';'Jun.1999';'Dec.1999';'Jun.2000';'Dec.2000';'Jun.2001';'Dec.2001';'Jun.2002';'Dec.2002';'Jun.2003';'Dec.2003';'Jun.2004';'Dec.2004';'Jun.2005';'Dec.2005';'Jun.2006';'Dec.2006';'Jun.2007';'Dec.2007';'Jun.2008';'Dec.2008';'Jun.2009';'Dec.2009';'Jun.2010';'Dec.2010'};


InterestRateSwaps = [29363.384;36261.583;38371.912;43936.128;47992.731;48768.121;51407.156;58897.149;68234.102;79119.94;94582.591;111209.39;127570.321;150631.34;163749.44;169106.215;207587.936;229693.079;272215.746;309588.274;356771.552;341127.621;341902.73;349287.982;347508.057;364377.58];

CurrencySwaps = [1947.476;2253.169;2349.578;2443.948;2604.998;3193.699;3831.728;3941.894;4214.766;4502.809;5158.91;6371.299;7033.458;8222.767;8235.917;8503.919;9696.343;10791.611;12311.639;14346.678;16306.97;14940.767;15071.935;16509.007;16346.706;19271.07];

CreditDefaultSwaps = [0;0;0;0;0;0;0;0;0;0;0;0;0;6395.744;10211.378;13908.285;20352.307;28650.265;42580.546;58243.721;57402.758;41882.684;36098.18;32692.694;30260.934;29897.586];
FIGS.LineColors = [[0, 0, 123]; [55, 36, 138]; [127, 138, 199]; [214, 229, 230]; [115, 115, 115]; [165, 169, 176]; [183, 202, 211]]/255;
FIGS.fig1 = figure('Color',[1 1 1]);
plot(1:26, InterestRateSwaps/1000, 'LineWidth', 2.25, 'Color', FIGS.LineColors(1, :));
set(gca,'XTick',1:4:26)
set(gca,'XTickLabel',DateLabels(1:4:26));
xlim([1 26]);
hold on
plot(1:26, CurrencySwaps/1000, 'LineWidth', 2, 'Color', FIGS.LineColors(3, :));
plot(1:26, CreditDefaultSwaps/1000, 'LineWidth', 2, 'Color', FIGS.LineColors(4, :));
legend('Interest Rate Swaps','Currency Swaps','Credit Default Swaps', 'Location', 'NorthWest');
ylabel('Notional amts outstanding (US$ trillions)');
title('Source: Bank for International settlements (bis.org)');
