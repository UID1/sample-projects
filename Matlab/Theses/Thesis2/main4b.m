% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% This Matlab script  is intended to review  the results of the computa-
% tions in the 'main3.m' script.

% We start by loading the data:


load armaxdata002(corrected).mat

% We set up the configuration for our plots

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;


FIGS.fig1 = figure('Color',[1 1 1]);
surf(par1c.fepmtx(:,:,1))
view(227.5, 30)
title(['\fontsize{12}FPE surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on detrended 1 year data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig2 = figure('Color',[1 1 1]);
surf(par1c.fepmtx(:,:,2))
view(227.5, 30)
title(['\fontsize{12}FPE surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on detrended 1 year data at \itr\rm = 2'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig3 = figure('Color',[1 1 1]);
surf(par1c.fepmtx(:,:,3))
view(227.5, 30)
title(['\fontsize{12}FPE surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on detrended 1 year data at \itr\rm = 3'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)



FIGS.fig4 = figure('Color',[1 1 1]);
surf(par2c.fepmtx(:,:,1))
view(227.5, 30)
title(['\fontsize{12}FPE surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on detrended first 3 months of data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig5 = figure('Color',[1 1 1]);
surf(par2c.fepmtx(:,:,2))
view(227.5, 30)
title(['\fontsize{12}FPE surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on detrended first 3 months of data at \itr\rm = 2'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig6 = figure('Color',[1 1 1]);
surf(par2c.fepmtx(:,:,3))
view(227.5, 30)
title(['\fontsize{12}FPE surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on detrended first 3 months of data at \itr\rm = 3'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

