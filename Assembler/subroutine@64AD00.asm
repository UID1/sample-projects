0064AD00 sub_64AD00      proc near               ; CODE XREF: sub_64AFA0+1A6p
0064AD00                                         ; sub_64B1B0+92p
0064AD00 
0064AD00 var_8           = dword ptr -8
0064AD00 var_4           = dword ptr -4
0064AD00 
0064AD00                 sub     esp, 8
0064AD03                 mov     ecx, [esi+4]    ; Stacked #S/N
0064AD06                 mov     eax, [esi+8]
0064AD09                 push    ebx
0064AD0A                 push    edi
0064AD0B                 lea     edi, [esi+4]
0064AD0E                 or      eax, ecx        ; The two halves of the S/N are ORed into one
0064AD10                 shr     ecx, 10h
0064AD13                 and     eax, 0FFFFh     ; Strip the 2nd half off the "ORed" #S/N
0064AD18                 or      eax, ecx        ; "OR" the "ORed" #S/N into one 16-bit word
0064AD1A                 mov     ecx, [esi]      ; Move some value (11h) at the base of si to ecx
0064AD1C                 push    ecx
0064AD1D                 push    eax
0064AD1E                 mov     [esp+18h+var_4], 0
0064AD26                 mov     [esi+18h], eax
0064AD29                 call    sub_64AC10      ; Process the generated signature through this subroutine
0064AD2E                 mov     ecx, [edi]      ; Get the least significant dword of the S/N into ecx
0064AD30                 mov     ebx, ecx        ; and ebx
0064AD32                 shl     ebx, 10h        ; shift ebx 16 steps left
0064AD35                 or      ebx, eax        ; Concatenate the least significant word of #S/N with the signature
0064AD37                 mov     [esi+14h], eax  ; move the signature into memory
0064AD3A                 mov     eax, [esi+8]    ; and get the most significant dword of #S/N
0064AD3D                 lea     edx, [esi+1Ch]  ; Get something (?) from another part of this memory
0064AD3D                                         ; that now equals 0
0064AD40                 shr     ecx, 10h
0064AD43                 shl     eax, 10h
0064AD46                 or      ecx, eax        ; We're picking out the dword from the very middle of the #S/N
0064AD48                 mov     [edx], ebx      ; and moving the least significant word together with
0064AD48                                         ; the computed signature into memloc 1C
0064AD4A                 push    edx             ; and push the location into the stack
0064AD4B                 mov     [esi+20h], ecx  ; the middle part of #S/N goes into memory
0064AD4B                                         ; and now it's neatly gathered in the memory
0064AD4E                 lea     ebx, [esi+24h]  ; Get an empty segment for &ebx
0064AD51                 lea     ecx, [esi+0Ch]  ; and get ecx to point at the lsb of the entered #L/K
0064AD54                 push    ebx             ; We now push the empty memloc into stack
0064AD55                 call    sub_64A970      ; eax = f(bx, dx=>#S/N:signature) (???)
0064AD55                                         ; ebx now => an even more scrambled version of >#L/K
0064AD5A                 lea     ecx, [esp+20h+var_8] ; ecx => var_8
0064AD5E                 push    edi             ; push => #S/N
0064AD5F                 push    ecx             ; push var_8
0064AD60                 mov     ecx, ebx        ; ecx => .SC>#L/K (@esi+24h)
0064AD62                 call    sub_64AB50      ; sub_64AB50(& var_8, [esi+4]=#S/N)
0064AD62                                         ; where var_8 now contains a scrambled version
0064AD62                                         ; of >#L/K (together with var_4)
0064AD67                 mov     ecx, [esp+28h+var_4] ; \
0064AD67                                         ;   ecx, eax = SC >#L/K
0064AD6B                 mov     eax, [esp+28h+var_8] ; /
0064AD6F                 xor     ecx, [esi+8]    ; XOR 1st half of SC>#L/K with 1st half of #S/N
0064AD72                 add     esp, 18h
0064AD75                 xor     eax, [edi]      ; XOR 2nd half of SC>#L/K with 2nd half of #S/N
0064AD77                 pop     edi
0064AD78                 pop     ebx
0064AD79                 jnz     short loc_64AD88 ; Maybe eax is supposed to be zero if the correct License key
0064AD79                                         ; is entered, if not; clear it, and move esp additional 8
0064AD79                                         ; steps forward in memory (down)
0064AD7B                 test    ecx, ecx        ; Test whether the other half of the XORed value is zero
0064AD7D                 jnz     short loc_64AD88 ; and move esp an additional 8 steps forward and 
0064AD7D                                         ; if not...
0064AD7F                 mov     eax, 1
0064AD84                 add     esp, 8
0064AD87                 retn
0064AD88 ; ---------------------------------------------------------------------------
0064AD88 
0064AD88 loc_64AD88:                             ; CODE XREF: sub_64AD00+79j
0064AD88                                         ; sub_64AD00+7Dj
0064AD88                 xor     eax, eax
0064AD8A                 add     esp, 8
0064AD8D                 retn
0064AD8D sub_64AD00      endp
