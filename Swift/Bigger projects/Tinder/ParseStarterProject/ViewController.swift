/**
* Copyright (c) 2015-present, Parse, LLC.
* All rights reserved.
*
* This source code is licensed under the BSD-style license found in the
* LICENSE file in the root directory of this source tree. An additional grant
* of patent rights can be found in the PATENTS file in the same directory.
*/

import UIKit
import Parse
import Macaw

class ViewController: TinderBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let imagesToUpload = ["01be3d395dd399cb7a1780083126f30b_L.jpg",
                              "IMG_9686.jpg",
                              "4816002659_83670fcf4b_b.jpg",
                              "Iceland6.jpg",
                              "Asdis-Ran.jpg",
                              "kate-upton.jpg",
                              "Bientot-la-suppression-de-Miss-Islande.jpg",
                              "maxresdefault.jpg",
                              "Halla-Vilhjalmsdottir.jpg",
                              "top-10-sexiest-women-in-iceland-2015-10.jpg"]
        let userNames = ["TindraK33", "Heppla34", "Ðuþarinn", "Krista12", "Svæmppi", "Dragomirr", "Fjatlla", "Ðreía", "Úrsúla1111", "✌️🍺Innsigli✌️"]
        let passwords = ["user1", "user2", "user3", "user4", "user5", "user6", "user7", "user8", "user9", "user10"]
         if imagesToUpload.count == userNames.count, userNames.count == passwords.count {
            for i in 0..<imagesToUpload.count {
                let currentUserEntry = PFUser()
                let userACL = PFACL()
                userACL.getPublicWriteAccess = true
                userACL.getPublicReadAccess = true
                currentUserEntry.acl = userACL
                currentUserEntry["isFemale"] = true
                currentUserEntry["interestedInWomen"] = false
                
                currentUserEntry.username = userNames[i]
                currentUserEntry.password = passwords[i]
                let imageData = UIImageJPEGRepresentation(UIImage(named: imagesToUpload[i])!, 0.9)
                currentUserEntry["Photo"] = PFFile(name: "profileImage.jpeg", data: imageData!)!
                currentUserEntry.signUpInBackground { (success, error) in
                    if error != nil {
                        if let errorContainer = error as? NSError {
                            self.createAlert(title: "Submission failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                        } else {
                            self.createAlert(title: "Submission failure", message: "Please try again later.")
                        }
                    } else {
                        print("User \(userNames[i]) signed up")
                    }
                }
               
                
            }
        }
        
        /*
        let testObject = PFObject(className: "TestObject2")
        testObject["foo"] = "bar"
        testObject.saveInBackground { (success, error) -> Void in
            // added test for success 11th July 2016
            if success {
                print("Object has been saved.")
            } else {
                if error != nil {
                    print (error ?? "(no error description was provided)")
                } else {
                    print ("Error")
                }
            }
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
