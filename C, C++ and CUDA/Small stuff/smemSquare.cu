#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <time.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

#define BDIMX 32
#define BDIMY 32
#define IPAD  1

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/


#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

void printData(char *msg, int *in,  const int size, const int pColor)
{
   colors_t colors;
   colors.setFg(hYellow);
   std::cout << msg;
   colors.setFg(White); std::cout << ":" << std::endl;
   int hColor(hCyan), dColor(dCyan);
   switch(pColor)
   {
   case dBlue :
   case hBlue :
      dColor = dBlue;
      hColor = hBlue;
      break;
   case dGreen :
   case hGreen :
      dColor = dGreen;
      hColor = hGreen;
      break;
   case dRed :
   case hRed :
      dColor = dRed;
      hColor = hRed;
      break;
   case dMagenta :
   case hMagenta :
      dColor = dMagenta;
      hColor = hMagenta;
      break;
   case dYellow :
   case hYellow :
      dColor = dYellow;
      hColor = hYellow;
      break;
   case dGray :
   case hGray :
      dColor = dGray;
      hColor = hGray;
      break;
   case White :
      dColor = hGray;
      hColor = White;
   }
   for (int i = 0; i < size; ++i)
   {
      if(colors.getFg()==dColor) colors.setFg(hColor);
      else colors.setFg(dColor);
      std::cout << std::setw(5) << in[i] << std::flush;
   }
   colors.setFg(hGray); std::cout << std::endl;
   return;
}


__global__ void setRowReadRow(int *out)
{
   /* Stastic shared memory */
   __shared__ int tile[BDIMY][BDIMX];
   
   /* Mapping from thread index to global memrory index */
   unsigned int idx = threadIdx.y*blockDim.x + threadIdx.x;
   
   /* Shared memory store operation */
   tile[threadIdx.y][threadIdx.x] = idx;
   
   /* Wait for all threads to complete */
   __syncthreads();
   
   /* Shared memory load operation */
   out[idx] = tile[threadIdx.y][threadIdx.x];
}




__global__ void setColReadCol(int *out)
{
   /* Static shared memory */
   __shared__ int tile[BDIMX][BDIMY];
   
   /* Mapping from thread index to global memory index */
   unsigned int idx = threadIdx.y*blockDim.x + threadIdx.x;
   
   /* Shared memory store operation */
   tile[threadIdx.x][threadIdx.y] = idx;
   
   /* Wait for all threads to complete */
   __syncthreads();
   
   /* Shared memory load operation */
   out[idx] = tile[threadIdx.x][threadIdx.y];
}

__global__ void setRowReadCol (int *out)
{
   /* Static shared memory */
   __shared__ int tile[BDIMX][BDIMY];
   
   /* Mapping from thread index to global memory index */
   unsigned int idx = threadIdx.y*blockDim.x + threadIdx.x;
   
   /* Shared memory store operation */
   tile[threadIdx.y][threadIdx.x] = idx;
   
   /* Wait for all threads to complete */
   __syncthreads();
   
   /* Shared memory load operation */
   out[idx] = tile[threadIdx.x][threadIdx.y];
}

__global__ void setRowReadColDyn (int *out)
{
   /* Dynamic shared memory */
   
   /* The extern classifier together with the [] brackets
      allows the size to be set at the kernel call as an extra
      parameter inside the <<<grid, size, ... >>> brackets of
      the call.
      */
   extern __shared__ int tile[];
   
   /* Mapping from thread index to global memory index */
   unsigned int row_idx = threadIdx.y * blockDim.x + threadIdx.x;
   unsigned int col_idx = threadIdx.x * blockDim.y + threadIdx.y;
   
   /* Shared memory store operation */
   tile[row_idx] = row_idx;
   
   /* Wait for all threads to complete */
   __syncthreads();
   
   /* Shared memory load operation */
   out[row_idx] = tile[col_idx];
}

__global__ void setRowReadColPad (int *out)
{
   /* Static shared memory */
   __shared__ int tile[BDIMY][BDIMX+IPAD];
   /* Mapping from thread index to global memory offset */
   unsigned int idx = threadIdx.y * blockDim.x + threadIdx.x;
   
   /* Shared memory store operation */
   tile[threadIdx.y][threadIdx.x] = idx;
   
   /* Wait for all threads to complete */
   __syncthreads();
   
   /* Shared memory load operation */
   out[idx] = tile[threadIdx.x][threadIdx.y];
}

__global__ void setRowReadColDynPad (int *out)
{
   /* Dynamic shared memory */
   extern __shared__ int tile[];
   
   /* Mapping from thread index to global memory index */
   unsigned int row_idx = threadIdx.y * (blockDim.x + IPAD) + threadIdx.x;
   unsigned int col_idx = threadIdx.x * (blockDim.x + IPAD) + threadIdx.y;
   
   unsigned int g_idx   = threadIdx.y * blockDim.x + threadIdx.x;
   
   /* Shared memory store operation */
   tile[row_idx] = g_idx;
   
   /* Wait for all threads to complete */ 
   __syncthreads();
   
   /* Shared memory load operation */
   out[g_idx] = tile[col_idx];
}


int main(int argc, char **argv)
{
   colors_t colors;
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));
   
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", row-major vs column major access of shared memory.";
   std::cout << std::endl;

   cudaSharedMemConfig pConfig;
   CHECK(cudaDeviceGetSharedMemConfig(&pConfig));
   colors.setFg(hGray); std::cout << "Bank mode: ";
   colors.setFg(dYellow); std::cout << (pConfig == 1)? "4-Byte" : "8-byte";
   colors.setFg(hGray); std::cout << std::endl;
   
   /* Set up array size */
   int nx  = BDIMX;
   int ny  = BDIMY;
   int64 nxy = static_cast<int64>(nx)*ny;
   
   size_t nBytes = nx*ny*sizeof(int);
   
   
   /* Configuration of execution */
   int blockx(32), blocky(32);
   dim3 block (blockx, blocky);
   dim3 grid ((nx+block.x-1)/block.x, (ny+block.y-1)/block.y);
   
   
   /* Malloc device memory */
   int *d_C;
   CHECK(cudaMalloc((int **)&d_C, nBytes));
   int *gpuRef = new int[nxy];
   memset(gpuRef,  0, nBytes);
   
   CHECK(cudaMemset(d_C, 0, nBytes));   
   double iStart = cpuSecond();
   setColReadCol<<<grid, block>>>(d_C);
   double iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "setColReadCol ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << " (";
   colors.setFg(dMagenta); std::cout << "G";
   colors.setFg(hMagenta); std::cout << "RI";
   colors.setFg(dMagenta); std::cout << "D";
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   printData("Data sCrC", gpuRef, nx * ny, hCyan);

   CHECK(cudaMemset(d_C, 0, nBytes));   
   iStart = cpuSecond();
   setRowReadRow<<<grid, block>>>(d_C);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "setRowReadRow ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << " (";
   colors.setFg(dMagenta); std::cout << "G";
   colors.setFg(hMagenta); std::cout << "RI";
   colors.setFg(dMagenta); std::cout << "D";
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   printData("Data sRrR", gpuRef, nx * ny, hBlue);

   CHECK(cudaMemset(d_C, 0, nBytes));   
   iStart = cpuSecond();
   setRowReadCol<<<grid, block>>>(d_C);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "setRowReadCol ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << " (";
   colors.setFg(dMagenta); std::cout << "G";
   colors.setFg(hMagenta); std::cout << "RI";
   colors.setFg(dMagenta); std::cout << "D";
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   printData("Data sRrC", gpuRef, nx * ny, hGray);

   CHECK(cudaMemset(d_C, 0, nBytes));   
   iStart = cpuSecond();
   setRowReadColDyn<<<grid, block, nBytes>>>(d_C);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "setRowReadColDyn ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << " (";
   colors.setFg(dMagenta); std::cout << "G";
   colors.setFg(hMagenta); std::cout << "RI";
   colors.setFg(dMagenta); std::cout << "D";
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   printData("Data sRrCDyn", gpuRef, nx * ny, hYellow);

   CHECK(cudaMemset(d_C, 0, nBytes));   
   iStart = cpuSecond();
   setRowReadColPad<<<grid, block>>>(d_C);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "setRowReadColPad ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << " (";
   colors.setFg(dMagenta); std::cout << "G";
   colors.setFg(hMagenta); std::cout << "RI";
   colors.setFg(dMagenta); std::cout << "D";
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   printData("Data sRrCPd", gpuRef, nx * ny, hMagenta);

   CHECK(cudaMemset(d_C, 0, nBytes));   
   iStart = cpuSecond();
   setRowReadColDynPad<<<grid, block, (BDIMX+1)*BDIMY*sizeof(int)>>>(d_C);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "setRowReadColDynPad ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << grid.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << grid.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << ", ";
   colors.setFg(White); std::cout << "(";
   colors.setFg(hGreen); std::cout << block.x;
   colors.setFg(White); std::cout << ", ";
   colors.setFg(hGreen); std::cout << block.y;
   colors.setFg(White); std::cout << ")";
   colors.setFg(hBlue); std::cout << ">>>";
   colors.setFg(White); std::cout << " (";
   colors.setFg(dMagenta); std::cout << "G";
   colors.setFg(hMagenta); std::cout << "RI";
   colors.setFg(dMagenta); std::cout << "D";
   colors.setFg(White); std::cout << ")";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<double>(1000.0*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;

   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   printData("Data sRrCDynPd", gpuRef, nx * ny, hRed);
   
   CHECK(cudaDeviceSynchronize());
   
   /* Free host and device memory */
   CHECK(cudaFree(d_C));
   delete[] gpuRef;
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}
