% Artificial Neural Networks FFR135
% (c) Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2b Solution
%
%

N = 100;
p = 10;
T = 100000;
clear Zeta;
clear m_1;
Zeta = 2*(rand(N,p) > .5) -1;

W_ij = 1/N*Zeta*Zeta';
W_ij = W_ij - diag(diag(W_ij));

S = Zeta(:,1);

beta_0 = 2;
g = @(x)(1/(1+exp(-2*beta_0*x)));
h6 = waitbar(0,'Beraknar ...');
for t = 1:T;
   istep = round(N*rand);
   istep = (istep == 0) + (istep ~= 0)*istep;
   b_i = W_ij*S;
   S(istep) = (rand <= g(b_i(istep))) - (rand > g(b_i(istep)));
   m_1(t) = 1/N*Zeta(:,1)'*S;
   waitbar(t/T, h6);
end
close(h6);
FIGS.fig4 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; [230, 229, 214]; [115, 115, 115]; [176, 169, 165]; [211, 202, 183]]/255;
plot(1:T, m_1, 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
xlabel('\fontsize{12}Time t');
ylabel('\fontsize{12}Monitored order parameter m_1');
title('\fontsize{12}Simulated magnetization over time')

