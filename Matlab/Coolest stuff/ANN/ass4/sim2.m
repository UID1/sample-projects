% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Assignment 4
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

tic
wb1 = waitbar(0,'Initializing ...');
movegui(wb1, 'southeast');
CLwinsize = 1000;
SPwinsize = 10;
DATA = importdata('data_classify.txt');

FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]]/255;



% The weights are for 2 input nodes and 20 output nodes and are to be
% initially uniformly distributed over [-1, 1]

M = 2; % # input nodes
N = 20; % # output nodes/neurons
w_ki = 2*rand(M, N);
% Distance definitions for the neurons
%alpha_i = [ linspace(min(DATA(:,2)), max(DATA(:,2)), N); ...
%            linspace(max(DATA(:,3)), min(DATA(:,3)), N)];

% The competitive learning process

eta_cl = .02;
sigma_cl = 1;
steps_cl = 1e5;
steps_sp = 3000;

for i = 1:steps_cl
   rndSel = randi(length(DATA));
   % The following is essentially equation (1)
   g_i = exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2) ...
		 / sum(exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2));
   i_0 = find(g_i == max(g_i));
   Lambda_ii0 = exp(-((1:N)-i_0).^2/(2*sigma_cl^2));
%   if isnan(prod(prod(w_ki))) || abs(prod(prod(w_ki))) == Inf
%      w_ki
%	  g_i
%	  i_0
%      keyboard
%   end
   deltaw_ki = eta_cl*((ones(M,1)*Lambda_ii0).*(DATA(rndSel, 2:3)'*ones(1,N)-w_ki));
   w_ki = w_ki + deltaw_ki;
   if mod(i, CLwinsize) == 0
      SPC = toc;
      waitbar(i/steps_cl, wb1, sprintf('Training Hakonen network at %.2f iterations/s', CLwinsize/SPC));
	  tic;
   end
end

% The simple perceptron training process

W_i = 2*(rand(N,1)-.5);
beta_0 = .5;
theta = 0;
eta_sp = .1;

for i = 1:steps_sp
   IdxIter = randi(length(DATA));
   g_i = exp(-sum((DATA(IdxIter, 2:3)'*ones(1,N)-w_ki).^2)/2) ...
		 / sum(exp(-sum((DATA(IdxIter, 2:3)'*ones(1,N)-w_ki).^2)/2));
   O_mu = tanh(beta_0*(g_i*W_i-theta));
   derivO_mu = beta_0*(1-O_mu.^2);
   deltaW_i = eta_sp*(((DATA(IdxIter,1)-O_mu)).*derivO_mu.*g_i)';
   W_i = W_i + deltaW_i;
   deltatheta = -eta_sp*sum((DATA(IdxIter,1)-O_mu).*derivO_mu);
   theta = theta + deltatheta;
   if mod(i, SPwinsize) == 0
      SPC = toc;
      waitbar(i/steps_sp, wb1, sprintf('Training Hakonen network at %.2f iterations/s', SPwinsize/SPC));
	  tic;
   end
end
close(wb1);

% Provoke the system to return values given that the weights are set
% in a Monte-Carlo simulation

SimSize = 1000;
O_MC = ones(SimSize,1);
INP = [(max(DATA(:,2))-min(DATA(:,2)))*rand(SimSize,1)+min(DATA(:,2)) ...
           (max(DATA(:,3))-min(DATA(:,3)))*rand(SimSize,1)+min(DATA(:,3)) ];
for i = 1:SimSize
   g_i = exp(-sum((INP(i,:)'*ones(1,N)-w_ki).^2)/2) ...
		 / sum(exp(-sum((INP(i,:)'*ones(1,N)-w_ki).^2)/2));
   O_MC(i) = tanh(beta_0*(g_i*W_i-theta));
end

TST = [INP(:,1) INP(:,2) sign(O_MC)];
FIGS.fig7 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')

