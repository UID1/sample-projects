currentNN.Weights = bestNNWeights;

fprintf('Setting up parameters for vehicle simulation ...');
VSParams.VehicleParameters.mass  =                              20000;
VSParams.VehicleParameters.gConstant =     9.806653210048415426463522;
VSParams.VehicleParameters.gearVector = [7 5 4 3 2.5 2 1.6 1.4 1.2 1];
VSParams.VehicleParameters.brakeCoolingFactor =                    30;
VSParams.VehicleParameters.deltat =                                .5;
VSParams.VehicleParameters.engineCoef =                          3000;
VSParams.VehicleParameters.brakeCoef =                             40;
VSParams.VehicleParameters.Tamb =                                 283;
VSParams.VehicleParameters.gearDeadTime =                           2;

VSParams.Boundaries.travelDistance = 1000;
VSParams.Boundaries.maxBrakeTemp =    750;
VSParams.Boundaries.minVelocity =       5;
VSParams.Boundaries.maxVelocity =      25;
VSParams.Boundaries.maxSlope =         15;

VSParams.StartValues.startPosition       = 0;
VSParams.StartValues.startVelocity      = 20;
VSParams.StartValues.startBrakeTemp    = 500;
VSParams.StartValues.startBrakePressure  = 0;
VSParams.StartValues.startGear           = 7;
VSParams.StartValues.startAcceleration = NaN;
VSParams.StartValues.lastGearChange =      0;

SlopeGenerationParameters.numSlopes = 5;
SlopeGenerationParameters.flatnesses = [.5 2 5 10 30];
SlopeGenerationParameters.filterFactor = 2;
SlopeGenerationParameters.numElements = 1001;

VSParams.SlopeParameters.deltax                              = 1;
VSParams.SlopeParameters.alphaMax = VSParams.Boundaries.maxSlope;
VSParams.SlopeParameters.angleVector            = zeros(1, ...
                          SlopeGenerationParameters.numElements);

TimeSeries.position      = [];
TimeSeries.velocity      = [];
TimeSeries.acceleration  = [];
TimeSeries.selectedGear  = [];
TimeSeries.brakePressure = [];
TimeSeries.brakeTemp     = [];
TimeSeries.nnInputs      = [];
TimeSeries.nnOutputs     = [];
simStruct.prematureTermination = false;
simStruct.TimeSeries      = TimeSeries;
simStruct.numTimeSteps             = 0;
simStruct.endPosition              = 0;
simStruct.fitness                  = 0;

simulations = repmat(simStruct, 1, SlopeGenerationParameters.numSlopes);

GAParams.FFNNDimensions.hlCols = 7; % Hidden Layer nodes
GAParams.FFNNDimensions.hlRows = 4; % Input nodes + bias node
GAParams.FFNNDimensions.olCols = 2; % Output nodes 
GAParams.FFNNDimensions.olRows = 8; % Hidden Layer nodes + bias node
GAParams.FFNNDimensions.interval.lowerBound = -10;
GAParams.FFNNDimensions.interval.upperBound = 10;

NeuralNetworkParameters.Parameters.sigmoidC = 1;
NeuralNetworkParameters.Weights = currentNN.Weights;
NeuralNetworkParameters.Parameters.upperBound = ...
                                   GAParams.FFNNDimensions.interval.upperBound;
NeuralNetworkParameters.Parameters.lowerBound = ...
                                   GAParams.FFNNDimensions.interval.lowerBound;

FIGS.Colors = [191 219 255; 150 185 255; 112 165 255; 70 124 237; ...
               49 103 219]/255;
FIGS.LineWidths = [2 2 1.5 1.3 1.2];

fprintf('done.\nInitializing slopes ... ');
% Initialize slopes
trainingSlopes = zeros(SlopeGenerationParameters.numSlopes, ...
                       SlopeGenerationParameters.numElements);
xValues = (1:SlopeGenerationParameters.numElements) * ...
           VSParams.SlopeParameters.deltax;
FIGS.fig1 = figure();
hold on;
for i = 1:SlopeGenerationParameters.numSlopes
  trainingSlopes(i, :) = semiStochasticSlopeGenerator( ...
                              VSParams.Boundaries.maxSlope, ...
                              SlopeGenerationParameters.filterFactor, ...
                              SlopeGenerationParameters.flatnesses(i), ...
                              SlopeGenerationParameters.numElements);
   plot(xValues, getSlopeProfile(trainingSlopes(i,:), ...
                                 VSParams.VehicleParameters.deltat), ...
        'Color', FIGS.Colors(i, :), 'LineWidth', FIGS.LineWidths(i));
   VSParams.SlopeParameters.angleVector = trainingSlopes(i, :);
   simulations(i) = vehicleSimulation(VSParams.VehicleParameters, ...
                        VSParams.SlopeParameters, NeuralNetworkParameters, ...
                                    VSParams.Boundaries, VSParams.StartValues);
end
xlim([0 VSParams.Boundaries.travelDistance]);
fprintf('done\n');
title('\fontsize{13}Profiles of validation slopes', 'FontWeight', 'Normal');
xlabel('Travel distance (m)');
ylabel('Slope latitude (m)');

FIGS.fig2 = figure('Position', [550, 250, 800, 600]);
hold on;
for i = 1:SlopeGenerationParameters.numSlopes
  tN = simulations(i).numTimeSteps;
  timePoints = (1:tN) * VSParams.VehicleParameters.deltat;
  xValues = simulations(i).TimeSeries.position(1:tN);
  subplot(4, 1, 1), plot(xValues, simulations(i).TimeSeries.velocity(1:tN), ...
                 'Color', FIGS.Colors(i, :), 'LineWidth', FIGS.LineWidths(i));
  xlim([0 VSParams.Boundaries.travelDistance]);
  ylim([VSParams.Boundaries.minVelocity VSParams.Boundaries.maxVelocity]);
  hold on;
  if simulations(i).TimeSeries.position(tN) < VSParams.Boundaries.travelDistance
    subplot(4, 1, 1), plot(xValues(end), min(simulations(i).TimeSeries.velocity(tN), ...
                 VSParams.Boundaries.maxVelocity), '.', 'MarkerEdgeColor', 'Red', ...
                 'MarkerSize', 12);
  end
  title('\fontsize{13}Results from simulations', 'FontWeight', 'Normal');
  ylabel('Speed (m/s)');
  subplot(4, 1, 2), plot(xValues, simulations(i).TimeSeries.selectedGear(1:tN), ...
                 'Color', FIGS.Colors(i, :), 'LineWidth', FIGS.LineWidths(i));
  ylabel('Gear');
  ylim([1 length(VSParams.VehicleParameters.gearVector)]);
  xlim([0 VSParams.Boundaries.travelDistance]);
  hold on;
  subplot(4, 1, 3), plot(xValues, simulations(i).TimeSeries.brakePressure(1:tN), ...
                 'Color', FIGS.Colors(i, :), 'LineWidth', FIGS.LineWidths(i));
  ylabel('P_b');
  xlim([0 VSParams.Boundaries.travelDistance]);
  hold on;
  subplot(4, 1, 4), plot(xValues, simulations(i).TimeSeries.brakeTemp(1:tN), ...
                 'Color', FIGS.Colors(i, :), 'LineWidth', FIGS.LineWidths(i));
  ylabel('T_b (K)');
  xlabel('Position (m)');
  ylim([VSParams.VehicleParameters.Tamb inf]);
  xlim([0 VSParams.Boundaries.travelDistance]);
  hold on;
end
