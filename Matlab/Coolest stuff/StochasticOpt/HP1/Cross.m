function newChromosomePair = Cross(chromosome1, chromosome2)
  nGenes = size(chromosome1,2); % Both chromosomes must have the same length!
  crossoverPoint = 1 + fix(rand*(nGenes-1));
  coPt = crossoverPoint;
  newChromosomePair(1,:) = [chromosome1(1:coPt) chromosome2(coPt+1:end)];
  newChromosomePair(2,:) = [chromosome2(1:coPt) chromosome1(coPt+1:end)];
end