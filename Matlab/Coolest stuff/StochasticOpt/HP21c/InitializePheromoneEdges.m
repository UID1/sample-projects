function edge = InitializePheromoneEdges(cityLocation)
  nCities = length(cityLocation);
  edge = zeros(nCities);
  nCities = length(cityLocation);
  for i = 1:nCities-1
    for j = i+1:nCities
      edge(i,j) = line([0 0], [0 0]);
      edge(j,i) = edge(i,j);
    end
  end
end