function decodedWeights = decodeChromosome(chromosome, dimensions)
  numElHiddenLayer = dimensions.hlRows*dimensions.hlCols;
  numElOutputLayer = dimensions.olRows*dimensions.olCols;
  hlRawMatrix = reshape(chromosome(1:numElHiddenLayer), ...
                             dimensions.hlRows, dimensions.hlCols);
  olRawMatrix = reshape(chromosome(numElHiddenLayer+1:end), ...
                             dimensions.olRows, dimensions.olCols);
  lBound = dimensions.interval.lowerBound;
  uBound = dimensions.interval.upperBound;
  decodedWeights.hiddenLayer = lBound + (uBound - lBound) * hlRawMatrix;
  decodedWeights.outputLayer = lBound + (uBound - lBound) * olRawMatrix;
end