% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1a Solution
% by Robin Axelsson
%
function m = annealfun1(a, b, c, alpha_0, timeupd)
   sigma_n = randperm(length(a));
   sigma_opt = sigma_n;
   mu_m = randperm(length(b));
   lprcnt = 0;

   CT = sort([cumsum(a(sigma_n(1:(end-1)))) cumsum(b(mu_m))]);
   c_test1 = sort([CT(1) diff(CT)], 'descend'); % = "c_tilde"

   H1 = sum(((c-c_test1).^2./c));
   H=H1;
   m.H_vec = H1;
   TGL = -1; % Toggle flag for alternating config swaps
   time = 0;
   % alpha_0 = 1e-6;

   fprintf('Energy level: ')
   while H > 0 && H > eps(1)
      time = time + 1;
      beta_0 = alpha_0*exp(alpha_0*time);

      TGL=-TGL;   % Alternate between swapping elements in mu and sigma we unroll this in CUDA
      if TGL > 0
         SWP = randi(length(sigma_n), 1, 2);  % Randomize two element indices to swap
         sigma_n([SWP(2) SWP(1)]) = sigma_n([SWP(1) SWP(2)]); % do the swapping of elements
      else
         SWP = randi(length(mu_m), 1, 2);  % Same procedures as for sigma
	      mu_m([SWP(2) SWP(1)]) = mu_m([SWP(1) SWP(2)]); % ...
      end
      CT = sort([cumsum(a(sigma_n(1:(end-1)))) cumsum(b(mu_m))]); % Evaluate the permutation
      c_test2 = sort([CT(1) diff(CT)], 'descend'); % through this formula
      H2 = sum(((c-c_test2).^2./c)); % H2 is the energy of the new system and H1 is of the old
      if H2 < H1 % If the energy of the new system is lower than of the old system
         H = H2; % Update the energy indicator with the new energy
	      sigma_opt = sigma_n; % Set the optimum to the new permutation of sigma
	      mu_opt = mu_m;       % ... and mu
	      c_test1 = c_test2; % And the resulting partitions of the protein chain for that permutation
      else
         if rand < exp(-beta_0*(H2-H1)) % If the new system has higher energy
            H = H2;                     % there is a chance that we update anyway
		      c_test1 = c_test2;
         else
            sigma_n = sigma_opt; % If the change being made doesn't decrease the energy 
            mu_m    = mu_opt;    % then we have to restore the swap back to original state
         end
      end
      H1 = H;
      m.H_vec = [m.H_vec H];
      if mod(time, timeupd) == 0
         fprintf(1, repmat('\b',1,lprcnt));
         lprcnt = fprintf('%d, Time step: %i', H, time);
      end
   end
   m.time = time;
   m.H = H;
   m.mu = mu_opt;
   m.sigma = sigma_opt;
   m.c = c_test1;
   fprintf('\n')
end
