//
//  ViewController.swift
//  API Demo
//
//  Created by Iberius Pred on 01/03/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBAction func submitWeatherQuery(_ sender: Any) {
        self.view.endEditing(true)
        if let queryString = locationTextField.text {
            weatherDescriptionLabel.text = "Fetching weather information, please hold the line ..."
            let queryURL = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(queryString.replacingOccurrences(of: " ", with: "%20")),no&appid=b50307df4eea3a09a5bbcb4aac94cd88")!
            let queryTask = URLSession.shared.dataTask(with: queryURL) { (retrievedData, URLResponse, error) in
                if error != nil {
                    print(error ?? "Error: Error with no description")
                    print(URLResponse ?? "No response description.")
                } else {
                    if let urlContent = retrievedData {
                        do {
                            // Parse the retrieved data as a JSON
                            if let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: .mutableContainers) as? [String: Any], let weatherDetails = jsonResult["weather"] as? [[String: Any]], let weatherDescription = weatherDetails[0]["description"] as? String {
                                self.weatherDescriptionLabel.text = weatherDescription
                            } else {
                                print("Could not fetch weather details. Perhaps service provider has changed JSON format.")
                            }
                        } catch {
                            print("Parsing data as a JSON throws an exception.")
                        }
                    } else {
                        print("No data, server response: \(URLResponse)")
                    }
                }
            }
            queryTask.resume()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=Oslo,no&appid=b50307df4eea3a09a5bbcb4aac94cd88")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error ?? "Failed to report the error.")
            } else {
                if let urlContent = data {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: Any]
                        print(jsonResult)
//                        if let description = jsonResult["weather"][0]["description"] {
//                            
//                        }
                        print("City: \(jsonResult["name"])")
                        if let weatherDetails = jsonResult["weather"] as? [[String: Any]] {
                            print("Weather description: \(weatherDetails[0]["description"] ?? "No description.")")
                        } else {
                            print("Unable to subcast JSON with subkey 'weather'")
                        }
                        
                    } catch {
                        print("Error parsing JSON.")
                    }
                }
            }
            
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Make the keyboard close when not editing
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


}


/* This is the structure of the openweather forecasting JSON object. The first container, i.e. root container is a pure dictionary.
 So the first casting step could be a [String:Any]. The object retrieved will then be a dictionary. We want to access the items
 founc underneath weather. The container encapsulating the contents beneath the keyword "weather" is an Array container with only
 1 element. So the next casting should be [[String:Any]] where the outer square brackets denote the Array container. Let's assume
 that the name of the object from these two casting steps is secondaryCast, then the contents underneath the "description" is accessed
 as secondaryCast[0]["description"].
 
 In short:
 
 if let firstCast = retrievedJSONObject as? [String:Any], secondCast["weather"] = [[String:Any]] {
    print(secondCast[0]["description"])
 }
 
{ -- Dictionary
    "coord":
        { -- (Sub)Dictionary
            "lon":10.75
            "lat":59.91
        },
    "weather":
        [ -- (Sub)Array
            { -- (SubSub)Dictionary
                "id":802,
                "main":"Clouds",
                "description":"scattered clouds",
                "icon":"03n"
            }
        ],
    "base":"stations",
    "main":
        { -- (Sub)Dictionary
            "temp":271.136,
            "pressure":969.44,
            "humidity":85,
            "temp_min":271.136,
            "temp_max":271.136,
            "sea_level":998.63,
            "grnd_level":969.44
        },
    "wind":
        { -- (Sub)Dictionary
            "speed":1.16,
            "deg":176.501
        },
 "clouds":
    { -- (Sub)Dictionary
        "all":32
    },
        "dt":1488389686,
    "sys":
    { -- (Sub)Dictionary
        "message":0.0023,
        "country":"NO",
        "sunrise":1488348770,
        "sunset":1488386798
    },
        "id":3143244,
        "name":"Oslo",
        "cod":200
 }
 
 */

