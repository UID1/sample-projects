%
%
%  Laboration i mekanik 2 2013
%  Stekroppssimulering av
%  asteroiden Toutatis
%
%

% Nu unders�ker vi periodiciteten i v�r serie

% <numera borttagen plot>

% Plotten ovan som �r en fourieranalys av serien visar att vi har en
% periodtid p� c:a 5 dygn. Den observerade periodtiden var 7.35 dygn
% hur kan vi justera den modellerade kroppens massa f�r att f� samma
% periodtid? Kanske kan vi justera den med m*7.35/5 ?

% �h, vi skiter i Fourier och provar en annan enklare metod:

tau_ass = 2*abs(T_ass(find((min(w_ass(:,1)) == w_ass(:,1)),1),1) ...
          -T_ass(find((max(w_ass(:,1)) == w_ass(:,1)),1),1));

tau_sym = 2*abs(T_s(find((min(w_s(:,1)) == w_s(:,1)),1),1) ...
          -T_s(find((max(w_s(:,1)) == w_s(:,1)),1),1));

% F�rklaring: F�rst best�mmer vi max respektive minv�rde f�r serien.
% Sedan lokaliserar vi det index f�r n�r f�rsta max intr�ffar och
% sedan index f�r n�r det f�rsta min intr�ffar. Dessa index stoppar
% vi in i v�r tidsvektor och tar differensen mellan dessa tv� tid-
% punkter. Vi vet att en ren sinussv�ngning g�r fr�n max till min
% under halva dess periodtid, d�rf�r �r v�r periodtid tv� g�nger
% beloppet p� v�r tidsdifferens.

% Metoden ovan visar sig inte vara tillr�ckligt stabil f�r att ge
% p�litliga skattningar p� periodtid. Topparna p� sinussv�ngningen
% fluktuerar s� det �r inte givet att max hamnar p� f�rsta sinustopp.
% Man skulle kunna fixa detta genom att f�rst multiplicera serien med
% en elementvis stadigt avtagande funktion.

% Jag v�ljer dock en annan metod: Jag m�ter tidsavst�ndet mellan de tv�
% f�rsta g�ngerna sinussv�ngningen passerar medelpunkten. Resultatet
% blev f�ljande funktion

tau_sym = 2*abs(diff(T_s(find([(w_s(1,1) < mean(w_s(:,1)))' ...
          (w_s(:,1) < mean(w_s(:,1)))'] - [(w_s(:,1) < ...
	  mean(w_s(:,1)))' (w_s(1,end) < mean(w_s(:,1)))'], 2))));

tau_sym2 = 2*abs(diff(T_s(find([(w_s(1,1) < mean(w_s(:,1)))' ...
           (w_s(:,1) < mean(w_s(:,1)))'] - [(w_s(:,1) < ...
	   mean(w_s(:,1)))' (w_s(1,end) < mean(w_s(:,1)))'], 3))));

tau_sym3 = 2*abs(diff(T_s(find([(w_s(1,2) < mean(w_s(:,2)))' ...
           (w_s(:,2) < mean(w_s(:,2)))'] - [(w_s(:,2) < ...
	   mean(w_s(:,2)))' (w_s(1,end) < mean(w_s(:,2)))'], 3))));

tau_sym4 = 2*abs(diff(T_s(find([(w_s(1,3) < mean(w_s(:,3)))' ...
           (w_s(:,3) < mean(w_s(:,3)))'] - [(w_s(:,3) < ...
	   mean(w_s(:,3)))' (w_s(1,end) < mean(w_s(:,3)))'], 3))));


% Svaret �r p� fr�gan i det �versta stycket �r att periodtiden �r
% oberoende av massan, givet observerade startv�rden p� vinkelrota-
% tionerna. D�remot s� blir periodtiden olika beroende p� hur tr�g-
% hetsmomenten skiljer sig relativt varandra.


% I f�ljande for-loop tittar vi hur periodtiden p�verkas f�r olika
% v�rden p� principaltr�gheten p� kortaxlarna relativt l�ngaxeln hos
% den symmetriska kroppen och hos den assymetriska kroppen. F�r den
% assymetriska kroppen l�ter vi den minsta principalaxeln (den n�r-
% mast l�ngaxeln) vara konstant medan vi varierar en av de st�rre
% axlarna och studerar hur detta p�verkar periodtiden.

Tisym = zeros(1,200);
Tiass = zeros(1,200);
symmin = Inf;
asymin = Inf;
options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4 1e-5]);
for i = 1:200
   I_sym = m*[k_0*(.8 + .01*i) k_0*(.8 + .01*i) k_zeta];
   I_p = I_sym;
   [T_s,w_s] = ode45(@wdot,[0 480],w_0,options);
   Tisym(i) = 2*abs(diff(T_s(find([(w_s(1,1) < mean(w_s(:,1)))' ...
          (w_s(:,1) < mean(w_s(:,1)))'] - [(w_s(:,1) < ...
	  mean(w_s(:,1)))' (w_s(1,end) < mean(w_s(:,1)))'], 2))));
   symdist = abs(Tisym(i) - 7.35);
   if symdist < symmin
      clear T_smin w_smin I_smin
      T_smin = T_s;
      w_smin = w_s;
      I_smin = I_sym;
   end
   I_ass = 2/5*m*([1+.01*i 2.5 1]*(2500^2+1500^2));
   I_p = I_ass;
   [T_s,w_s] = ode45(@wdot,[0 960],w_0,options);
   tiass1 = 2*abs(diff(T_s(find([(w_s(1,1) < mean(w_s(:,1)))' ...
           (w_s(:,1) < mean(w_s(:,1)))'] - [(w_s(:,1) < ...
	   mean(w_s(:,1)))' (w_s(1,end) < mean(w_s(:,1)))'], 3))));
   tiass2 = 2*abs(diff(T_s(find([(w_s(1,2) < mean(w_s(:,2)))' ...
           (w_s(:,2) < mean(w_s(:,2)))'] - [(w_s(:,2) < ...
	   mean(w_s(:,2)))' (w_s(1,end) < mean(w_s(:,2)))'], 3))));
   tiass3 = 2*abs(diff(T_s(find([(w_s(1,3) < mean(w_s(:,3)))' ...
           (w_s(:,3) < mean(w_s(:,3)))'] - [(w_s(:,3) < ...
	   mean(w_s(:,3)))' (w_s(1,end) < mean(w_s(:,3)))'], 3))));
   Tiass(i) =  min([mean(tiass1); mean(tiass2); mean(tiass3)]);
   asydist = abs(Tiass(i) - 7.35);
   if (asydist < asymin) & (I_ass(1) > I_ass(3))
      clear T_amin w_amin I_amin
      T_amin = T_s;
      w_amin = w_s;
      I_amin = I_ass;
   end

end

% If-satserna i for-loopen plockar allts� ut den kropp som ger en
% periodtid som ligger n�rmast den f�r Toutatis; en bland de
% symmetriska kandidaterna och en bland de asymmetriska kandidaterna.
% Dessa 'vinnare' skall vi unders�ka n�rmare lite l�ngre ner i koden
% efter att vi har visualiserat de olika kandidaternas periodtider
% med avseende p� deras olika tr�ghetsmoment.

FIGS.fig5 = figure('Color', [1 1 1]);
plot(k_0*(.8 + .01*(1:200)), Tisym/24, ...
     'LineWidth', 1.5,'Color', FIGS.LineColors(2, :))
hold on
% Den v�gr�ta linjen markerar periodtiden 7.35 dagar f�r asteroiden
% Toutatis
plot(k_0*(.8 + .01*[1 200]), [7.35 7.35], ...
      'LineWidth', 1.5, 'Color', FIGS.LineColors(7, :))
title('\fontsize{12}Periodtider f�r den symmetriska kroppen')
ylabel('\fontsize{12}\it\tau\rm  (\itdygn\rm)')
xlabel('\fontsize{12}Tr�ghetsmoment \itI_{0} \rm(\itkgm^2\rm)')
box off
xlim(k_0*(.8 + .01*[1 200]));
ylim([min(Tisym), max(Tisym)]/24);

FIGS.fig6 = figure('Color', [1 1 1]);
plot(2/5*m*(1+.1*(1:200)*(2500^2+1500^2)), Tiass/24, ...
     'LineWidth', 1.5,'Color', FIGS.LineColors(2, :))
hold on
% Den v�gr�ta linjen markerar �ven h�r periodtiden 7.35 dagar
% f�r asteroiden Toutatis
plot(2/5*m*(1+.1*[1 200]*(2500^2+1500^2)), [7.35 7.35], ...
      'LineWidth', 1.5, 'Color', FIGS.LineColors(7, :))
title('\fontsize{12}Periodtider f�r den asymmetriska kroppen')
ylabel('\fontsize{12}\it\tau\rm  (\itdygn\rm)')
xlabel('\fontsize{12}Tr�ghetsmoment \itI_{\xi}\rm  (\itkgm^2\rm)')
box off
xlim(2/5*m*(1+.1*[1 200]*(2500^2+1500^2)));
ylim([min(Tiass), max(Tiass)]/24);

% Vi unders�ker nu r�relsen hos v�ra tv� vinnande kandidater,
% proceduren �r densamma som i f�reg�ende matlabfil

H_amin = zeros(1,length(w_amin));
H_smin = zeros(1,length(w_smin));
Hv_amin = zeros(3,length(w_amin));
Hv_smin = zeros(3,length(w_smin));
for i = 1:length(w_amin)
    H_amin(i) = norm(diag(I_amin)*w_amin(i,:)');
    Hv_amin(:,i) = diag(I_amin)*w_amin(i,:)';
end
for i = 1:length(w_smin)
    H_smin(i) = norm(diag(I_smin)*w_smin(i,:)');
    Hv_smin(:,i) = diag(I_smin)*w_smin(i,:)';
end
Hv_amin = Hv_amin';
Hv_smin = Hv_smin';

FIGS.fig7 = figure('Color', [1 1 1]);
plot3(w_amin(:,1),w_amin(:,2), w_amin(:,3), 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Rotation hos den asymmetriska passningen')
xlabel('\fontsize{12}\it\omega_{\xi}\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\omega_{\eta}\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\omega_{\zeta}\rm', 'interpreter', 'tex')
FIGS.fig8 = figure('Color', [1 1 1]);
plot3(w_smin(:,1),w_smin(:,2), w_smin(:,3), 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(2, :))
title('\fontsize{12}Rotation hos den symmetriska passningen')
xlabel('\fontsize{12}\it\omega_{\xi}\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\omega_{\eta}\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\omega_{\zeta}\rm', 'interpreter', 'tex')
FIGS.fig9 = figure('Color', [1 1 1]);
plot3(w_amin(:,1)/norm(w_amin(:,1)),w_amin(:,2)/norm(w_amin(:,2)), ...
      w_amin(:,3)/norm(w_amin(:,3)), 'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(1, :))
hold on
plot3(Hv_amin(:,1)/norm(Hv_amin(:,1)), Hv_amin(:,2)/norm(Hv_amin(:,2)), ...
      Hv_amin(:,1)/norm(Hv_amin(:,1)), 'LineWidth', 2, ...
      'Color', FIGS.LineColors(7, :))
title('\fontsize{12}Storhetsriktningar hos den assymetriska passningen');
FIGS.leg3 = legend(['$\it \hat{\omega}\rm$'], ...
       ['$\it \hat{H}\rm$']);
set(FIGS.leg3,'interpreter', 'latex', 'fontsize', 12, ...
              'Location', 'NorthEast');
xlabel('\fontsize{12}\it\xi\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\eta\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\zeta\rm', 'interpreter', 'tex')
% Vi litar p� att matlab g�r sitt jobb den h�r g�ngen
%xlim([-.2 .2]);
%ylim([-.2 .2]);
%zlim([-.2 .2]);
FIGS.fig4 = figure('Color', [1 1 1]);
plot3(w_smin(:,1)/norm(w_smin(:,1)),w_smin(:,2)/norm(w_smin(:,2)), ...
      w_smin(:,3)/norm(w_smin(:,3)),'LineWidth', 1.5, ...
      'Color', FIGS.LineColors(1, :))
hold on
plot3(Hv_smin(:,1)/norm(Hv_smin(:,1)), Hv_smin(:,2)/norm(Hv_smin(:,2)), ...
      Hv_smin(:,1)/norm(Hv_smin(:,1)), 'LineWidth', 2, ...
      'Color', FIGS.LineColors(7, :))
title('\fontsize{12}Storhetsriktningar hos den symmetriska passningen')
FIGS.leg4 = legend(['$\it \hat{\omega}\rm$'], ...
       ['$\it \hat{H}\rm$']);
set(FIGS.leg4, 'interpreter', 'latex', 'fontsize', 12, ...
               'Location', 'NorthEast');
xlabel('\fontsize{12}\it\xi\rm', 'interpreter', 'tex')
ylabel('\fontsize{12}\it\eta\rm', 'interpreter', 'tex')
zlabel('\fontsize{12}\it\zeta\rm', 'interpreter', 'tex')
% Vi litar p� att matlab g�r sitt jobb den h�r g�ngen
%xlim([-.22 .22]);
%ylim([-.22 .22]);
%zlim([-.22 .22]);


