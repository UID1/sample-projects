% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Assignment 4 - task 2
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

tic % Initialize our clock for performance measurment
wb1 = waitbar(0,'Initializing ...','Color',[1 1 1]);
set(findobj(wb1,'Type','Patch'),'EdgeColor',[.01 .01 .01],'FaceColor',[.1 .1 .3]);
CLwinsize = 100; % Refresh rate of GUIs during competitive learning
SPwinsize = 10; % Refresh rate when iterating the simple perceptron
UP = 2; % Size of stochastic selection of update patterns
numRuns = 2; % Number of runs
trailSize = 1000; % The length of the movement trail in the Kohonen updates
DATA = importdata('data_classify.txt');
REC = false; % This flag toggles video recording of interactive plot
iFrame = 0;

set(0,'units','pixels');
PixSS = get(0,'screensize');
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [55, 36, 138]]/255;
FIGS.MarkerColors = [[12 95 45]; [35 226 75]; [188 255 196]]/255;


% The weights are for 2 input nodes and 5 output nodes and are to be
% initially uniformly distributed over [-1, 1]

M = 2; % # input nodes
N = 20; % # output nodes/neurons

% Initialize our parameters for interactive plot windows

movegui(wb1, 'southeast');

Xmin = min(DATA(:,2)); Xmax = max(DATA(:,2));
Xstep = (max(DATA(:,2))-min(DATA(:,2)))/100;
Ymin = min(DATA(:,3)); Ymax = max(DATA(:,3));
Ystep = (max(DATA(:,3))-min(DATA(:,3)))/100;
FigX = 750; FigY = 600;

% The competitive learning process

eta_cl = .02;
sigma_0cl = .6;
sigma_1cl = .1;
tau_cl = 1000/sigma_0cl;
steps_cl = 1e5;
steps_sp = 3000;

% Some further initialization steps

gETRN = ones(steps_sp, numRuns);%, 'gpuArray');
gEVLD = ones(steps_sp, numRuns);%, 'gpuArray');
gCvTRN = ones(steps_sp, numRuns);%, 'gpuArray');
gCvVLD = ones(steps_sp, numRuns);%, 'gpuArray');
O_mu = ones(UP,1);
Ewin = 1e15; % Set the winning energy to something big

for i_r = 1:numRuns
   iFrame = 0;
   w_ki = 2*rand(M, N);
   trailX = NaN*ones(trailSize, N);
   trailY = NaN*ones(trailSize, N);
   set(wb1, 'Name', sprintf('Run %d of %d', i_r, numRuns));

   % Initialize our interactive window

   if ishandle(FIGS.fig1)
      close(FIGS.fig1);
   end
   FIGS.fig1 = figure('Color', [0 0 0]);
   set(FIGS.fig1, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
   plot(DATA(find(DATA(:,1)==-1),2), DATA(find(DATA(:,1)==-1),3), 'o', 'LineWidth', 1, ... 
                            'markers', 10, 'color', FIGS.LineColors(6,:))
   hold on
   plot(DATA(find(DATA(:,1)==1),2), DATA(find(DATA(:,1)==1),3), '+', 'LineWidth', 1.3, ...
                            'markers', 10, 'color', FIGS.LineColors(5,:));
   %set(gca,'Color',[0 0 0]);
   FIGS.title1 = title('\fontsize{12}Locations of data points');
   FIGS.canvas1 = gca;
   set(FIGS.canvas1, 'Color', [0 0 0], 'XColor', [.85 .80 .65], 'YColor', [.85 .80 .65]);
   set(FIGS.title1, 'Color', [.85 .80 .65]);

   FIGS.plot1 = plot(trailX, trailY, 'color', FIGS.MarkerColors(2,:));

   FIGS.plot2 = plot(w_ki(1,:), w_ki(2,:), '.', 'MarkerSize', 12, 'MarkerEdgeColor', ...
                  FIGS.MarkerColors(3,:), 'MarkerFaceColor', FIGS.MarkerColors(1,:), ...
                       'LineWidth', 1.5);
   FIGS.label1 = xlabel('Initializing ...');
   xlim([Xmin Xmax]);
   ylim([Ymin Ymax]);

   % Iterate our Kohonen competitive learning network
   
   for i = 1:steps_cl
      CLwinsize = (i<=1000)*5+(i>1000)*(i<=2000)*10+(i>2000)*100;
      sigma_cl = (i <= 1e4)*sigma_0cl + (i >1e4)*sigma_1cl*exp(-(i-1e4+1)/tau_cl);
      rndSel = randi(length(DATA));
      % The following is essentially equation (1)
      g_i = exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2) ...
   		 / sum(exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2));
      i_0 = find(g_i == max(g_i));
      Lambda_ii0 = exp(-((1:N)-i_0).^2/(2*sigma_cl^2));
      deltaw_ki = eta_cl*((ones(M,1)*Lambda_ii0).*(DATA(rndSel, 2:3)'*ones(1,N)-w_ki));
      w_ki = w_ki + deltaw_ki;
	   trailX(1:end-1,:) = trailX(2:end,:);
      trailY(1:end-1,:) = trailY(2:end,:);
      trailX(end,:) = w_ki(1,:);
      trailY(end,:) = w_ki(2,:);

      if mod(i, CLwinsize) == 0
         SPC = toc;
		 if ishandle(wb1)
            waitbar(.5*i/steps_cl, wb1, ...
			         sprintf('Computing Kohonen network at %.2f iterations/s', ...
					          CLwinsize/SPC));
			if ishandle(FIGS.fig1)
			   for xx = 1:N
			      set(FIGS.plot1(xx), 'XData', trailX(:,xx), 'YData', trailY(:,xx));
			   end
			   set(FIGS.plot2, 'XData', w_ki(1,:), 'YData', w_ki(2,:));
               set(FIGS.label1, 'String', ...
			       sprintf('\\it \\eta \\rm  = %f, Iteration step: %d', eta_cl, i));
			   drawnow;
			   %if i < 1000
			   %   pause(.5)
			   %end
               if REC
                  iFrame = iFrame + 1;
                  % Matlab's built-in export function works fine here strangely enough
                  %hgexport(FIGS.fig1, sprintf('frame%07.f.png', iFrame), ...
                  %          hgexport('factorystyle'), 'Format', 'png');
                  % But the external export_fig has better anti-aliasing features
                  export_fig(FIGS.fig1, sprintf('frame%07.f.png', iFrame), '-png', '-nocrop');
               end
			end
         end
   	  tic;
      end
   end
   
   % Initialize interactive plot for our percetptron
   
   if ishandle(FIGS.fig1)
      close(FIGS.fig1);
   end
   FIGS.fig1 = figure('Color', [0 0 0]);
   Qx = (Xmin:Xstep:Xmax)'*ones(1,length(Xmin:Xstep:Xmax));
   Qy = meshgrid((Ymin:Ystep:Ymax)',1:length(Ymin:Ystep:Ymax));
   gridVec = [Qx(:) Qy(:)];
   CInit = meshgrid(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax);
   contourf(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, CInit, 50, 'LineStyle', 'none');
   colormap(bone)
   hold on
   gridOut = [gridVec randi(2, ((Xmax-Xmin)/Xstep+1)*((Ymax-Ymin)/Ystep+1),1).*2-3];
   contour(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, ...
                reshape(gridOut(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)', ...
   			 [-2 0], '-', 'LineWidth', 3, 'color', FIGS.LineColors(2,:));
   plot(DATA(find(DATA(:,1)==-1),2), DATA(find(DATA(:,1)==-1),3), 'o', 'LineWidth', 1, ... 
                               'markers', 10, 'color', FIGS.LineColors(4,:))
   plot(DATA(find(DATA(:,1)==1),2), DATA(find(DATA(:,1)==1),3), '+', 'LineWidth', 1.3, ...
                               'markers', 10, 'color', FIGS.LineColors(6,:));
   FIGS.plot3 = findall(FIGS.fig1,'Type','hggroup'); % This circumvents a Matlab bug
   set(FIGS.fig1, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
   FIGS.title1 = title('\fontsize{12}Locations of Gaussian nodes and decision boundary');
   FIGS.label1 = xlabel('Initializing ...');

   FIGS.title1 = title('\fontsize{12}Training the simple perceptron step');
   FIGS.canvas1 = gca;
   set(FIGS.canvas1, 'Color', [0 0 0], 'XColor', [.85 .80 .65], 'YColor', [.85 .80 .65]);
   set(FIGS.title1, 'Color', [.85 .80 .65]);
   plot(w_ki(1,:), w_ki(2,:), 'v', 'MarkerSize', 12, 'MarkerEdgeColor', ...
         FIGS.MarkerColors(2,:), 'MarkerFaceColor', FIGS.MarkerColors(1,:), ...
         'LineWidth', 1.5)
   
   % The simple perceptron training process
   
   W_i = 2*(rand(N,1)-.5);
   beta_0 = .5;
   theta = 0;
   eta_sp = .1;
   % Randomly split the input data into a training and validation matrix
   TRNidx = randperm(length(DATA), floor(length(DATA)*.7));
   TRN = DATA(TRNidx, :);
   VLD = DATA;
   VLD(TRNidx,:) = [];
   diffDwTRN = ones(length(TRN), N, M);%, 'gpuArray');
   diffDwVLD = ones(length(VLD), N, M);%, 'gpuArray');
   CNO_muTRN = ones(length(TRN), N);%, 'gpuArray');
   CNO_muVLD = ones(length(VLD), N);%, 'gpuArray');
   PSO_muTRN = ones(length(TRN), 1);%, 'gpuArray');
   PSO_muVLD = ones(length(VLD), 1);%, 'gpuArray');

   % No performance enhancements with CUDA :(
   gTRN = TRN(:,2:3); %gpuArray(TRN(2:3));
   gVLD = VLD(:,2:3); %gpuArray(VLD(2:3));
   gOTRN = TRN(:,1); %gpuArray(TRN(2:3));
   gOVLD = VLD(:,1); %gpuArray(VLD(2:3));


   
   for i = 1:steps_sp
      SPwinsize = (i<=300)*1+(i>300)*10;
      IdxIter = randperm(length(TRN), UP);
      for j = 1:UP
         g_i(j,:) = exp(-sum((TRN(IdxIter(j), 2:3)'*ones(1,N)-w_ki).^2)/2) ...
                      / sum(exp(-sum((TRN(IdxIter(j), 2:3)'*ones(1,N)-w_ki).^2)/2));
         O_mu(j) = tanh(beta_0*(g_i(j,:)*W_i-theta));
      end
      derivO_mu = beta_0*(1-O_mu.^2);
      deltaW_i = eta_sp*g_i'*((TRN(IdxIter,1)-O_mu).*derivO_mu);
      W_i = W_i + deltaW_i;
      deltatheta = -eta_sp*sum((TRN(IdxIter,1)-O_mu)'*derivO_mu);
      theta = theta + deltatheta;
      for ii = 1:size(w_ki, 2)
        for kk = 1:size(w_ki, 1)
           diffDwTRN(:,ii,kk) = (gTRN(:,kk)-w_ki(kk,ii)).^2;
           diffDwVLD(:,ii,kk) = (gVLD(:,kk)-w_ki(kk,ii)).^2;
   	    end
      end
      
      % Evaluating classification error and energy level
      CNO_muTRN = exp(-sum(diffDwTRN,3)/2)./repmat(sum(exp(-sum(diffDwTRN,3)/2), 2), 1, N);
      CNO_muVLD = exp(-sum(diffDwVLD,3)/2)./repmat(sum(exp(-sum(diffDwVLD,3)/2), 2), 1, N);
      PSO_muTRN = tanh(beta_0*(CNO_muTRN*W_i-theta));
      PSO_muVLD = tanh(beta_0*(CNO_muVLD*W_i-theta));
   
      gETRN(i,i_r) = .5*sum((gOTRN(:,1)-PSO_muTRN(:)).^2);
      gEVLD(i,i_r) = .5*sum((gOVLD(:,1)-PSO_muVLD(:)).^2);
      gCvTRN(i,i_r) = mean(abs(gOTRN(:,1) - sign(PSO_muTRN)));
      gCvVLD(i,i_r) = mean(abs(gOVLD(:,1) - sign(PSO_muVLD)));
   
      if mod(i, SPwinsize) == 0
         SPC = toc;
		 if ishandle(wb1)
            waitbar(.5+.5*i/steps_sp, wb1, ...
			        sprintf('Training simple perceptron at %.2f iterations/s', ...
			                 SPwinsize/SPC));
		 end
   	     tic;
		 if ishandle(FIGS.fig1)
            O_gd = ones(length(gridVec),1);
            O_sf = ones(length(gridVec),1);
            for j = 1:length(gridVec)
               g_i = exp(-sum((gridVec(j,:)'*ones(1,N)-w_ki).^2)/2) ...
            		 / sum(exp(-sum((gridVec(j,:)'*ones(1,N)-w_ki).^2)/2));
               O_gd(j) = tanh(beta_0*(g_i*W_i-theta));
               O_sf(j) = beta_0*(g_i*W_i-theta);
            end
            gridOut = [gridVec(:,1) gridVec(:,2) sign(O_gd)];
            CMesh = reshape(gridOut(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
            DMesh = reshape(O_gd, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
            EMesh = reshape(O_sf, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
            set(FIGS.plot3(1), 'ZData', DMesh);
            set(FIGS.plot3(2), 'ZData', EMesh, 'LevelList', ...
                         linspace(min(min(EMesh)), max(max(EMesh)), 51));
						 
            set(FIGS.label1, 'String', ...
			sprintf('\\it C_v\\rm  = %f, \\it \\eta \\rm  = %f, Iteration step: %d', ...
                      gCvTRN(i,i_r), eta_sp, i));
            drawnow;
            if REC
               iFrame = iFrame + 1;
               % Matlab's built-in export function works fine here strangely enough
               %hgexport(FIGS.fig1, sprintf('frame%07.f.png', iFrame), ...
               %          hgexport('factorystyle'), 'Format', 'png');
               % But the external export_fig has better anti-aliasing features
               export_fig(FIGS.fig1, sprintf('frame%07.f.png', iFrame), '-png', '-nocrop');
            end
         end
   	  end
   end
   % Choosing a winner for task 1c
   if Ewin > gETRN(end,i_r)
      Ewin = gETRN(end,i_r);
      w_kiwin = w_ki;
	  W_iwin = W_i;
	  i_rwin = i_r;
   end
end
if ishandle(wb1)
   close(wb1);
end

% Provoke the system to return values given that the weights are set
% in a Monte-Carlo simulation

SimSize = 1000;
O_MC = ones(SimSize,1);
INP = [(max(DATA(:,2))-min(DATA(:,2)))*rand(SimSize,1)+min(DATA(:,2)) ...
           (max(DATA(:,3))-min(DATA(:,3)))*rand(SimSize,1)+min(DATA(:,3)) ];
for i = 1:SimSize
   g_i = exp(-sum((INP(i,:)'*ones(1,N)-w_kiwin).^2)/2) ...
		 / sum(exp(-sum((INP(i,:)'*ones(1,N)-w_kiwin).^2)/2));
   O_MC(i) = tanh(beta_0*(g_i*W_iwin-theta));
end

TST = [INP(:,1) INP(:,2) sign(O_MC)];
FIGS.fig2 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')

FIGS.fig3 = figure('Color', [1 1 1]);
if size(gCvTRN,2) > 1
   surf(gCvTRN, 'EdgeColor', 'None')
   view(135, 25);
   xlabel('\fontsize{12}Runs');
   ylabel('\fontsize{12}Steps');
   zlabel('\fontsize{12}Classification error  \itC_v\rm');
else
   plot(gCvTRN, 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
   xlabel('\fontsize{12}Steps');
   ylabel('\fontsize{12}Classification error  \itC_v\rm');
end
title('\fontsize{12}Training data')

FIGS.fig4 = figure('Color', [1 1 1]);
if size(gCvVLD,2) > 1
   surf(gCvVLD, 'EdgeColor', 'None')
   view(135, 25);
   xlabel('\fontsize{12}Runs');
   ylabel('\fontsize{12}Steps');
   zlabel('\fontsize{12}Classification error  \itC_v\rm');
else
   plot(gCvVLD, 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
   xlabel('\fontsize{12}Steps');
   ylabel('\fontsize{12}Classification error  \itC_v\rm');
end
title('\fontsize{12}Validation data')

FIGS.fig5 = figure('Color', [1 1 1]);
if size(gETRN,2) > 1
   surf(gETRN/(.7*size(DATA,1)), 'EdgeColor', 'None')
   view(135, 25);
   xlabel('\fontsize{12}Runs');
   ylabel('\fontsize{12}Steps');
   zlabel('\fontsize{12}Energy level of the system   \itH\rm');
else
   plot(gETRN/(.7*size(DATA,1)), 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
   xlabel('\fontsize{12}Steps');
   ylabel('\fontsize{12}Energy level of the system   \itH\rm');
end
title('\fontsize{12}Training data')

FIGS.fig6 = figure('Color', [1 1 1]);
if size(gEVLD,2) > 1
   surf(gEVLD/(.3*size(DATA,1)), 'EdgeColor', 'None')
   view(135, 25);
   ylabel('\fontsize{12}Steps');
   zlabel('\fontsize{12}Energy level of the system   \itH\rm');
   xlabel('\fontsize{12}Runs');
else
   plot(gEVLD/(.3*size(DATA,1)), 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
   xlabel('\fontsize{12}Steps');
   ylabel('\fontsize{12}Energy level of the system   \itH\rm');
end
title('\fontsize{12}Validation data')

if ishandle(FIGS.fig1)
   close(FIGS.fig1);
end
FIGS.fig1 = figure('Color', [1 1 1]);

Qx = (Xmin:Xstep:Xmax)'*ones(1,length(Xmin:Xstep:Xmax));
Qy = meshgrid((Ymin:Ystep:Ymax)',1:length(Ymin:Ystep:Ymax));
gridVec = [Qx(:) Qy(:)];
CInit = meshgrid(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax);
contourf(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, CInit, 50, 'LineStyle', 'none');
colormap(bone)
hold on
gridOut = [gridVec randi(2, ((Xmax-Xmin)/Xstep+1)*((Ymax-Ymin)/Ystep+1),1).*2-3];
contour(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, ...
             reshape(gridOut(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)', ...
			 [-2 0], '-', 'LineWidth', 3, 'color', FIGS.LineColors(2,:));
plot(DATA(find(DATA(:,1)==-1),2), DATA(find(DATA(:,1)==-1),3), 'o', 'LineWidth', 1, ... 
                            'markers', 10, 'color', FIGS.LineColors(4,:))
plot(DATA(find(DATA(:,1)==1),2), DATA(find(DATA(:,1)==1),3), '+', 'LineWidth', 1.3, ...
                            'markers', 10, 'color', FIGS.LineColors(6,:));
FIGS.plot3 = findall(FIGS.fig1,'Type','hggroup'); % This circumvents a Matlab bug

set(FIGS.fig1, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
FIGS.title1 = title('\fontsize{12}Locations of Gaussian nodes and decision boundary');
%FIGS.label1 = xlabel('Initializing ...');
O_gd = ones(length(gridVec),1);
O_sf = ones(length(gridVec),1);
for i = 1:length(gridVec)
   g_i = exp(-sum((gridVec(i,:)'*ones(1,N)-w_kiwin).^2)/2) ...
		 / sum(exp(-sum((gridVec(i,:)'*ones(1,N)-w_kiwin).^2)/2));
   O_gd(i) = tanh(beta_0*(g_i*W_iwin-theta));
   O_sf(i) = beta_0*(g_i*W_iwin-theta);
end
gridOut = [gridVec(:,1) gridVec(:,2) sign(O_gd)];
CMesh = reshape(gridOut(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
DMesh = reshape(O_gd, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
EMesh = reshape(O_sf, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  set(FIGS.plot3(1), 'ZData', DMesh);
	  set(FIGS.plot3(2), 'ZData', EMesh, 'LevelList', ...
	                        linspace(min(min(EMesh)), max(max(EMesh)), 51));
drawnow;
plot(w_ki(1,:), w_ki(2,:), 'v', 'MarkerSize', 12, 'MarkerEdgeColor', ...
     FIGS.MarkerColors(2,:), 'MarkerFaceColor', FIGS.MarkerColors(1,:), ...
	 'LineWidth', 1.5)
