//
//  UserDetailsViewController.swift
//  Tinder
//
//  Created by Iberius Pred on 05/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import Parse

class UserDetailsViewController: TinderBaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var userImage: UIImageView!
    @IBAction func changeProfileImage(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImage.image = image
            userImage.alpha = 1
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var genderSwitch: UISwitch!
    @IBOutlet weak var interestedInSwitch: UISwitch!
    @IBAction func updateProfile(_ sender: Any) {
        print("I am a woman: \(genderSwitch.isOn)")
        print("I am interested in women: \(interestedInSwitch.isOn)")
        self.setFreezeView(activated: true)
        PFUser.current()?["isFemale"] = genderSwitch.isOn
        PFUser.current()?["interestedInWomen"] = interestedInSwitch.isOn
        let imageData = UIImageJPEGRepresentation(userImage.image!, 0.9)
        PFUser.current()?["Photo"] = PFFile(name: "profileImage.jpeg", data: imageData!)
        PFUser.current()?.saveInBackground(block: { (success, error) in
            self.setFreezeView(activated: false)
            if error != nil {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Update failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Update failure", message: "Please try again later.")
                }
            } else {
                print("User updated his/her profile")
                self.performSegue(withIdentifier: "showSwipeViewController", sender: self)
            }

        })
    }
    
    @IBOutlet weak var errorLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setFreezeView(activated: true)
        if let isFemale = PFUser.current()?["isFemale"] as? Bool {
            genderSwitch.isOn = isFemale
        }
        if let isInterestedInWomen = PFUser.current()?["interestedInWomen"] as? Bool {
            interestedInSwitch.isOn = isInterestedInWomen
        }
        if let photo = PFUser.current()?["Photo"] as? PFFile {
            photo.getDataInBackground(block: { (data, error) in
                if error == nil {
                    if let imageData = data, let downloadedImage = UIImage(data: imageData) {
                        self.userImage.image = downloadedImage
                        self.userImage.alpha = 1
                    }
                } else {
                    if let errorContainer = error as? NSError {
                        self.createAlert(title: "Image fetch failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                    } else {
                        self.createAlert(title: "Image fetch failure", message: "Please try again later.")
                    }
                }
            })
        }
        self.setFreezeView(activated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
