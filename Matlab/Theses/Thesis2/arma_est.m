%
% Stokastisk modellering av elpriser f�r utformning av elprisderivat
%
% R, Axelsson
% Chalmers CTH, G�teborg
% H�sten, 2011
%

function output = arma_est(y, p, q)

% This function estimates the best fitting ARMA(p, q) model given the
% observations y[t] and orders p and q of the model. I.e. we are assuming
%
%  y[t] = phi(1)y[t-1] + ... + phi(p)y[t-p] + epsilon[t] +
%         theta(1)epsilon[t-1] + ... + theta(q)epsilon[t-q]
%
% and we solve it using maximum likelihood on Kalman filter estimations
% of the model, which then is established in state-space form.
% Before we can use the Kalman for estimation we need som reasonable
% intial values to start with. Hannan and Kavalieris suggest in their
% paper to first find the best fitting AR model out of the AR(1) -
% AR(ceil(log(N)^1.5)) models and then estimate the AR(best_fit+max(p,q))
% for the observed data. This estimation will give good starting
% coefficients for the Kalman filter estimation.

function [a_start b_start] = HannanKavalieris(y, p, q)

	best_fit = 1e+12;
	y_mean = mean(y);
	y_var = var(y);
	% The AR estimation are performed by using Yule-Walker methods
	% and the best order for the autoregression is choosen for all
	% possible autoregressions between 1 and ceil(log(N)^1.5)
	for h = p:ceil(log(length(y)^1.5))

		% We begin by constructing a correlation matrix R_p	
		r_k = zeros(1, h);		
		for i = 1:h
			r_k(i) = (mean(y(1+i:end).*y(1:end-i)) ...
			          - y_mean^2)/y_var;
		end
		X = zeros(length(y)-h+1, h);
		R_p = zeros(h);
		for i = 1:h
			if i < h			
				R_p(:,i) = [zeros(1, i) r_k(1:end-i)];
			end
			% I also throw in the buildup routine for the
			% estimation matrix X to save computations 
			X(:,i) = y(h-i+1:end-i+1)';
		end
		R_p = R_p + R_p' + eye(h);

		% The coefficients, residuals and the variance of these
		% residuals are estimated. The estimations are evaluated
		% using Bayesian Information Criterion (BIC)

		phi_y = R_p\r_k';
		%keyboard
		y_resid = y(h:end)-(X*phi_y)';
		y_var_resid = (y_resid*y_resid')/length(y_resid);
		y_BIC = log(y_var_resid)+h*log(length(y))/length(y);
		if y_BIC < best_fit
			best_fit = y_BIC;
			h_bf = h;
			resid_bf = y_resid;
		end
	end
	
	% Run regression on the lagged values and the residuals
	y_lag = max(p,q);
	X = zeros(length(y)-h_bf-y_lag, p+q);
	for i = 1:p
		X(:,i) = y(h_bf+y_lag-i+1:end-i);
	end
	for i = p+1:p+q
		X(:,i) = resid_bf(y_lag-i+p+1:end-i+1);
	end
	phi_y = (X'*X)\X'*y(h_bf+y_lag+1:end)';
	a_start = phi_y(1:p);
	b_start = phi_y(p+1:p+q);

	a_start = phi_y(1:p);
	b_start = phi_y(p+1:p+q);
	
	% Try estimating them using Yule_Walker, this code section is
	% experimental

	% I expect the following to be length(y)+h_bf

	%y_ywlag  = [y resid_bf];
	%y_ywmean = mean(y_ywlag);
	%y_ywvar  = var(y_ywlag);
	%r_k = zeros(1,p+q);
	%for i = 1:p+q
	%	r_k(i) = (mean(y_ywlag(1+i:end).*y_ywlag(1:end-i)) ...
	%	          - y_mean^2)/y_var;
	%end
	%R_p = zeros(p+q);
	%for i = 1:p+q-1
	%	R_p(:,i) = [zeros(1, i) r_k(1:end-i)];
	%end
	%R_p = R_p + R_p' + eye(p+q);
	%phiyw_y = R_p\r_k';

	%y1_resid = y_ywlag(p+q:end)-X*phiyw_y;
	%y1_var_resid = (y1_resid'*y1_resid)/length(y1_resid);
	%y1_BIC = log(y1_var_resid)+(p+q)* ...
	%         log(length(y_ywlag))/length(y_ywlag);
	%y2_resid = y(p+q:end)-X*phi_y;
	%y2_var_resid = (y2_resid'*y2_resid)/length(y2_resid);
	%y2_BIC = log(y2_var_resid)+(p+q)* ...
	%         log(length(y_ywlag))/length(y_ywlag);
end

function  [L sigmahat] = ll_ARMA(coefs)
	% This function yields the negative log-likelihood of the ARMA(p,q)
	% model as given above. The a_start and b_start coefficients are
	% merged into one argument to overcome the limitations of the matlab
	% command fminunc.

	phi = coefs(1:p);
	theta = coefs(p+1:p+q);
	r = max(p,q+1);
	T = length(y);
        % Build matrices of state space representation:
        %
        %      x[t+1] = A x[t] + R eps[t+1]
        %      y[t]   = Z'x[t]
        %
        
        A = zeros(r,r); R = zeros(r,1); Z = zeros(r,1);
        A(1:p,1)=phi; A(1:r-1,2:r)=eye(r-1);
        R(1) = 1; R(2:q+1) = theta;
        Z(1) = 1; Q = R*R';
        
        % Initialize state and covariance matrix
        xhat_tt  = zeros(r,1);
        Sigma_tt = reshape( (eye(r^2)-kron(A,A) ) \ Q(:), r, r );
        
        logsum = 0;
        sumsq  = 0;

        for i=0:T-1     % Start Kalman Filter Recursion
        	xhat_t1t  = A*xhat_tt;
        	Sigma_t1t = A*Sigma_tt*A' + Q; 
        	yhat_t1t  = Z'*xhat_t1t;
        	omega     = Z'*Sigma_t1t*Z;
        	delta_t1  = Sigma_t1t*Z/omega;
        	innov     = y(i+1) - yhat_t1t;
        	xhat_t1t1 = xhat_t1t + delta_t1*innov;
         	Sigma_t1t1= Sigma_t1t - delta_t1*Z'*Sigma_t1t;
            
        	% Add likelihood terms:
        	logsum = logsum + log(omega);
         	sumsq  = sumsq  + innov^2/omega;
            
        	% Update estimates;
        	xhat_tt  = xhat_t1t1; 
        	Sigma_tt = Sigma_t1t1;
        end
        
        L = logsum + T*log(sumsq);
        sigmahat = sqrt(sumsq/T);
        
end % function log_likelihood



% Main code that executes the functions above

[phi_start theta_start] = HannanKavalieris(y, p, q);

fmparams = optimset('Largescale','off','Display','off');

mlcoefs = fminunc(@ll_ARMA, [phi_start' theta_start'], fmparams);
[L sigmahat] = ll_ARMA(mlcoefs);

output.ar = mlcoefs(1:p);
output.ma = mlcoefs(p+1:p+q);
output.sigma = sigmahat;
output.ll = -L;
end

