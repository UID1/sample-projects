function nearestNeighbourPath = GetNearestNeighbourPath(cityLocations)
  numberOfCities = length(cityLocations);
  currentPosition = randi(numberOfCities,1);
  currentPath = 1:numberOfCities;
  for i = 1:numberOfCities-1
    currentPath([i currentPosition]) = currentPath([currentPosition i]);
    currentLocMtx = meshgrid(cityLocations(currentPosition,:),i+1:numberOfCities);
    otherLocsMtx = cityLocations(currentPath(i+1:end),:);
    currentDistances = sum((otherLocsMtx - currentLocMtx).^2, 2).^.5;
    [~, minIndex] = min(currentDistances);
    currentPosition = currentPath(i+minIndex);
  end
  nearestNeighbourPath = currentPath;
end