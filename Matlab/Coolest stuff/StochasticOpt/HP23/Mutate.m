function mutatedChromosome = Mutate(chromosome, mutationProbability, ...
                                    creepMutationProbability, creepLength)
  
  nGenes = length(chromosome);
  mutationPoints =      rand(1, nGenes) < mutationProbability;
  creepMutationPoints = mutationPoints .* ( ...
                                  rand(1, nGenes) < creepMutationProbability);
  mutationPoints = (mutationPoints - creepMutationPoints) > 0; % '&& ~' doesn't work
  mutatedChromosome = chromosome;
  mutatedChromosome(mutationPoints) = rand(1,length(find(mutationPoints)));
  creepMutations = creepLength * (rand(1,length(find(creepMutationPoints))) ...
                   * 2 - 1);
  creepMutatedGenes = chromosome(logical(creepMutationPoints)) + creepMutations;
  mutatedChromosome(logical(creepMutationPoints)) = ...
                                             max(0, min(1, creepMutatedGenes));
end
% There are some serious issues in MATLAB (r2016a); elements in logical arrays
% get inadvertently converted to integers that cannot be handled by logical
% operations, i.e. '&& ~' doesn't work and logical() or find() must be used for
% array indexing.