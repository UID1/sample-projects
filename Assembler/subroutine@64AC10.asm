0064AC10 ; Attributes: bp-based frame
0064AC10 
0064AC10 sub_64AC10      proc near               ; CODE XREF: sub_64AD00+29p                                                      
0064AC10                                                                                                            
0064AC10 arg_0           = dword ptr  8                                                                             
0064AC10 arg_4           = dword ptr  0Ch                                                                           
0064AC10                                                                                                            
0064AC10                 push    ebp                                                                                
0064AC11                 mov     ebp, esp                                                                           
0064AC13                 mov     eax, [ebp+arg_0] ; The #S/N signature is being moved here                          
0064AC16                 push    ebx             ; The entered #L/K is now pushed to the stack                      
0064AC16                                         ; and the Base pointer is at the very bottom                       
0064AC17                 mov     [ebp+arg_0], eax ; The #S/N signature is being moved here (redundant)              
0064AC1A                 mov     ebx, [ebp+arg_0]                                                                   
0064AC1D                 mov     eax, ebx        ; ?!? redundant code                                               
0064AC1F                 xor     ecx, ecx                                                                           
0064AC21                                                                                                            
0064AC21 loc_64AC21:                             ; CODE XREF: sub_64AC10+18j                                       
0064AC21                 shr     ebx, 1          ; The #S/Nsig is modified with a right shift                       
0064AC23                 jz      short loc_64AC2A                                                                   
0064AC25                 adc     ecx, 0                                                                             
0064AC28                 jmp     short loc_64AC21 ; Count the number of binary "ones" found in the signature        
0064AC2A ; ---------------------------------------------------------------------------                              
0064AC2A                                                                                                            
0064AC2A loc_64AC2A:                             ; CODE XREF: sub_64AC10+13j                                       
0064AC2A                 adc     ecx, 0                                                                             
0064AC2D                 rol     ax, cl          ; Rotate the signature leftwards just as                           
0064AC2D                                         ; many times as the number of ones found in it                     
0064AC30                 mov     ecx, eax                                                                           
0064AC32                 xor     eax, [ebp+arg_4] ; XOR the original signature with 11h                             
0064AC35                 lea     ebx, ds:674480h                                                                    
0064AC3B                 xlat                    ; Replace the value of al with the value found at                  
0064AC3B                                         ; [ebx+al] (mov al,[bx+al]). al=20h                                
0064AC3C                 xchg    al, ah                                                                             
0064AC3E                 xlat                                                                                       
0064AC3F                 xchg    al, ah                                                                             
0064AC41                 xor     eax, ecx        ; The signature is now transformed through the lookup              
0064AC41                                         ; table found at 674480. Now being XORed with the                  
0064AC41                                         ; "rotated" signature                                              
0064AC43                 mov     ebx, eax                                                                           
0064AC45                 xor     ecx, ecx                                                                           
0064AC47                                                                                                            
0064AC47 loc_64AC47:                             ; CODE XREF: sub_64AC10+3Ej                                       
0064AC47                 shr     ebx, 1                                                                             
0064AC49                 jz      short loc_64AC50                                                                   
0064AC4B                 adc     ecx, 0                                                                             
0064AC4E                 jmp     short loc_64AC47 ; Once again the number of ones found in the transformed          
0064AC4E                                         ; signature is being counted                                       
0064AC50 ; ---------------------------------------------------------------------------                              
0064AC50                                                                                                            
0064AC50 loc_64AC50:                             ; CODE XREF: sub_64AC10+39j                                       
0064AC50                 adc     ecx, 0          ; Number of "ones" found equal to 9                                
0064AC53                 ror     ax, cl          ; Once again the signature is being rotated                        
0064AC53                                         ; but this time the number is 9                                    
0064AC56                 mov     [ebp+arg_0], eax ; &arg_0 = multiprocessed #S/N signature                          
0064AC59                 mov     ecx, [ebp+arg_0] ; Doesn't make sense!                                             
0064AC5C                 mov     [ebp+arg_0], ecx ; ?!? redundant code                                              
0064AC5F                 xor     ecx, ecx        ; ?!? redundant code                                               
0064AC61                 xor     edx, edx                                                                           
0064AC63                 mov     eax, [ebp+arg_0] ; ?!? redundant code                                              
0064AC66                 lea     ebx, ds:674400h ; Retrieve a certain constant (what's this?)                       
0064AC6C                                                                                                            
0064AC6C loc_64AC6C:                             ; CODE XREF: sub_64AC10+6Cj                                       
0064AC6C                 shr     ax, 1           ; This loop is processing the multiprocessed signature             
0064AC6F                 jnb     short loc_64AC79 ; Jump if shr right-shifts a zero                                 
0064AC71                 xor     ecx, [ebx]      ; XOR the first constant of the current pair into cx               
0064AC71                                         ; as many times as there are ones in the multigenerated            
0064AC71                                         ; S/N signature                                                    
0064AC73                 xor     edx, [ebx+4]    ; XOR the second constant of the current pair into dx              
0064AC73                                         ; as many times as there are ones in the multigenerated            
0064AC73                                         ; S/N signature                                                    
0064AC76                 test    ax, ax                                                                             
0064AC79                                                                                                            
0064AC79 loc_64AC79:                             ; CODE XREF: sub_64AC10+5Fj                                       
0064AC79                 lea     ebx, [ebx+8]    ; Get to the next pair of values in the pre-defined                
0064AC79                                         ; constant table. Beware! When a zero is being shifted             
0064AC79                                         ; the current values are being skipped and not XORed               
0064AC79                                         ; into cx, dx respectively!                                        
0064AC7C                 jnz     short loc_64AC6C ; Jump as long as the successively shifting signature-            
0064AC7C                                         ; contained register is not empty                                  
0064AC7E                 xor     eax, eax        ; CX = 77F9, DX = 57C2                                             
0064AC80                                                                                                            
0064AC80 loc_64AC80:                             ; CODE XREF: sub_64AC10+94j                                       
0064AC80                 btr     dx, 0Fh         ; The most significant bit of dx is being moved to CF              
0064AC85                 sbb     bx, bx          ; Subtract with Carry;if the moved dx bit is one, bx               
0064AC85                                         ; will equal FFFFh otherwise 0h (edx={0067FFFFh or 00670000h})     
0064AC88                 and     bx, cx          ; bx is now masked with the XORed constant composition at cx       
0064AC8B                 xor     ax, bx          ; and XORed into ax                                                
0064AC8E                 rol     dx, 7           ; rotate the XORed constants 7 steps left                          
0064AC92                 ror     cx, 7           ; and 7 steps right, yielding CF=1, since the last bit             
0064AC92                                         ; being rotated equals 1                                           
0064AC96                 sbb     bx, bx          ; Once again subtract bx with carry, this time giving              
0064AC96                                         ; bx=FFFF                                                          
0064AC99                 and     bx, 0C0Dh       ; mask bx with 0C0Dh                                               
0064AC9E                 xor     cx, bx          ; XOR the masked value of bx into cx                               
0064ACA1                 test    dx, dx                                                                             
0064ACA4                 jnz     short loc_64AC80 ; Loop until all of the bits in dx are passed out to CF           
0064ACA4                                         ; The final values: cx=AB61h ax=EF46                               
0064ACA6                 mov     [ebp+arg_0], eax ; &arg_0=eax                                                      
0064ACA9                 mov     edx, [ebp+arg_0]                                                                   
0064ACAC                 mov     [ebp+arg_0], edx ; ?!? redundant code                                              
0064ACAF                 mov     ebx, [ebp+arg_0]                                                                   
0064ACB2                 mov     eax, ebx        ; ?!? redundant code                                               
0064ACB4                 xor     ecx, ecx                                                                           
0064ACB6                                                                                                            
0064ACB6 loc_64ACB6:                             ; CODE XREF: sub_64AC10+ADj                                       
0064ACB6                 shr     ebx, 1          ; Count the bits of the signature that equals one                  
0064ACB8                 jz      short loc_64ACBF ; Jump when all of the bits in ebx are shifted out                
0064ACBA                 adc     ecx, 0                                                                             
0064ACBD                 jmp     short loc_64ACB6                                                                   
0064ACBF ; ---------------------------------------------------------------------------                              
0064ACBF                                                                                                            
0064ACBF loc_64ACBF:                             ; CODE XREF: sub_64AC10+A8j                                       
0064ACBF                 adc     ecx, 0          ; 10 of the bits equal one                                         
0064ACC2                 rol     ax, cl          ; rotate ax left as many times                                     
0064ACC5                 mov     ecx, eax        ; and move it into ecx                                             
0064ACC7                 xor     eax, [ebp+arg_4] ; XOR eax with the value of &arg_4 (=11h)                         
0064ACCA                 lea     ebx, ds:674480h ; Again, we get our well known lookup table                        
0064ACCA                                         ; to translate the value of ax                                     
0064ACD0                 xlat                    ; mov al,[bx+al]                                                   
0064ACD1                 xchg    al, ah                                                                             
0064ACD3                 xlat                    ; mov al,[bx+al]                                                   
0064ACD4                 xchg    al, ah                                                                             
0064ACD6                 xor     eax, ecx        ; eax = <translated value> XOR <rotated previous value>            
0064ACD8                 mov     ebx, eax        ; Move it down to ebx                                              
0064ACDA                 xor     ecx, ecx                                                                           
0064ACDC                                                                                                            
0064ACDC loc_64ACDC:                             ; CODE XREF: sub_64AC10+D3j                                       
0064ACDC                 shr     ebx, 1          ; Count the number of ones in the new signature                    
0064ACDE                 jz      short loc_64ACE5                                                                   
0064ACE0                 adc     ecx, 0                                                                             
0064ACE3                 jmp     short loc_64ACDC                                                                   
0064ACE5 ; ---------------------------------------------------------------------------                              
0064ACE5                                                                                                            
0064ACE5 loc_64ACE5:                             ; CODE XREF: sub_64AC10+CEj                                       
0064ACE5                 adc     ecx, 0          ; We have 9 ones                                                   
0064ACE8                 ror     ax, cl          ; We rotate ax right as many times                                 
0064ACEB                 mov     [ebp+arg_0], eax ; So we move the new value into &arg_0                            
0064ACEE                 mov     eax, [ebp+arg_0] ; ?!? redundant code                                              
0064ACF1                 pop     ebx             ; And so we pop back the hex value of the entered                  
0064ACF1                                         ; License key (left part)                                          
0064ACF2                 pop     ebp             ; (and right part)                                                 
0064ACF3                 retn                    ; And here we return with the 16bit signature computed             
0064ACF3 sub_64AC10      endp                    ; from the #S/N of the device in ax                                
0064ACF3                                                                                                            
0064ACF3 ; ---------------------------------------------------------------------------                              
0064ACF4                 align 10h                                                                                  
0064AD00                                                                                                            
0064AD00 ; ��������������� S U B R O U T I N E ���������������������������������������                              
0064AD00                                                                                                            
0064AD00                                                                                                            
0064AD00 sub_64AD00      proc near               ; CODE XREF: sub_64AFA0+1A6p                                      
0064AD00                                         ; sub_64B1B0+92p                                                  
0064AD00                                                                                                            
0064AD00 var_8           = dword ptr -8                                                                             
0064AD00 var_4           = dword ptr -4                                                                             
0064AD00                                                                                                            
0064AD00                 sub     esp, 8                                                                             
0064AD03                 mov     ecx, [esi+4]    ; Stacked #S/N                                                     
0064AD06                 mov     eax, [esi+8]                                                                       
0064AD09                 push    ebx                                                                                
0064AD0A                 push    edi                                                                                
0064AD0B                 lea     edi, [esi+4]                                                                       
0064AD0E                 or      eax, ecx        ; The two halves of the S/N are ORed into one                      
0064AD10                 shr     ecx, 10h                                                                           
0064AD13                 and     eax, 0FFFFh     ; Strip the 2nd half off the "ORed" #S/N                           
0064AD18                 or      eax, ecx        ; "OR" the "ORed" #S/N into one 16-bit word                        
0064AD1A                 mov     ecx, [esi]      ; Move some value (11h) at the base of si to ecx                   
0064AD1C                 push    ecx                                                                                
0064AD1D                 push    eax                                                                                
0064AD1E                 mov     [esp+18h+var_4], 0                                                                 
0064AD26                 mov     [esi+18h], eax                                                                     
0064AD29                 call    sub_64AC10      ; Process the generated signature through this subroutine          
0064AD2E                 mov     ecx, [edi]      ; Get the least significant dword of the S/N into ecx              
0064AD30                 mov     ebx, ecx        ; and ebx                                                          
0064AD32                 shl     ebx, 10h        ; shift ebx 16 steps left                                          
0064AD35                 or      ebx, eax        ; Concatenate the least significant word of #S/N with the signature
0064AD37                 mov     [esi+14h], eax  ; move the signature into memory                                   
0064AD3A                 mov     eax, [esi+8]    ; and get the most significant dword of #S/N                       
0064AD3D                 lea     edx, [esi+1Ch]  ; Get something (?) from another part of this memory               
0064AD3D                                         ; that now equals 0                                                
0064AD40                 shr     ecx, 10h                                                                           
0064AD43                 shl     eax, 10h                                                                           
0064AD46                 or      ecx, eax        ; We're picking out the dword from the very middle of the #S/N     
0064AD48                 mov     [edx], ebx      ; and moving the least significant word together with              
0064AD48                                         ; the computed signature into memloc 1C                            
0064AD4A                 push    edx             ; and push the location into the stack                             
0064AD4B                 mov     [esi+20h], ecx  ; the middle part of #S/N goes into memory                         
0064AD4B                                         ; and now it's neatly gathered in the memory                       
0064AD4E                 lea     ebx, [esi+24h]  ; Get an empty segment for &ebx                                    
0064AD51                 lea     ecx, [esi+0Ch]  ; and get ecx to point at the lsb of the entered #L/K              
0064AD54                 push    ebx             ; We now push the empty memloc into stack                          
0064AD55                 call    sub_64A970      ; eax = f(bx, dx=>#S/N:signature) (???)                            
0064AD55                                         ; ebx now => an even more scrambled version of >#L/K               
0064AD5A                 lea     ecx, [esp+20h+var_8] ; ecx => var_8                                                
0064AD5E                 push    edi             ; push => #S/N                                                     
0064AD5F                 push    ecx             ; push var_8                                                       
0064AD60                 mov     ecx, ebx        ; ecx => .SC>#L/K (@esi+24h)                                       
0064AD62                 call    sub_64AB50      ; sub_64AB50(& var_8, [esi+4]=#S/N)                                
0064AD62                                         ; where var_8 now contains a scrambled version                     
0064AD62                                         ; of >#L/K (together with var_4)                                   
0064AD67                 mov     ecx, [esp+28h+var_4] ; \                                                           
0064AD67                                         ;   ecx, eax = SC >#L/K                                            
0064AD6B                 mov     eax, [esp+28h+var_8] ; /                                                           
0064AD6F                 xor     ecx, [esi+8]    ; XOR 1st half of SC>#L/K with 1st half of #S/N                    
0064AD72                 add     esp, 18h                                                                           
0064AD75                 xor     eax, [edi]      ; XOR 2nd half of SC>#L/K with 2nd half of #S/N                    
0064AD77                 pop     edi                                                                                
0064AD78                 pop     ebx                                                                                
0064AD79                 jnz     short loc_64AD88 ; Maybe eax is supposed to be zero if the correct License key     
0064AD79                                         ; is entered, if not; clear it, and move esp additional 8          
0064AD79                                         ; steps forward in memory (down)                                   
0064AD7B                 test    ecx, ecx        ; Test whether the other half of the XORed value is zero           
0064AD7D                 jnz     short loc_64AD88 ; and move esp an additional 8 steps forward and                  
0064AD7D                                         ; if not...                                                        
0064AD7F                 mov     eax, 1                                                                             
0064AD84                 add     esp, 8                                                                             
0064AD87                 retn                                                                                       
0064AD88 ; ---------------------------------------------------------------------------                              
0064AD88                                                                                                            
0064AD88 loc_64AD88:                             ; CODE XREF: sub_64AD00+79j                                       
0064AD88                                         ; sub_64AD00+7Dj                                                  
0064AD88                 xor     eax, eax                                                                           
0064AD8A                 add     esp, 8                                                                             
0064AD8D                 retn                                                                                       
0064AD8D sub_64AD00      endp                                                                                       