%% CIRPATHDEMO
%% 
% The Cox-Ingersoll-Ross process
%%
% $$dr = a(b-r)dt + \sigma\sqrt{r}dW$$ 
%
% is a popular model of interest-rate dynamics.
%%
help cirpath
%% Example 1
% The exercise partially reproduces an example in Glasserman (2004), _Monte
% Carlo methods in financial engineering_, Springer.
t  = [0 0.25];  % observation times
a  = 0.2;       % mean-reversion parameter
b  = 0.05;      % long-term mean
s  = 0.1;       % volatility
r0 = 0.04;      % starting value
%%
n = 1e4;
x = nan(n,1);
for i = 1:n
    r = cirpath(t,a,b,s,r0);
    x(i) = r(end);
end
[f,xi] = ksdensity(x);
plot(xi,f/100,'k-')
title('Empirical distribution \itf(r(0.25)|r(0) = 0.04)')
%% Example 2
t  = 0:0.05:1; 
a  = 0.2;       
b  = 0.05;      
s  = [0.01 .1 .5];
r0 = 0.04;
%%
n = length(t);
m = length(s);
r = nan(n,m);
for i = 1:m
    r(:,i) = cirpath(t,a,b,s(i),r0);
end
%%
plot(t,r,'.')
legend('\sigma = 0.01','\sigma = 0.1','\sigma = 0.5')
title('Sample paths of CIR processes with different values of \sigma')
%%
%Example : cirpathdemo (Oh, the FEX code metrics..)
