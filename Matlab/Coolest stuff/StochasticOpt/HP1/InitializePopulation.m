function population = InitializePopulation(populationSize, numberOfGenes);
  population = rand(populationSize, numberOfGenes) >= .5;
end