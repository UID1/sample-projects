function results = vehicleSimulation(VehicleParameters, SlopeParameters, ...
                                     NeuralNetworkParameters, ...
                                     Boundaries, StartValues)

  TimeSeries.position      = [];
  TimeSeries.velocity      = [];
  TimeSeries.acceleration  = [];
  TimeSeries.selectedGear  = [];
  TimeSeries.brakePressure = [];
  TimeSeries.brakeTemp     = [];
  TimeSeries.nnInputs      = [];
  TimeSeries.nnOutputs     = [];

  TimeSeries.position(1)           = StartValues.startPosition;
  TimeSeries.velocity(1)           = StartValues.startVelocity;
  %TimeSeries.acceleration(1)   = StartValues.startAcceleration;
  TimeSeries.selectedGear(1)           = StartValues.startGear;
  TimeSeries.brakeTemp(1)         = StartValues.startBrakeTemp;
  TimeSeries.brakePressure(1) = StartValues.startBrakePressure;

  AccParams.vehicleMass               = VehicleParameters.mass;
  AccParams.gravitationConstant  = VehicleParameters.gConstant;
  AccParams.maxBrakeTemp             = Boundaries.maxBrakeTemp;
  AccParams.brakeTemp                = TimeSeries.brakeTemp(1);
  AccParams.gearFactor       = VehicleParameters.gearVector(7);
  AccParams.brakePressure        = TimeSeries.brakePressure(1);
  AccParams.position                  = TimeSeries.position(1);
  
  % Use this for linear brake pressure output from NN node
  decodeNNPb = @(y) ((-NeuralNetworkParameters.Parameters.sigmoidC * ...
                       NeuralNetworkParameters.Parameters.lowerBound + ...
                      log(y./(1-y)))/ (( ...
                      NeuralNetworkParameters.Parameters.upperBound - ...
                      NeuralNetworkParameters.Parameters.lowerBound) * ...
                      NeuralNetworkParameters.Parameters.sigmoidC));
  
  runSimulation = true;
  timeIdx = 0;
  numGears = length(VehicleParameters.gearVector);
  lastGearChange = StartValues.lastGearChange;
  cleanUp = '';
  
  TimeSeries.acceleration(1) = getAcceleration(AccParams, SlopeParameters);
  while runSimulation
    timeIdx = timeIdx + 1;
    maxPosReached = Boundaries.travelDistance <= ...
                                  TimeSeries.position(timeIdx);
    maxBrakeTempReached = Boundaries.maxBrakeTemp <= ...
                                 TimeSeries.brakeTemp(timeIdx);
    maxVelocityReached = Boundaries.maxVelocity <= ...
                                  TimeSeries.velocity(timeIdx);
    minVelocityReached = Boundaries.minVelocity > ...
                                  TimeSeries.velocity(timeIdx);
    runSimulation = ~(maxPosReached || maxBrakeTempReached ...
                      || minVelocityReached || maxVelocityReached);
    AccParams.brakeTemp = TimeSeries.brakeTemp(timeIdx);
    AccParams.gearFactor =  VehicleParameters.engineCoef * ...
                VehicleParameters.gearVector(TimeSeries.selectedGear(timeIdx));
    AccParams.brakePressure = TimeSeries.brakePressure(timeIdx);
    AccParams.position = TimeSeries.position(timeIdx);
    currentAcceleration = getAcceleration(AccParams, SlopeParameters);
    TimeSeries.acceleration(timeIdx) = currentAcceleration;
    TimeSeries.velocity(timeIdx+1) = TimeSeries.velocity(timeIdx) + ...
                                  currentAcceleration*VehicleParameters.deltat;
    TimeSeries.position(timeIdx+1) = TimeSeries.position(timeIdx) + ...
                     TimeSeries.velocity(timeIdx+1) * VehicleParameters.deltat;
    if TimeSeries.brakePressure(timeIdx) < .01
      deltaDeltaTb = - VehicleParameters.deltat * ( ...
                  TimeSeries.brakeTemp(timeIdx) - ...
                VehicleParameters.Tamb) / VehicleParameters.brakeCoolingFactor;
    else
      deltaDeltaTb = VehicleParameters.brakeCoef * ... 
                  TimeSeries.brakePressure(timeIdx) * VehicleParameters.deltat;
    end
    TimeSeries.brakeTemp(timeIdx+1) = TimeSeries.brakeTemp(timeIdx) + ...
                                         deltaDeltaTb;
    
    % Extract outputs from our neural network
    velocityInput =      TimeSeries.velocity(timeIdx+1)/Boundaries.maxVelocity;
    angularInput  = SlopeFun(TimeSeries.position(timeIdx+1), ...
                                   SlopeParameters) / SlopeParameters.alphaMax;
    brakeTempInput = TimeSeries.brakeTemp(timeIdx+1) / Boundaries.maxBrakeTemp;
    neuralNetworkInputs =          [velocityInput angularInput brakeTempInput];
    TimeSeries.nnInputs = [TimeSeries.nnInputs; neuralNetworkInputs]; 
    neuralNetworkOutputs = FFNeuralNetwork(neuralNetworkInputs, ...
                           NeuralNetworkParameters.Weights, ...
                                           NeuralNetworkParameters.Parameters);
    TimeSeries.nnOutputs = [TimeSeries.nnOutputs; neuralNetworkOutputs];
    % Interpret nnoutputs and adjust parameters accordingly
    gearsAvailable =          lastGearChange >= VehicleParameters.gearDeadTime;
    if neuralNetworkOutputs(1) < .3
      TimeSeries.selectedGear(timeIdx + 1) = max(1, min( ...
                 TimeSeries.selectedGear(timeIdx) - 1*gearsAvailable, ...
                                                                    numGears));
      lastGearChange = min((lastGearChange + VehicleParameters.deltat) * ...
                              ~gearsAvailable, VehicleParameters.gearDeadTime);
    else
      if neuralNetworkOutputs(1) > .7
        TimeSeries.selectedGear(timeIdx + 1) = max(1, min( ...
                 TimeSeries.selectedGear(timeIdx) + 1*gearsAvailable, ...
                                                                    numGears));
        lastGearChange = min((lastGearChange + VehicleParameters.deltat) * ...
                              ~gearsAvailable, VehicleParameters.gearDeadTime);
      else
        TimeSeries.selectedGear(timeIdx +1) = TimeSeries.selectedGear(timeIdx);
        lastGearChange = min(lastGearChange + VehicleParameters.deltat, ...
                                               VehicleParameters.gearDeadTime);
      end
    end
    TimeSeries.brakePressure(timeIdx +1) =         neuralNetworkOutputs(2).^.6;

    outputString = sprintf( ...
          'Position: %.2f, Speed: %.1f, Acceleration: %.3f, Iteration: %d', ...
                    TimeSeries.position(timeIdx), ...
                    TimeSeries.velocity(timeIdx), ...
                    TimeSeries.acceleration(timeIdx), timeIdx);
  end
  results.prematureTermination =                                ~maxPosReached;
  results.TimeSeries =                                              TimeSeries;
  results.numTimeSteps =                                               timeIdx;
  results.endPosition =                           TimeSeries.position(timeIdx);
  results.fitness = TimeSeries.position(timeIdx) ^ 2 / (timeIdx * ...
                                                     VehicleParameters.deltat);
  if isnan(results.fitness)
    keyboard
  end
end