#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

void checkResult(float *hostRef, float *gpuRef, const int N)
{
   double epsilon = 1.0e-8;
   for(int i=0; i<N; ++i)
      if(abs(hostRef[i]-gpuRef[i]) > epsilon)
      {
         colors_t colors;
         colors.setFg(hGray);
         std::cout << "Host ";
         colors.setFg(dCyan);
         std::cout << std::setprecision(6) << hostRef[i];
         colors.setFg(hGray);
         std::cout << " GPU ";
         colors.setFg(dCyan);
         std::cout << gpuRef[i];
         colors.setFg(hGray);
         std::cout << ": ";
         colors.setFg(hRed);
         std::cout << "Arrays do not match!";
         colors.setFg(hGray);
         std::cout << " Diff: " << abs(hostRef[i]-gpuRef[i]);
         std::cout << std::endl << std::endl;
         colors.setColors(colors.initial_colors);
         break;
      }

}

void initialData(float *ip, const int size)
{
   for(int i=0; i < size;++i)
      ip[i] = (float)(rand() & 0xFF)/10.0f;
}

void sumArraysOnHost (float *A, float *B, float *C, const int N)
{
   for(int idx=0; idx<N;++idx)
      C[idx] = A[idx] + B[idx];
}

__global__ void sumArraysOnGPU(float *A, float *B, float *C, const int N)
{
   int i = blockIdx.x * blockDim.x + threadIdx.x;
   if (i<N) C[i] = A[i] + B[i];
}

__global__ void sumArraysZeroCopy(float *A, float *B, float *C, const int N)
{
   int i = blockIdx.x*blockDim.x + threadIdx.x;
   if(i<N) C[i] = A[i] + B[i];
}
int main (int argc, char** argv)
{
   colors_t colors;

   /* Set up device */
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));

   /* Check if mapped memory is supported on the device */
   if(!deviceProp.canMapHostMemory)
   {
      colors.setFg(hRed); std::cout << "Device ";
      colors.setFg(dRed); std::cout << dev;
      colors.setFg(hRed);
      std::cout << " does not support mapping CPU host memory!";
      colors.setFg(hGray);
      std::cout << std::endl;
      CHECK(cudaDeviceReset());
      exit(EXIT_SUCCESS);
   }

   colors.setFg(hGray);
   std::cout << "Using device "; colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray); std::cout << ": "; colors.setFg(dYellow);
   std::cout << deviceProp.name << std::endl; colors.setFg(hGray);

   /* Set up vector data size*/
   int ipower = 10;
   if(argc > 1) ipower = atoi(argv[1]);
   
   int nElem = 1 << ipower;
   size_t nBytes = nElem*sizeof(float);
   
   bool isLarge = false;
   if(ipower < 18) isLarge = false;
   else isLarge = true;
   
   colors.setFg(hGray);
   std::cout << "Vector size (#elements): "; colors.setFg(dCyan);
   std::cout << nElem; colors.setFg(hGray);
   std::cout << ", power: "; colors.setFg(dCyan);
   std::cout << ipower; colors.setFg(hGray);
   std::cout << ", datasize: "; colors.setFg(dCyan);
   std::cout << std::setprecision(1);
   std::cout << static_cast<float>
                (nBytes/(isLarge?(1024.0f*1024.0f):1024.0f));
   colors.setFg(White);
   std::cout << (isLarge?" M":" k") << "B";
   colors.setFg(hGray); std::cout << "." << std::endl;
   
   /* Part 1: Using device memory
              malloc host memory   */
   float *h_A, *h_B, *hostRef, *gpuRef;
   h_A     = (float *)malloc(nBytes);
   h_B     = (float *)malloc(nBytes);
   hostRef = (float *)malloc(nBytes);
   gpuRef  = (float *)malloc(nBytes);
   
   /* Initialize data at host side */
   initialData(h_A, nElem);
   initialData(h_B, nElem);
   memset(hostRef, 0, nBytes);
   memset(gpuRef, 0, nBytes);

   /* Vector addition on the host side for verification */
   sumArraysOnHost(h_A, h_B, hostRef, nElem);
   
    /* Data transfer from host to device */
    float *d_A, *d_B, *d_C;
    CHECK(cudaMalloc((float **)&d_A, nBytes));
    CHECK(cudaMalloc((float **)&d_B, nBytes));
    CHECK(cudaMalloc((float **)&d_C, nBytes));
    CHECK(cudaMemcpy(d_A, h_A, nBytes, cudaMemcpyHostToDevice));
    CHECK(cudaMemcpy(d_B, h_B, nBytes, cudaMemcpyHostToDevice));

    /* Execution configuration */
    int iLen = 512;
    dim3 block (iLen);
    dim3 grid  ((nElem + block.x - 1) / block.x);

    sumArraysOnGPU<<<grid, block>>>(d_A, d_B, d_C, nElem);

    /* Copy kernel result back to host side */
    CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));

    /* Device results verified */
    checkResult(hostRef, gpuRef, nElem);

    /* Free device global memory */
    CHECK(cudaFree(d_A));
    CHECK(cudaFree(d_B));

    /* free host memory */
    free(h_A);
    free(h_B);

   /* Part 2: Using zerocopy memory for the arrays A and B
              allocate zerocopy memory
   */

   CHECK(cudaHostAlloc((void **)&h_A, nBytes, cudaHostAllocMapped));
   CHECK(cudaHostAlloc((void **)&h_B, nBytes, cudaHostAllocMapped));
   
   /* Initialize data at host side */
   initialData(h_A, nElem);
   initialData(h_B, nElem);
   memset(hostRef, 0, nBytes);
   memset(gpuRef, 0, nBytes);
   
   /* Pass the pointer to the zerocopy memory on the host to the device */
   CHECK(cudaHostGetDevicePointer((void **)&d_A, (void *)h_A, 0));
   CHECK(cudaHostGetDevicePointer((void **)&d_B, (void *)h_B, 0));
   /* The third argument is reserved for futur use and is should currently
      always be set to 0 ... */
   
   /* Vector addition on the host side for verification */
   sumArraysOnHost(h_A, h_B, hostRef, nElem);

   /* CUDA kenrnel with Zerocopy block */
   sumArraysZeroCopy<<<grid, block>>>(d_A, d_B, d_C, nElem);
   
   /* 
      Since CUDA Compute 2.0, Unified Virtual Addressing is supported (UVA)
      which means that the device pointer need not be transferred and the
      kernel above can use h_A and h_B directly.
   */
   
   /* Copy the kernel results back to host memory, only A and B are in
      zerocopy memory, not C 
   */
   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   
    /* Device results verified */
    checkResult(hostRef, gpuRef, nElem);

   /* Free memory */
   CHECK(cudaFree(d_C));
   CHECK(cudaFreeHost(h_A));
   CHECK(cudaFreeHost(h_B));
   
   free(hostRef);
   free(gpuRef);
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}
