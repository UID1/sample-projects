//
//  ViewController.swift
//  Back to Bach
//
//  Created by Iberius Pred on 28/02/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var player = AVAudioPlayer()
    var timer = Timer()
    let audioPath = Bundle.main.path(forResource: "sheep", ofType: "mp3")
    
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var positionSlider: UISlider!
    
    func updateScrubber() {
        positionSlider.value = Float(player.currentTime)
    }
    @IBAction func play(_ sender: Any) {
        player.play()
        timer = Timer(timeInterval: 1, target: self, selector: #selector(ViewController.updateScrubber), userInfo: nil, repeats: true)
    }
    @IBAction func volumeChanged(_ sender: Any) {
        player.volume = volumeSlider.value
    }
    @IBAction func positionChanged(_ sender: Any) {
        player.currentTime = TimeInterval(positionSlider.value)
    }
    @IBAction func pause(_ sender: Any) {
        player.pause()
        timer.invalidate()
    }
    @IBAction func stop(_ sender: Any) {
        player.pause()
        timer.invalidate()
        do {
            try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
        } catch {
            // process error
        }
        positionSlider.value = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        do {
            try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
            positionSlider.maximumValue = Float(player.duration)
        } catch {
            // process error
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

