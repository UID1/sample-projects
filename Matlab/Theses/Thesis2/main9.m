% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% See the help information for prior main<x>.m scripts  for a background
% story. In main9.m we continue with our more sophisticated testing met-
% hod and use it to find the best fit to our local series.

%
% /----------- : BEGIN : Preparations --------------------------------/
%
%

if ~any(strcmp(who,'ELSPOTh2010'))
   fid1 = fopen('elspoth2010.bin', 'r');
   ELSPOTh2010 = fread(fid1, 8760, 'double');
   fclose(fid1);
end
if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
deltaELmin = 1e7;
for k = 1:length(ELSPOTh2010)-90*24
	deltaEL = max(ELSPOTh2010(k:k+90*24)) - ...
	          min(ELSPOTh2010(k:k+90*24));
	if deltaEL < deltaELmin
		deltaELmin = deltaEL;
		kmin = k;
	end
end
min90 = (kmin:kmin+90*24);

ELSPEC1 = fspectrum3(ELSPOTh2010(min90), 1);
ELTREND = est_ma(ELSPOTh2010(min90), 500);
ELSPOTdt = ELSPOTh2010(min90)-ELTREND.removable;
ELSPECdt = fspectrum3(ELSPOTdt, 1);
TMPSPEC = fspectrum3(tmp2010.national(min90), 1);

[filt.n1, filt.d1] = butter(2, [450 750]/5e4, 'stop');
[filt.n2, filt.d2] = butter(2, [4100 4300]/5e4, 'stop'); % Partial
[filt.n3, filt.d3] = butter(2, [8350 8420]/5e4, 'stop'); % Max partial
[filt.n4, filt.d4] = butter(2, [1150 1250]/5e4, 'stop');

ELhdes = filter(filt.n1, filt.d1, ...
	               filter(filt.n2, filt.d2, ...
		                ELSPOTh2010(kmin-300:kmin+90*24)));
for i = 1:2
   ELhdes = filter(filt.n3, filt.d3, ...
                        filter(filt.n1, filt.d1, ...
                                ELhdes));
   ELhdes = filter(filt.n3, filt.d3, ELhdes);
end
ELSPECdes = fspectrum3(ELhdes, 1);
ELSPOTds = ELhdes(301:end);
ELTRENDds = est_ma(ELSPOTds, 500);
ELSPOTdsdt = ELhdes(301:end)-ELTRENDds.removable;
ELSPECdsdt = fspectrum3(ELSPOTdsdt, 1);

ELloghds = (ELSPOTds);
ELTRENDdslog = est_ma(ELloghds, 500);
ELloghdsdt = (ELloghds - ELTRENDdslog.removable);
ELloghdsdt = ELloghdsdt/1000;

%
% /----------- :  END  : Preparations --------------------------------/
%
%

% Run the estimations using the new sophisticated method and immediately
% save them all to disk. Warning: These calculations take about 6 hours
% on a modern mainstream computer.

[m1a.data m1a.par] = armabat4(ELSPOTdsdt, tmp2010.national(min90), ...
                              1:24, 1:24, 1:24, 1, '1a')
[m1b.data m1b.par] = armabat4(ELloghdsdt, tmp2010.national(min90), ...
                              1:24, 1:24, 1:24, 1, '1b')
[m2a.data m2a.par] = armabat4(ELSPOTdsdt, 'nil', ...
                              1:24, 1:24, 0, 1, '2a')
[m2b.data m2b.par] = armabat4(ELloghdsdt, 'nil', ...
                              1:24, 1:24, 0, 1, '2b')
savefile = sprintf('ARMAX2est(%s).mat', ...
                     datestr(now, 'mmm-dd_HH-MM.FFF'));
save(savefile, 'm1a', 'm1b', 'm2a', 'm2b')

% Comparison  of the different kernels  used by ksdensity  was done with
% the following lines:
%
% [f_emp x_emp] = ksdensity(ELSPOTdsdt, 'kernel', 'box');
% hist(ELSPOTdsdt, 250)
% hold on
% plot(x_emp, f_emp*abs(trapz(ELSPOTdsdt))/2, 'r')
%
% where 'box'  which is the least smooth kernel,  can be changed  to the
% kernels 'triangle','epanechnikov' or 'normal'.
%
% The comparison  reveals that the choice  of kernel  has no significant
% impact on the selection of the best fitting model.

% The following method can be used to visualize the estimations:

% Extract the seasonalities
ELs = (ELSPOTh2010(min90)-ELSPOTds);

plot(armaxsim(m1b.data, tmp2010.national(min90), ...
       length(min90), 1, 0) + ELTRENDds.removable + ELs)
hold on
plot(ELSPOTh2010(min90), 'r')
title('Simulated estimations (blue) vs real observations (red)')

% All of the best fits but the first (m1a) can be simulated.
