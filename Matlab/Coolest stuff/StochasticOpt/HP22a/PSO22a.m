% Particle Swarm Optimization

% Parameter initialization
maxRuns = 1000;
C1 = 2; C2 = 2;
swarmSize = 4000;
vmax = 4;
% Adjust with beta < 1 but close to 1, set w constant when ~.3-.4.
initialInertia = 1.4;
inertiaAdjustment = 0.9995;
inertiaLowLimit = .3;
deltat = .7;
globalMinimum = 1;
machineEpsilon = 1e-4;
alphaVelocity = 1; % [0,1]
xMin = -10; xMax = 10;
yMin = -10; yMax = 10;
startPoint = [xMax yMax]; % Arbitrary value
xSpan = xMax - xMin;
ySpan = yMax - yMin;

% Swarm initialization
Particles.xij = zeros(swarmSize, 2);
Particles.xij(:,1) = xMin + rand(swarmSize,1) * (xSpan);
Particles.xij(:,2) = yMin + rand(swarmSize,1) * (ySpan);
Particles.vij(:,1) = alphaVelocity/deltat * (- xSpan / 2 + rand(swarmSize,1) * xSpan);
Particles.vij(:,2) = alphaVelocity/deltat * (- ySpan / 2 + rand(swarmSize,1) * ySpan);
swarmValues = zeros(swarmSize, 1);

Particles.previouslyBestSwarmValues = ones(swarmSize, 1) * ...
                            PSOFunction(startPoint(1), startPoint(2));
Particles.xijPB = meshgrid(startPoint, 1:swarmSize);
Particles.swarmBestIndex = 1;
Particles.xijSB = Particles.xij(1,:);
fxSB = PSOFunction(Particles.xijSB(1), Particles.xijSB(2));

% Plot initialization
numberOfGenerations = 500;
fitnessFigureHandle = figure;
hold on;
set(fitnessFigureHandle, 'Position', [50,50,500,200]);
set(fitnessFigureHandle, 'DoubleBuffer', 'on');
axis([1 numberOfGenerations 0 10]);
bestPlotHandle = plot(1:numberOfGenerations, zeros(1,numberOfGenerations));
textHandle = text(30,1,sprintf('best: %4.3f', 0.0));
hold off;
drawnow;

surfaceFigureHandle = figure;
hold on;
set(surfaceFigureHandle, 'DoubleBuffer', 'on');
delta = .1;
variableRange = xMax;
limit = fix(2*variableRange/delta) + 1;
[xValues, yValues] = meshgrid(-variableRange:delta:variableRange,...
    -variableRange:delta:variableRange);
zValues = zeros(limit, limit);
for j = 1:limit
    for k = 1:limit
        zValues(j,k) = log(PSOFunction(xValues(j,k), yValues(j,k)));
    end
end
surfl(xValues,yValues,zValues)
colormap gray;
shading interp;
view([-7 -9 10]);

populationPlotHandle = plot3(Particles.xij(:,1), Particles.xij(:,2), ...
             log(PSOFunction(Particles.xij(:,1), Particles.xij(:,2))),'kp');
hold off;
drawnow;


% PSO Algorithm
numIterations = 0;
currentInertia = initialInertia;
while fxSB - globalMinimum > machineEpsilon
  swarmValues = PSOFunction(Particles.xij(:,1), Particles.xij(:,2));
  Particles = updateBestParticles(Particles, swarmValues);
  Particles.vij = updateVelocities(Particles, C1, C2, deltat, vmax, currentInertia);
  Particles.xij = Particles.xij + Particles.vij*deltat;
  numIterations = numIterations + 1;
  if currentInertia > inertiaLowLimit
    currentInertia = currentInertia * inertiaAdjustment;
  end
  
  fxSB = PSOFunction(Particles.xijSB(1), Particles.xijSB(2));
  plotvector = get(bestPlotHandle, 'YData');
  plotvector = [plotvector(2:end) log(PSOFunction(Particles.xijSB(1), ...
                                      Particles.xijSB(2)))];
  set(bestPlotHandle, 'YData', plotvector);
  set(textHandle, 'String', sprintf('#iterations: %d, inertia: %1.3f, best: %4.3f', ...
                                    numIterations, currentInertia, fxSB - globalMinimum));
  set(populationPlotHandle, 'XData', Particles.xij(:,1), 'YData', ...
      Particles.xij(:,2), 'ZData', ...
      log(PSOFunction(Particles.xij(:,1), Particles.xij(:,2))));
  drawnow;

end

disp('Minimum Value');
disp(PSOFunction(Particles.xijSB(1), Particles.xijSB(2)));
disp('Point');
disp(Particles.xijSB);




























