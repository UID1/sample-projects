% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2a Solution
% by Robin Axelsson
%

% This program analyses iteration times over
% a larger number of independent runs. It also
% tests for different alphas to see how different
% alpha values may affect the speed at which a
% solution might be found.

a2=[9979, 9348, 8022, 4020, 2693, 1892, 1714, 1371, 510, 451];
b2=[9492, 8453, 7749, 7365, 2292, 2180, 1023, 959, 278, 124, 85];
c2=[7042, 5608, 5464, 4371, 3884, 3121, 1901, 1768, 1590, 959, ...
   899, 707, 702, 510, 451, 412, 278, 124, 124, 85];

alpha_0 = [1e-4]; % tested alpha values
numruns = 1; % Number of runs for each session

% Parameter initialization
runtimes = [];
meanrt = zeros(1, length(alpha_0));
stdrt = zeros(1, length(alpha_0));
sigma_opts = zeros(length(a2), 1);
mu_opts = zeros(length(b2), 1);
H_verif =  [];

% Here we start with a random configuration for σ and μ
%sigma_n = randperm(length(a2));
%mu_m = randperm(length(b2));

% Here we start with another local minimum from a previous run
sigma_n = [9 5 7 8 3 10 6 1 2 4];
mu_m = [11 6 7 5 3 10 9 4 1 2 8];

global DATA2;

wb1 = waitbar(0,'Running neural simulation');
for k = 1:length(alpha_0)
   for i=1:numruns
      DATA1 = annealfun2(a2, b2, c2, alpha_0(k), sigma_n, mu_m, 1e5);
      runtimes = [runtimes DATA1.time];
      dblcheck = 0;
      for j = 1:size(mu_opts,2)
         dblcheck = dblcheck + sum(mu_opts(:,j) == DATA1.mu');
      end
      H_verif = [H_verif DATA1.H];
      if dblcheck == 0
         mu_opts = [mu_opts DATA1.mu'];
         sigma_opts = [sigma_opts DATA1.sigma'];
      end
      dblcheck = 0;
      for j = 1:size(sigma_opts,2)
         dblcheck = dblcheck + sum(sigma_opts(:,j) == DATA1.sigma');
      end
      if dblcheck == 0
         mu_opts = [mu_opts DATA1.mu'];
         sigma_opts = [sigma_opts DATA1.sigma'];
      end
      waitbar((k-1)/length(alpha_0)+i/(length(alpha_0)*numruns), wb1);
   end
   meanrt(k) = mean(runtimes((end-numruns+1):end));
   stdrt(k) = std(runtimes((end-numruns+1):end));
end
close(wb1);

sigma_opts(:,1) = [];
mu_opts(:,1) = [];
fprintf('\nSummary:\n')
fprintf('\nMean time: %f \n', mean(runtimes))
fprintf('Stddev time: %f \n', std(runtimes))
fprintf('# of runs yielding non-zero energy: %d\n', sum(H_verif ~= 0))
fprintf('# of combinations of a and b found for given c: %d \n', size(mu_opts,2))


FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; [230, 229, 214]; [115, 115, 115]; [176, 169, 165]; [211, 202, 183]]/255;

plot(1:length(DATA1.H_vec), DATA1.H_vec, 'LineWidth', 2, 'Color', FIGS.LineColors(3, :));
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system H(S^{(k)})');
title('\fontsize{12}Evolution of energy level over time')


% The mean runtime for 250 runs is about 12000 time steps. The mean
% is not significantly affected by choice of alpha and therefore I'm
% not compelled to try other alphas. I can find 2 unique solutions to the
% system. The program manage to get zero energy on all (100% of) the runs.

% Solution1:
% sigma = 5 3 2 1 6 4
% mu = 4 6 3 5 2 1
% The following code for c(nu)
CT = sort([cumsum(a2(sigma_opts(1:(end-1),1))) cumsum(b2(mu_opts(:,1)))]);
c_opt1 = [CT(1) diff(CT)];
nu_order = zeros(1, length(c_opt1));
for i=1:length(c_opt1)
   nu_order(i) = find(c_opt1(i) == c1);
end

% Yields the order nu = 
%  10 5 9 4 8 6 2 7 1 11 3

% Solution2:
% sigma = 1 4 6 2 3 5
% mu = 1 2 6 3 5 4
% The following code for c(nu)
CT = sort([cumsum(a2(sigma_opts(1:(end-1),2))) cumsum(b2(mu_opts(:,2)))]);
c_opt1 = [CT(1) diff(CT)];
nu_order = zeros(1, length(c_opt1));
for i=1:length(c_opt1)
   nu_order(i) = find(c_opt1(i) == c1);
end

% Yields the order nu = 
%  1 3 11 7 2 6 9 4 8 5 10


One Solution:
  time: 384000000
        H: 32.5139
       mu: [11 6 7 5 3 10 9 4 1 2 8]
    sigma: [9 5 7 8 3 10 6 1 2 4]
	    c: [7041 5536 5392 4443 3956 3061 1892 1755 1629 959 ...
            938 708 663 579 451 425 278 124 85 85];
			
Another solution:

     time: 3.806700000000000e+09
        H: 39.343424531724168
       mu: [6 11 10 5 9 3 8 2 1 4 7]
    sigma: [7 8 5 3 6 9 10 2 1 4]
	    c: [ 6930 5611 5467 4368 3881 2997 1892 1714 1596 1023 959 ...
		     819 696  510 466  451 278 133 124 85]

