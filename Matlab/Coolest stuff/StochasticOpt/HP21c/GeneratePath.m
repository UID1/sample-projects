function path = GeneratePath(pheromoneLevel, visibility, alpha, beta)
  n = length(visibility);
  currentPath = 1:n;
  currentPosition = randi(n,1);
  for i = 1:n-2
    currentPath([i currentPosition]) = currentPath([currentPosition i]);
    tauijAlpha = pheromoneLevel(currentPosition, currentPath(i+1:end)).^alpha;
    etaijbeta  =     visibility(currentPosition, currentPath(i+1:end)).^beta;
    edgeProbabilities = tauijAlpha .* etaijbeta / (tauijAlpha*etaijbeta');
    rouletteWheelSelection = cumsum(edgeProbabilities);
    winningIndex = find(rand < rouletteWheelSelection, 1);
    currentPosition = currentPath(winningIndex+i);
  end
  path = currentPath;
end