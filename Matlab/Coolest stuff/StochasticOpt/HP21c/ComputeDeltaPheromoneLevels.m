function deltaPheromoneLevels = ComputeDeltaPheromoneLevels(pathCollection,pathLengthCollection)
  numberOfAnts   = size(pathCollection,1);
  numberOfCities = size(pathCollection,2);
  deltaPheromoneLevels = zeros(numberOfCities);
  for i = 1:numberOfAnts
    pathCurrent  = pathCollection(i,:);
    pathPrevious = circshift(pathCurrent, [0 1]);
    pathLength   = pathLengthCollection(i);
    dimPLMatrix  = size(deltaPheromoneLevels);
    deltaIndices = sub2ind(dimPLMatrix, pathCurrent, pathPrevious);
    deltaIndicesTransposed = sub2ind(dimPLMatrix, pathPrevious, pathCurrent);
    deltaPheromoneLevels(deltaIndices) = ...
         deltaPheromoneLevels(deltaIndices) + pathLength^-1;
    deltaPheromoneLevels(deltaIndicesTransposed) = ...
         deltaPheromoneLevels(deltaIndicesTransposed) + pathLength^-1;
  end
end