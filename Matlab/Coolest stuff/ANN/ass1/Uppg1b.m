% Artificial Neural Networks FFR135
% (c) Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1b Solution
%
%



N = 100; % Index i or j ranges over 1,..., N
p = 2:2:100;  % Index \nu or \mu ranges over 1, ..., p

% Parameter sets: N = [10, 50, 100]; p = [2, 10, 20];
clear PWS;
h1 = waitbar(0,'Beraknar ...');
for(tstep=1:length(p))
   clear Zeta;
   Zeta = 2*(rand(N,p(tstep)) > .5) -1;

   % We construct W_ij according to the Hebb rule as defined in
   % page 16 in the lecture notes under section 1.4.2

   W_ij = 1/N*Zeta*Zeta';
   W_ij = W_ij - diag(diag(W_ij));

   % Let's make a stability test

   S = Zeta(:,1);

   S_orig = S;
   S(end-1) = -S(end-1);
   %S(N-2) = -S(N-2);

   Error = sum(sum(-sign(W_ij*S) == S_orig));

   % We compute the cross talk term
   C_k = 0; 
   C_knu = zeros(N,p(tstep));
   %for(mu = 1:p)
   %	for(nu=1:p)
   %		for(j=1:N)
   %			for(k=1:N)
   %				C_k = C_k + Zeta(k,nu)*Zeta(k,mu)*Zeta(j,mu)*Zeta(j,nu)*(j~=k)*(mu~=nu);
   %				C_knu(k,nu) += Zeta(k,nu)*Zeta(k,mu)*Zeta(j,mu)*Zeta(j,nu)*(j~=k)*(mu~=nu);
   %			end
   %		end
   %	end
   %end
   for(k = 1:N)
      for(nu=1:p(tstep))
         for(j=1:N)
            for(mu=1:p(tstep))
               C_knu(k,nu) = C_knu(k,nu) + Zeta(k,nu)*Zeta(k,mu)*Zeta(j,mu)*Zeta(j,nu)*(mu~=nu);
            end
         end
      end
   end

   C_k = -C_k/N;
   C_knu = -C_knu/N;


   C_knuvec = reshape(C_knu, 1, prod(size(C_knu)));
   PDFgrain = 50; %length(C_knuvec);
   Px = linspace(min(min(C_knu)), max(max(C_knu)), PDFgrain);
   PC_knu = zeros(1,PDFgrain-1);
   for(i=2:PDFgrain)
      PC_knu(i-1) = sum(C_knuvec > Px(i-1) & C_knuvec <= Px(i))/length(C_knuvec);
   end
   P_Error(tstep) = sum(C_knuvec > 1)/length(C_knuvec);
   PWS(tstep).C_knu = C_knu;
   waitbar(tstep/length(p), h1);
end
close(h1);

FIGS.fig3 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; [230, 229, 214]; [115, 115, 115]; [176, 169, 165]; [211, 202, 183]]/255;

plot(p,100*P_Error,'LineWidth', 2, 'Color', FIGS.LineColors(6, :));
xlabel('\fontsize{12}Memory p');
ylabel('\fontsize{12}P_{Error} (%)');
title('\fontsize{12}Error probability with respect to memory');
hold on;
plot(2*(1:length(p)), 15*.5*(1-erf(sqrt(N./(4*(1:length(p)))))), 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
legend('Numeric' , 'Analytic');
