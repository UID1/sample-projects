#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <time.h>
#include <windows.h>
#include <limits>

typedef long long int64;
typedef unsigned long long uint64;

#define LEN 1 << 22

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

/*
   We want to evaluate the difference between arrays of structures (AOS) and
   structure of arrays (SOA).
*/

struct innerStruct /* AOS */
{
   double x;
   double y;
};

struct innerArray /* SOA*/
{
   double x[LEN];
   double y[LEN];
};
__global__ void warmup (innerStruct *data, innerStruct *result,
                                const int N)
{
   unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
   if(i<N)
   {
      innerStruct tmp = data[i];
      tmp.x += 10.f;
      tmp.y += 20.f;
      result[i] = tmp;
   }
}

void checkInnerStruct(innerStruct *hostRef, innerStruct *gpuRef, const int N)
{
   const double epsilon = 1.0E-8;
   bool match = 1;
   colors_t colors;
   for(int i=0; i<N; ++i)
   {
      if(abs(hostRef[i].x - gpuRef[i].x) > epsilon)
      {
         match = 0;
         colors.setFg(hRed); std::cout << "Arrays don't match!";
         std::cout << std::endl;
         std::cout << "Diverging x element " << i << ":";
         std::cout << "host value = " << hostRef[i].x;
         std::cout << ", device value = " << gpuRef[i].x;
         colors.setColors(colors.initial_colors);
         std::cout << std::endl;
         break;
      }
     if(abs(hostRef[i].y - gpuRef[i].y) > epsilon)
      {
         match = 0;
         colors.setFg(hRed); std::cout << "Arrays don't match!";
         std::cout << std::endl;
         std::cout << "Diverging y element " << i << ":";
         std::cout << "host value = " << hostRef[i].y;
         std::cout << ", device value = " << gpuRef[i].y;
         colors.setColors(colors.initial_colors);
         std::cout << std::endl;
         break;
      }
   }
   if(match)
      colors.setFg(hGreen);
      std::cout << "Arrays match!" << std:: endl;
   colors.setColors(colors.initial_colors);
}

void initialInnerStruct(innerStruct *ip, int size)
{
   for(int i=0;i<size;++i)
   {
      ip[i].x = (double)(rand()/10000.0f);
      ip[i].y = (double)(rand()/10000.0f);
   }
   return;
}

void testInnerStructHost(innerStruct *A, innerStruct *C, const int N)
{
   for(int idx=0;idx<N;++idx)
   {
      C[idx].x = A[idx].x + 10.f;
      C[idx].y = A[idx].y + 20.f;
   }
   return;
}

__global__ void testInnerStruct(innerStruct *data, innerStruct *result,
                                const int N)
{
   unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
   if(i<N)
   {
      innerStruct tmp = data[i];
      tmp.x += 10.f;
      tmp.y += 20.f;
      result[i] = tmp;
   }
}

int main(int argc, char **argv)
{
   colors_t colors;
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));

  
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", test array of structs." << std::endl;

   /* Allocate host memory */
   int nElem = LEN;
   size_t nBytes = nElem * sizeof(innerStruct);
   innerStruct     *h_A = (innerStruct *) malloc(nBytes);
   innerStruct *hostRef = (innerStruct *) malloc(nBytes);
   innerStruct *gpuRef  = (innerStruct *) malloc(nBytes);
   
   /* Initialize the host array */
   initialInnerStruct(h_A, nElem);
   testInnerStructHost(h_A, hostRef, nElem);
   
   /* Allocate device memory */
   innerStruct *d_A, *d_C;
   CHECK(cudaMalloc((innerStruct **)&d_A, nBytes));
   CHECK(cudaMalloc((innerStruct **)&d_C, nBytes));
   
   /* Copy data from host to device */
   CHECK(cudaMemcpy(d_A, h_A, nBytes, cudaMemcpyHostToDevice));
   
   /* Set up offset for summary */
   int blockSize = 128;
   if(argc>1) blockSize = atoi(argv[1]);
   
   /* Execution configuration */
   dim3 block (blockSize);
   dim3 grid ((nElem+block.x-1)/block.x);
   
   /* Evaluation of kernel 1 */
   double iStart = cpuSecond();
   warmup <<<grid, block>>> (d_A, d_C, nElem);
   CHECK(cudaDeviceSynchronize());
   double iElaps = cpuSecond() - iStart;
   
   colors.setFg(hGray); std::cout << "warmup      ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   colors.setFg(White); std::cout << "() ";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<int>(1000*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;
   
   iStart = cpuSecond();
   testInnerStruct <<<grid, block>>> (d_A, d_C, nElem);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   colors.setFg(hGray); std::cout << "innerStruct ";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   colors.setFg(White); std::cout << "() ";
   colors.setFg(hGray); std::cout << " completed in ";
   colors.setFg(dCyan); std::cout << static_cast<int>(1000*iElaps);
   colors.setFg(White); std::cout << " ms"; colors.setFg(hGray);
   std::cout << "." << std::endl;
  
   /* Copy kernel result back to host side and verify device results */
   CHECK(cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost));
   checkInnerStruct(hostRef, gpuRef, nElem);
   
   /* Free host and device memory */
   CHECK(cudaFree(d_A));
   CHECK(cudaFree(d_C));
   free(h_A);
   free(hostRef);
   free(gpuRef);
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   colors.setColors(colors.initial_colors);
   return EXIT_SUCCESS;
}
