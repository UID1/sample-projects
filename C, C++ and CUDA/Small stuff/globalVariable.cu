#include <stdio.h>
#include <iostream>
#include <cuda_runtime.h>
#include <windows.h>

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}                                                                             \

__device__ float devData;

__global__ void checkGlobalVariable()
{
   /* Display the original value */
   printf("Device: the value of the global variable is %f\n", devData);
   /* Alter the value */
   devData -= 1.0f;
}

int main(void)
{
   /* Initialize the global variable */
   float value = 3.14f;
   CHECK(cudaMemcpyToSymbol(devData, &value, sizeof(float)));
   colors_t colors; colors.setFg(hGray);
   std::cout << "Host:   copied "; colors.setFg(dCyan); std::cout << value;
   colors.setFg(hGray); std::cout << " to the global variable." << std::endl;
   
   /* Invoke the kernel */
   checkGlobalVariable<<<1, 1>>> ();
   CHECK(cudaMemcpyFromSymbol(&value, devData, sizeof(float)));
   std::cout << "Host:   the value changed by the kernel to ";
   colors.setFg(dCyan); std::cout << value; colors.setFg(hGray);
   std::cout << std::endl;
   
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}
