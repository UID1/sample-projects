#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}

int main (int argc, char** argv)
{
   colors_t colors;

   /* Set up device */
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));

   uint64 iSize = 1<<22;
   uint64 nBytes = iSize * sizeof(float);
   
   colors.setFg(dYellow);
   std::cout << argv[0]; colors.setFg(hGray);
   std::cout << " starting at device ";
   colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray);std::cout << ": ";
   colors.setFg(dYellow); std::cout << deviceProp.name; colors.setFg(hGray);
   std::cout << ", memory size: "; colors.setFg(dCyan); std::cout << iSize;
   colors.setFg(hGray); std::cout << ", nByte "; colors.setFg(dCyan);
   std::cout << std::setprecision(4) << nBytes/(1024.0f*1024.0f);
   colors.setFg(hGray); std::cout << " MB." << std::endl;
   
   /* Allocate host memory */
   float *h_a; /* = (float *)malloc(nBytes); in the previous example */
   CHECK(cudaMallocHost((float **)&h_a, nBytes));
   
   /* Allocate device memory */
   float *d_a;
   CHECK(cudaMalloc((float **)&d_a, nBytes));
   
   /* Initialize host memory */
   memset(h_a, 0, nBytes);


   for(uint64 i=0;i<iSize;++i) h_a[i] = 100.10f;

   /* Transfer data from the host to the device */
   CHECK(cudaMemcpy(d_a, h_a, nBytes, cudaMemcpyHostToDevice));

   /* Transfer data from the device to the host */
   CHECK(cudaMemcpy(h_a, d_a, nBytes, cudaMemcpyDeviceToHost));

   /* Free memory */
   CHECK(cudaFree(d_a));
   CHECK(cudaFreeHost(h_a)); /* DON'T use free() here!!! */
   
   /* Reset the device */
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}
