%Plottar exakta l�sningar till Isingmodellen utan magnetf�lt i 2 dimensioner


if (muB==0)
kT = .05:.001:5; %temperaturintervallet 
[x,y]=ellipke(4*(sinh(2*J./kT)).^2./(cosh(2*J./kT)).^4);  %elliptiska integraler... 
k=J./kT;

%energi
subplot(1,3,1);plot(kT, -J*coth(2*k).*(1+(2/pi)*(2*(tanh(2*k)).^2-1).*x),'k'); hold on
title('Medelenergi'), ylabel('<E>/N'), xlabel('kT')

%specifikt v�rme 
subplot(1,3,2);plot(kT, (4/pi)*(k.*coth(2*k)).^2.*(x-y-(1-(tanh(2*k)).^2).*((pi/2)+(2*(tanh(2*k)).^2-1).*x)),'k'); hold on
title('Specifikt v�rme'), ylabel('C/Nk'), xlabel('kT')

%magnetisering
subplot(1,3,3); kT = .05:.001:(J/.44); 
plot(kT, 1-(1-(tanh(J./kT)).^2.).^4./(16*(tanh(J./kT)).^4.),'k'); hold on
subplot(1,3,3); kT = (J/.44):.001:5; 
plot(kT, 0,'k'); hold on
title('Magnetisering'), ylabel('M/N'), xlabel('kT')

end
