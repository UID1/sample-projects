0064A970 ; Attributes: bp-based frame                                                                        
0064A970                                                                                                     
0064A970 sub_64A970      proc near               ; CODE XREF: sub_64AD00+55p                                
0064A970                                                                                                     
0064A970 var_8           = dword ptr -8                                                                      
0064A970 var_4           = dword ptr -4                                                                      
0064A970 arg_0           = dword ptr  8                                                                      
0064A970 arg_4           = dword ptr  0Ch                                                                    
0064A970                                                                                                     
0064A970                 push    ebp             ; Push ebp so that we can restore it when we return         
0064A971                 mov     ebp, esp        ; Make a new stack frame for this routine                   
0064A973                 sub     esp, 8                                                                      
0064A976                 mov     eax, [ebp+arg_4] ; eax => #S/N                                              
0064A979                 push    ebx             ; \                                                         
0064A97A                 push    esi             ; |                                                         
0064A97B                 push    edi             ;  > "Stdcall" measures                                     
0064A97C                 push    eax             ; |                                                         
0064A97D                 push    ecx             ; /                                                         
0064A97E                 lea     edx, [ebp+var_8] ; edx = &var_8 => ?subroutine???                           
0064A981                 push    edx                                                                         
0064A982                 call    sub_64A870      ; sub_64A870(& var_8, cx, arg_4)                            
0064A982                                         ; ax = a computed signature of >#L/K and                    
0064A982                                         ; {var_4 var_8} now contains the scrambled >#L/K            
0064A987                 add     esp, 0Ch        ; Move Stack Pointer 12 steps down                          
0064A98A                 xor     eax, eax        ; \                                                         
0064A98C                 xor     ebx, ebx        ; |                                                         
0064A98C                                         ;  > Clear all data registers                               
0064A98E                 xor     ecx, ecx        ; |                                                         
0064A990                 xor     edx, edx        ; /                                                         
0064A992                 mov     esi, [ebp+var_8] ; esi = 1st scrambled half of >#L/K                        
0064A995                 lea     edi, ds:674980h ; edi => some place in the binary file                      
0064A99B                                                                                                     
0064A99B loc_64A99B:                             ; CODE XREF: sub_64A970+3Fj                                
0064A99B                 shr     esi, 1          ; This loop shifts the bits out of esi that contains the    
0064A99B                                         ; 1st half of the scrambled >#L/K until it's empty and      
0064A99B                                         ; XOR some values of binary table into registers eax-edx    
0064A99B                                         ; as many times as there are binary ones in the 1/2:SC>#L/K 
0064A99D                 jnb     short loc_64A9AC                                                            
0064A99F                 xor     eax, [edi]                                                                  
0064A9A1                 xor     ebx, [edi+4]                                                                
0064A9A4                 xor     ecx, [edi+8]                                                                
0064A9A7                 xor     edx, [edi+0Ch]                                                              
0064A9AA                 test    esi, esi                                                                    
0064A9AC                                                                                                     
0064A9AC loc_64A9AC:                             ; CODE XREF: sub_64A970+2Dj                                
0064A9AC                 lea     edi, [edi+10h]                                                              
0064A9AF                 jnz     short loc_64A99B ; Jump as long as esi isn't empty                          
0064A9B1                 mov     esi, [ebp+var_4] ; Now, we get the other half of the scrambled >#L/K        
0064A9B4                 lea     edi, ds:674B80h ; and get another binary table                              
0064A9BA                                                                                                     
0064A9BA loc_64A9BA:                             ; CODE XREF: sub_64A970+5Ej                                
0064A9BA                 shr     esi, 1          ; This loop is doing exactly the same thing as the one above
0064A9BA                                         ; but with the other half of the scrambled >#L/K and a      
0064A9BA                                         ; different binary table                                    
0064A9BC                 jnb     short loc_64A9CB                                                            
0064A9BE                 xor     eax, [edi]                                                                  
0064A9C0                 xor     ebx, [edi+4]                                                                
0064A9C3                 xor     ecx, [edi+8]                                                                
0064A9C6                 xor     edx, [edi+0Ch]                                                              
0064A9C9                 test    esi, esi                                                                    
0064A9CB                                                                                                     
0064A9CB loc_64A9CB:                             ; CODE XREF: sub_64A970+4Cj                                
0064A9CB                 lea     edi, [edi+10h]                                                              
0064A9CE                 jnz     short loc_64A9BA ; end of loop                                              
0064A9D0                 xor     esi, esi        ; \                                                         
0064A9D0                                         ;   Now, we clear esi and edi                               
0064A9D2                 xor     edi, edi        ; /                                                         
0064A9D4                                                                                                     
0064A9D4 loc_64A9D4:                             ; CODE XREF: sub_64A970+7Ej                                
0064A9D4                                         ; sub_64A970+8Bj                                           
0064A9D4                 bt      edx, 4          ; Copy 4th bit of edx into Carry Flag                       
0064A9D4                                         ; This loop is repeated until ecx and                       
0064A9D4                                         ; ecs equal zero                                            
0064A9D8                 rcl     ecx, 4          ; Rotate ecx 4 steps left through the Carry Flag            
0064A9DB                 jnb     short loc_64A9FD ; Jump if CF turns zero after rotation 0                   
0064A9DD                 xor     esi, eax        ; XOR eax into esi and                                      
0064A9DF                 xor     edi, ebx        ; XOR ebx into edi                                          
0064A9E1                                                                                                     
0064A9E1 loc_64A9E1:                             ; CODE XREF: sub_64A970+8Fj                                
0064A9E1                                         ; sub_64A970+93j                                           
0064A9E1                 rcr     edx, 5          ; Rotate 5 steps right through CF                           
0064A9E4                 bt      ebx, 1Bh        ; Copy 27th bit to CF                                       
0064A9E8                 rcr     eax, 4          ; Shake                                                     
0064A9EB                 rcl     ebx, 5          ; and shuffle                                               
0064A9EE                 jnb     short loc_64A9D4 ; Jump back if CF=0                                        
0064A9F0                 xor     eax, 800008h    ; otherwise XOR these two values into eax                   
0064A9F5                 xor     ebx, 80000300h  ; and ebx respectively                                      
0064A9FB                 jmp     short loc_64A9D4 ; before we make the jump                                  
0064A9FD ; ---------------------------------------------------------------------------                       
0064A9FD                                                                                                     
0064A9FD loc_64A9FD:                             ; CODE XREF: sub_64A970+6Bj                                
0064A9FD                 test    edx, edx                                                                    
0064A9FF                 jnz     short loc_64A9E1 ; jump as long as edx is nonzero                           
0064AA01                 test    ecx, ecx                                                                    
0064AA03                 jnz     short loc_64A9E1 ; jump as long as ecx is nonzero                           
0064AA03                                         ; We end up with eax=8C47DF2, ebx=02314D55                  
0064AA03                                         ; esi = 6334C958 and edi = 73F3D796                         
0064AA05                 mov     [ebp+var_8], esi ; \                                                        
0064AA05                                         ;   We replace the scrambled value of >#L/K with esi, edi   
0064AA08                 mov     [ebp+var_4], edi ; /                                                        
0064AA0B                 mov     eax, [ebp+arg_4] ; eax => #S/N + signature                                  
0064AA0E                 mov     edx, [ebp+arg_0] ; edx => reserved empty space                              
0064AA11                 push    eax             ; Push mem loc of #S/N+sig into stack                       
0064AA12                 lea     ecx, [ebp+var_8] ; ecx => var_4 = 1st half of scrambled >#L/K               
0064AA15                 push    ecx             ; Push => var_4 into stack                                  
0064AA16                 push    edx             ; Finally push reserved "empty" space into stack            
0064AA17                 call    sub_64A870      ; ax = sub_64A870(arg_0, & var_8, arg_4);                   
0064AA17                                         ; &arg_0 => 16 bytes even more scrambled value              
0064AA1C                 add     esp, 0Ch                                                                    
0064AA1F                 pop     edi                                                                         
0064AA20                 pop     esi                                                                         
0064AA21                 pop     ebx                                                                         
0064AA22                 mov     esp, ebp                                                                    
0064AA24                 pop     ebp             ; returned values are now found in eax, ecx and edx         
0064AA25                 retn                                                                                
0064AA25 sub_64A970      endp                                                                                