% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% The  Matlab script  'main2.m' uses  the  internal  matlab package  and
% function ARMAX  to fit ARMA  models into the  electricity  price data.
% The model estimations are evaluated  using a subset of the information
% criteria  and prediction  error  estimators  introduced  in 'main1.m'.
% Trend and seasonalities are  evaluated and  filtered out of  the price
% data. The spikes in  the data are  also analyzed  and methods to over-
% come the hurdles that are caused by these spikes are investigated.
%
% 'main4.m' is a continuation  of main2.m  and cannot be run without it.
% This script runs the ARMAX estimations  given the data computed in the
% previous man scripts.  main3 and main4 are  computationally  intensive
% and takes  a few hours  to run on a  mainstream computer.  This script
% makes ARMAX estimations on the log() values of the observed data, i.e.
% log(ELh2010) and log(deseasonalized ELh2010)

% +----------------+
% |     Step 4     |
% +----------------+

if ~any(strcmp(who,'ELh2010_des1'))
   warning('This script should not be run before main2.m.')
end

% In this part we apply the logarithm before we continue. We don't need
% to  recalculate the filters  but we need to readjust  the temperature
% influence and the trend estimation before we can fit data with armax.

% In case we didn't to run main3.m
if ~any(strcmp(who,'tmp2010'))
   % Get the temperature data for year 2010
   tmp2010 = temp_proc(8760, 'cubic');
end

% We establish the log data.
ELDATA = log(ELh2010_des1);
% All elements are >0 so far so good

A1        = [ELDATA(3:end-1) ELDATA(2:end-2) ...
             ELDATA(1:end-3)];
el_acoeff = (A1'*A1)\A1'*tmp2010.national(4:end);
ELDATA1t = ELDATA(4:end) - A1*el_acoeff;
ELTREND1t = est_ma(ELDATA1t, 500);

% Run ARMAX estimations on all data

[m1a par1a] = armabat2(log(ELSPOTh2010), ...
                        tmp2010.national, 1:20, 1:20, 1:3, 2, '1a')
[m1b par1b] = armabat2(ELDATA, ...
                        tmp2010.national, 1:20, 1:20, 1:3, 2, '1b')
[m1c par1c] = armabat2(ELDATA(4:end)-ELTREND1t.removable, ...
			tmp2010.national(4:end), ...
			1:20, 1:20, 1:3, 2, '1c')

% Run on the first 3 months of data

[m2a par2a] = armabat2(log(ELSPOTh2010(1:2190)), ...
                        tmp2010.national(1:2190), ...
			1:20, 1:20, 1:3, 2, '2a')
[m2b par2b] = armabat2(ELDATA(1:2190), ...
                        tmp2010.national(1:2190), ...
			1:20, 1:20, 1:3, 2, '2b')
[m2c par2c] = armabat2(ELDATA(4:2193) - ...
                        ELTREND1t.removable(1:2190), ...
                        tmp2010.national(4:2193), ...
			1:20, 1:20, 1:3, 2, '2c')


% Results:
%
% 1a: p: 20, q: 18, r: 3, minaic: -5.0428
% 1b: p: 20, q: 8,  r: 3, minaic: -5.7913
% 1c: p: 20, q: 18, r: 3, minaic: -5.7936
%
% 2a: p: 19, q: 17, r: 2, minaic: -5.6269
% 2b: p: 18, d: 15, r: 3, minaic: -5.3446
% 2c: p: 20, q: 19, r: 3, minaic: -5.3432
%
% Further investigation shows that it has local minima at r=5, 20 and 31
% where 31 has the lowest value. r = 48 yields and even lower AIC value.
% A local minimum is found at p = 46 (given q = 18 and r = 31).  An even
% lower value is found at 84 (which is lower than for p = 89).
%
% Selecting  subdata  2000 - 4400  does not make a  better fit  than the
% whole  data  but the fit  is not much worse either  when  tweaking the
% parameters. The winner is:
% 
%     p: 84, q: 18, r: 31, minaic: -5.9698
% 
% when fitting the whole data. I conclude that the reason why the fit is
% worse is because the filtration  is adapted for the whole series which
% makes the seasonal adjustments less accurate for the local data.  This
% is also seen when applying the  ARMAX() directly  in the raw data (2a)
% where no seasonal corrections have been made which yields a lower AIC.


