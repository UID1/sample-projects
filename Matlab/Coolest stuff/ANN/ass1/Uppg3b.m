% Artificial Neural Networks FFR135
% (c) Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 3b Solution
%
%

alpha_0 = 0.025;

%Where m_1 has dropped 50%

clear beta_0, TM_1tb;
beta_0 = (.1:.1:.8).^-1;
N = 1000;
p = round(N*alpha_0);
T = 40000;
AVGWin = 1000;
TM_1b = zeros(T, length(beta_0));
% Dim 1; beta_0 Dim2: tavg_m_1(t) < .5*m_1(0)
h2 = waitbar(0,'Beraknar ...');
for b_c = 1:length(beta_0);
   clear Zeta;
   clear m_1;
   Zeta = 2*(rand(N,p) > .5) -1;

   W_ij = 1/N*Zeta*Zeta';
   W_ij = W_ij - diag(diag(W_ij));

   S = Zeta(:,1);

   g = @(x)(1/(1+exp(-2*beta_0(b_c)*x)));

   for t = 1:T;
      istep = round(N*rand);
      istep = (istep == 0) + (istep ~= 0)*istep;
      b_i = W_ij*S;
      S(istep) = (rand <= g(b_i(istep))) - (rand > g(b_i(istep)));
      m_1(t) = 1/N*Zeta(:,1)'*S;
	  TM_1tb(t) = mean(m_1(max(t-AVGWin,1):t));
   end
   TM_1b(:, b_c) = cumsum(m_1')./(1:length(m_1))';
   i_crit = find(abs(diff(TM_1tb < .5)));
   if prod(size(i_crit)) == 0
      m_c(b_c) = 0;
   else
      m_c(b_c) = m_1(i_crit);
   end
   waitbar(b_c/length(beta_0), h2);
end
close(h2);
