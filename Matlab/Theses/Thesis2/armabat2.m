function [mbest,bestpar]=armabat2(x,y,pvec,qvec,rvec,delay,msg)
% SYNOPSIS
%
%  [mbest, bestpar] = ARMABAT2(x, y, pvec, qvec, rvec, delay, msg)
%
% DESCRIPTION
%
%  This functions computes ARMA(p,q,r) models for (p,q,r) in
%  (x, y, pvec, qvec, rvec) data; it returns the best according to AIC
%  where AIC has been modified to account for fixed parameters.
%
% INPUT
%
%
%   x                 : input data
%   y                 : exogeneous data supposedly affecting x
%   pvec              : vector of p's ; set pvec=[0] for no AR
%   qvec              : vector of q's; set qvec=[0] for no MA
%   rvec              : vector of r's; for pure ARMA estimation
%                       use armabat3
%   delay             : the delay before the exogeneous data y begins
%                       to affect x specified in number of time steps.
%                       Minimum delay is '1'.
%   msg               : a message or tag to use to identify the run
%                       process and monitor progress when running
%                       multiple batches
%
% OUTPUT
%
%   mbest             : matlab model structure for the best one found
%                       using the matlab aic the model parameters can be
%                       retreived from mbest
%   bestpar.minaic    : value of the aic at the min
%   bestpar.p         : best value of p found
%   bestpar.q         : best value of q found
%   bestpar.r         : best value of r found
%   bestpar.aicmtx    : a matrix of all computed AIC values
%   bestpar.fepmtx    : a matrix of all computed FPE values
%   bestpar.newaicmtx : a matrix of modified AIC values
%
% NOTES
%
%  Designed for the final master thesis project in Physics to study
%  model estimation of electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011


nx=length(x);
np=length(pvec);
nq=length(qvec);
nr=length(rvec);
aicsave=-99*ones(np,nq,nr);
newaicsave=-99*ones(np,nq,nr);
fpesave=-99*ones(np,nq,nr);
minaic=1e+6;
delay = max(1, delay);
for pp=1:np 
    p=pvec(pp);
    for qq=1:nq
        q=qvec(qq);
	for rr = 1:nr
	r = rvec(rr);
           if p+q+r ~=0 % ~= is 'not' equal, or '!='
               orders=[p q r delay];
               m=armax(iddata(x,y),orders);
               resids=pe(m,iddata(x,y));         % pe returns the prediction errors
               nres=length(resids.OutputData);
               rhores=acf(resids.OutputData,1); % acf normalized by the variance
               nrho=length(rhores);  
               % next compute the Ljung - Box statistic and P-values
               deltak=floor(nrho/10)+1;
               kvec=[p+q+1:deltak:p+q+1+4*deltak];
               for kk=1:5
                   Qsum=0;
                   for j=2:kvec(kk)+1
                       Qsum=Qsum+(rhores(j).^2)/(nx-j);
                   end
                   Qsum=nx*(nx-1)*Qsum;
                   ljpv(kk)=1-chi2cdf(Qsum,kvec(kk)-p-q);  % df=kvec(kk)-p-q
               end
               aicsave(pp,qq,rr)=aic(m);
               fpesave(pp,qq,rr)=fpe(m);
               AICTEST=1;
               if AICTEST
                   nval = m.EstimationInfo.DataLength;
                   vval = m.EstimationInfo.LossFcn;
                   totalpars=m.na+m.nb+m.nc;
                   netpars=totalpars-length(m.fix);
                   aicval=log(vval)+2*netpars/nval;
                   disp(sprintf('AIC : %g  NEWAIC : %g',aicsave(pp,qq,rr),aicval));
		   newaicsave(pp,qq,rr) = aicval;
               end
               if aicsave(pp,qq,rr) < minaic
                   minaic=aicsave(pp,qq,rr); % save the min
                   bestpar.p=p;
                   bestpar.q=q;
                   mbest=m;
		   bestpar.r=r;
		   bestpar.minaic = minaic;
               end
               disp(sprintf('(p,q,r)=(%d,%d,%d) aic=%g  fpe=%g', ...
	                     p,q,r,aicsave(pp,qq,rr),fpesave(pp,qq,rr)));
               QQ=[kvec;ljpv];
               disp(sprintf('Ljung-Box P-values (%s) : ', msg));
               disp(sprintf('  K=%d P-v=%6.3e \n',QQ(:)));
           end
        end
    end
end
bestpar.aicmtx = aicsave;
bestpar.fepmtx = fpesave;
bestpar.newaicmtx = newaicsave;
end % armabat2.m

