//
//  RiderLocationViewController.swift
//  Uber
//
//  Created by Iberius Pred on 12/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import MapKit
import Parse

class RiderLocationViewController: UIViewController, MKMapViewDelegate {
    
    var requestLocation = CLLocationCoordinate2D()
    var requestUserName = String()
    var requestUserId = String()
    

    @IBOutlet weak var riderMapView: MKMapView!
    @IBOutlet weak var acceptRequestButton: UIButton!
    @IBAction func acceptRequest(_ sender: Any) {
        let query = PFQuery(className: "RiderRequest")
        query.whereKey("userName", equalTo: requestUserName)
        print("User ID of requested user: \(requestUserId), name: \(requestUserName)")
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                print("Query responded with no errors")
                if let riderRequests = objects {
                    print("Found objects: \(riderRequests.count)")
                    for riderRequest in riderRequests {
                        print("Username of rider: \(riderRequest)")
                        riderRequest["driverResponded"] = (PFUser.current()?.username)!
                        print("Added driver name: \(PFUser.current()?.username) to response")
                        riderRequest.saveInBackground()
                        print("This line should come after error message")
                        let requestCLLocation = CLLocation(latitude: self.requestLocation.latitude, longitude: self.requestLocation.longitude)
                        CLGeocoder().reverseGeocodeLocation(requestCLLocation, completionHandler: { (placemarks, error) in
                            if let placemarks = placemarks {
                                if placemarks.count > 0 {
                                    let mkPlacemark = MKPlacemark(placemark: placemarks[0])
                                    let mapItem = MKMapItem(placemark: mkPlacemark)
                                    mapItem.name = self.requestUserName
                                    let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
                                    mapItem.openInMaps(launchOptions: launchOptions)
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("Fetched user location: \(requestLocation)")
        let region = MKCoordinateRegion(center: requestLocation, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        riderMapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = requestLocation
        annotation.title = requestUserName
        riderMapView.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
