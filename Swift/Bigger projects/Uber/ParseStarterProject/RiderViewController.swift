//
//  RiderViewController.swift
//  Uber
//
//  Created by Iberius Pred on 09/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import MapKit
import Parse

class RiderViewController: UberBaseViewController, MKMapViewDelegate {

    var driverOnTheWay = false
    var locationManager = CLLocationManager()
    var userLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var riderRequestActive = false
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var callAnUberButton: UIButton!
    
    @IBAction func callAnUber(_ sender: Any) {
        if riderRequestActive {
            callAnUberButton.setTitle("Call an Uber", for: [])
            riderRequestActive = false
            // Delete request entries from database
            let query = PFQuery(className: "RiderRequest")
            query.whereKey("userName", equalTo: (PFUser.current()?.username)!)
            query.findObjectsInBackground(block: { (objects, error) in
                if error == nil {
                    if let riderRequests = objects {
                        for riderRequest in riderRequests {
                            riderRequest.deleteInBackground()
                        }
                    }
                }
            })
            
        } else {
            if userLocation.latitude != 0, userLocation.longitude != 0 {
                riderRequestActive = true
                self.callAnUberButton.setTitle("Cancel Uber call.", for: [])
                let riderRequest = PFObject(className: "RiderRequest")
                riderRequest["userName"] = PFUser.current()?.username
                riderRequest["location"] = PFGeoPoint(latitude: userLocation.latitude, longitude: userLocation.longitude)
                riderRequest.saveInBackground(block: { (success, error) in
                    if success {
                        print("Called an uber")
                    } else {
                        self.createAlert(title: "Parse Error", message: "Unable to call Uber. Try again.")
                        self.callAnUberButton.setTitle("Call an Uber", for: [])
                        self.riderRequestActive = false
                    }
                })
            } else {
                self.createAlert(title: "GPS Error", message: "Unable to detect your location.")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Initizialize routine for updating the location on the mapview
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        // Do any additional setup after loading the view.
        
        callAnUberButton.isHidden = true
        let query = PFQuery(className: "RiderRequest")
        query.whereKey("userName", equalTo: (PFUser.current()?.username)!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if let riderRequests = objects, riderRequests.count > 0 {
                    self.riderRequestActive = true
                    self.callAnUberButton.setTitle("Cancel Uber call.", for: [])
                }
                self.callAnUberButton.isHidden = false
            }
        })
    }
    
    // This function is called whenever the GPS location is updated
    // In our case we make the map update according to this location.
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let location = manager.location?.coordinate {
//            //let center = CLLocationCoordinate2D()
//            let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//            //self.mapView.setRegion(region, animated: true)
//            
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logoutSegue" {
            locationManager.stopUpdatingLocation()
            PFUser.logOut()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// This function is called whenever the GPS location is updated. In our case we make the map update according to this location.
// This is the way it is done in Swift 3.
extension RiderViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            if let location = manager.location?.coordinate {
                //let center = CLLocationCoordinate2D()
                userLocation = location
                if !driverOnTheWay {
                    let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                    self.mapView.setRegion(region, animated: true)
                }
                // Set an annotation on the map
                self.mapView.removeAnnotations(self.mapView.annotations) // Clears the map of all existing annotations.
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = "Your location"
                self.mapView.addAnnotation(annotation)
                // Update the users GPS coordinates
                let query = PFQuery(className: "RiderRequest")
                query.whereKey("userName", equalTo: (PFUser.current()?.username)!)
                query.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        if let riderRequests = objects {
                            for riderRequest in riderRequests {
                                riderRequest["location"] = PFGeoPoint(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                                riderRequest.saveInBackground()
                            }
                        }
                    }
                })

            }
            if riderRequestActive == true {
                let query = PFQuery(className: "RiderRequest")
                query.whereKey("userName", equalTo: (PFUser.current()?.username)!)
                query.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        if let riderRequests = objects {
                            for riderRequest in riderRequests {
                                if let driverUsername = riderRequest["driverResponded"] as? String {
                                    let query = PFQuery(className: "DriverLocation")
                                    query.whereKey("username", equalTo: driverUsername)
                                    query.findObjectsInBackground(block: { (objects, error) in
                                        if let driverLocations = objects {
                                            for driverLocationObject in driverLocations {
                                                if let driverLocation = driverLocationObject["location"] as? PFGeoPoint {
                                                    self.driverOnTheWay = true
                                                    let driverCLLocation = CLLocation(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
                                                    let riderCLLocation = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                                                    let distance = riderCLLocation.distance(from: driverCLLocation) / 1000
                                                    let distanceLabel = String(format: "%.2f km away ", distance)
                                                    self.callAnUberButton.setTitle("Driver \(distanceLabel)is on his way!", for: [])
                                                    let latDelta = abs(driverLocation.latitude - self.userLocation.latitude) * 2 + 0.005
                                                    let lonDelta = abs(driverLocation.longitude - self.userLocation.longitude) * 2 + 0.005
                                                    let region = MKCoordinateRegion(center: self.userLocation, span: MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta))
                                                    self.mapView.removeAnnotations(self.mapView.annotations)
                                                    self.mapView.setRegion(region, animated: true)
                                                    let userLocationAnnotation = MKPointAnnotation()
                                                    userLocationAnnotation.coordinate = self.userLocation
                                                    userLocationAnnotation.title = "Your location"
                                                    self.mapView.addAnnotation(userLocationAnnotation)
                                                    let driverLocationAnnotation = MKPointAnnotation()
                                                    driverLocationAnnotation.coordinate = CLLocationCoordinate2D(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
                                                    driverLocationAnnotation.title = "Your driver"
                                                    self.mapView.addAnnotation(driverLocationAnnotation)
                                                }
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                })
            }
        }
        
    }
    
}
