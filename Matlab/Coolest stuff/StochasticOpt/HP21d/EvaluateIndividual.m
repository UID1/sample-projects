function fitnessValue = EvaluateIndividual(currentItinerary, cityLocations)
  %cityLocations = LoadCityLocations;
  travelingDistances = (cityLocations(currentItinerary,:) - ...
        cityLocations(circshift(currentItinerary, [0 1]),:)).^2;
  totalTravelingDistance = sum(sum(travelingDistances));
  fitnessValue = 1 / totalTravelingDistance;
end

