%
%  Course: Stochastic optimization
%  Assignment: Hand-In 1.1
%  Author: Robin Axelsson
%  Term: Fall 2016
%
%    x_0 = starting point
%     mu = penalty coefficient
%    eta = gradient descent step length
%      T = threshold
%

function optimumPoint = GradientDescent(x_0, mu, eta, T)
  xPoint = x_0;
  gradValue = inf;
  while (norm(gradValue) >= T)
    gradValue = Gradient(xPoint, mu);
    xPoint = xPoint - eta * gradValue;
    if(~isfinite(norm(xPoint)))
      break
    end
  end
  optimumPoint = xPoint;