%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Ant system (AS) for TSP.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cityLocation = LoadCityLocations();
numberOfCities = length(cityLocation);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numberOfAnts = 100;  % To do: Set to appropriate value.
alpha = 1.0;        % To do: Set to appropriate value.
beta = 5.0;         % To do: Set to appropriate value.
rho = 0.2;          % To do: set to appropriate value.

% To do: Write the GetNearestNeighbourPathLength function
nearestNeighbourPathLength = GetNearestNeighbourPathLength(cityLocation); % To do: Write the GetNearestNeighbourPathLength function
tau0 = numberOfAnts/nearestNeighbourPathLength;

targetPathLength = 123.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

range = [0 20 0 20];
tspFigure = PreInitializeTspPlot(cityLocation, range);
tspLabel = xlabel('Initializing ...', 'Interpreter', 'tex');
tspPathLengthLabel = ylabel(' ');
pheromoneEdges = InitializePheromoneEdges(cityLocation);
Initialize2TspPlot(cityLocation);
connection = InitializeConnections(cityLocation);
pheromoneLevel = InitializePheromoneLevels(numberOfCities, tau0); % To do: Write the InitializePheromoneLevels
visibility = GetVisibility(cityLocation);                         % To do: write the GetVisibility function
plot(cityLocation(:,1), cityLocation(:,2), 'o', 'MarkerSize', 6, ...
     'MarkerFaceColor', [0 .4 .65], 'MarkerEdgeColor', [0 .25 .5]);
pheromoneFigure = figure(2);
pheromonePlot = surfl(pheromoneLevel+triu(ones(50)+NaN)');
set(pheromonePlot,'EdgeAlpha', .3, 'EdgeLighting', 'gouraud', ...
                      'FaceAlpha', .6, 'FaceLighting', 'gouraud', ...
                      'FaceColor', 'interp');
axis([1 50 1 50 0 1]);
colormap('Hot');
view([45 20]);
title('\fontsize{12}Pheromone path distribution between nodes', 'FontWeight','Normal');
zlabel('Pheromone level \tau_{ij}', 'Interpreter', 'tex');
xlabel('i', 'FontAngle', 'italic');
ylabel('j', 'FontAngle', 'italic');
movegui(pheromoneFigure, [30 550]);
set(tspFigure, 'Position', [640, 250, 700, 550]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
minimumPathLength = inf;

iIteration = 0;

while (minimumPathLength > targetPathLength)
 iIteration = iIteration + 1;
 set(tspLabel, 'String', [ ...
     sprintf('Colony size: %d, \\alpha = %1.2f, ', numberOfAnts, alpha) ...
     sprintf('\\beta = %1.2f, \\rho = %1.2f, ', beta, rho) ...
     sprintf('Iteration: %d', iIteration) ]);
 set(tspPathLengthLabel, 'String', ...
     sprintf('Path length: %.5f', minimumPathLength));
 PlotPheromones(pheromoneEdges, pheromoneLevel, cityLocation);
 set(pheromonePlot, 'ZData', pheromoneLevel+triu(ones(50)+NaN)');
 drawnow;
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%
 % Generate paths:
 %%%%%%%%%%%%%%%%%%%%%%%%%%

 pathCollection = [];
 pathLengthCollection = [];
 for k = 1:numberOfAnts
  path = GeneratePath(pheromoneLevel, visibility, alpha, beta);   % To do: Write the GeneratePath function (and any necessary functions called by GeneratePath).
  pathLength = GetPathLength(path,cityLocation);                  % To do: Write the GetPathLength function
  if (pathLength < minimumPathLength)
    minimumPathLength = pathLength;
    disp(sprintf('Iteration %d, ant %d: path length = %.5f',iIteration,k,minimumPathLength));
    PlotPath(connection, cityLocation, path);
  end
  pathCollection = [pathCollection; path];           
  pathLengthCollection = [pathLengthCollection; pathLength];
 end

 %%%%%%%%%%%%%%%%%%%%%%%%%%
 % Update pheromone levels
 %%%%%%%%%%%%%%%%%%%%%%%%%%

 deltaPheromoneLevel = ComputeDeltaPheromoneLevels(pathCollection,pathLengthCollection);  % To do: write the ComputeDeltaPheromoneLevels function
 pheromoneLevel = UpdatePheromoneLevels(pheromoneLevel,deltaPheromoneLevel,rho);          % To do: write the UpdatePheromoneLevels function

end







