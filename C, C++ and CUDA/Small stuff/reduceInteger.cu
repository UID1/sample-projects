#include <stdio.h>
#include <cuda_runtime.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <time.h>
#include <windows.h>

typedef long long int64;
typedef unsigned long long uint64;

/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}                                                                             \

/* The following C function is a recursive implementation of the 
   interleaved pair approach.
*/

int64 recursiveReduce(int64 *data, int64 const size)
{
   /* Check for termination */
   if(size == 1) return data[0];
   
   /* Renew the stride */
   int64 const stride = size / 2;
   
   /* In-place reduction */
   for(int64 i=0; i<stride; ++i) data[i] += data[i + stride];
   
   /* Call recursively */
   return recursiveReduce(data, stride);
}

template <typename iox1>
__global__ void reduceNeighbored(iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   /* Set the thread ID */
   uint64 tid (threadIdx.x);
   uint64 idx (blockIdx.x * blockDim.x + threadIdx.x); 
   
   /* Convert global data pointer to the local pointer of this block*/
   iox1 *idata = g_idata + blockIdx.x * blockDim.x;
   /* Boundary check */
   if(idx>=n) return;
   /* In-place reduction in global memory */
   for(uint64 stride=1; stride < blockDim.x; stride*=2)
   {
      if((tid%(2*stride))==0) idata[tid] += idata[tid+stride];
      __syncthreads();
   }
   if(tid==0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceNeighboredLess (iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x*blockDim.x + threadIdx.x;
   
   /* Convert global data pointer to a local pointer for each block, 
      so each pointer roams between 0 and blockDim.x-1 for each block.
   */
   iox1 *idata = g_idata + blockIdx.x * blockDim.x;
   
   /* Boundary check */
   if(idx >=n) return;
   
   /* In-place reduction in global memory */
   for(int64 stride = 1; stride < blockDim.x;stride*=2)
   {
      uint64 index=2*stride*tid;
      if(index<blockDim.x) idata[index] += idata[index+stride];
      __syncthreads();
   }
   /* When finished, write the results back to global memory */
   if(tid==0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceInterleaved (iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x*blockDim.x + threadIdx.x;
   
   /* Convert global data pointer to a local pointer for each block, 
      so each pointer roams between 0 and blockDim.x-1 for each block.
   */
   iox1 *idata = g_idata + blockIdx.x*blockDim.x;

   /* Boundary check */
   if(idx >=n) return;
   
   /* In-place reduction in global memory */
   for(int64 stride = blockDim.x/2; stride > 0; stride >>= 1)
   {
      if(tid<stride) idata[tid] += idata[tid+stride];
      __syncthreads();
   }
   if(tid==0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceUnrolling2 (iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x * blockDim.x*2 + threadIdx.x;
   
   /* Convert global data pointer to the local pointer of this block */ 
   iox1 *idata = g_idata + blockIdx.x * blockDim.x*2;
   
   /* Unrolling 2 data blocks */
   if(idx+blockDim.x<n) g_idata[idx] += g_idata[idx+blockDim.x];
   __syncthreads();
   
   /* In-place reduction in global memory */
   for(int64 stride = blockDim.x/2; stride > 0; stride >>=1)
   {
      if(tid<stride) idata[tid] += idata[tid+stride];
      __syncthreads();
   }
   /* Write result for this block to global memory */
   if(tid == 0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceUnrolling4 (iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x*blockDim.x*4+threadIdx.x;

   /* Convert global data pointer to the local pointer of this block */ 
   iox1 *idata = g_idata + blockIdx.x*blockDim.x*4;
   
   /* Unrolling 4 data blocks */
   if(idx+3*blockDim.x<n)
   {
      iox1 a1 = g_idata[idx];
      iox1 a2 = g_idata[idx +   blockDim.x];
      iox1 a3 = g_idata[idx + 2*blockDim.x];
      iox1 a4 = g_idata[idx + 3*blockDim.x];
      g_idata[idx] = a1 + a2 + a3 + a4;
   }
   __syncthreads();
   for(int64 stride = blockDim.x/2; stride > 0; stride >>=1)
   {
      if(tid<stride) idata[tid] += idata[tid+stride];
      __syncthreads();
   }
   if(tid == 0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceUnrolling8 (iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x*blockDim.x*8+threadIdx.x;

   /* Convert global data pointer to the local pointer of this block */ 
   iox1 *idata = g_idata + blockIdx.x*blockDim.x*8;
   
   /* Unrolling 8 data blocks */
   if(idx+7*blockDim.x<n)
   {
      iox1 a1 = g_idata[idx];
      iox1 a2 = g_idata[idx +   blockDim.x];
      iox1 a3 = g_idata[idx + 2*blockDim.x];
      iox1 a4 = g_idata[idx + 3*blockDim.x];
      iox1 a5 = g_idata[idx + 4*blockDim.x];
      iox1 a6 = g_idata[idx + 5*blockDim.x];
      iox1 a7 = g_idata[idx + 6*blockDim.x];
      iox1 a8 = g_idata[idx + 7*blockDim.x];
      g_idata[idx] = a1+a2+a3+a4+a5+a6+a7+a8;
   }
   __syncthreads();
   for(int64 stride = blockDim.x/2; stride > 0; stride >>=1)
   {
      if(tid<stride) idata[tid] += idata[tid+stride];
      __syncthreads();
   }
   if(tid == 0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceUnrollWarps8 (iox1 *g_idata, iox1 *g_odata, uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x*blockDim.x*8+threadIdx.x;

   /* Convert global data pointer to the local pointer of this block */ 
   iox1 *idata = g_idata + blockIdx.x*blockDim.x*8;
   
   /* Unrolling 8 data blocks */
   if(idx+7*blockDim.x<n)
   {
      iox1 a1 = g_idata[idx];
      iox1 a2 = g_idata[idx +   blockDim.x];
      iox1 a3 = g_idata[idx + 2*blockDim.x];
      iox1 a4 = g_idata[idx + 3*blockDim.x];
      iox1 a5 = g_idata[idx + 4*blockDim.x];
      iox1 a6 = g_idata[idx + 5*blockDim.x];
      iox1 a7 = g_idata[idx + 6*blockDim.x];
      iox1 a8 = g_idata[idx + 7*blockDim.x];
      g_idata[idx] = a1+a2+a3+a4+a5+a6+a7+a8;
   }
   __syncthreads();
   for(int64 stride = blockDim.x/2; stride > 32; stride >>=1)
   {
      if(tid<stride) idata[tid] += idata[tid+stride];
      __syncthreads();
   }
   
   /* Unroll the final warp */
   if(tid < 32)
   {
      volatile iox1 *vmem = idata;
      vmem[tid] += vmem[tid + 32];
      vmem[tid] += vmem[tid + 16];
      vmem[tid] += vmem[tid + 8];
      vmem[tid] += vmem[tid + 4];
      vmem[tid] += vmem[tid + 2];
      vmem[tid] += vmem[tid + 1];
   }
   
   /* Note that we dont need __syncthreads() here. Because when there are 32
      threads left to execute or less and warp execution is SIMT, there is
      an implicit intra-warp synchronization after each instruction. By not
      invoking synchronization logic through this function we get some
      performance gains.
   */
   
   /*
      Normally, when we declare variables, the compiler has the tendency to
      optimize out some reads and writes to global memory or simply use a
      cache to improve performance. This is not desirable when we want the
      output to such a variable to be accessible by other threads immediately
      after a write.
      
      The 'volatile' qualifier forces variables declared with it to be written
      directly to global memory and eliminates accessibility problems from
      other threads.
   */
   
   /* Transfer the results back to global memory */
   if(tid == 0) g_odata[blockIdx.x] = idata[0];
}

template <typename iox1>
__global__ void reduceCompleteUnrollWarps8 (iox1 *g_idata, iox1 *g_odata,
                                            uint64 n)
{
   uint64 tid = threadIdx.x;
   uint64 idx = blockIdx.x*blockDim.x*8+threadIdx.x;

   /* Convert global data pointer to the local pointer of this block */ 
   iox1 *idata = g_idata + blockIdx.x*blockDim.x*8;
   
   /* Unrolling 8 data blocks */
   if(idx+7*blockDim.x<n)
   {
      iox1 a1 = g_idata[idx];
      iox1 a2 = g_idata[idx +   blockDim.x];
      iox1 a3 = g_idata[idx + 2*blockDim.x];
      iox1 a4 = g_idata[idx + 3*blockDim.x];
      iox1 a5 = g_idata[idx + 4*blockDim.x];
      iox1 a6 = g_idata[idx + 5*blockDim.x];
      iox1 a7 = g_idata[idx + 6*blockDim.x];
      iox1 a8 = g_idata[idx + 7*blockDim.x];
      g_idata[idx] = a1+a2+a3+a4+a5+a6+a7+a8;
   }
   __syncthreads();
   
   /* In-place reduction and complete unroll */
   if(blockDim.x >= 1024 && tid < 512) idata[tid] += idata[tid+512];
   __syncthreads();
   if(blockDim.x >= 512 && tid < 256) idata[tid] += idata[tid+256];
   __syncthreads();
   if(blockDim.x >= 256 && tid < 128) idata[tid] += idata[tid+128];
   __syncthreads();
   if(blockDim.x >= 128 && tid < 64) idata[tid] += idata[tid+64];
   __syncthreads();
   
   /* Unroll the final warp */
   if(tid < 32)
   {
      volatile iox1 *vmem = idata;
      vmem[tid] += vmem[tid + 32];
      vmem[tid] += vmem[tid + 16];
      vmem[tid] += vmem[tid + 8];
      vmem[tid] += vmem[tid + 4];
      vmem[tid] += vmem[tid + 2];
      vmem[tid] += vmem[tid + 1];
   }
   
   /* Note that we dont need __syncthreads() here. Because when there are 32
      threads left to execute or less and warp execution is SIMT, there is
      an implicit intra-warp synchronization after each instruction. By not
      invoking synchronization logic through this function we get some
      performance gains.
   */
   
   /* Transfer the results back to global memory */
   if(tid == 0) g_odata[blockIdx.x] = idata[0];
}

int main (int argc, char **argv)
{
   colors_t colors;
   /* Set up device */
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));

   colors.setFg(hGray);
   std::cout << "Device "; colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray); std::cout << ": "; colors.setFg(dYellow);
   std::cout << deviceProp.name << std::endl; colors.setFg(hGray);

   bool bResult = false;
   
   /* Data runtime initialization */
   int64 size = 1 << 25; // This be the total amount of elements to reduce
   colors.setFg(hGray);
   std::cout << "with array size "; colors.setFg(dCyan);
   std::cout << size << "  "; colors.setFg(hGray); std::cout << std::endl;
   
   /* Configuration of execution */
   int blockSize (1024); // Initial block size
   if(argc>1) blockSize = atoi(argv[1]);
   dim3 block (blockSize);
   dim3 grid ((size+block.x-1)/block.x);
   colors.setFg(hGray); std::cout << "Grid size = "; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", block size = ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hGray);
   std::cout << std::endl;
   
   /* Allocate memory on the host */
   uint64 bytes = size*sizeof(int64);
   int64 *h_idata = (int64 *) malloc(bytes);
   int64 *h_odata = (int64 *) malloc(grid.x*sizeof(int64));
   int64 *tmp     = (int64 *) malloc(bytes);
   
   /* Initialize the array */
   srand(static_cast<int64>(cpuSecond()));
   for(int64 i=0;i<size;++i) h_idata[i] = (int)(rand() & 0xFF);
   colors.setFg(dGray);
   std::cout << "CPU time: "; colors.setFg(dCyan);
   std::cout << static_cast<int64>(cpuSecond()) << std::endl;
   colors.setFg(dGray);
   std::cout << "Random value: "; colors.setFg(dCyan);
   std::cout << (int)(rand() & 0xFF) << std::endl;
   colors.setFg(hGray);
   memcpy(tmp, h_idata, bytes);
   int64 vld_sum = 0;
   for(int64 i=0;i<size;++i) vld_sum += h_idata[i];
   colors.setFg(hGray); std::cout << "Validation value : "; colors.setFg(hCyan);
   std::cout << vld_sum << std::endl; colors.setFg(hGray);
   int64 gpu_sum (0);
   
   /* Allocate memory on the CUDA device */
   int64 *d_idata = NULL;
   int64 *d_odata = NULL;
   cudaMalloc((void **) &d_idata, bytes);
   cudaMalloc((void **) &d_odata, grid.x*sizeof(int64));
   
   /* CPU reduction */
   double iStart = cpuSecond();
   int64 cpu_sum = recursiveReduce(tmp, size);
   double iElaps = cpuSecond() - iStart;
   
   /* CPU label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hYellow); std::cout << "CPU";
   colors.setFg(White); std::cout << "] ";
   colors.setFg(hGray);
   
   colors.setFg(hGray); std::cout << "recursiveReduce";
   colors.setFg(White); std::cout << "()"; colors.setFg(hGray);
   std::cout << " completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   std::cout << "CPU sum: ";
   if(cpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << cpu_sum;
   colors.setFg(hGray); std::cout << std::endl;
   
   /* Kernel 1 reduce the Neighboured elements - warmup session*/
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceNeighbored<<<grid, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "GPU worm injection ";
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64),
         cudaMemcpyDeviceToHost));
   colors.setFg(hGray); std::cout << "reduceNeighbored";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   for(int64 i=0; i<grid.x; ++i) gpu_sum += h_odata[i];
   std::cout << " GPU sum: "; colors.setFg(dCyan);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);
   
   /* Kernel 1 reduce the Neighboured elements - for real this time */
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceNeighbored<<<grid, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;
   
   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";
   
   colors.setFg(hGray); std::cout << "reduceNeighbored";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64),
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   colors.setFg(hGray); std::cout << "reduceNeighbored";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   for(int64 i=0; i<grid.x; ++i) gpu_sum += h_odata[i];
   std::cout << " GPU sum: "; 
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);
   
   /* Comparing full dev->host transaction vs partial transaction */
   //CHECK(cudaDeviceSynchronize());
   //iElaps = cpuSecond() - iStart;
   //colors.setFg(dGray);
   //std::cout << " Full CPU-> host memory transaction time:    ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setBg(Black); colors.setFg(hGray);
   //std::cout << std::endl;
   //iStart = cpuSecond();
   //cudaMemcpy(h_odata, d_odata, grid.x/8*sizeof(int64),
   //           cudaMemcpyDeviceToHost);
   //
   //gpu_sum = 0;
   //CHECK(cudaDeviceSynchronize());
   //iElaps = cpuSecond()-iStart;
   //colors.setFg(dGray);
   //std::cout << " Reduced CPU-> host memory transaction time: ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setBg(Black); colors.setFg(hGray);
   //std::cout << std::endl;
   
   //colors.setFg(hGray); std::cout << "GPU Cmptnroll ";
   //std::cout << "completed in "; colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   //std::cout << " milliseconds." << std::endl;

   //colors.setFg(hGray); std::cout << "reduceNeighbored";
   //colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   //std::cout << grid.x/8; colors.setFg(hGray); std::cout << ", ";
   //colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   //std::cout << ">>>"; colors.setFg(hGray);
   //std::cout << " Reduced GPU sum: ";
   //for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   //if(gpu_sum==vld_sum) colors.setFg(dCyan);
   //else colors.setFg(dRed);
   //std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }
   
   /* Kernel 2 reduce the Neighboured elements with non-interleaved threads */
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceNeighboredLess<<<grid, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceNeighboredLess";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64),
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceNeighboredLess";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: "; colors.setFg(dCyan);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   //colors.setFg(dGray);
   //std::cout << " Full CPU-> host memory transaction time:    ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //
   //iStart = cpuSecond();
   //cudaMemcpy(h_odata, d_odata, grid.x/8*sizeof(int64),
   //           cudaMemcpyDeviceToHost);
   //
   //gpu_sum = 0;
   //CHECK(cudaDeviceSynchronize());
   //for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   //iElaps = cpuSecond()-iStart;
   //
   //colors.setFg(dGray);
   //std::cout << " Reduced CPU-> host memory transaction time: ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //colors.setFg(hGray); std::cout << "reduceNeighboredLess";
   //colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   //std::cout << grid.x/8; colors.setFg(hGray); std::cout << ", ";
   //colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   //std::cout << ">>>"; colors.setFg(hGray);
   //std::cout << " Reduced GPU sum: ";
   //if(gpu_sum==vld_sum) colors.setFg(dCyan);
   //else colors.setFg(dRed);
   //std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }

   /*
     The following code explores the '>>= 1' operation
   */
   
   //colors.setFg(dGray); std::cout << "Let's examine what the x >>= 1 ";
   //std::cout << "operator does to a number." << std::endl;
   //std::cout << "Let's try with ";
   //colors.setFg(dCyan); std::cout << blockSize; colors.setFg(dGray);
   //std::cout << " as is used for block size." << std::endl; 
   //std::cout << "Stride: "; colors.setFg(dCyan);
   //for(int64 stride = blockSize; stride > 0; stride >>= 1)
   //   std::cout << stride << " ";
   //colors.setFg(hGray); std::cout << std::endl;
   //colors.setFg(White); std::cout << "Conclusion: "; colors.setFg(hGray);
   //std::cout << "The >>= 1 operation is essentially dividing by two and";
   //std::cout << std::endl << "rounding down, i.e. returning the floor ";
   //std::cout << "integer value of the division." << std::endl;
   
   /* Kernel 3 reduce the Neighboured elements with interleaved threads */
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceInterleaved<<<grid, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceInterleaved";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64),
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceInterleaved";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: ";
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   //colors.setFg(dGray);
   //std::cout << " Full CPU-> host memory transaction time:    ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //
   //iStart = cpuSecond();
   //cudaMemcpy(h_odata, d_odata, grid.x/8*sizeof(int64),
   //           cudaMemcpyDeviceToHost);
   //
   //gpu_sum = 0;
   //CHECK(cudaDeviceSynchronize());
   //for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   //iElaps = cpuSecond()-iStart;
   //
   //colors.setFg(dGray);
   //std::cout << " Reduced CPU-> host memory transaction time: ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //colors.setFg(hGray); std::cout << "reduceInterleaved";
   //colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   //std::cout << grid.x/8; colors.setFg(hGray); std::cout << ", ";
   //colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   //std::cout << ">>>"; colors.setFg(hGray);
   //std::cout << " Reduced GPU sum: ";
   //if(gpu_sum==vld_sum) colors.setFg(dCyan);
   //else colors.setFg(dRed);
   //std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }

   /* Kernel 4 reduce elements with interleaved threads on a paired unroll */
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceUnrolling2<<<grid.x/2, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceUnrolling2";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64)/2,
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x/2; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceUnrolling2";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: ";
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   //colors.setFg(dGray);
   //std::cout << " Full CPU-> host memory transaction time:    ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //
   //iStart = cpuSecond();
   //cudaMemcpy(h_odata, d_odata, grid.x/8*sizeof(int64),
   //           cudaMemcpyDeviceToHost);
   //
   //gpu_sum = 0;
   //CHECK(cudaDeviceSynchronize());
   //for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   //iElaps = cpuSecond()-iStart;
   //
   //colors.setFg(dGray);
   //std::cout << " Reduced CPU-> host memory transaction time: ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //colors.setFg(hGray); std::cout << "reduceUnrolling2";
   //colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   //std::cout << grid.x/8; colors.setFg(hGray); std::cout << ", ";
   //colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   //std::cout << ">>>"; colors.setFg(hGray);
   //std::cout << " Reduced GPU sum: ";
   //if(gpu_sum==vld_sum) colors.setFg(dCyan);
   //else colors.setFg(dRed);
   //std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }
   
   /* Kernel 5 reduce elements with interleaved threads on a 4-wise unroll */
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceUnrolling4<<<grid.x/4, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceUnrolling4";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64)/4,
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x/4; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceUnrolling4";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: ";
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   //colors.setFg(dGray);
   //std::cout << " Full CPU-> host memory transaction time:    ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //
   //iStart = cpuSecond();
   //cudaMemcpy(h_odata, d_odata, grid.x/8*sizeof(int64),
   //           cudaMemcpyDeviceToHost);
   //
   //gpu_sum = 0;
   //CHECK(cudaDeviceSynchronize());
   //for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   //iElaps = cpuSecond()-iStart;
   //
   //colors.setFg(dGray);
   //std::cout << " Reduced CPU-> host memory transaction time: ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //colors.setFg(hGray); std::cout << "reduceUnrolling4";
   //colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   //std::cout << grid.x/8; colors.setFg(hGray); std::cout << ", ";
   //colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   //std::cout << ">>>"; colors.setFg(hGray);
   //std::cout << " Reduced GPU sum: ";
   //if(gpu_sum==vld_sum) colors.setFg(dCyan);
   //else colors.setFg(dRed);
   //std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }

   /* Kernel 6 reduce elements with interleaved threads on a 8-wise unroll */
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceUnrolling8<<<grid.x/8, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceUnrolling8";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64)/8,
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceUnrolling8";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: ";
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   //colors.setFg(dGray);
   //std::cout << " Full CPU-> host memory transaction time:    ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //
   //iStart = cpuSecond();
   //cudaMemcpy(h_odata, d_odata, grid.x/8*sizeof(int64),
   //           cudaMemcpyDeviceToHost);
   //
   //gpu_sum = 0;
   //CHECK(cudaDeviceSynchronize());
   //for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   //iElaps = cpuSecond()-iStart;
   //
   //colors.setFg(dGray);
   //std::cout << " Reduced CPU-> host memory transaction time: ";
   //colors.setFg(dCyan);
   //std::cout << static_cast<int>(1000*iElaps); colors.setFg(White);
   //std::cout << "  ms "; colors.setFg(hGray); std::cout << std::endl;
   //
   //colors.setFg(hGray); std::cout << "reduceUnrolling8";
   //colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   //std::cout << grid.x/8; colors.setFg(hGray); std::cout << ", ";
   //colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   //std::cout << ">>>"; colors.setFg(hGray);
   //std::cout << " Reduced GPU sum: ";
   //if(gpu_sum==vld_sum) colors.setFg(dCyan);
   //else colors.setFg(dRed);
   //std::cout << gpu_sum << std::endl;colors.setFg(hGray);

   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }
   
   /* Kernel 7 interleaved threads on a 8-wise unroll and a warp unroll*/
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceUnrollWarps8<<<grid.x/8, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceUnrollWarps8";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64)/8,
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceUnrollWarps8";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: ";
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);
   
   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }
   
   /* Kernel 8 interleaved threads on a 8-wise unroll and a warp unroll*/
   CHECK(cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice));
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   reduceCompleteUnrollWarps8<<<grid.x/8, block>>> (d_idata, d_odata, size);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;

   /* CUDA label */
   colors.setFg(White); std::cout << "[";
   colors.setFg(hGreen); std::cout << "CUDA";
   colors.setFg(White); std::cout << "] ";

   colors.setFg(hGray); std::cout << "reduceCompleteUnrollWarps8";
   colors.setFg(White); std::cout << "() "; colors.setFg(hGray);
   std::cout << "completed in "; colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps); colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   iStart = cpuSecond();
   CHECK(cudaMemcpy(h_odata, d_odata, grid.x*sizeof(int64)/8,
         cudaMemcpyDeviceToHost));
   gpu_sum = 0;
   for(int64 i=0; i<grid.x/8; ++i) gpu_sum += h_odata[i];
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray); std::cout << "reduceCompleteUnrollWarps8";
   colors.setFg(hBlue); std::cout << "<<<"; colors.setFg(dCyan);
   std::cout << grid.x; colors.setFg(hGray); std::cout << ", ";
   colors.setFg(dCyan); std::cout << block.x; colors.setFg(hBlue);
   std::cout << ">>>"; colors.setFg(hGray);
   std::cout << " GPU sum: ";
   if(gpu_sum==vld_sum) colors.setFg(dCyan);
   else colors.setFg(dRed);
   std::cout << gpu_sum << std::endl;colors.setFg(hGray);
   
   /* Check the results */
   bResult = (gpu_sum == cpu_sum);
   if(!bResult)
   {
      colors.setFg(hRed);
      std::cout << "Test failed!" << std::endl;
      colors.setFg(hGray);
   }
   
   /* Free host memory */
   free(h_idata);
   free(h_odata);
   
   /* Free GPU memory */
   CHECK(cudaFree(d_idata));
   CHECK(cudaFree(d_odata));
   
   /* Reset the device */
   CHECK(cudaDeviceReset());
   
 
   return EXIT_SUCCESS;
}

