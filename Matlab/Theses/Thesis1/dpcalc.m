%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%

% +------------------------------------------------------------------------------------------------
% ####################################################################################
% #####################################################################
% /////////////////////////////////////////////////////                     |
% //////////////////////////                                               -+-
% ///////                                                                   |
%
%           Definitions
%

% Additional information:
%
%  t = 0 since PS(t) is not defined at other time values

% Maturities and rates for swaps

Mt   = [0 5     10    15    20    25    30   ];
Rswp = [0 3.249 4.074 4.463 4.675 4.775 4.811]*.01;

% Risk profiles with their intensities and probabilities

% In this risk profile we assume that the first period starts at t = 0. This is
% a qualified assumption since the survival probability is 1 at that time. Alter-
% natively we can assume a later time which then would be a matter of adding a
% discount factor. The year differences are specified in the column vector 'dT'. 
% The first column in LR (Low Risk), MR (Medium Risk) and HR (Human Resources)
% contains the intensities and the second column contains the probabilities.

% Since the probabilities already are computed we can use them directly without
% further computations.

LR = [ .0036 .0036 .0065 .0099 .0111 .0177 .0177 .0177 .0177 .0177; ...
      1      .9964 .9834 .9638 .9424 .8931 .8164 .7463 .6822 .6236]';
MR = [ .0202 .0202 .0231 .0266 .0278 .0349 .0349 .0349 .0349 .0349; ...
      1      .9796 .9348 .8857 .8371 .7527 .6305 .5280 .4423 .3705]';
HR = [ .0534 .0534 .0564 .0600 .0614 .0696 .0696 .0696 .0696 .0696; ...
      1      .9470 .8447 .7478 .6603 .5342 .3753 .2636 .1851 .1301]';
dT = [0     1     2     2     2     3     5     5     5     5     ]';

% I want to find a way to construct a detection algorithm that matches the
% elements in LR, MR, and HR with the proper year. Perhaps find(pT == Mt(i)) to
% return those indices and then use them on LR, MR and HR where

pT = cumsum(dT);

% The function for a fair strike price of a CDS agreement is
% (WARNING: For valid S_ab the parameters a,b must be a < b)

S_ab = @(P_0T,a,b, beta)((P_0T(a)-P_0T(b))./(beta*sum(P_0T(a+1:b))));

% This comes directly from equation (4.2).

% So we need to use the Black-Scholes model for swaptions as implemented by Brigo
% (eq 1.26 Brigo-Pallavicini)

d_1= @(K, F, v)((log(F./K)+v.^2/2)./v);
d_2= @(K, F, v)((log(F./K)-v.^2/2)./v);

Bl = @(K, F, v, w)(F.*w.*normcdf(w.*d_1(K,F,v)) - K.*w.*normcdf(w.*d_2(K,F,v)) );

sigmaBS = .17;

% The cumulative density function for a piecewise constant intensity is
% 

F_tau = @(t, lambda)(1-exp(-lambda(:,1)'*(((t>lambda(:,3)).*(lambda(:,3)-lambda(:,2)))+((t<=lambda(:,3))-(t<=lambda(:,2)))*(t-lambda(:,2)'*((t<=lambda(:,3))-(t<=lambda(:,2)))))));

% where 
% lambda = a matrix containing the parameters of the intensity which is
%          assumed to be a step-function with respect to time. The first
%          column of this matrix contains the intensity values, the second
%          column contains the time points where each intensity value
%          starts and the third column contains the time points where
%          each of them stop. So the second and third column defines the
%          start and the end of each time step such that the entire
%          intensity function can be defined as a sum of Heaviside step
%          functions H(t), i.e.
%
%           lambda(t) = lambda(:,1)'*(H(t-lambda(:,2))-H(t-lambda(:,3)))
%                     = lambda(:,1)'*((t > lambda(:,2))-(t > lambda(:,3)))
%                     = lambda(:,1)'*((t <= lambda(:,3))-(t <= lambda(:,2)))
%
%         so the sums are expressed here as scalar products
%
% So in this case: lambda = [#R(:,1) pT [pT(2:end);100] ] where # is L, M or H
% and Q(\tau > t) =  1 - F_tau(t, [#R(:,1) pT [pT(2:end);100])

% The risk-free discount functions are only defined on discrete points in time. 
% If we want to find out the values between these points we need to interpolate:

% This defines a discount function that follows a piecewise constant interest rate

P0c_t0 = @(t, R0T, Mt)(exp(-[R0T(1);R0T]'*[0 diff(t <= Mt)]'.*t));
P0c_t = @(t, R0T, Mt)(exp(-[R0T(1);R0T]'*[0*(1:length(t))' diff(meshgrid(t,Mt) <= meshgrid(Mt,t)')']'.*t));

% This defines a discount function that uses an interest rate that follows an
% linearly interpolated continuous function. It is also adapted to accept time
% as a vector.

% (Original function for troubleshooting purposes: R0f_t = @(t, R0T, Mt)((diff([0;R0T])'./diff(Mt))*(max(min(t, Mt(2:end)),Mt(1:end-1))-Mt(1:end-1))');)

P0f_t = @(t, R0T, Mt)(exp(-(diff([0;R0T])'./diff(Mt))*(max(min(meshgrid(t, Mt(2:end))' , meshgrid(Mt(2:end),t)),meshgrid(Mt(1:end-1),t))-meshgrid(Mt(1:end-1),t))'.*t));

% This is the interest rate function (for verification). I assume that this is the
% interest rate with respect to maturity, i.e. that it is already integrated and
% I can apply it directly as a "fixed" rate with respect to the given time-period.

R0f_t = @(t, R0T, Mt)((diff([0;R0T])'./diff(Mt))*(max(min(meshgrid(t, Mt(2:end))' , meshgrid(Mt(2:end),t)),meshgrid(Mt(1:end-1),t))-meshgrid(Mt(1:end-1),t))');

% We also need to redefine the S_ab with respect to these interpolations

S_abf = @(t, R0T, Mt)((P0f_t(t(1), R0T, Mt)-P0f_t(t(end), R0T, Mt))./(mean(diff(t))*sum(P0f_t(t(2:end), R0T, Mt))));

% We also redefine S_ab for use with a piecewise constant interest rate function
S_abf2 = @(t, R0T, Mt)((P0c_t(t(1), R0T, Mt)-P0c_t(t(end), R0T, Mt))./(mean(diff(t))*sum(P0c_t(t(2:end), R0T, Mt))));

% Note that the mean(diff) is put there because I assume that the time points are
% 'monospaced'. If the time intervals are heterogeneous, all we have to do is to
% remove the mean() and the sum() and change the expression denominator to a scalar
% product, i.e. to 'diff(t)'*P0x_t(t(2:end))'.

% The following is for calculations straight off the table values and the table
% time-grid (5 years) using piecewise constant interest rates

PSA_LR = zeros(length(Mt)-1, 1);
PSA_MR = PSA_LR;
PSA_HR = PSA_LR;

% We also want to try the same evaluations for the interpolated values

PSAf_LR = zeros(length(Mt)-1, 1);
PSAf_MR = PSAf_LR;
PSAf_HR = PSAf_LR;
PSPf_LR = PSAf_LR;
PSPf_MR = PSAf_LR;
PSPf_HR = PSAf_LR;

% Let's also try the same evaluations for the piecewise constant interest rate
% but with a finer time-grid

PSAf2_LR = zeros(length(Mt)-1, 1);
PSAf2_MR = PSAf2_LR;
PSAf2_HR = PSAf2_LR;
PSPf2_LR = PSAf2_LR;
PSPf2_MR = PSAf2_LR;
PSPf2_HR = PSAf2_LR;

% +------------------------------------------------------------------------------------------------
% #####################################################################################
% #####################################################################
% /////////////////////////////////////////////////////                        |  \ /   /
% //////////////////////////                                                  -+-  X   / 
% ///////                                                                      |  / \ /   
%
%          Computations
%

% Computing the P(0, T_i) from the table values

P0T = zeros(1,length(Rswp))';
for j = 1:length(Rswp)
    P0T(j) = (1-5*Rswp(j)*sum(P0T))./(1+5*Rswp(j));
end

R0T = -log(P0T(2:end))./Mt(2:end)';

% Here begins the big foor-loop that does all the necessary calculations. The variable
% k is for the different maturities of the swaps where k = 2 3 4 5 6 7 (length(Mt))
% which is from the table values. The for-loops below basically use equations (1.28)
% and (21.41) in Brigo-Pallavicini.

for k = 2:length(Mt)
    
    % Calculations of anticipated values done pretty much straigh from the table values
    
    for i = 2:k
        SabC = S_ab(P0T(i-1:k), 1, k-i+2, 5);
	%SabC(find(isnan(SabC))) = 0;  % This is in case SabC returns NaN (when inp arg a = b)
        PSA_LR(k-1) = PSA_LR(k-1) + (LR(find(Mt(i-1)==pT),2) - LR(find(Mt(i)==pT),2)).* ...
           Bl(Rswp(k),SabC,sigmaBS.*Mt(i-1).^.5,1).*5.*sum(P0T(1:k));
        PSA_MR(k-1) = PSA_MR(k-1) + (MR(find(Mt(i-1)==pT),2) - MR(find(Mt(i)==pT),2)).* ...
           Bl(Rswp(k),SabC,sigmaBS.*Mt(i-1).^.5,1).*5.*sum(P0T(1:k));
        PSA_HR(k-1) = PSA_HR(k-1) + (HR(find(Mt(i-1)==pT),2) - HR(find(Mt(i)==pT),2)).* ...
           Bl(Rswp(k),SabC,sigmaBS.*Mt(i-1).^.5,1).*5.*sum(P0T(1:k));
    end
    for i = 1:Mt(k)
	% We now calculate the anticipated adjustments for the interpolated functions using
	% annual time-grid
	
        PSAf_LR(k-1) = PSAf_LR(k-1) + (F_tau(i, [LR(:,1) pT [pT(2:end);100]]) - ...
           F_tau(i-1, [LR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf(i-1:Mt(k), R0T, Mt), ...
           sigmaBS.*(i-1)^.5, 1).*sum(P0f_t(i-1:Mt(k), R0T, Mt));
        PSAf_MR(k-1) = PSAf_MR(k-1) + (F_tau(i, [MR(:,1) pT [pT(2:end);100]]) - ...
           F_tau(i-1, [MR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf(i-1:Mt(k), R0T, Mt), ...
           sigmaBS.*(i-1)^.5, 1).*sum(P0f_t(i-1:Mt(k), R0T, Mt));
        PSAf_HR(k-1) = PSAf_HR(k-1) + (F_tau(i, [HR(:,1) pT [pT(2:end);100]]) - ...
           F_tau(i-1, [HR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf(i-1:Mt(k), R0T, Mt), ...
           sigmaBS.*(i-1)^.5, 1).*sum(P0f_t(i-1:Mt(k), R0T, Mt));

        % We now calculate the anticipated adjustments for the stepwise constant
        % interest rate functions but with an annual time-grid instead of the
	% "quintoannual" (5 years) time-grid that was used for the table-based
        % calculations above.

        PSAf2_LR(k-1) = PSAf2_LR(k-1) + (F_tau(i, [LR(:,1) pT [pT(2:end);100]]) - ...
            F_tau(i-1, [LR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf2(i-1:Mt(k), R0T, Mt), ...
            sigmaBS.*(i-1)^.5, 1).*sum(P0f_t(i-1:Mt(k), R0T, Mt));
        PSAf2_MR(k-1) = PSAf2_MR(k-1) + (F_tau(i, [MR(:,1) pT [pT(2:end);100]]) - ...
            F_tau(i-1, [MR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf2(i-1:Mt(k), R0T, Mt), ...
            sigmaBS.*(i-1)^.5, 1).*sum(P0f_t(i-1:Mt(k), R0T, Mt));
        PSAf2_HR(k-1) = PSAf2_HR(k-1) + (F_tau(i, [HR(:,1) pT [pT(2:end);100]]) - ...
            F_tau(i-1, [HR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf2(i-1:Mt(k), R0T, Mt), ...
            sigmaBS.*(i-1)^.5, 1).*sum(P0f_t(i-1:Mt(k), R0T, Mt));
        if i < Mt(k)
            % The postponed adjustments for the interpolated functions are calculated here

            PSPf_LR(k-1) = PSPf_LR(k-1) + (F_tau(i, [LR(:,1) pT [pT(2:end);100]]) - ...
               F_tau(i-1, [LR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf(i:Mt(k), R0T, Mt), ...
               sigmaBS.*(i)^.5, 1).*sum(P0f_t(i:Mt(k), R0T, Mt));
            PSPf_MR(k-1) = PSPf_MR(k-1) + (F_tau(i, [MR(:,1) pT [pT(2:end);100]]) - ...
               F_tau(i-1, [MR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf(i:Mt(k), R0T, Mt), ...
               sigmaBS.*(i)^.5, 1).*sum(P0f_t(i:Mt(k), R0T, Mt));
            PSPf_HR(k-1) = PSPf_HR(k-1) + (F_tau(i, [HR(:,1) pT [pT(2:end);100]]) - ...
               F_tau(i-1, [HR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf(i:Mt(k), R0T, Mt), ...
               sigmaBS.*(i)^.5, 1).*sum(P0f_t(i:Mt(k), R0T, Mt));

            % The same adjustments are calculated but for the stepwise constant interest rate
            % function

            PSPf2_LR(k-1) = PSPf2_LR(k-1) + (F_tau(i, [LR(:,1) pT [pT(2:end);100]]) - ...
               F_tau(i-1, [LR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf2(i:Mt(k), R0T, Mt), ...
               sigmaBS.*(i)^.5, 1).*sum(P0f_t(i:Mt(k), R0T, Mt));
            PSPf2_MR(k-1) = PSPf2_MR(k-1) + (F_tau(i, [MR(:,1) pT [pT(2:end);100]]) - ...
               F_tau(i-1, [MR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf2(i:Mt(k), R0T, Mt), ...
               sigmaBS.*(i)^.5, 1).*sum(P0f_t(i:Mt(k), R0T, Mt));
	    PSPf2_HR(k-1) = PSPf2_HR(k-1) + (F_tau(i, [HR(:,1) pT [pT(2:end);100]]) - ...
               F_tau(i-1, [HR(:,1) pT [pT(2:end);100]])).*Bl(Rswp(k), S_abf2(i:Mt(k), R0T, Mt), ...
               sigmaBS.*(i)^.5, 1).*sum(P0f_t(i:Mt(k), R0T, Mt));
	end
    end
end

fprintf('My calculations of a payer swap using the Low, Medium and High risk\nscenarios\n\n')
fprintf('Calculations of anticipated payer swaps using pure table values\n(columns: Maturities, LR, MR, HR):\n')

[Mt(2:end)' PSA_LR PSA_MR PSA_HR]

fprintf('Calculations of anticipated payer swaps using fully interpolated interest\nrates and manually calculated default probabilities:\n')

[Mt(2:end)' PSAf_LR PSAf_MR PSAf_HR]

fprintf('The same calculations but on postponed accruals:\n')

[Mt(2:end)' PSPf_LR PSPf_MR PSPf_HR]

fprintf('Calculations of anticipated payer swaps assuming piecewise constant interest\nrates and manually calculated default probabilities:\n')

[Mt(2:end)' PSAf2_LR PSAf2_MR PSAf2_HR]

fprintf('The same calculations but on postponed accruals:\n')

[Mt(2:end)' PSPf2_LR PSPf2_MR PSPf2_HR]

