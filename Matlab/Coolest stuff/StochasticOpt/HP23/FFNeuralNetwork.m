function nnOutputs = FFNeuralNetwork(inputs, Weights, Parameters)
  sigmoidFun = @(s) 1./(1+exp(-Parameters.sigmoidC.*s));
  inputBias = ones(size(inputs, 1), 1);
  hiddenLayerOutputs = sigmoidFun([inputs inputBias]*Weights.hiddenLayer);
  hiddenLayerBias = ones(size(hiddenLayerOutputs, 1), 1);
  nnOutputs = sigmoidFun([hiddenLayerOutputs hiddenLayerBias]*Weights.outputLayer);
end