function pathLength = GetPathLength(path,cityLocations)
  travelingDistanceComponentsSquared = (cityLocations(path,:) - ...
                       cityLocations(circshift(path, [0 1]),:)).^2;
  pathLength = sum(sum(travelingDistanceComponentsSquared,2).^.5);
end