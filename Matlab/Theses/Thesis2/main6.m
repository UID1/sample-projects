% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% The  Matlab script  'main2.m' uses  the  internal  matlab package  and
% function ARMAX  to fit ARMA  models into the  electricity  price data.
% The model estimations are evaluated  using a subset of the information
% criteria  and prediction  error  estimators  introduced  in 'main1.m'.
% Trend and seasonalities are  evaluated and  filtered out of  the price
% data. The spikes in  the data are  also analyzed  and methods to over-
% come the hurdles that are caused by these spikes are investigated.
%
% 'main6.m' is a continuation of main3.m  and main4.m and it is intended
% to further analyze the computations made in main3.m and main4.m.  This
% script  runs  the ARMAX  estimations  given the  data computed  in the
% previous  man scripts.  main3 and main4  are computationally intensive
% and takes a few hours to run on a mainstream computer. In this session
% we will try to fit the ARMAX using log() temperatures on log() prices.

% Load data needed for computations

load eldata1.mat
load armaxdata003.mat

% In case we didn't to run main3.m
if ~any(strcmp(who,'tmp2010'))
   % Get the temperature data for year 2010
   % convert it to Kelvin and then take the logarithm.
   tmp2010 = temp_proc(8760, 'cubic');
   tmp2010.national = log(272.15+tmp2010.national);
end

ELDATA = log(ELh2010_des1);
A1        = [ELDATA(3:end-1) ELDATA(2:end-2) ...
             ELDATA(1:end-3)];
el_acoeff = (A1'*A1)\A1'*tmp2010.national(4:end);
ELDATA1t = ELDATA(4:end) - A1*el_acoeff;
ELTREND1t = est_ma(ELDATA1t, 500);

% Run ARMAX estimations on all data

[m1a par1a] = armabat2(log(ELSPOTh2010), ...
                        tmp2010.national, 1:20, 1:20, 1:3, 2, '1a')
[m1b par1b] = armabat2(ELDATA, ...
                        tmp2010.national, 1:20, 1:20, 1:3, 2, '1b')
[m1c par1c] = armabat2(ELDATA(4:end)-ELTREND1t.removable, ...
			tmp2010.national(4:end), ...
			1:20, 1:20, 1:3, 2, '1c')

% We also want to try pure ARMA(p,q) estimations

[m2a par2a] = armabat3(log(ELSPOTh2010), 1:30, 1:30, '2a')
[m2b par2b] = armabat3(ELDATA, 1:30, 1:30,'2b')
[m2c par2c] = armabat3(ELDATA(4:end)-ELTREND1t.removable, ...
                        1:30, 1:30, '2c')
[m2d par2d] = armabat3(ELDATA(4:end)-ELTREND1t.removable - ...
                        tmp2010.national(4:end), 1:30, 1:30, '2d')

savefile = sprintf('lltempARMAX(%s).mat', ...
                     datestr(now, 'mmm-dd_HH-MM.FFF'));
save(savefile, 'm1a', 'm1b', 'm1c', 'm2a', 'm2b', 'm2c', 'm2d', ...
               'par1a', 'par1b','par1c', 'par2a', 'par2b', ...
	       'par2c', 'par2d');

% None of the best cho�ces  are stable using this approach.  It would be
% interesting to use one trajectory and see how different lenghts on the
% moving averages affect the simulation of that trajectory.
