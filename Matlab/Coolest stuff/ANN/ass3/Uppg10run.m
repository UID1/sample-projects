% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2a automated 10 run session
% by Robin Axelsson
%

Ewinsize = 1e4; % Window size
NumIter = 1e6;
Eat = zeros(floor(NumIter/Ewinsize), 10);
Eav = zeros(floor(NumIter/Ewinsize), 10);
Cvav = zeros(floor(NumIter/Ewinsize), 10);
Cvat = zeros(floor(NumIter/Ewinsize), 10);

fprintf('Running network 1 simulations .')
for ii=1:10
   close all
   Uppg2a2
   Cvat(:,ii) = Cvvec1;
   Cvav(:,ii) = Cvvec2;
   Eat(:, ii) = Evec1';
   Eav(:, ii) = Evec2';
   fprintf('.')
end
fprintf(' done.\n')
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]]/255;
close all
FIGS.fig1 = figure('Color', [1 1 1]);
surf(1:10, (1:floor(NumIter/Ewinsize))*Ewinsize, Eat);
view(135, 25);
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Steps');
zlabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Training data')

FIGS.fig2 = figure('Color', [1 1 1]);
surf(1:10, (1:floor(NumIter/Ewinsize))*Ewinsize, Eav);
view(135, 25);
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Steps');
zlabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Validation data')

FIGS.fig3 = figure('Color', [1 1 1]);
surf(1:10, (1:floor(NumIter/Ewinsize))*Ewinsize, Cvat);
view(135, 25);
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Steps');
zlabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Training data')

FIGS.fig4 = figure('Color', [1 1 1]);
surf(1:10, (1:floor(NumIter/Ewinsize))*Ewinsize, Cvav);
view(135, 25);
xlabel('\fontsize{12}Runs');
ylabel('\fontsize{12}Steps');
zlabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Validation data')
