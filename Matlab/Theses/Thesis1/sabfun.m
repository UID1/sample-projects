%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%
% The built in matlab function linspace cannot handle matrices
% so S_ab can no longer be an anonymous function because we
% need a for-loop to build the time grid.
%

function S_ab = sabfun(t, T_a, T_b, N_t, beta)

gamma_CIR = @(alpha, sigma)(sqrt(alpha.^2+2.*sigma.^2));
CIR_A = @(T, gamma, alpha, mu, nu)(log(2.*gamma.*exp((gamma+alpha).*T.*.5)./((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma)).*2.*alpha.*mu./nu.^2);
CIR_B = @(T, gamma, alpha)(2.*(exp(gamma.*T)-1)./ ((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma));

P_CIR = @(t, kappa, nu, mu, y_0)(exp(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)-CIR_B(t, gamma_CIR(kappa, nu), kappa).*y_0));

TimeGrid = zeros(N_t, length(T_a));
for i=1:length(T_a)
    TimeGrid(:,i)=linspace(T_a(i), T_b, N_t)';
end

Sab = N_t*(P_CIR(T_a-t, beta(2), beta(4), beta(3), beta(1))-P_CIR(T_b-t, beta(2), beta(4), beta(3), beta(1)))./((T_b-T_a).*sum(P_CIR(max((TimeGrid-t),0), beta(2), beta(4), beta(3), beta(1)).*(TimeGrid>t)));
Sab(find(isnan(Sab)))=0;
S_ab = Sab;
