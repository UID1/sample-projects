#include <stdio.h>
#include <cuda_runtime.h>
#include <iostream>
#include <string>
#include <iomanip>
#include <windows.h>

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

int main(int argc, char *argv[])
{
   int iDev = 0;
   
   /* Let's experiment a little with different console
      colors.
   */
   colors_t colors;
   std::string clrs[] = { "Black", "dBlue", "dGreen", "dCyan",
                     "dRed", "dMagenta", "dYellow", "hGray",
                     "dGray", "hBlue", "hGreen", "hCyan", "hRed", "hMagenta", "hYellow", "White" };
   const char* const sHex = "0123456789ABCDEF";
   
   /* And we go back to our CUDA programming */
   cudaDeviceProp iProp;
   cudaGetDeviceProperties(&iProp, iDev);
   
   colors.setFg(hGray);
   std::cout << "Device "; colors.setFg(dCyan); std::cout << iDev;
   colors.setFg(hGray); std::cout << ": "; colors.setFg(dYellow);
   std::cout << iProp.name << std::endl; colors.setFg(hGray);
   
   std::cout << "Number of multiprocessors: "; colors.setFg(dCyan);
   std::cout << iProp.multiProcessorCount << std::endl; colors.setFg(hGray);
   
   std::cout << "Total amount of constant memory: "; colors.setFg(dCyan);
   std::cout << std::fixed << std::setprecision(2);
   std::cout << iProp.totalConstMem/1024.0; colors.setFg(White);
   std::cout << " kB" << std::endl; colors.setFg(hGray);
   
   std::cout << "Total amount of shared memory per block: "; colors.setFg(dCyan);
   std::cout << std::fixed << std::setprecision(2);
   std::cout << iProp.sharedMemPerBlock/1024.0; colors.setFg(White);
   std::cout << " kB" << std::endl; colors.setFg(hGray);

   std::cout << "Total number of registers available per block: "; colors.setFg(dCyan);
   std::cout << iProp.regsPerBlock << std::endl; colors.setFg(hGray);

   std::cout << "Warp size: "; colors.setFg(dCyan);
   std::cout << iProp.warpSize << std::endl; colors.setFg(hGray);
   
   std::cout << "Maximum number of threads per block: "; colors.setFg(dCyan);
   std::cout << iProp.maxThreadsPerBlock << std::endl; colors.setFg(hGray);

   std::cout << "Maximum number of threads per multiprocessor: "; colors.setFg(dCyan);
   std::cout << iProp.maxThreadsPerMultiProcessor << std::endl; colors.setFg(hGray);

   std::cout << "Maximum number of warps per multiprocessor: "; colors.setFg(dCyan);
   std::cout << iProp.maxThreadsPerMultiProcessor/32 << std::endl; colors.setFg(hGray);

   
   std::cout << std::endl;
   
   std::cout << "--- Testing console colors ---" << std::endl;
   for(int i=0; i<0x10; ++i)
   {
      if(i<2)
         colors.setBg(dGray);
      colors.setFg(i);
      std::cout << "Color: 0x" << sHex[i];
      std::cout << " (" << clrs[i];
      std::cout << ")";
      if(i<2)
         colors.setColors(0);
      std::cout << std::endl;
   }
   colors.setColors(colors.initial_colors);
   return EXIT_SUCCESS;
   
}
