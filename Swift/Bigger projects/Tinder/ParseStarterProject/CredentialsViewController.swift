//
//  CredentialsViewController.swift
//  Tinder
//
//  Created by Iberius Pred on 04/03/17.
//  Copyright © 2017 Parse. All rights reserved.
//

import UIKit
import Parse

class CredentialsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPassWordTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var changeStateButton: UIButton!
    @IBOutlet weak var stateMessage: UILabel!
    @IBOutlet weak var repeatPassConstraint: NSLayoutConstraint!
    @IBOutlet weak var changeStateButtonWidth: NSLayoutConstraint!
    var isLogin = false
    var activityIndicator = UIActivityIndicatorView()
    
    @IBAction func changeCredentialState(_ sender: Any) {
        self.view.endEditing(true)
        if isLogin {
            setSignUpState()
        } else {
            setLoginState()
        }
    }
    private func setLoginState() {
        isLogin = true
        stateMessage.text = "Don't have an account?"
        repeatPassWordTextField.isHidden = true
        changeStateButton.setTitle("Sign Up", for: [])
        submitButton.setTitle("Log in", for: [])
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1, animations: {
            self.repeatPassConstraint.constant = 0
            self.changeStateButtonWidth.constant = 56
            self.view.layoutIfNeeded()
        })
    }
    private func setSignUpState() {
        isLogin = false
        stateMessage.text = "Already have an account?"
        repeatPassWordTextField.isHidden = false
        repeatPassWordTextField.text = ""
        changeStateButton.setTitle("Log In", for: [])
        submitButton.setTitle("Sign Up", for: [])
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1, animations: {
            self.repeatPassConstraint.constant = 30
            self.changeStateButtonWidth.constant = 42
            self.view.layoutIfNeeded()
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        redirectUser()
    }
    @IBAction func submitCredentials(_ sender: Any) {
        self.view.endEditing(true)
        if let userName = userNameTextField.text, userName != "" {
            if let userPass = passwordTextField.text, userPass != "" {
                if isLogin {
                    userLogIn(username: userName, password: userPass)
                    //performSegue(withIdentifier: "goToUserDetails", sender: self)
                    redirectUser()
                } else {
                    if let userRepeat = repeatPassWordTextField.text, userRepeat == userPass {
                        // Sign up user
                        userSignUp(username: userName, password: userPass)
                        performSegue(withIdentifier: "goToUserDetails", sender: self)
                    } else {
                        // Report wrong credentials
                        createAlert(title: "Signup Error", message: "Password mismatch")
                    }
                }
            } else {
                // Report invalid password entered
                createAlert(title: (isLogin ? "Login " : "Signup ") + "Error", message: "Invalid password entered")
            }
        } else {
            // Report invalid username
            createAlert(title: (isLogin ? "Login " : "Signup ") + "Error", message: "Invalid username")
        }
    }
    
    private func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func userSignUp(username: String, password: String) {
        setFreezeView(activated: true)
        let userEntry = PFUser()
        let userACL = PFACL()
        userEntry.username = username
        userEntry.password = password
        userACL.getPublicWriteAccess = true
        userEntry.acl = userACL
        userEntry.signUpInBackground { (success, error) in
            self.setFreezeView(activated: false)
            if error != nil {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Submission failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Submission failure", message: "Please try again later.")
                }
            } else {
                print("User signed up")
            }
        }
    }
    
    private func userLogIn(username: String, password: String) {
        self.setFreezeView(activated: true)
        PFUser.logInWithUsername(inBackground: username, password: password) { (user, error) in
            self.setFreezeView(activated: false)
            if error != nil {
                if let errorContainer = error as? NSError {
                    self.createAlert(title: "Login failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                } else {
                    self.createAlert(title: "Login failure", message: "Please try again later.")
                }
            } else {
                print("User logged in")
            }
        }
    }
    private func redirectUser() {
        if PFUser.current()?.objectId != nil {
            print("Welcome \(PFUser.current()?.username)!")
            if PFUser.current()?["isFemale"] != nil, PFUser.current()?["interestedInWomen"] != nil, PFUser.current()?["Photo"] != nil {
                performSegue(withIdentifier: "showSwipeViewController", sender: self)
            } else {
                performSegue(withIdentifier: "goToUserDetails", sender: self)
                //showSwipeViewController
            }
        }
    }
    private func setFreezeView(activated: Bool) {
        if activated {
            self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            self.activityIndicator.center = self.view.center
            self.activityIndicator.hidesWhenStopped = true
            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            view.addSubview(activityIndicator)
            self.activityIndicator.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
        } else {
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Make the keyoard close when not editing
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
