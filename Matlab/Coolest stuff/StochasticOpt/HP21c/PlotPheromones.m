function PlotPheromones(connection, pheromones, cityLocation)
  maxLevel = max(max(pheromones));
  nCities = length(cityLocation);
  for i = 1:nCities-1
    for j = i+1:nCities
      pheromoneColor = [1 .99 .999]*(1 - pheromones(i,j));
      set(connection(i,j),'XData',[cityLocation(i,1) cityLocation(j,1)], ...
          'Color', pheromoneColor);
      set(connection(i,j),'YData',[cityLocation(i,2) cityLocation(j,2)]);
    end
  end
  drawnow;
end