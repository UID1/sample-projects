function nearestNeighbourPathLength = GetNearestNeighbourPathLength(cityLocations)
  numberOfCities = length(cityLocations);
  currentPath = 1:numberOfCities;
  nearestNeighbourPathLengths = zeros(numberOfCities, 1);
  for j = 1:numberOfCities
    currentPosition = j;
    for i = 1:numberOfCities-1
      currentPath([i currentPosition]) = currentPath([currentPosition i]);
      currentLocMtx = meshgrid(cityLocations(currentPosition, :), ...
                               i+1:numberOfCities);
      otherLocsMtx = cityLocations(currentPath(i+1:end), :);
      currentDistances = sum((otherLocsMtx - currentLocMtx).^2, 2).^.5;
      [~, minIndex] = min(currentDistances);
      currentPosition = currentPath(i + minIndex);
    end
    nearestNeighbourPathLengths(j) = GetPathLength(currentPath, cityLocations);
  end
  % I concocted the additional j for-loop to be able to extract min(L_nn)
  nearestNeighbourPathLength = ...
            nearestNeighbourPathLengths(randi(numberOfCities, 1));
end