fprintf('Setting up parameters ... ');
% Set up parameters for genetic algorithm
GAParams.numSlopes = 10;
GAParams.populationSize = 1000;
GAParams.crossoverProbability = .15;
GAParams.variableRange = 10.0;
GAParams.numberOfGenerations = 200;
GAParams.tournamentSize = 5;
GAParams.tournamentSelectionParameter = .7;

GAParams.FFNNDimensions.hlCols = 7; % Hidden Layer nodes
GAParams.FFNNDimensions.hlRows = 4; % Input nodes + bias node
GAParams.FFNNDimensions.olCols = 2; % Output nodes 
GAParams.FFNNDimensions.olRows = 8; % Hidden Layer nodes + bias node
GAParams.FFNNDimensions.interval.lowerBound = -10;
GAParams.FFNNDimensions.interval.upperBound = 10;

GAParams.numberOfGenes = GAParams.FFNNDimensions.hlCols * ...
                            GAParams.FFNNDimensions.hlRows + ...
                         GAParams.FFNNDimensions.olCols * ...
                            GAParams.FFNNDimensions.olRows;

GAParams.mutationProbability = 2 / GAParams.numberOfGenes;
GAParams.creepMutationProbability = .5;
GAParams.creepLength = .1;

% Set up parameters for vehicle simulation
VSParams.VehicleParameters.mass  =                              20000;
VSParams.VehicleParameters.gConstant =     9.806653210048415426463522;
VSParams.VehicleParameters.gearVector = [7 5 4 3 2.5 2 1.6 1.4 1.2 1];
VSParams.VehicleParameters.brakeCoolingFactor =                    30;
VSParams.VehicleParameters.deltat =                                .5;
VSParams.VehicleParameters.engineCoef =                          3000;
VSParams.VehicleParameters.brakeCoef =                             40;
VSParams.VehicleParameters.Tamb =                                 283;
VSParams.VehicleParameters.gearDeadTime =                           2;

VSParams.Boundaries.travelDistance = 1000;
VSParams.Boundaries.maxBrakeTemp =    750;
VSParams.Boundaries.minVelocity =       5;
VSParams.Boundaries.maxVelocity =      25;
VSParams.Boundaries.maxSlope =         20;

VSParams.StartValues.startPosition       = 0;
VSParams.StartValues.startVelocity      = 20;
VSParams.StartValues.startBrakeTemp    = 500;
VSParams.StartValues.startBrakePressure  = 0;
VSParams.StartValues.startGear           = 7;
VSParams.StartValues.startAcceleration = NaN;
VSParams.StartValues.lastGearChange =      0;

SlopeGenerationParameters.numSlopes = GAParams.numSlopes;
SlopeGenerationParameters.flatnesses = [.5 1 5 10 10 20 20 25 25 30];
SlopeGenerationParameters.filterFactor = 2;
SlopeGenerationParameters.numElements = 1001;

VSParams.SlopeParameters.deltax                              = 1;
VSParams.SlopeParameters.alphaMax = VSParams.Boundaries.maxSlope;
VSParams.SlopeParameters.angleVector            = zeros(1, ...
                          SlopeGenerationParameters.numElements);

NeuralNetworkParameters.Parameters.sigmoidC = 1;
NeuralNetworkParameters.Weights.hiddenLayer = zeros( ...
               GAParams.FFNNDimensions.hlRows, GAParams.FFNNDimensions.hlCols);
NeuralNetworkParameters.Weights.outputLayer = zeros( ...
               GAParams.FFNNDimensions.olRows, GAParams.FFNNDimensions.olCols);
NeuralNetworkParameters.Parameters.upperBound = ...
                                   GAParams.FFNNDimensions.interval.upperBound;
NeuralNetworkParameters.Parameters.lowerBound = ...
                                   GAParams.FFNNDimensions.interval.lowerBound;

maxPossibleFitness = VSParams.Boundaries.maxVelocity * ...
                                            VSParams.Boundaries.travelDistance;
fprintf('done\nIdeal optimal fitness: %.f\n', maxPossibleFitness);
fprintf('Initializing population ... ');
% Initialize population
GAOpt.population = rand(GAParams.populationSize, GAParams.numberOfGenes);
GAOpt.decodedPopulation = repmat(NeuralNetworkParameters.Weights, ...
                                 GAParams.populationSize, 1);
GAOpt.fitness = zeros(GAParams.populationSize, GAParams.numSlopes);
GAOpt.overallFitness = zeros(GAParams.populationSize, 1);
nextGenPopulation = GAOpt.population;
fitnessVector = repmat(NaN, 1, GAParams.numberOfGenerations);
fprintf('done\nInitializing slopes ... ');
% Initialize slopes
trainingSlopes = zeros(SlopeGenerationParameters.numSlopes, ...
                       SlopeGenerationParameters.numElements);
for i = 1:SlopeGenerationParameters.numSlopes
  trainingSlopes(i, :) = semiStochasticSlopeGenerator( ...
                              VSParams.Boundaries.maxSlope, ...
                              SlopeGenerationParameters.filterFactor, ...
                              SlopeGenerationParameters.flatnesses(i), ...
                              SlopeGenerationParameters.numElements);
end
fprintf('done\n');

% Plot initialization
fitnessFigureHandle = figure('Name', 'Kung Fu score');
hold on;
set(fitnessFigureHandle, 'Position', [50,50,500,200]);
set(fitnessFigureHandle, 'DoubleBuffer', 'on');
axis([- GAParams.numberOfGenerations 0 -1000 5000]);
bestPlotHandle = plot(1:GAParams.numberOfGenerations, fitnessVector, ...
                      'LineWidth', 2);
grid on;
title('Vehicle fitness evolution over generations', 'FontWeight','Normal');
yTextHandle = ylabel(sprintf('Best: %4.5f', 0.0)); 
hold off;

drawnow;

% Apply genetic algorithm
hW = waitbar(0, 'Initializing');
set(hW, 'Name', 'Kung Fu training in progress');
for currentGeneration = 1:GAParams.numberOfGenerations
  fprintf('Generation %d, ', currentGeneration);
  maximumFitness = .0; % Assumes non-negative fitness values!
  bestIndividual = NeuralNetworkParameters.Weights;
  bestIndividualIndex = 0;
  
  % Update fitness values and select best fitness
  for i = 1:GAParams.populationSize
    watchString = sprintf('Generation: %d/%d, Individual: %d/%d', ...
                           currentGeneration, GAParams.numberOfGenerations, ...
                           i, GAParams.populationSize);
    waitbar(i/GAParams.populationSize, hW, watchString);
    GAOpt.decodedPopulation(i) = decodeChromosome(GAOpt.population(i,:), ...
                                                      GAParams.FFNNDimensions);
    for j = 1:SlopeGenerationParameters.numSlopes
      VSParams.SlopeParameters.angleVector = trainingSlopes(j, :);
      NeuralNetworkParameters.Weights = GAOpt.decodedPopulation(i);
      SimulationResults = vehicleSimulation(VSParams.VehicleParameters, ...
                        VSParams.SlopeParameters, NeuralNetworkParameters, ...
                                    VSParams.Boundaries, VSParams.StartValues);
      GAOpt.fitness(i, j) = SimulationResults.fitness;
      
      waitbar(i/GAParams.populationSize, hW, watchString);
    end
    GAOpt.overallFitness(i) = mean(GAOpt.fitness(i,:));
    if (GAOpt.overallFitness(i) > maximumFitness)
      maximumFitness = GAOpt.overallFitness(i);
      bestIndividualIndex = i;
      bestNNWeights = GAOpt.decodedPopulation(i);
      BestSimulationResults = SimulationResults;
    end
  end

  % Report to GUI
  fprintf('max fitness: %.5f\n', maximumFitness);
  fitnessVector(currentGeneration) = maximumFitness;
  
  plotvector = get(bestPlotHandle, 'YData');
  plotvector = fitnessVector;
  set(bestPlotHandle, 'YData', plotvector);
  set(yTextHandle, 'String', sprintf('Best: %4.3f', maximumFitness));
  axis([1 min(currentGeneration+1, GAParams.numberOfGenerations) ...
        min(plotvector) max(plotvector)+.001]);
  drawnow;

  % Apply elitism
  nextGenPopulation(1,:) = GAOpt.population(bestIndividualIndex,:);
  
  % Tournament select new population, mutate, and crossover
  for i=2:GAParams.populationSize
    tournamentSelectionVector = randi(GAParams.populationSize, 1, 5);
    nextGenPopulation(i,:) = GAOpt.population(tournamentSelectionVector( ...
         TournamentSelect(GAOpt.overallFitness(tournamentSelectionVector), ...
                                                  GAParams.tournamentSize, ...
                                    GAParams.tournamentSelectionParameter)), :);
    nextGenPopulation(i,:) = Mutate(nextGenPopulation(i,:), ...
                                    GAParams.mutationProbability, ...
                                    GAParams.creepMutationProbability, ...
                                    GAParams.creepLength);
    if i > 2 && rand < GAParams.crossoverProbability
      crossoverPair = Cross(nextGenPopulation(i,:), nextGenPopulation(i-1,:));
      nextGenPopulation(i,:) = crossoverPair(1,:);
      nextGenPopulation(i-1,:) = crossoverPair(1,:);
    end
  end
  GAOpt.population = nextGenPopulation;
end
close(hW);
% Output
disp('bestNNWeights');