0064AA30 sub_64AA30      proc near               ; CODE XREF: sub_64AB50+12p                                        
0064AA30                                         ; sub_64AB50+A9p                                                   
0064AA30                                                                                                             
0064AA30 arg_0           = dword ptr  10h                                                                            
0064AA30 arg_4           = dword ptr  14h                                                                            
0064AA30 arg_8           = dword ptr  18h                                                                            
0064AA30                                                                                                             
0064AA30                 push    ebx             ; Call1: Push => SC >#L/K                                           
0064AA31                 push    esi             ; Call1: Push => "Base"                                             
0064AA32                 push    edi             ; Call1: Push =>#S/N                                                
0064AA33                 mov     esi, [esp+arg_4] ; esi => SC>#L/K                                                   
0064AA37                 mov     ecx, [esi]      ; \                                                                 
0064AA37                                         ;   ecx, edx = SC >#L/K                                             
0064AA39                 mov     edx, [esi+4]    ; /                                                                 
0064AA3C                 shld    edx, ecx, 8     ; We left shift the most significant byte of ecx                    
0064AA3C                                         ; into the right end of edx (this instruction leaves ecx unmodified)
0064AA40                 and     ecx, 0FFFFFFh   ; and clear that shifted byte of ecx                                
0064AA46                 mov     eax, ecx                                                                            
0064AA48                 xor     eax, edx        ; eax = acx XOR edx                                                 
0064AA4A                 mov     ebx, 88888888h  ; ebx = ...10001000h                                                
0064AA4F                 and     ebx, eax        ; We mask eax into ebx...                                           
0064AA51                 xor     eax, ebx        ; and XOR the mask back into eax                                    
0064AA53                 shr     ebx, 3          ; Shake                                                             
0064AA56                 shl     eax, 1          ; and shuffle                                                       
0064AA58                 lea     ebx, [ebx+ebx*8] ; ebx = 9*ebx                                                      
0064AA5B                 xor     eax, ebx        ; and XOR it back into eax                                          
0064AA5D                 mov     ebx, 88888888h  ; \                                                                 
0064AA62                 and     ebx, eax        ; |                                                                 
0064AA64                 xor     eax, ebx        ; |                                                                 
0064AA66                 shr     ebx, 3          ;  > The same mask and shuffle as above                             
0064AA69                 shl     eax, 1          ; |                                                                 
0064AA6B                 lea     ebx, [ebx+ebx*8] ; |                                                                
0064AA6E                 xor     eax, ebx        ; /                                                                 
0064AA70                 mov     ebx, 88888888h  ; \                                                                 
0064AA75                 and     ebx, eax        ; |                                                                 
0064AA77                 xor     eax, ebx        ; |                                                                 
0064AA79                 shr     ebx, 3          ;  > Mask and shuffle ...                                           
0064AA7C                 shl     eax, 1          ; |                                                                 
0064AA7E                 lea     ebx, [ebx+ebx*8] ; |                                                                
0064AA81                 xor     eax, ebx        ; /                                                                 
0064AA83                 xor     ecx, eax        ; \                                                                 
0064AA83                                         ;   We XOR the 3-step Mask'n'shuffled value into                    
0064AA83                                         ;   the modified SC>#L/K                                            
0064AA85                 xor     edx, eax        ; /                                                                 
0064AA87                 shl     ecx, 8          ; \                                                                 
0064AA87                                         ;   We replace the most significant byte                            
0064AA87                                         ;   of ecx with the least significant byte of edx                   
0064AA8A                 shrd    ecx, edx, 8     ; /                                                                 
0064AA8E                 shr     edx, 8          ; and pop that byte off edx with a right shift                      
0064AA91                 lea     ebx, ds:674880h ; Now we get a lookup table                                         
0064AA97                 mov     edi, [esp+arg_8] ; and get edi to point at #S/N                                     
0064AA9B                 bt      ecx, 15h        ; Copy the value of the 21th bit of ecx into CF                     
0064AA9F                 rcr     dx, 2           ; and right rotate that into dx 2 steps                             
0064AAA3                 rcl     ecx, 0Bh        ; and rotate ecx through CF 11 steps left                           
0064AAA6                 bt      ecx, 1Eh        ; Copy the 18th bit of ecx into CF                                  
0064AAAA                 rcr     dx, 0Bh         ; and shake                                                         
0064AAAE                 rcl     ecx, 2          ; and shuffle                                                       
0064AAB1                 mov     eax, [edi]      ; We load the 1st half of #S/N into eax                             
0064AAB3                 xor     eax, ecx        ; and XOR it with the 1st /2 of multiprocessed SC>#L/K              
0064AAB5                 xlat                    ; \                                                                 
0064AAB6                 ror     eax, 8          ; |                                                                 
0064AAB9                 xlat                    ; |                                                                 
0064AABA                 ror     eax, 8          ; |                                                                 
0064AABA                                         ;  > And now we do a bytewise lookup translation                    
0064AABD                 xlat                    ; |  of it (mov al, [bx+al])                                        
0064AABE                 ror     eax, 8          ; |                                                                 
0064AAC1                 xlat                    ; |                                                                 
0064AAC2                 ror     eax, 8          ; /                                                                 
0064AAC5                 xor     ecx, eax        ; and we XOR that back into ecx                                     
0064AAC7                 mov     eax, [edi+4]    ; Now, we get the other /2 of #S/N                                  
0064AACA                 xor     eax, edx        ; and XOR it with the 2nd /2 of multiprocessed SC>#L/K              
0064AACC                 xlat                    ; \                                                                 
0064AACC                                         ; |  But this time we only translate the the lower word             
0064AACD                 xchg    al, ah          ;  > of it (it seems like serial numbers are assumed to             
0064AACF                 xlat                    ; |  have no more than 12 digits, is it the EANsystem?)             
0064AAD0                 xchg    al, ah          ; /                                                                 
0064AAD2                 xor     edx, eax        ; This part is now being XORed back into the                        
0064AAD2                                         ; other /2 of the  multiprocessed SC>#L/K                           
0064AAD4                 bt      ecx, 1          ; Copy 1st bit of ecx into CF                                       
0064AAD8                 rcl     dx, 0Bh         ; and shake                                                         
0064AADC                 rcr     ecx, 2          ; and shuffle                                                       
0064AADF                 bt      ecx, 0Ah        ; Copy the 10th bit into CF                                         
0064AAE3                 rcl     dx, 2           ; and shake                                                         
0064AAE7                 rcr     ecx, 0Bh        ; and shuffle                                                       
0064AAEA                 shld    edx, ecx, 8     ; Now, we shift the most significant byte of ecx                    
0064AAEA                                         ; into into the least significant position of edx                   
0064AAEE                 and     ecx, 0FFFFFFh   ; and clear that byte                                               
0064AAF4                 mov     eax, ecx        ; \                                                                 
0064AAF4                                         ;  eax = ecx XOR edx                                                
0064AAF6                 xor     eax, edx        ; /                                                                 
0064AAF8                 mov     ebx, 88888888h  ; \                                                                 
0064AAFD                 and     ebx, eax        ; |                                                                 
0064AAFF                 xor     eax, ebx        ; |                                                                 
0064AB01                 shr     ebx, 3          ; |                                                                 
0064AB04                 shl     eax, 1          ; |                                                                 
0064AB06                 lea     ebx, [ebx+ebx*8] ; |                                                                
0064AB09                 xor     eax, ebx        ; |                                                                 
0064AB0B                 mov     ebx, 88888888h  ; |                                                                 
0064AB10                 and     ebx, eax        ; |                                                                 
0064AB12                 xor     eax, ebx        ; |                                                                 
0064AB14                 shr     ebx, 3          ;  > The same 3-step mask'n'shuffle procedure as above              
0064AB17                 shl     eax, 1          ; |                                                                 
0064AB19                 lea     ebx, [ebx+ebx*8] ; |                                                                
0064AB1C                 xor     eax, ebx        ; |                                                                 
0064AB1E                 mov     ebx, 88888888h  ; |                                                                 
0064AB23                 and     ebx, eax        ; |                                                                 
0064AB25                 xor     eax, ebx        ; |                                                                 
0064AB27                 shr     ebx, 3          ; |                                                                 
0064AB2A                 shl     eax, 1          ; |                                                                 
0064AB2C                 lea     ebx, [ebx+ebx*8] ; |                                                                
0064AB2F                 xor     eax, ebx        ; /                                                                 
0064AB31                 xor     ecx, eax        ; \                                                                 
0064AB31                                         ;   And we XOR it into the multiprocessed SC>#L/K                   
0064AB33                 xor     edx, eax        ; /                                                                 
0064AB35                 shl     ecx, 8          ; \                                                                 
0064AB38                 shrd    ecx, edx, 8     ;   Now we transfer the least significant byte of edx right         
0064AB38                                         ;   on top of the most significant byte of ecx                      
0064AB3C                 shr     edx, 8          ; /                                                                 
0064AB3F                 mov     esi, [esp+arg_0]                                                                    
0064AB43                 mov     [esi], ecx                                                                          
0064AB45                 mov     [esi+4], edx    ; Now, &arg_0 => another multiprocessed version of >#L/K            
0064AB48                 pop     edi             ; \                                                                 
0064AB49                 pop     esi             ;   Stdcall                                                         
0064AB4A                 pop     ebx             ; /                                                                 
0064AB4B                 retn                                                                                        
0064AB4B sub_64AA30      endp                                                                                        