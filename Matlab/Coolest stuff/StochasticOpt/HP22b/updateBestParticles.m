function updatedParticles = updateBestParticles(Particles, newSwarmValues)
  updatedParticles = Particles;
  numVars = size(Particles.xij, 2);
  newValuesAreBetter = (Particles.previouslyBestSwarmValues > newSwarmValues);
  newValsAreBetterMtx = repmat(newValuesAreBetter, [1 numVars]);
  updatedParticles.xijPB =  newValsAreBetterMtx.*Particles.xij + ...
                           ~newValsAreBetterMtx.*Particles.xijPB;
  updatedParticles.previouslyBestSwarmValues = newValuesAreBetter.*newSwarmValues + ...
                             ~newValuesAreBetter.*Particles.previouslyBestSwarmValues;
  [~, updatedParticles.swarmBestIndex] = min(updatedParticles.previouslyBestSwarmValues);
  updatedParticles.xijSB = updatedParticles.xijPB(updatedParticles.swarmBestIndex,:);
end