% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% This function simulates an ARMAX(p,q,r) from a gaussian process
% assuming the relation
%   y(t) = a_1*y(t-1)+a_2*y(t-2) + ... + a_p*y(t-p) +
%            b_1*u(t-d) + b_2*u(t-d-1) + ... + B_r*u(t-d-r) +
%            e(t) + c_1*e(t-1) + ... + c_q*e(t-q)
%
% where  u is an  exogeneous variable,  d is the delay in  discrete time
% steps.
%
% Syntax: simdat = armaxsim(m_in, xdata, nval, deg, beg) where
%
%  simdat is the simulated output data
%  m_in   is an idpoly object retreived from the armax() function
%  xdata  is some exogeneous data that was used in the ARMAX()
%         estimation. Setting this to 'nil' will make the simulations
%         run without influence from exogeneous data.
%  nval   is the number of values to simulate. Note that the number of
%         points to simulate should not exceed the length of xdata as
%         I have not yet implemented j mod length(xdata) for longer
%         time series.
%  deg    this allows for simulations of data points more than once
%         which is especially useful if one want to simulate longer time
%         series. Each point in the ouput vector will then be averaged
%         along the 'deg' dimension.
%  beg    Setting this to 1 makes the simulations start at 1 and
%         recursively build the data from what's available. Setting this
%         to anything else or not at all makes the simulations begin
%         only where there are enough elements for parameters p, r and r
%         i.e. start at max(p,q,r).

function simdat = armaxsim(m_in, xdata, nval, randdata, beg)

evec = randdata;
avec = -1*m_in.a(2:end);
bvec = m_in.b;
cvec = m_in.c;
p = length(avec);
q = length(cvec);
deg=1;
simul = zeros(deg, nval);
if ~strcmp(xdata, 'nil') % When simuations are run with exogeneous data
   if size(xdata, 1) > 1
   	xdata = xdata';
   end
   r = length(bvec);
   for i = 1:deg
      for j = ((beg~=1)*max([p q r])+(beg==1)):nval
         simul(i,j) = (j>1)*simul(i, max(j-p,1):max(j-1,1))* ...
                      avec(1:max(min(j-1, end),1))'+ ...
                      xdata(max(j-r+1,1):j)*bvec(1:min(j, end))' + ...
                      evec(i, max(j-q+1, 1):j)*cvec(1:min(j, end))';
      end
   end
else
   for i = 1:deg % For simulations without exogeneous data
      for j = ((beg~=1)*max([p q])+(beg==1)):nval
         simul(i,j) = (j>1)*simul(i, max(j-p,1):max(j-1,1))* ...
                      avec(1:max(min(j-1, end),1))'+ ...
                      evec(i, max(j-q+1, 1):j)*cvec(1:min(j, end))';
      end
   end
end
simdat = simul';
end % armaxsim
