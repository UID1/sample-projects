% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 2c Solution 2
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

%TRN = importdata('train_data1.txt');
TRN = importdata('train_data_new.txt');
VLD = importdata('valid_data_new.txt');
TRNn = [(TRN(:,1)-mean(TRN(:,1)))/std(TRN(:,1)) ...
        (TRN(:,2)-mean(TRN(:,2)))/std(TRN(:,2)) TRN(:,3)];
	
TRNn = TRN;
P = length(TRN); % Number of patterns in the system
UP = 2; % Number of patterns to update with
NumIter = 1e6;
SPC = 1e5; % Seconds per calculation

% Define the weight functions for the system
I = 1; % Number of outputs in the system
J = 3; % Number of nodes in the hidden layer
K = 2; % Number of inputs in the system

i0 = 2; % Number of inputs in the system
i1 = 3; % Number of nodes in hidden layer 1
i2 = 2; % Number of nodes in hidden layer 2
i3 = 1; % Number of inputs in the system

wjk = 2*(rand(J,K)-.5)*.2;
Wij = 2*(rand(I,J)-.5)*.2;

w_i0i1 = 2*(rand(i0,i1)-.5)*.2;
w_i1i2 = 2*(rand(i1,i2)-.5)*.2;
w_i2i3 = 2*(rand(i2,i3)-.5)*.2;

% Set predefined parameters

% Let's try a linearly decreasing η, say that
% η = 1 at i=1, and η = 10^-5 at i = 3*10^5
% where we see that the system converges. We
% can define an η_k and η_m such that
%  η = η_k/(i-η_m) the values of the parameters
% can be determined by solving η_k/(1-η_m) = 1
% and η_k/(3*10^5-η_m) = 10^-5

% I set the decline position to 1 and manually
% offset it in the formula down below.

eta_0 = [.6 .1 .01]; % Step values before decline
i_s = 1; % Start position of decline
i_e = 4*1e5; % End/target position
eta_s = .01; % Start value
eta_e = 1e-6; % End/target value
% Final decline parameters
eta_m = (eta_e*i_e-eta_s*i_s)/(eta_e-eta_s);
eta_k = eta_s*(i_s-eta_m);


theta_i1 = zeros(1,i1);
theta_i2 = zeros(1,i2);
theta_i3 = zeros(1,i3);
eta = .01;
beta_0 = .5; % We set g(b) = tanh(beta*b) as defined on page 108


% Initialization of help variables
wb1 = waitbar(0,'Initializing ...');
movegui(wb1, 'southeast');
Ewinsize = 1e4; % Window size
Ewin = zeros(1,Ewinsize); % Time span for rolling avg of E
Evec1 = zeros(1,floor(NumIter/Ewinsize)); % Vector of avg E:s
Evec2 = zeros(1,floor(NumIter/Ewinsize));
Cvvec1 = zeros(1,floor(NumIter/Ewinsize));
Cvvec2 = zeros(1,floor(NumIter/Ewinsize));
V_muj = zeros(P,J);

% We set up a Monte-Carlo simulation during training
simulationUpdateFrequency = 1000;
% Monte-Carlo simulation
% SimSize = 1000;
% INP = [rand(SimSize, 1)-.5 rand(SimSize,1)];
% Ordered grid
Xmin = -.5; Xmax = .5; Xstep = .01;
Ymin = 0;   Ymax = 1;  Ystep = .01;
MC1 = (Xmin:Xstep:Xmax)'*ones(1,length(Xmin:Xstep:Xmax));
MC2 = meshgrid((Ymin:Ystep:Ymax)',1:length(Ymin:Ystep:Ymax));
INP = [MC1(:) MC2(:)];
SimSize = length(INP);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [55, 36, 138]]/255;
FIGS.fig6 = figure('Color', [1 1 1]);
CInit = meshgrid(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax);
contourf(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, CInit, 50, 'LineStyle', 'none');
colormap(bone)
hold on
TST = [INP randi(2, ((Xmax-Xmin)/Xstep+1)*((Ymax-Ymin)/Ystep+1),1).*2-3];
contour(Xmin:Xstep:Xmax, Ymin:Ystep:Ymax, ...
             reshape(TST(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)', ...
			 [-2 0], '-', 'LineWidth', 3, 'color', FIGS.LineColors(2,:));
plot(TRN(find(TRN(:,3)==-1),1), TRN(find(TRN(:,3)==-1),2), 'o', ... 
                            'markers', 10, 'color', FIGS.LineColors(4,:))
plot(TRN(find(TRN(:,3)==1),1), TRN(find(TRN(:,3)==1),2), 'o', ...
                            'markers', 10, 'color', FIGS.LineColors(8,:))
FIGS.plot3 = findall(FIGS.fig6,'Type','hggroup'); % This circumvents a Matlab bug
set(0,'units','pixels');
PixSS = get(0,'screensize');
FigX = 750; FigY = 600;
set(FIGS.fig6, 'Position', [(PixSS(3)-FigX)/2, (PixSS(4)-FigY)/2, FigX, FigY]);
title('\fontsize{12}Simple perceptron with dual hidden layer during training')

% Training/Learning process
for i=1:NumIter
   IdxIter = 1+floor(length(TRNn)*rand(UP,1));
   eta = (i <= 3*1e4)*eta_0(1) + (i > 3*1e4)*(i <= 4*1e4)*eta_0(2) + ...
         (i > 4*1e4)*(i <= 7*1e4)*eta_0(3) + (i>7*1e4)*eta_k/(i-eta_m-7e4+1);
   % Forward propagation
   V_mui0 = TRNn(IdxIter,1:2);
   V_mui1 = tanh(beta_0*(w_i0i1'*V_mui0')'-ones(UP,1)*theta_i1);
   V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(UP,1)*theta_i2);
   V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(UP,1)*theta_i3); % = O_mui3 - system outtoputtu
   % Backward propagation
   delta_mui3 = beta_0*(1-V_mui3.^2).*(TRNn(IdxIter,3)-V_mui3);
   delta_mui2 = beta_0*(1-(V_mui2).^2).*(delta_mui3*w_i2i3');
   delta_mui1 = beta_0*(1-(V_mui1).^2).*(delta_mui2*w_i1i2');
   % Random update of the system
   Deltaw_i2i3 = eta*V_mui2'*delta_mui3;
   Deltaw_i1i2 = eta*V_mui1'*delta_mui2;
   Deltaw_i0i1 = eta*V_mui0'*delta_mui1;
   Deltatheta_i3 = -eta*sum(delta_mui3);
   Deltatheta_i2 = -eta*sum(delta_mui2);
   Deltatheta_i1 = -eta*sum(delta_mui1);

   w_i2i3 = w_i2i3 + Deltaw_i2i3;
   w_i1i2 = w_i1i2 + Deltaw_i1i2;
   w_i0i1 = w_i0i1 + Deltaw_i0i1;
   theta_i3 = theta_i3 + Deltatheta_i3;
   theta_i2 = theta_i2 + Deltatheta_i2;
   theta_i1 = theta_i1 + Deltatheta_i1;

   % Energy calculations
   Ewin(1+mod(i,Ewinsize)) = .5*sum((TRNn(IdxIter,3)-V_mui3).^2);
   if mod(i,Ewinsize) == 0
      % Test the funzioni
	  V_mui0 = VLD(:,1:2);
	  V_mui1 = tanh(beta_0*(w_i0i1'*V_mui0')'-ones(length(V_mui0),1)*theta_i1);
      V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(length(V_mui0),1)*theta_i2);
      V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(length(V_mui0),1)*theta_i3);
	  H = .5*sum((VLD(:,3)-V_mui3).^2);
	  Evec2(i/Ewinsize) = H;
	  Cvvec2(i/Ewinsize) = mean(abs(VLD(:,3) - sign(V_mui3)));
	  V_mui0 = TRNn(:,1:2);
	  V_mui1 = tanh(beta_0*(w_i0i1'*V_mui0')'-ones(length(V_mui0),1)*theta_i1);
      V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(length(V_mui0),1)*theta_i2);
      V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(length(V_mui0),1)*theta_i3);
	  H = .5*sum((TRNn(:,3)-V_mui3).^2);
	  Evec1(i/Ewinsize) = H;
	  Cvvec1(i/Ewinsize) = mean(abs(TRNn(:,3) - sign(V_mui3)));
	  
      SPC = toc;
      waitbar(i/NumIter, wb1, sprintf('Calculating at %.2f iterations/s', Ewinsize/SPC));
	  tic;
   end
   % Updating the decision boundary
   if mod(i,simulationUpdateFrequency) == 0
	  V_mui1t = tanh(beta_0*(w_i0i1'*INP')'-ones(SimSize,1)*theta_i1);
      V_mui2t = tanh(beta_0*(w_i1i2'*V_mui1t')'-ones(SimSize,1)*theta_i2);
      V_mui3t = tanh(beta_0*(w_i2i3'*V_mui2t')'-ones(SimSize,1)*theta_i3);

	  V_mui1s = 2*atan(beta_0*(w_i0i1'*INP')'-ones(SimSize,1)*theta_i1)/pi;
      V_mui2s = 2*atan(beta_0*(w_i1i2'*V_mui1s')'-ones(SimSize,1)*theta_i2)/pi;
      V_mui3s = beta_0*(w_i2i3'*V_mui2s')'-ones(SimSize,1)*theta_i3;
	  
	  V_mui1tr = tanh(beta_0*(w_i0i1'*TRNn(:,1:2)')'-ones(length(TRNn),1)*theta_i1);
      V_mui2tr = tanh(beta_0*(w_i1i2'*V_mui1tr')'-ones(length(V_mui1tr),1)*theta_i2);
      V_mui3tr = tanh(beta_0*(w_i2i3'*V_mui2tr')'-ones(length(V_mui2tr),1)*theta_i3);
	  
      TST = [INP(:,1) INP(:,2) sign(V_mui3t)];
	  CMesh = reshape(TST(:,3), (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
      DMesh = reshape(V_mui3t, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
      EMesh = reshape(V_mui3s, (Xmax-Xmin)/Xstep +1,(Ymax-Ymin)/Ystep + 1)';
	  set(FIGS.plot3(1), 'ZData', DMesh);
	  set(FIGS.plot3(2), 'ZData', EMesh, 'LevelList', ...
	                        linspace(min(min(EMesh)), max(max(EMesh)), 51));
	  xlabel(sprintf('\\it C_v\\rm  = %f, \\it \\eta \\rm  = %f, Iteration step: %d', ...
	                  mean(abs(TRNn(:,3) - sign(V_mui3tr))), eta, i));
	  drawnow;   
   end
end
close(wb1);

% Plot the energy over iteration steps
FIGS.fig5 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(3, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Evec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(4, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
title('\fontsize{12}Evolution of energy level over time')

FIGS.fig6 = figure('Color', [1 1 1]);

plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec1, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(5, :));
hold on
plot(Ewinsize*(1:(NumIter/Ewinsize)), Cvvec2, 'LineWidth', 2, ...
              'Color', FIGS.LineColors(6, :));
legend('Training', 'Validation')
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
title('\fontsize{12}Evolution of classification error over time')

% Provoke the system to return values given that the weights are set
% in a Monte-Carlo simulation

SimSize = 1000;
INP2 = [rand(SimSize, 1)-.5 rand(SimSize,1)];
%V_mui1 = zeros(SimSize,J);
V_mui1 = tanh(beta_0*(w_i0i1'*INP2')'-ones(SimSize,1)*theta_i1);
V_mui2 = tanh(beta_0*(w_i1i2'*V_mui1')'-ones(SimSize,1)*theta_i2);
V_mui3 = tanh(beta_0*(w_i2i3'*V_mui2')'-ones(SimSize,1)*theta_i3); 
TST2 = [INP2(:,1) INP2(:,2) sign(V_mui3)];
FIGS.fig7 = figure('Color', [1 1 1]);
plot(TST2(find(TST2(:,3)==-1),1), TST2(find(TST2(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST2(find(TST2(:,3)==1),1), TST2(find(TST2(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')

% How to save figures
%h=figure;
%plot(x,y,'-bs','Linewidth',1.4,'Markersize',10);
% ...
%saveas(h,name,'fig')
%saveas(h,name,'jpg')

% When using the saveas function the resolution isn't as good
% as when manually saving the figure with File-->Save As...,
% It's more recommended to use hgexport instead, as follows:

% hgexport(gcf, 'figure1.jpg', hgexport('factorystyle'), 'Format', 'jpeg');
% How to encode video:
% http://video.stackexchange.com/questions/7903/how-to-losslessly-encode-a-jpg-image-sequence-to-a-video-in-ffmpeg

% But it's better to encode from png images like here:

% http://superuser.com/questions/533695/how-can-i-convert-a-series-of-png-images-to-a-video-for-youtube
