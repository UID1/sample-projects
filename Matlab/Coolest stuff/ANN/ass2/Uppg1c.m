% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Exercise 1c Solution
% by Robin Axelsson
%

% This program analyses iteration times over
% a larger number of independent runs. It also
% tests for different alphas to see how different
% alpha values may affect the speed at which a
% solution might be found.

a1=[8479, 4868, 3696, 2646, 169, 142];
b1=[11968, 5026, 1081, 1050, 691, 184];
c1=[8479, 4167, 2646, 1081, 881, 859, 701, 691, 184, 169, 142];
numruns = 250; % Number of runs for each session

% Parameter initialization
runtimes = [];
sigma_opts = zeros(length(a1), 1);
mu_opts = zeros(length(b1), 1);
H_verif =  [];

wb2 = waitbar(0,'Running neural simulation');
for i=1:numruns
   DATA1 = randsrchfun1(a1, b1, c1, 20);
   runtimes = [runtimes DATA1.time];
   dblcheck = 0;
   for j = 1:size(mu_opts,2)
      dblcheck = dblcheck + sum(mu_opts(:,j) == DATA1.mu');
   end
   H_verif = [H_verif DATA1.H];
   if dblcheck == 0
      mu_opts = [mu_opts DATA1.mu'];
      sigma_opts = [sigma_opts DATA1.sigma'];
   end
   dblcheck = 0;
   for j = 1:size(sigma_opts,2)
      dblcheck = dblcheck + sum(sigma_opts(:,j) == DATA1.sigma');
   end
   if dblcheck == 0
      mu_opts = [mu_opts DATA1.mu'];
      sigma_opts = [sigma_opts DATA1.sigma'];
   end
   waitbar(i/numruns, wb2);
end
meanrt = mean(runtimes);
stdrt = std(runtimes);
close(wb2);

sigma_opts(:,1) = [];
mu_opts(:,1) = [];
fprintf('\nSummary:\n')
fprintf('\nMean time: %f \n', mean(runtimes))
fprintf('Stddev time: %f \n', std(runtimes))
fprintf('# of runs yielding non-zero energy: %d\n', sum(H_verif ~= 0))
fprintf('# of combinations of a and b found for given c: %d \n', size(mu_opts,2))


FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; [230, 229, 214]; [115, 115, 115]; [176, 169, 165]; [211, 202, 183]]/255;

plot(1:length(DATA1.H_vec), DATA1.H_vec, 'LineWidth', 2, 'Color', FIGS.LineColors(3, :));
xlabel('\fontsize{12}Time (in iterative steps)');
ylabel('\fontsize{12}Energy level of the system H(S^{(k)})');
title('\fontsize{12}Evolution of energy level over time')


% The mean runtime for 250 runs is about 7200 time steps. Also this
% program manages to get zero energy on all (100% of) the runs.
% However, it found different solutions to the problem.

% Solution1:
% sigma = 5 3 2 6 4 1
% mu = 4 3 6 5 2 1
% The following code for c(nu)
CT = sort([cumsum(a1(sigma_opts(1:(end-1),1))) cumsum(b1(mu_opts(:,1)))]);
c_opt1 = [CT(1) diff(CT)];
nu_order = zeros(1, length(c_opt1));
for i=1:length(c_opt1)
   nu_order(i) = find(c_opt1(i) == c1);
end

% Yields the order nu = 
%  10 5 4 9 8 6 2 7 11 3 1

% Solution2:
% sigma = 1 6 4 2 3 5
% mu = 1 2 3 6 5 4
% The following code for c(nu)
CT = sort([cumsum(a1(sigma_opts(1:(end-1),2))) cumsum(b1(mu_opts(:,2)))]);
c_opt1 = [CT(1) diff(CT)];
nu_order = zeros(1, length(c_opt1));
for i=1:length(c_opt1)
   nu_order(i) = find(c_opt1(i) == c1);
end

% Yields the order nu = 
%  1 11 3 7 2 6 4 9 8 5 10






