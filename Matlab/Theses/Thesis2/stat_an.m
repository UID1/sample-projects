% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% This file analyzes the hourly electricity price data and plots
% relevant graphs


% We start by loading the data to be analyzed

if ~any(strcmp(who,'ELSPOTh2010'))
   fid1 = fopen('elspoth2010.bin', 'r');
   ELSPOTh2010 = fread(fid1, 8760, 'double');
   fclose(fid1);
end
if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
tmpdaily = mean(reshape(tmp2010.national, 24, 365));
if ~any(strcmp(who,'ELSPOTd2010'))
   load ELSPOTd2010.mat;
end
load ELCONSd2010.mat
load ELCONSh2010.mat


% Next we compute the weekday mean for each hour of the day and the wee-
% kend mean.  First day of 2010 was on a friday. Weekdays = {1, 4, 5, 6}
% 1 = friday,  2 = saturday,  3 = sunday,  4 = monday,  5 = tuesday, 6 =
% wednesday, 0 = thursday.

Weekdays = (mod(1:365,7) == 1) + (mod(1:365,7) == 4) + ...
           (mod(1:365,7) == 5) + (mod(1:365,7) == 6) + ...
	   (mod(1:365,7) == 0);
Weekends = (mod(1:365,7) == 2) + (mod(1:365,7) == 3);

WDM = meshgrid(Weekdays, 1:24);
WEM = meshgrid(Weekends, 1:24);
WinterHrs = (1:8760 <= 1416) + (1:8760 > 8016);
SpringHrs = (1:8760 > 1416) - (1:8760 >= 3624);
SummerHrs = (1:8760 > 3624) - (1:8760 >= 5808);
AutumnHrs = (1:8760 > 5808) - (1:8760 >= 8016);
Mn_spot_wd = zeros(1, 24);
Mn_spot_we = zeros(1, 24);

Mn_spot_wi = zeros(1, 24);
Mn_spot_sp = zeros(1, 24);
Mn_spot_su = zeros(1, 24);
Mn_spot_au = zeros(1, 24);

for i = 0:23
   Mn_spot_wd(i+1) = ELSPOTh2010'*(WDM(:).*(mod(1:8760, 24) == i)')/...
                     sum(WDM(:).*(mod(1:8760, 24) == i)');
   Mn_spot_we(i+1) = ELSPOTh2010'*(WEM(:).*(mod(1:8760, 24) == i)')/...
                     sum(WEM(:).*(mod(1:8760, 24) == i)');
   Mn_spot_wi(i+1) = ELSPOTh2010'*(WDM(:).*WinterHrs'.* ...
                     (mod(1:8760, 24) == i)')/ ...
                     sum(WDM(:).*WinterHrs'.*(mod(1:8760, 24) == i)');
   Mn_spot_sp(i+1) = ELSPOTh2010'*(WDM(:).*SpringHrs'.* ...
                     (mod(1:8760, 24) == i)')/ ...
                     sum(WDM(:).*SpringHrs'.*(mod(1:8760, 24) == i)');
   Mn_spot_su(i+1) = ELSPOTh2010'*(WDM(:).*SummerHrs'.* ...
                     (mod(1:8760, 24) == i)')/ ...
                     sum(WDM(:).*SummerHrs'.*(mod(1:8760, 24) == i)');
   Mn_spot_au(i+1) = ELSPOTh2010'*(WDM(:).*AutumnHrs'.* ...
                     (mod(1:8760, 24) == i)')/ ...
                     sum(WDM(:).*AutumnHrs'.*(mod(1:8760, 24) == i)');
end

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

FIGS.fig1 = figure('Color',[1 1 1]);

plot(Mn_spot_wd, 'Color', FIGS.LineColors(7,:), 'LineWidth', 2)
hold on
plot(Mn_spot_we, 'Color', FIGS.LineColors(3,:), 'LineWidth', 2)
legend('Weekdays', 'Weekends', 'Location', 'SouthEast')
xlabel('\fontsize{12}Hour (1 = midnight)')
ylabel('\fontsize{12}SEK/MWh');
xlim([1 24])

% A plot that compares consumption and  electricity prices over a 10-day
% period is a two-scale plot over a 240-hour period. Let's start at hour
% 4344 and 240 hours from that.

FIGS.fig2 = figure('Color',[1 1 1]);
[FIGS.fun2 FIGS.h1 FIGS.h2] = plotyy((1:240)/24, ...
                              ELCONSh2010.se(4344+(0:239))/1000, ...
			      (1:240)/24, ELSPOTh2010(4344+(0:239)));
set(get(FIGS.fun2(1), 'Ylabel'), 'String', ...
        '\fontsize{12}Swedish demand (GW)', ...
        'Color', FIGS.LineColors(2,:));
set(get(FIGS.fun2(2), 'Ylabel'), 'String', ...
        '\fontsize{12}\bfElectricity price (SEK/MWh)', ...
        'Color', FIGS.LineColors(7,:)*.8)
set(FIGS.h1, 'Color', FIGS.LineColors(2,:), 'LineWidth', 1)
set(FIGS.h2, 'Color', FIGS.LineColors(7,:), 'LineWidth', 2.5)
set(FIGS.fun2(1), 'Ycolor', [0 0 0])
set(FIGS.fun2(2), 'Ycolor', FIGS.LineColors(7,:)*.6)
xlabel('\fontsize{12}Days (30 June - 10 July 2010)')
set(FIGS.fun2(1), 'XTick', [])
set(FIGS.fun2(2), 'XTick', [])
set(gca, 'XTick', 0:10)
set(gca, 'FontSize', 12, 'XTicklabel', {'30/6', '1/7', ...
     '2/7', '3/7', '4/7', '5/7', '6/7', '7/7', '8/7', '9/7', '10/7'})

% Now we plot the weekday average season by season as computed in the 24
% iterations long for-loop above.

FIGS.fig3 = figure('Color',[1 1 1]);

plot(Mn_spot_wi, 'Color', FIGS.LineColors(7,:), 'LineWidth', 2)
hold on
plot(Mn_spot_sp, 'Color', FIGS.LineColors(3,:), 'LineWidth', 1.5)
plot(Mn_spot_su, 'Color', FIGS.LineColors(2,:), 'LineWidth', 1.5)
plot(Mn_spot_au, 'Color', FIGS.LineColors(4,:), 'LineWidth', 3)

legend('Winter', 'Spring', 'Summer', 'Autumn', 'Location', 'NorthWest')
xlabel('\fontsize{12}Hour (1 = midnight)')
ylabel('\fontsize{12}SEK/MWh');
xlim([1 24])

% Histogram plot
FIGS.fig4 = figure('Color',[1 1 1]);
hist(ELSPOTh2010, 100)
hold on
xrange = 1:10:1000;
plot(xrange,  1e5/std(ELSPOTh2010)*exp(-(xrange - ...
     mean(ELSPOTh2010)).^2/var(ELSPOTh2010)), 'Color', ...
     FIGS.LineColors(3,:), 'LineWidth', 2)

% Now we make a quantile-quantile plot of the electricity data
FIGS.fig5 = figure('Color',[1 1 1]);
qqplot(ELSPOTh2010)
title('\fontsize{12}Normal Probability Plot')
xlabel('\fontsize{12}Probability Quantiles')
ylabel('\fontsize{12}Quantiles of Electricity Price Data')


% We analyze the correlations of the data

ELcorr1 = zeros(1, 1000);
ELcorr2 = zeros(1, 1000);

for i = 0:999
   ELcorr1(i+1) = corr(ELSPOTh2010(i+1:end), ELSPOTh2010(1:end-i));
   ELcorr2(i+1) = corr(ELSPOTh2010(i+1:end).^2, ELSPOTh2010(1:end-i).^2);
end
FIGS.fig6 = figure('Color',[1 1 1]);
plot(ELcorr1, 'Color', FIGS.LineColors(3,:), 'LineWidth', 2)
xlabel('\fontsize{12}Lag')
ylabel('\fontsize{12}Autocorrelation')

FIGS.fig7 = figure('Color',[1 1 1]);
plot(ELcorr2, 'Color', FIGS.LineColors(3,:), 'LineWidth', 2)
xlabel('\fontsize{12}Lag')
ylabel('\fontsize{12}Autocorrelation')

% Some summary statistics of the electricity price data

disp('Summary Statistics')
disp('Mean')
mean(ELSPOTh2010)
disp('Minimum')
min(ELSPOTh2010)
disp('Maximum')
max(ELSPOTh2010)
disp('Standard Deviation')
std(ELSPOTh2010)
disp('Skewness')
skewness(ELSPOTh2010)
disp('Kurtosis')
kurtosis(ELSPOTh2010)


FIGS.fig8 = figure('Color',[1 1 1]);
[FIGS.fun2 FIGS.h1 FIGS.h2] = plotyy((1:240)/24, ...
                              ELCONSh2010.se(1:240)/1000, ...
			      (1:240)/24, ELSPOTh2010(1:240));
set(get(FIGS.fun2(1), 'Ylabel'), 'String', ...
        '\fontsize{12}Swedish demand (GW)', ...
        'Color', FIGS.LineColors(2,:));
set(get(FIGS.fun2(2), 'Ylabel'), 'String', ...
        '\fontsize{12}\bfElectricity price (SEK/MWh)', ...
        'Color', FIGS.LineColors(7,:)*.8)
set(FIGS.h1, 'Color', FIGS.LineColors(2,:), 'LineWidth', 1)
set(FIGS.h2, 'Color', FIGS.LineColors(7,:), 'LineWidth', 2.5)
set(FIGS.fun2(1), 'Ycolor', [0 0 0])
set(FIGS.fun2(2), 'Ycolor', FIGS.LineColors(7,:)*.6)
xlabel('\fontsize{12}Days (1 - 11 January 2010)')
set(FIGS.fun2(1), 'XTick', [])
set(FIGS.fun2(2), 'XTick', [])
set(gca, 'XTick', 0:10)
set(gca, 'FontSize', 12, 'XTicklabel', {'1/1', '2/1', ...
     '3/1', '4/1', '5/1', '6/1', '7/1', '8/1', '9/1', '10/1', '11/1'})

% Let us plot some 1-day and 5-day OHLC charts

FIGS.fig9 = figure('Color',[1 1 1]);
subplot(4, 1, 1:3)
ESt2010 = reshape(ELSPOTh2010, 120, 73);
highlow(max(ESt2010)', min(ESt2010)', ESt2010(1,:)', ...
        ESt2010(end,:)', [0 0 0])
xlim([1 73])
set(gca, 'XTick', [])
title(['\fontsize{12}5-day electricity spot high-low ' ...
       'trading chart for 2010'])
ylabel('\fontsize{12}SEK/MWh')
hold on
STD_ESt = ones(1, length(1:120:8760-4*120));
MN_ESt = ones(1, length(1:120:8760-4*120));
BL_PTS = 1:120:8760-4*120;
for i = 1:length(BL_PTS)
   STD_ESt(i) = std(ELSPOTh2010(BL_PTS(i):BL_PTS(i)+4*120));
   MN_ESt(i) = mean(ELSPOTh2010(BL_PTS(i):BL_PTS(i)+4*120));
end
plot(5:73, MN_ESt-2*STD_ESt, 'Color', FIGS.LineColors(7,:), ...
     'LineWidth', 2)
plot(5:73, MN_ESt+2*STD_ESt, 'Color', FIGS.LineColors(7,:), ...
     'LineWidth', 2)
plot(5:73, MN_ESt, 'Color', FIGS.LineColors(5,:))
subplot(4, 1, 4)
bar(sum(reshape(ELCONSh2010.se, 120, 73))/1e6, 'histc')
xlim([1 73])
ylim([0 3])
set(gca, 'XTick', 1:18:73)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July' ...
		    'October', 'December'})
ylabel('\fontsize{12}TWh')

FIGS.fig10 = figure('Color',[1 1 1]);
subplot(4, 1, 1:3)
ESt2010 = reshape(ELSPOTh2010, 24, 365);
highlow(max(ESt2010)', min(ESt2010)', ESt2010(1,:)', ...
        ESt2010(end,:)', [0 0 0])
xlim([1 365])
set(gca, 'XTick', [])
title(['\fontsize{12} Daily electricity spot high-low ' ...
       'trading chart for 2010'])
ylabel('\fontsize{12}SEK/MWh')
hold on
STDd_ESt = ones(1, length(1:24:8760-20*24));
MNd_ESt = ones(1, length(1:24:8760-20*24));
BLd_PTS = 1:24:8760-20*24;
for i = 1:length(BLd_PTS)
   STDd_ESt(i) = std(ELSPOTh2010(BLd_PTS(i):BLd_PTS(i)+20*24));
   MNd_ESt(i) = mean(ELSPOTh2010(BLd_PTS(i):BLd_PTS(i)+20*24));
end
plot(21:365, MNd_ESt-2*STDd_ESt, 'Color', FIGS.LineColors(7,:), ...
     'LineWidth', 2)
plot(21:365, MNd_ESt+2*STDd_ESt, 'Color', FIGS.LineColors(7,:), ...
     'LineWidth', 2)
plot(21:365, MNd_ESt, 'Color', FIGS.LineColors(5,:))
subplot(4, 1, 4)
bar(sum(reshape(ELCONSh2010.se, 24, 365))/1e3, 'histc')
xlim([1 365])
ylim([0 600])
set(gca, 'XTick', 1:90:365)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July' ...
		    'October', 'December'})
ylabel('\fontsize{12}GWh')

