function outputValue = PSOFunction(var)
  A1 = -[15 27 36 18 12];
  A2 = [ 35 -20 -10  32 -10; ...
        -20  40 -6  -31  32; ...
        -10 -6   11 -6  -10; ...
         32 -31 -6   38 -20; ...
        -10  32 -10 -20  31 ];
  if min(size(var)) == 1
    outputValue = A1*var(:) + var(:)'*A2*var(:);
  else
    numPoints = size(var,1);
    outputValue = zeros(numPoints, 1);
    for i = 1:numPoints
      outputValue(i) = round(A1*var(i,:)' + var(i,:)*A2*var(i,:)');
    end
  end
end