%
% Counterparty risk and contingent CDS-valuation
% under closed-form expressions
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%

% Paper CIR: dr(t) =     k*(theta - r(t))*dt + sigma*sqrt( r(t))*dW(t), r(0)=r_0
%    My CIR: dy(t) = kappa*(mu_t  - y(t))*dt +    nu*sqrt(mu_t))*dW(t), y(0)=y_0

% My name | Brigo name
%       r = y
%       k = kappa
%   theta = mu_t
%   sigma = nu

%    Other names
%       h = gamma_CIR
%       k = alpha = kappa

gamma_CIR = @(alpha, sigma)(sqrt(alpha.^2+2.*sigma.^2));
CIR_A = @(T, gamma, alpha, mu, nu)(log(2.*gamma.*exp((gamma+alpha).*T.*.5)./((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma)).*2.*alpha.*mu./nu.^2);
CIR_B = @(T, gamma, alpha)(2.*(exp(gamma.*T)-1)./ ((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma));

% P_CIR(t) = P(0, t) => P(t, T) = P_CIR(T-t,...), A(t, T) = CIR_A(T-t,...), B(t, T) = CIR_B(T-t,...)

P_CIR = @(t, kappa, nu, mu, y_0)(exp(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)-CIR_B(t, gamma_CIR(kappa, nu), kappa).*y_0)); % This is the same as equation (3.24)

beta_CIR = [y_0, kappa, mu_t, nu]; % = [r_0, k, theta, sigma]


rbar = @(X, t, kappa, nu, mu, y_0)(log(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)./X)./CIR_B(t, gamma_CIR(kappa, nu), kappa));
rho = @(t, T, kappa, nu)(2*gamma_CIR(kappa, nu)./(nu.^2*exp(gamma_CIR(kappa, nu)*(T-t))-1));
psi_CIR = @(kappa, nu)((kappa+gamma_CIR(kappa, nu))./nu.^2);

% We intend to use the non-central chi-squared distribution, denoted as chi2(.,v,lambda) where v is
% # degrees of freedom and lambda is the noncentrality parameter.

% The price at time 't' of a European call option with maturity T > t, strike price X,
% written on a zero-coupon bond maturing at S > T, and with the instantaneous rate at time t
% given by r(t) (I set it equal to y_t) is

ZBC_CIR = @(t, T, S, X, y_t, kappa, mu, nu)(P_CIR(S-t, kappa, nu, mu, y_t).*ncx2cdf(2*rbar(X, t, kappa, nu, mu, y_t)*(rho(t, T, kappa, nu)+psi_CIR(kappa, nu)+CIR_B(S-T, gamma_CIR(kappa, nu), kappa)),4*kappa.*mu./nu.^2,2*rho(t, T, kappa, nu).^2*y_t*exp(gamma_CIR(kappa, nu)*(T-t))./(rho(t, T, kappa, nu)+psi_CIR(kappa, nu)+CIR_B(S-T, gamma_CIR(kappa, nu),kappa)))-X.*P_CIR(T-t, kappa, nu, mu, y_t).*ncx2cdf(2*rbar(X, t, kappa, nu, mu, y_t)*(rho(t, T, kappa, nu)+psi_CIR(kappa, nu)),4*kappa.*mu./nu.^2,2*rho(t, T, kappa, nu).^2*y_t*exp(gamma_CIR(kappa, nu)*(T-t))./(rho(t, T, kappa, nu)+psi_CIR(kappa, nu)))); % This is eq (3.26) in Brigo, Mercurio

% The function f^M(t) = - \frac{\partial ln P^M(0,T)}{\partial T} (i.e. -dln P(0,T)/dT) is the
% market instantaneous forward rate at time 0 for the maturity T and P^M(0,T) is the market
% discount factor for the maturity T.

% The instantaneous short rate under the risk neutral measure Q is defined by
% r_t = x_t + \phi_t(t;\alpha), t >= 0     (3.66)
% where x is a stochastic process. Then Corollary 3.8.1 says that this short rate model (3.66) fits
% the currently observed term structure if and only if
%
% \phi(t; \alpha) = f^M(0,t) - f^x(0,t;\alpha)
%
% where x represents a model estimation, in this case CIR. f^CIR is defined as in equation (3.77).
% The question is what is f^M and how can we implement that.


% Once the \f^M(t;\alpha) and the P^M(t,T) functions are determined, the values can be computed
% using equation (3.77) and (3.78).
