function iSelected = TournamentSelect(fitness, tournamentSize, tournamentSelectionParameter)
  populationSize = length(fitness);
  if (tournamentSize > populationSize)
    fprintf('Error: Tournament size exceeds population size.');
    return;
  end
  tournamentMembers = 1 + fix(rand(1, tournamentSize) * ...
                      populationSize);
  while (length(tournamentMembers) > 1)
    [maxValue maxIndex] = max(fitness(tournamentMembers));
    if (rand < tournamentSelectionParameter)
      iSelected = tournamentMembers(maxIndex);
      return;
    else
      tournamentMembers(maxIndex) = [];
    end
  end
  iSelected = tournamentMembers;
end