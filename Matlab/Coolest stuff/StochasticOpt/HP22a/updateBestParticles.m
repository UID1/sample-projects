function updatedParticles = updateBestParticles(Particles, newSwarmValues)
  updatedParticles = Particles;
  newValuesAreBetter = (Particles.previouslyBestSwarmValues > newSwarmValues);
  updatedParticles.xijPB(:,1) = newValuesAreBetter.*Particles.xij(:,1) + ...
                             ~newValuesAreBetter.*Particles.xijPB(:,1);
  updatedParticles.xijPB(:,2) = newValuesAreBetter.*Particles.xij(:,2) + ...
                             ~newValuesAreBetter.*Particles.xijPB(:,2);
  updatedParticles.previouslyBestSwarmValues = newValuesAreBetter.*newSwarmValues + ...
                             ~newValuesAreBetter.*Particles.previouslyBestSwarmValues;
  [~, updatedParticles.swarmBestIndex] = min(updatedParticles.previouslyBestSwarmValues);
  updatedParticles.xijSB = updatedParticles.xijPB(updatedParticles.swarmBestIndex,:);
end