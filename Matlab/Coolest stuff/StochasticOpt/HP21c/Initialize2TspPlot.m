function Initialize2TspPlot(cityLocation)
city = [];
hold on;
for i = 1:length(cityLocation)
   text(cityLocation(i,1)+.2, cityLocation(i,2)-.2, sprintf('\\fontsize{8}%d', i));
end