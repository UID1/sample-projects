%2DIsing.m

% Simulering av 2D Isingmodellen med periodiska randvillkor (Metropolisalgoritmen).

% Programmet plottar medelenergi, specifikt v"rme och magnetisering som
% funktion av "temperaturen" kT f"r en Isingkedja med N spinn med antagandet
% att termisk j"mvikt uppn�tts efter c:a K steg.

clear

global N J kT I muB K

clf;

home;


% INDATA
L = input('Ange L som ger antalet spinn N = LxL: ');
N = L * L;
J = input('Spinnkoppling J: ');
muB = input('magnetf"lt muB: ');
I = input('Antalet iterationer I: ');
% Hur m�nga iterationer vi vill g"ra p� modellen innan vi b"rjar summera energierna
K = input('Starta sampling, K: ');

%STARTKONFIGURATION

%Vi vill slumpa fram en vektor (matris av dimension 1) av l"ngden N
%som inneh�ller tillst�nden -1 resp 1

% Vi ser till att slumpgeneratorn befinner sig i ett nytt tillst�nd vid
% varje k"rning genom att basera tillst�ndet p� avl"sning av tid och datum
rand('state', sum(100*clock)) 
%s = ones(L); % Ferromagnetisk kall start
s =1-2*round(rand(L)); % Varm start


%ber"kna energin och magnetisering f"r starttillst�ndet

new_energy=0;
magnetization=0;
for i = 1:L
   for j = 1:L
      new_energy=new_energy-J*(s(j,i)*s(mod(j,L)+1,i) + s(j,i)*s(j,mod(i,L)+1))-muB*s(j,i);   %periodic BC: N+1<=>1
      magnetization=magnetization+s(j,i);
   end
end

for p=1:20  % Stega fram temperaturvariabeln "kT"
kT = (21.-p)*.25;
Temperatur = kT % Temperaturflagga


% Initialisera energi och spin summor

sumenergy = 0; 
squaresumenergy = 0;
sumspin = 0;
squaresumspin = 0;



for t=1:I  % Stega fram i tiden

old_energy=new_energy;

site(1) = floor(rand.*L + 1);  % V"lj slumpvis ut ett spinn fr�n en vektor av l"ngd L
site(2) = floor(rand.*L + 1);  % V"lj slumpvis ut ett spinn fr�n en vektor av l"ngd L
%site = [floor(floor(site/L)) mod(site, L)+1 ]; % Oms"tt denna position i en motsvarande kvadratisk matris
s(site(1), site(2)) = (-1).*s(site(1), site(2)); % Flippa det valda spinnet --> ny konfiguration

% Nya spinnkonfigurationens energi och magnetisering
new_energy = old_energy -2*J.*( s(site(1), site(2)).*ones(1,4) )*[s(mod(site(1)+[0 -2],L)+1,site(2)); s(site(1), mod(site(2)+[0 -2],L)+1)']-2*muB*s(site(1),site(2)); 
magnetization=magnetization+2*s(site(1),site(2));

if (new_energy >= old_energy) & (exp((old_energy-new_energy)./(kT)) <= rand)
 
 
s(site(1), site(2)) = (-1).*s(site(1),site(2)); 
new_energy=old_energy;
magnetization=magnetization+2*s(site(1),site(2));
% F"rkasta den nya spinnkonfigurationen
% (dvs. flippa tillbaks det slumpm"ssigt
% valda spinnet!) om dess energi "r st"rre
% "n den "gamla" konfigurationens energi OCH
% den relativa Boltzmannvikten "r mindre "n
% ett slumpm"ssigt valt tal i intervallet [0,1].

end 

if (t>=K)  %r"kna med det nya eller gamla tillst�ndet

sumenergy = sumenergy + new_energy;
squaresumenergy = squaresumenergy + (new_energy)^2.;
sumspin = sumspin + magnetization;
squaresumspin = squaresumspin + (magnetization)^2.;

end



end %t-loop




%ensembelmedelv"rden vid given temp

x(p) = kT;  
Medelenergi(p) = sumenergy/((I-K+1)*N);
Specifiktvarme(p) = (squaresumenergy/(I-K+1)-(sumenergy/(I-K+1))^2.)/(N*(kT)^2.);
Magnetisering(p) = sumspin/(N*(I-K+1));
Susceptibilitet(p) = kT*(squaresumspin/(I-K+1)-(sumspin/(I-K+1))^2.)/(N*(kT)^2.);

end  %p-loop, temp

% PLOT

subplot(2,2,1); plot(x,Medelenergi); hold on;
title('Medelenergi'), ylabel('<E>/N'), xlabel('kT')
subplot(2,2,2); plot(x,abs(Magnetisering),'g'); hold on
title('Magnetisering'), ylabel('M/N'), xlabel('kT')
subplot(2,2,3); plot(x,Specifiktvarme,'r'); hold on
title('Specifikt v�rme'), ylabel('C/Nk'), xlabel('kT')
subplot(2,2,4); plot(x,Susceptibilitet,'r'); hold on
title('Susceptibilitet'), ylabel('\chi/\muk'), xlabel('kT')

%Plottar exakta l�sningar till Isingmodellen utan magnetf�lt i 2 dimensioner


if (muB==0)
kT = .05:.001:5; %temperaturintervallet 
[x,y]=ellipke(4*(sinh(2*J./kT)).^2./(cosh(2*J./kT)).^4);  %elliptiska integraler... 
k=J./kT;

%energi
subplot(2,2,1);plot(kT, -J*coth(2*k).*(1+(2/pi)*(2*(tanh(2*k)).^2-1).*x),'k'); hold on

%specifikt v�rme 
subplot(2,2,3);plot(kT, (4/pi)*(k.*coth(2*k)).^2.*(x-y-(1-(tanh(2*k)).^2).*((pi/2)+(2*(tanh(2*k)).^2-1).*x)),'k'); hold on

%magnetisering
subplot(2,2,2); kT = .05:.001:(J/.44); 
plot(kT, 1-(1-(tanh(J./kT)).^2.).^4./(16*(tanh(J./kT)).^4.),'k'); hold on
subplot(2,2,2); kT = (J/.44):.001:5; 
plot(kT, 0,'k'); hold on

end




% Plot av exakta resultat f"r medelenergi och specifikt v"rme som j"mf"relse
% (Plishke-Bergersen, "Equilibrium Statistical Physics", sid 80 - 86)

%if (muB==0)
%subplot(1,3,1); kT = .05:.001:5; plot(kT, -J.*tanh(J./kT),'k'); hold on
%subplot(1,3,2); plot(kT, (J./kT).^2./(cosh(J./kT)).^2,'k'); hold on
%end 
%subplot(1,3,3); kT = .05:.2:5; 
%plot(kT, sinh(muB./kT)./((sinh(muB./kT)).^2+exp(-4*J./kT)).^.5,'ok'); hold on
