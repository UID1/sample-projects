% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% The  Matlab script  'main2.m' uses  the  internal  matlab package  and
% function ARMAX  to fit ARMA  models into the  electricity  price data.
% The model estimations are evaluated  using a subset of the information
% criteria  and prediction  error  estimators  introduced  in 'main1.m'.
% Trend and seasonalities are  evaluated and  filtered out of  the price
% data. The spikes in  the data are  also analyzed  and methods to over-
% come the hurdles that are caused by these spikes are investigated.
%
% 'main3.m' is a continuation  of main3.m  and cannot be run  without it
% This script runs the ARMAX estimations  given the data computed in the
% previous main scripts.  main3 and main4  are computationally intensive
% and takes a few hours to run on a mainstream computer.


% +----------------+
% |     Step 3     |
% +----------------+

if ~any(strcmp(who,'ELh2010_des1'))
   warning('This script should not be run before main2.m.')
end

% Let's try the  (1:20, 1:20, 1:3, 2) on  original data,  filtered data,
% and filtered and deseasonalized data:
%
%  ELSPOTh2010                                     _.: 1a :._
%  ELh2010_des1                                    _.: 1b :._
%  ELh2010_des1-ELTREND2010_2.removable            _.: 1c :._
%
%  Repeat  the  procedure  on  element  1:2190  (3 months data)  (2a-2c)
%  We pick the model with the lowest AIC, then we try different caps but
%  but with the same parameters. When we have found a cap, we fix it and
%  re-evaluate the best ARMA-parameters above.
% 

% Get the temperature data for year 2010

tmp2010 = temp_proc(8760, 'cubic');

% With temperature data included, we have the following model to analyze
%
%  X_t = m_t + s_t + Y_t + a_0*Z_t+a_1*Z_{t-1} + ... + a_d*Z_{t-d}
%
% where  Z_t represents  the  temperature  movements.  I consider it un-
% reasonable  to look beyond d = 2  so I only  consider d = 0,1,2.  So I
% estimate the parameters for these three models with the deseasonalized
% data. I also assume a one step lag before the temperature affects the
% electricity prices.

A1        = [ELh2010_des1(3:end-1) ELh2010_des1(2:end-2) ...
             ELh2010_des1(1:end-3)];
el_acoeff = (A1'*A1)\A1'*tmp2010.national(4:end);
ELh2010_des1t = ELh2010_des1(4:end) - A1*el_acoeff;
ELTREND2010_2t = est_ma(ELh2010_des1t, 500);

% The next question is how does the temperature data affect periodicity
% of the electricity price data. The function fspectrum3 analyses the
% spectrum of a given insignal. Let us define:
%
%    A0 = [ELSPOTh2010(3:end-1) ELSPOTh2010(2:end-2) ...
%          ELSPOTh2010(1:end-3)];
%    elspot_coef = (A0'*A0)\A0'*tmp2010.national(4:end)
%    ELSPOTh2010t = ELSPOTh2010 - A0*elspot_coef;
%    ELSPEC2010t2 = fspectrum3(ELSPOTh2010t,1)
% 
% The fspectrum is unable to get a proper pscale for the data but fortu-
% nately I can use the pscale from the original data so I have the fol-
% lowing plot:
%
%    plot(ELSPECh2010.pscale(3:end), ELSPEC2010t2.spectrum')
%    hold on
%    plot(ELSPECh2010.pscale(3:end), ELSPECh2010.spectrum', 'r')
%
% And it shows that removal of the temperature variations from the model
% does not affect the smaller periodicities. But it shows a considerable
% influence  on the longer periodicites above 5000 hours.  Tests of this
% are found in testtspec.m and they show that the temperature variations
% don't affect any other peaks for d = 0,1,2. They do however affect the
% spectrum to some degree.  So I estimate it to be safe to apply the de-
% seasonalization before estimating  the influences from the temperature
% variations.


% Run ARMAX estimations on all data

[m1a par1a] = armabat2(ELSPOTh2010, ...
                        tmp2010.national, 1:20, 1:20, 1:3, 2, '1a')
[m1b par1b] = armabat2(ELh2010_des1, ...
                        tmp2010.national, 1:20, 1:20, 1:3, 2, '1b')
[m1c par1c] = armabat2(ELh2010_des1(4:end)-ELTREND2010_2t.removable, ...
			tmp2010.national(4:end), ...
			1:20, 1:20, 1:3, 2, '1c')

% Run on the first 3 months of data

[m2a par2a] = armabat2(ELSPOTh2010(1:2190), ...
                        tmp2010.national(1:2190), ...
			1:20, 1:20, 1:3, 2, '2a')
[m2b par2b] = armabat2(ELh2010_des1(1:2190), ...
                        tmp2010.national(1:2190), ...
			1:20, 1:20, 1:3, 2, '2b')
[m2c par2c] = armabat2(ELh2010_des1(4:2193) - ...
                        ELTREND2010_2t.removable(1:2190), ...
                        tmp2010.national(4:2193), ...
			1:20, 1:20, 1:3, 2, '2c')


% Results:
%
% 1a: p: 20, q: 11, r: 3, minaic: 7.2824
% 1b: p: 17, q: 12, r: 3, minaic: 7.2693
% 1c: p: 20, q: 5,  r: 3, minaic: 7.2520
%
% 2a: p: 19, q: 19, r: 3, minaic: 8.1876
% 2b: p: 19, q: 19, r: 3, minaic: 8.1793
% 2c: p: 19, q: 19, r: 3, minaic: 8.1723
%

% I tried  the  3 month  period after  2000 elements  and  got half  AIC
% compared  to the subperiod  above.  The winter season  is particularly
% difficult  to model  compared  to the spring/summer season.  I'll look
% into later periods. The data for February give an even higher AIC than
% the 3 month period. It stands clear that the spikes prevents the model
% from properly fitting  the data.  Conclusion:  we need to do something
% about those spikes.

% I tried to improve 1c by trying higher values of r. The optimal value
% of r was found to be 27.  The number of parameters p seems to peak at
% 80.  Many numbers between 80 and 90 gives errors, but 84,85,88 and 89 
% work fine and neither give as low AIC as q=80.




