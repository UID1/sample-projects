% Let's try band plots and see where it goes
% This is based upon the results from uppg1
% or uppg2, i.e. we need the gCvTRN/VLD and/or
% the gETRN/VLD matrices for this to run smoothly
%
% Yours forever truly
% Robin (cCc) 2015

FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]; [55, 36, 138]]/255;
FIGS.MarkerColors = [[12 95 45]; [35 226 75]; [188 255 196]]/255;


FIGS.fig10 = figure('Color', [1 1 1]);
%plot(mean(gCvTRN')', 'LineWidth', 3, 'Color', FIGS.LineColors(2, :));
%hold on
%plot(max(gCvTRN')', 'LineWidth', 1, 'Color', FIGS.LineColors(7, :));
%plot(min(gCvTRN')', 'LineWidth', 1, 'Color', FIGS.LineColors(7, :));
fill([1:length(gCvTRN) length(gCvTRN):-1:1], ...
     [min(gCvTRN') fliplr(max(gCvTRN'))], ...
	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
hold on
FIGS.plot4 = plot(mean(gCvTRN')', 'LineWidth', 1, 'Color', circshift(FIGS.LineColors(1,:),[0 -1]));
FIGS.plot5 = plot(mean(gCvVLD')', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
fill([1:length(gCvTRN) length(gCvTRN):-1:1], ...
     [max(gCvTRN') fliplr(max(gCvVLD'))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
fill([1:length(gCvTRN) length(gCvTRN):-1:1], ...
     [min(gCvVLD') fliplr(min(gCvTRN'))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
xlim([0 500])
xlabel('\fontsize{12}Iteration steps');
ylabel('\fontsize{12}Classification error  \itC_v\rm');
legend([FIGS.plot4 FIGS.plot5], 'Training data', 'Validation data');
%title('\fontsize{12}Variation of \itC_v\rm over 20 independent runs');
title('\fontsize{12}Neural network with 20 Gaussian nodes');

% mean(gCvTRN')'
% max(gCvTRN')'
% (min(gCvTRN')'

FIGS.fig11 = figure('Color', [1 1 1]);
%plot(max(gETRN'/(.7*size(DATA,1)))', 'LineWidth', 1, 'Color', FIGS.LineColors(7, :));
%plot(min(gETRN'/(.7*size(DATA,1)))', 'LineWidth', 1, 'Color', FIGS.LineColors(7, :));
fill([1:length(gETRN) length(gETRN):-1:1], ...
     [min(gETRN'/(.7*size(DATA,1))) fliplr(max(gETRN'/(.7*size(DATA,1))))], ...
	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
hold on
FIGS.plot6 = plot(mean(gETRN'/(.7*size(DATA,1)))', 'LineWidth', 1, 'Color', circshift(FIGS.LineColors(1,:),[0 -1]));
FIGS.plot7 = plot(mean(gEVLD'/(.3*size(DATA,1)))', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
fill([1:length(gETRN) length(gETRN):-1:1], ...
     [max(gETRN'/(.7*size(DATA,1))) fliplr(max(gEVLD'/(.3*size(DATA,1))))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
fill([1:length(gETRN) length(gETRN):-1:1], ...
     [min(gETRN'/(.7*size(DATA,1))) fliplr(min(gEVLD'/(.3*size(DATA,1))))], ...
	 FIGS.LineColors(6, :), 'EdgeColor', 'None');
xlabel('\fontsize{12}Iteration steps');
ylabel('\fontsize{12}Energy level of the system   \itH\rm');
xlim([-.01 1000])
legend([FIGS.plot6 FIGS.plot7], 'Training data', 'Validation data');
%title('\fontsize{12}Variation of energy over 20 independent runs');
title('\fontsize{12}Neural network with 20 Gaussian nodes');

% FIGS.fig12 = figure('Color', [1 1 1]);
% fill([1:length(gCvVLD) length(gCvVLD):-1:1], ...
%      [min(gCvVLD') fliplr(max(gCvVLD'))], ...
% 	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
% hold on
% plot(mean(gCvVLD')', 'LineWidth', 2, 'Color', FIGS.LineColors(2, :));
% xlim([0 500])
% xlabel('\fontsize{12}Steps');
% ylabel('\fontsize{12}Classification error  \itC_v\rm');
% 
% 
% FIGS.fig13 = figure('Color', [1 1 1]);
% 
% fill([1:length(gEVLD) length(gEVLD):-1:1], ...
%      [min(gEVLD'/(.3*size(DATA,1))) fliplr(max(gEVLD'/(.3*size(DATA,1))))], ...
% 	 FIGS.LineColors(7, :), 'EdgeColor', 'None');
% hold on
% plot(mean(gEVLD'/(.3*size(DATA,1)))', 'LineWidth', 1, 'Color', FIGS.LineColors(2, :));
% 
% xlabel('\fontsize{12}Steps');
% ylabel('\fontsize{12}Energy level of the system   \itH\rm');
% xlim([-.01 1000])