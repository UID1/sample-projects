#include <stdio.h>
#include <cuda_runtime.h>
#include <iostream>
#include <string>
#include <iomanip>
#include <time.h>
#include <windows.h>


/*
 * This example implements matrix element-wise addition on the host and GPU.
 * sumMatrixOnHost iterates over the rows and columns of each matrix, adding
 * elements from A and B together and storing the results in C. The current
 * offset in each matrix is stored using pointer arithmetic. sumMatrixOnGPU2D
 * implements the same logic, but using CUDA threads to process each matrix.
 */
 
/*
 **********          BEGIN: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

struct colors_t
{
   HANDLE hStdOut;
   /* 
      The HANDLE variable type is a handle to an object and is
      defined as 
      
      '#typedef PVOID HANDLE'. PVOID is a standard ordinary
      void pointer and is defined as
      
      '#typedef void *PVOID'
   */
   int initial_colors;
   
   colors_t()  /* The constructor of the object */
   {
      hStdOut        = GetStdHandle(STD_OUTPUT_HANDLE);
      initial_colors = getColors();
   }
   ~colors_t()
   {
      setColors(initial_colors);
   }
   int getColors() const
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
      GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.wAttributes;
   }
   void setColors(int color)
   {
      SetConsoleTextAttribute(hStdOut, color);
   }
   void setFg(int color)
   {
      int current_colors = getColors();
      setColors((color & 0x0F) | (current_colors & 0xF0));
   }
   void setBg(int color)
   {
      int current_colors = getColors();
      setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
   }
   int getFg() const { return getColors() & 0x0F; }
   int getBg() const { return (getColors() >> 4) & 0x0F; }
};

enum
{
   Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
   dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
 **********            END: Windows System Console ANSI color enabler module
 ******************************************************************************
*/

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}                                                                             \

void initialData(float *ip, const int size)
{
   for(int i=0; i < size;++i)
      ip[i] = (float)(rand() & 0xFF)/10.0f;
}
 
void sumMatrixOnHost(float *A, float *B, float *C, const int nx, const int ny)
{
   float *ia = A;
   float *ib = B;
   float *ic = C;
   
   for(int iy=0; iy<ny;++iy)
   {
      for(int ix=0; ix<nx; ++ix)
         ic[ix] = ia[ix]+ib[ix];
      ia += nx;
      ib += nx;
      ic += nx;
   }
   return;
}

void checkResult(float *hostRef, float *gpuRef, const int N)
{
   double epsilon = 1.0e-8;
   for(int i=0; i<N; ++i)
      if(abs(hostRef[i]-gpuRef[i]) > epsilon)
      {
         colors_t colors;
         colors.setFg(hGray);
         std::cout << "Host ";
         colors.setFg(dCyan);
         std::cout << hostRef[i];
         colors.setFg(hGray);
         std::cout << " GPU ";
         colors.setFg(dCyan);
         std::cout << gpuRef[i];
         colors.setFg(hGray);
         std::cout << ": ";
         colors.setFg(hRed);
         std::cout << "Arrays do not match!";
         colors.setFg(hGray);
         std::cout << std::endl << std::endl;
         colors.setColors(colors.initial_colors);
         break;
      }
}

/* Grid 2D, Block 2D */
__global__ void sumMatrixOnGPU2D(float *A, float *B, float *C, int NX, int NY)
{
   unsigned int ix  = blockIdx.x* blockDim.x + threadIdx.x;
   unsigned int iy  = blockIdx.y* blockDim.y + threadIdx.y;
   unsigned int idx = iy*NX+ix;
   if(ix<NX&&iy<NY)
      C[idx]=A[idx]+B[idx];
}

int main(int argc, char **argv)
{
   colors_t colors;
   /* Set up device */
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   CHECK(cudaSetDevice(dev));

   colors.setFg(hGray);
   std::cout << "Device "; colors.setFg(dCyan); std::cout << dev;
   colors.setFg(hGray); std::cout << ": "; colors.setFg(dYellow);
   std::cout << deviceProp.name << std::endl; colors.setFg(hGray);
   
   /* Set up datasize of matrix */
   int nx(1 << 14);
   int ny(1 << 14);
   
   int nxy = nx * ny;
   int nBytes = nxy * sizeof(float);
   
   /* Allocate memory on the host */
   float *h_A, *h_B, *hostRef, *gpuRef;
   h_A = (float *)malloc(nBytes);
   h_B = (float *)malloc(nBytes);
   hostRef = (float *)malloc(nBytes);
   gpuRef = (float *)malloc(nBytes);
   
   /* Initialize data at host side */
   double iStart = cpuSecond();
   initialData(h_A, nxy);
   initialData(h_B, nxy);
   double iElaps = cpuSecond() - iStart;
   colors.setFg(hGray);
   std::cout << "Data initialization completed in ";
   colors.setFg(dCyan);
   std::cout << iElaps;
   colors.setFg(hGray);
   std::cout << " seconds." << std::endl;
   
   
   
   memset(hostRef, 0, nBytes);
   memset(gpuRef, 0, nBytes);
   
   /* Add matrix at host side for result checks */
   iStart = cpuSecond();
   sumMatrixOnHost(h_A, h_B, hostRef, nx, ny);
   iElaps = cpuSecond() - iStart;
   colors.setFg(hGray);
   std::cout << "sumMatrixOnHost";
   colors.setFg(White);
   std::cout << "()";
   colors.setFg(hGray);
   std::cout << " completed in ";
   colors.setFg(dCyan);
   std::cout << static_cast<long>(1000*iElaps);
   colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   
   /* Allocate global memory on the CUDA device */
   float *d_MatA, *d_MatB, *d_MatC;
   CHECK(cudaMalloc((void **)&d_MatA, nBytes));
   CHECK(cudaMalloc((void **)&d_MatB, nBytes));
   CHECK(cudaMalloc((void **)&d_MatC, nBytes));
   
   /* Transfer data from the host to the device */
   CHECK(cudaMemcpy(d_MatA, h_A, nBytes, cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(d_MatB, h_B, nBytes, cudaMemcpyHostToDevice));
   
   /* Initialize launch parameters for GPU kernel */
   int dimx(32);
   int dimy(32);
   
   if(argc>2)
   {
      dimx = atoi(argv[1]);
      dimy = atoi(argv[2]);
   }
   
   dim3 block(dimx, dimy);
   dim3 grid((nx+block.x-1)/block.x, (ny + block.y-1)/block.y);
   
   /* Execute the kernel */
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   sumMatrixOnGPU2D <<<grid, block>>> (d_MatA, d_MatB, d_MatC, nx, ny);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond()-iStart;
   
   colors.setFg(hGray);
   std::cout << "sumMatrixOnGPU2D ";
   colors.setFg(hBlue);
   std::cout << "<<<";
   colors.setFg(White);
   std::cout << "(";
   colors.setFg(dCyan);
   std::cout << grid.x;
   colors.setFg(hGray);
   std::cout << ", ";
   colors.setFg(dCyan);
   std::cout << grid.y;
   colors.setFg(White);
   std::cout << ")";
   colors.setFg(hGray);
   std::cout << ", ";
   colors.setFg(White);
   std::cout << "(";
   colors.setFg(dCyan);
   std::cout << block.x;
   colors.setFg(hGray);
   std::cout << ", ";
   colors.setFg(dCyan);
   std::cout << block.y;
   colors.setFg(White);
   std::cout << ")";
   colors.setFg(hBlue);
   std::cout << ">>>";
   colors.setFg(hGray);
   std::cout << " completed in ";
   colors.setFg(dCyan);
   std::cout << static_cast<int>(1000*iElaps);
   colors.setFg(hGray);
   std::cout << " milliseconds." << std::endl;
   CHECK(cudaGetLastError());
   
   /* Copy the kernel output back to host memory */
   CHECK(cudaMemcpy(gpuRef, d_MatC, nBytes, cudaMemcpyDeviceToHost));
   
   /* Verify device results */
   checkResult(hostRef, gpuRef, nxy);
   
   /* Free memory on the CUDA device */
   CHECK(cudaFree(d_MatA));
   CHECK(cudaFree(d_MatB));
   CHECK(cudaFree(d_MatC));
   
   /* Free memory on the host */
   free(h_A);
   free(h_B);
   free(hostRef);
   free(gpuRef);
   
   /* Reset device */
   CHECK(cudaDeviceReset());
   
   /* Ensure that the terminal color config remains unmodified */
   colors.setColors(colors.initial_colors);
   
   return EXIT_SUCCESS;
   
}
