function temp = temp_proc(sz, mtd)
% SYNOPSIS
%
%  temp = TEMP_PROC(sz, mtd)
%  
% DESCRIPTION
% 
%  This function loads and returns  modified hourly temperature data for
%  Sweden during 2010. The source data is on observations every three
%  hours. Also, some values are missing and have to be interpolated.
%
% INPUT
%
%   sz   : the size of the vector of temperature data to be returned, it
%          is recommended to use 8760 to get hourly temperature data
%   mtd  : the method to use for interpolation which could be 'cubic',
%          'linear', 'nearest', 'pchip' or 'spline'. See 'doc interp1'
%          or 'help interp1' for more information about these methods.
% 
% OUTPUT
%
%   temp.national     : the interpolated national average temperature.
%                       The average is estimated with 75% weight on the
%                       major cities in the southern Sweden and 25%
%                       weight on the northern parts of Sweden.
%   temp.north_orig   : the original raw and uninterpolated measurement
%                       data taken every three hours in the northern
%                       regions of Sweden
%   temp.south_orig   : the original raw and uninterpolated measurement
%                       data taken every three hours in the major cities
%                       in southern Sweden
%   temp.times_orig   : The time points at which each measure has been
%                       taken. This time vector coincides with the raw
%                       temperature data.
%   temp.nth_names    : the of name the locations where each weather
%                       station has taken temperature measures. Each
%                       name is linked to each column in the raw temp-
%                       erature data matrices above. The 'nth' is to
%                       denote that these stations are located in
%                       northern Sweden.
%   temp.majcit_names : the name of the locations where each weather
%                       station has taken temperature measures which is
%                       in each major city in southern Sweden. Each
%                       name is linked to each column in the raw temp-
%                       erature data matrices above.
%
% NOTES
%
%  Designed for the final master thesis project in Physics to study
%  model estimation of electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
%
%

load tempdata.mat

temp.nth_names = tempdata.nth_names;
temp.majcit_names = tempdata.majcit_names;
temp.north_orig = tempdata.north;
temp.south_orig = tempdata.majcit;
temp.times_orig = tempdata.times;
temp.north = zeros(sz, length(tempdata.north(1,:)));
temp.south = zeros(sz, length(tempdata.majcit(1,:)));
switch mtd
   case 'cubic'
   case 'linear'
   case 'nearest'
   case 'pchip'
   case 'spline'
   otherwise
      warning(['No valid interpolation method specified: ' ...
                'Using default Piecewise Cubic Hermite Interpolation'])
end   

% We start by interpolating the missing values
basevector = (1:length(tempdata.majcit(:,1)))';
newbase = linspace(1, basevector(end), sz)';
for i = 1:length(tempdata.majcit(1,:))
    nan_idcs = find(tempdata.majcit(:,i) == -999);
    croptemp = tempdata.majcit(:,i);
    croppos = basevector;
    if ~isempty(nan_idcs)
       croptemp(nan_idcs) = [];
       croppos(nan_idcs) = [];
    end
    switch mtd
       case 'linear'
          temp.south(:,i) = interp1(croppos, croptemp, ...
                                     newbase, 'linear');
       case 'nearest'
          temp.south(:,i) = interp1(croppos, croptemp, ...
                                     newbase, 'nearest');
       case 'spline'
          temp.south(:,i) = interp1(croppos, croptemp, ...
                                     newbase, 'spline');
       otherwise
          pp1 = interp1(croppos, croptemp, 'cubic', 'pp');
          temp.south(:,i) = ppval(pp1, newbase);
    end
end
basevector = (1:length(tempdata.north(:,1)))';
newbase = linspace(1, basevector(end), sz)';
for i = 1:length(tempdata.north(1,:))
    nan_idcs = find(tempdata.north(:,i) == -999);
    croptemp = tempdata.north(:,i);
    croppos = basevector;
    if ~isempty(nan_idcs)
       croptemp(nan_idcs) = [];
       croppos(nan_idcs) = [];
    end
    switch mtd
       case 'linear'
          temp.north(:,i) = interp1(croppos, croptemp, ...
                                     newbase, 'linear');
       case 'nearest'
          temp.north(:,i) = interp1(croppos, croptemp, ...
                                     newbase, 'nearest');
       case 'spline'
          temp.north(:,i) = interp1(croppos, croptemp, ...
                                     newbase, 'spline');
       otherwise
          pp2 = interp1(croppos, croptemp, 'cubic', 'pp');
          temp.north(:,i) = ppval(pp2, newbase);
    end
end
% The population is bigger and considerably denser in  the south than in
% the north.  Sure,  there is a heavy electricity  consuming industry in
% the north but the major  part of the temperature  sensitivity consump-
% tion takes place where most  people live which is in  the major cities
% in the south of Sweden.

temp.national = .75*mean(temp.south')' + .25*mean(temp.north')';
end % temp_proc

% A quick  examination shows  that the difference  between the different
% interpolation methods are rather small.
