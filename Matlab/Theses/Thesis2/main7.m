% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% The  Matlab script  'main2.m' uses  the  internal  matlab package  and
% function ARMAX  to fit ARMA  models into the  electricity  price data.
% The model estimations are evaluated  using a subset of the information
% criteria  and prediction  error  estimators  introduced  in 'main1.m'.
% Trend and seasonalities are  evaluated and  filtered out of  the price
% data. The spikes in  the data are  also analyzed  and methods to over-
% come the hurdles that are caused by these spikes are investigated.
%
% 'main7.m' is a continuation  of main3.m,  main4.m and main6.m scripts.
%
% Backstory:
%
% We are unable  to yield  proper simulations  on the data  even when we
% try  to use the logarithm  on the data.  The spikes are extreme events
% and they need to be taken care of  in a different way.  In this matlab
% script  we are trying to fit the model  on a time period where we have
% no such extreme events  and see how much success  that can be achieved
% with that.

% Load data needed for computations

load eldata1.mat
load armaxdata003.mat

% In case we didn't to run main3.m
if ~any(strcmp(who,'tmp2010'))
   % Get the temperature data for year 2010
   % convert it to Kelvin and then take the logarithm.
   tmp2010 = temp_proc(8760, 'cubic');
   tmp2010l.national = log(272.15+tmp2010.national);
end

% We are going to pick the 90 day period with the lowest difference
% between the maximum and minimum electricity price.

deltaELmin = 1e7;
for k = 1:length(ELSPOTh2010)-90*24
	deltaEL = max(ELSPOTh2010(k:k+90*24)) - ...
	          min(ELSPOTh2010(k:k+90*24));
	if deltaEL < deltaELmin
		deltaELmin = deltaEL;
		kmin = k;
	end
end
plot(kmin+(0:90*24), ELSPOTh2010(kmin:kmin+90*24))
[m6a par6a] = armabat3(log(ELh2010_des1(kmin:kmin+90*24)), 2, 50,'seek2')
ELTEST2 = est_ma(ELh2010_des1(kmin:kmin+90*24), 500);
MD_dev = sqrt(var(ELSPOTh2010(kmin:kmin+90*24)-ELTREND1.removable(kmin:kmin+90*24)));
plot(MD_dev*armaxsim(m6a, tmp2010.national, 2161, 1, 0)+ELTEST2.removable)
hold on
plot(ELh2010_des1(kmin:kmin+90*24))

% The following gives a reasonable simulation of the series:
%
% [m6a par6a] = armabat3(log(ELh2010_des1(kmin:kmin+90*24)), ...
%                         1, 10,'seek2')
% plot(5.8*armaxsim(m6a, tmp2010.national, 2161, 1, 0) + ...
%        ELTEST2.removable)
% hold on
% plot(ELh2010_des1(kmin:kmin+90*24),'r')
% 
% The 5.8  comes from matching  the variance  of the simulation with the
% variance of the detrended and deseasonalized observation data for that
% period. If the order of q is too low the simulation will get resonant
% even for r = 1. For q = 50 r = 2 is not resonant.
%
% [m6a par6a] = armabat2(log(ELh2010_des1(kmin:kmin+90*24)), ...
%  log(500+tmp2010.national(kmin:kmin+90*24)), 1, 50, 3, 2, 'seek2')
%
% Also yields some interesting processes when simulating them and no
% variance correction appears to be needed,
% plot(armaxsim(m6a, tmp2010.national, 2161, 1, 0)+ELTEST2.removable)
% suffices.

% Let's find a suitable model that can be simulated

qvect = 1:3:60;
rvect = 1:3:60;
[m6a par6a] = armabat2(log(ELh2010_des1(kmin:kmin+90*24)), ...
                       log(500+tmp2010.national(kmin:kmin+90*24)), ...
                       1, qvect, rvect, 2, 'seek2')
[m6b par6b] = armabat3(log(ELh2010_des1(kmin:kmin+90*24)), ...
                       2, 10:70,'seek3')


% It seems  that this parameter space  is more capable  to estimate  the
% data.  A mesh  of the AIC  indicates  that  the  minimization  is more
% stable.  We still  have  corner solutions though.  We need to redo the
% deseasonalization process for this data  as some peaks have come back.


