#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>

typedef long long int64;

/*
**********    BEGIN: Microsoft Visual Studio pre-2015 c99 compliance hotfix
******************************************************************************
*/
#if defined(_MSC_VER) && _MSC_VER < 1900

#define snprintf c99_snprintf
#define vsnprintf c99_vsnprintf

inline int c99_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap)
{
    int count = -1;

    if (size != 0)
        count = _vsnprintf_s(outBuf, size, _TRUNCATE, format, ap);
    if (count == -1)
        count = _vscprintf(format, ap);

    return count;
}

inline int c99_snprintf(char *outBuf, size_t size, const char *format, ...)
{
    int count;
    va_list ap;

    va_start(ap, format);
    count = c99_vsnprintf(outBuf, size, format, ap);
    va_end(ap);

    return count;
}

#endif
/*
**********      END: Microsoft Visual Studio pre-2015 c99 compliance hotfix
******************************************************************************
*/

/*
**********          BEGIN: Windows System Console ANSI color enabler module
******************************************************************************
*/

struct colors_t
{
	HANDLE hStdOut;

	int initial_colors;

	colors_t()
	{
		hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
		initial_colors = getColors();
	}
	~colors_t()
	{
		setColors(initial_colors);
	}
	int getColors() const
	{
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(hStdOut, &csbi);
		return csbi.wAttributes;
	}
	void setColors(int color)
	{
		SetConsoleTextAttribute(hStdOut, color);
	}
	void setFg(int color)
	{
		int current_colors = getColors();
		setColors((color & 0x0F) | (current_colors & 0xF0));
	}
	void setBg(int color)
	{
		int current_colors = getColors();
		setColors(((color & 0x0F) << 4) | (current_colors & 0x0F));
	}
	int getFg() const { return getColors() & 0x0F; }
	int getBg() const { return (getColors() >> 4) & 0x0F; }
	COORD getCurPos()
	{
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(hStdOut, &csbi);
		return csbi.dwCursorPosition;
	}
	void setCurPos(int x, int y) {
		COORD curPos;
		curPos.X = x;
		curPos.Y = y;
		SetConsoleCursorPosition(hStdOut, curPos);
	}
   SMALL_RECT getConsoleSize()
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(hStdOut, &csbi);
      return csbi.srWindow;
   }
   void setConsoleSize(short width, short height)
   {
      CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(hStdOut, &csbi);
      SMALL_RECT sizeParam;
      sizeParam.Left = 0;
      sizeParam.Top = 0;
      sizeParam.Right = width-1;
      sizeParam.Bottom = height-1;
      SetConsoleWindowInfo(hStdOut, true, &sizeParam);
   }
   void setConsoleHeight(short height)
   {
   SMALL_RECT sizeParam;
   sizeParam = getConsoleSize();
   sizeParam.Top = 0;
   sizeParam.Bottom = height-1;
   SetConsoleWindowInfo(hStdOut, true, &sizeParam);
   }
};

enum
{
	Black, dBlue, dGreen, dCyan, dRed, dMagenta, dYellow, hGray,
	dGray, hBlue, hGreen, hCyan, hRed, hMagenta, hYellow, White
};

/*
**********            END: Windows System Console ANSI color enabler module
******************************************************************************
*/

/*
**********                            BEGIN: System time measurement module
******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS = 11644473600000000;

struct timezone2
{
	__int32  tz_minuteswest; /* minutes W of Greenwich */
	bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
	__int32    tv_sec;         /* seconds */
	__int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
	FILETIME ft;
	__int64 tmpres = 0;
	TIME_ZONE_INFORMATION tz_winapi;
	int rez = 0;

	ZeroMemory(&ft, sizeof(ft));
	ZeroMemory(&tz_winapi, sizeof(tz_winapi));

	GetSystemTimeAsFileTime(&ft);

	tmpres = ft.dwHighDateTime;
	tmpres <<= 32;
	tmpres |= ft.dwLowDateTime;

	/*converting file time to unix epoch*/
	tmpres /= 10;  /*convert into microseconds*/
	tmpres -= DELTA_EPOCH_IN_MICROSECS;
	tv->tv_sec = (__int32)(tmpres*0.000001);
	tv->tv_usec = (tmpres % 1000000);

	//_tzset(),don't work properly, so we use GetTimeZoneInformation
	rez = GetTimeZoneInformation(&tz_winapi);

	if (tz) /* Check for NULL pointer, AARGH!!!! */
	{
		tz->tz_dsttime = (rez == 2) ? true : false;
		tz->tz_minuteswest = tz_winapi.Bias + ((rez == 2) ? tz_winapi.DaylightBias : 0);
	}
	return 0;
}

double cpuSecond()
{
	struct timeval2 tp;
	gettimeofday(&tp, NULL);
	return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}
/*
**********                              END: System time measurement module
******************************************************************************
*/

/*
**********      BEGIN: ANSI Double Digest Solver Graphical Interface module
******************************************************************************
*/
enum
{
   deviceSetup, computeConfig, statusReport
};

struct scAnsiGUILine
{
   /* A default constructor is a constructor
      without input arguments */
   scAnsiGUILine() : isRepeater(false) {}
   scAnsiGUILine(std::initializer_list<int> iDims,
                 std::initializer_list<int> iData,
                 std::initializer_list<int> iColSwFg,
                 std::initializer_list<int> iColSwBg,
                 std::initializer_list<int> iColorFg,
                 std::initializer_list<int> iColorBg,
                 std::initializer_list<int> iRepeater,
                 bool iIsRepeater)
                 :
                 dims(iDims), data(iData), colSwFg(iColSwFg),
                 colSwBg(iColSwBg), colorFg(iColorFg),
                 colorBg(iColorBg), isRepeater(iIsRepeater),
                 repeater(iRepeater)
                 {}

   std::vector<int> dims;
   std::vector<int> data;
   std::vector<int> colSwFg;
   std::vector<int> colSwBg;
   std::vector<int> colorFg;
   std::vector<int> colorBg;
   bool isRepeater;
   std::vector<int> repeater;
};

std::vector<const scAnsiGUILine> ansiGUILayout;

class genericAnsiGui
{
   protected:
      COORD guiBegin, guiEnd;
      /* Prints the background of the GUI interface */
      void printGUIbgkLine();
      /* Prints a line of the GUI element by element */
      void printGUILine(const scAnsiGUILine guiLine);
      /* Refreshes a custom region of a line in the GUI, element by element.
         The cursor must be at column 0 before calling this function.
      */
      void refreshGUILine(const scAnsiGUILine guiLine,
            int startPos, int endPos, bool *modVec);
      /* Right adjusted overlay a custom position with a background
         agnostic string */
      void overlayString(const std::vector<int> dimVec, int *charVec, bool *modVec,
                         int colorMode);
      /* Left adjusted overlay a custom position with a background
         agnostic string */
      void overlayStringLeft(const std::vector<int> dimVec, int *charVec, int strLength,
                             bool *modVec, int colorMode);
   public:
      /* Set cursor position at the top of the GUI */
      void getToGUITop() {
         colors_t colors;
         colors.setCurPos(guiBegin.X, guiBegin.Y);
      }
      /* Set cursor position at bottom of the GUI */
      void getToGUIBottom() {
         colors_t colors;
         colors.setCurPos(guiEnd.X, guiEnd.Y);
      }
};
void genericAnsiGui::printGUIbgkLine()
{
   colors_t colors;
   for(int i = 0; i < 51; ++i) {
      if((i>41) && (i<51) && (colors.getBg() == Black))
         colors.setBg(dBlue);
      if((i == 51) && (colors.getBg() == dBlue))
         colors.setBg(Black);
      putc(' ', stdout);
   }
   colors.setFg(hGray); colors.setBg(Black); std::cout << std::endl;
}
void genericAnsiGui::printGUILine(const scAnsiGUILine guiLine)
{
   colors_t colors;
   if(guiLine.isRepeater) {
      int xPos(0), swFgCtr(0);
      for(int i = 0; i<guiLine.dims.at(1); ++i)
         for(int j = 0; j<guiLine.repeater.at(i); ++j) {
            if(swFgCtr < guiLine.dims.at(3))
               if(xPos == guiLine.colSwFg.at(swFgCtr)) {
                  colors.setFg(guiLine.colorFg.at(swFgCtr));
                  ++swFgCtr;
               }
            if((xPos>41) && (xPos<51) && (colors.getBg() == Black))
               colors.setBg(dBlue);
            if((xPos == 51) && (colors.getBg() == dBlue))
               colors.setBg(Black);
            putc(guiLine.data.at(i % guiLine.dims.at(2)), stdout);
            ++xPos;
         }
   } else {
      int swFgCtr(0);
      int swBgCtr(0);
      for(int i = 0; i < guiLine.dims.at(0); ++i) {
         if(guiLine.colSwFg.size() != 0)
            if(swFgCtr < guiLine.dims.at(1))
               if((i == guiLine.colSwFg.at(swFgCtr)) ) {
                  colors.setFg(guiLine.colorFg.at(swFgCtr));
                  ++swFgCtr;
               }
         if(guiLine.colSwBg.size() != 0)
            if(swBgCtr < guiLine.dims.at(2))
               if(i == guiLine.colSwBg.at(swBgCtr)) {
                  colors.setBg(guiLine.colorBg.at(swBgCtr));
                  ++swBgCtr;
               }
         if((i>41) && (i<51) && (colors.getBg() == Black))
            colors.setBg(dBlue);
         if((i == 51) && (colors.getBg() == dBlue))
            colors.setBg(Black);
         putc(guiLine.data.at(i), stdout);
      }
   }
   colors.setFg(hGray); colors.setBg(Black); std::cout << std::endl;
}
void genericAnsiGui::refreshGUILine(const scAnsiGUILine guiLine,
                                    int startPos, int endPos, bool *modVec)
{
   colors_t colors;
   if(guiLine.isRepeater) {
      int xPos(0), swFgCtr(0);
      for(int i = 0; i<guiLine.dims.at(1); ++i)
         for(int j = 0; j<guiLine.repeater.at(i); ++j) {
            if(swFgCtr < guiLine.dims.at(3))
               if(xPos == guiLine.colSwFg.at(swFgCtr)) {
                  colors.setFg(guiLine.colorFg.at(swFgCtr));
                  ++swFgCtr;
               }
            if((xPos>41) && (xPos<51) && (colors.getBg() == Black))
               colors.setBg(dBlue);
            if((xPos == 51) && (colors.getBg() == dBlue))
               colors.setBg(Black);
            if((xPos >= startPos)&&(xPos <= endPos))
            {
               putc(guiLine.data.at(i % guiLine.dims.at(2)), stdout);
               modVec[xPos] = false;
            }
            ++xPos;
         }
   } else {
      int swFgCtr(0);
      int swBgCtr(0);
      for(int i = 0; i < guiLine.dims.at(0); ++i) {
         if(guiLine.colSwFg.size() != 0)
            if(swFgCtr < guiLine.dims.at(1))
               if((i == guiLine.colSwFg.at(swFgCtr)) ) {
                  colors.setFg(guiLine.colorFg.at(swFgCtr));
                  ++swFgCtr;
               }
         if(guiLine.colSwBg.size() != 0)
            if(swBgCtr < guiLine.dims.at(2))
               if(i == guiLine.colSwBg.at(swBgCtr)) {
                  colors.setBg(guiLine.colorBg.at(swBgCtr));
                  ++swBgCtr;
               }
         if((i>41) && (i<51) && (colors.getBg() == Black))
            colors.setBg(dBlue);
         if((i == 51) && (colors.getBg() == dBlue))
            colors.setBg(Black);
         if((i >= startPos)&&(i <= endPos))
         {
            putc(guiLine.data.at(i), stdout);
            modVec[i] = false;
         }
      }
   }
}
void genericAnsiGui::overlayString(const std::vector<int> dimVec, int *charVec,
                                   bool *modVec, int colorMode)
{
   colors_t colors;
   COORD curPos(colors.getCurPos());
   for (int i = curPos.X; i<dimVec.at(0); ++i)
   {
      if(colorMode == deviceSetup)
         if(rand() % 2)
            colors.setFg(hGreen);
         else
            colors.setFg(dGreen);
      if(colorMode == computeConfig)
         if(rand() % 2)
            colors.setFg(hMagenta);
         else
            colors.setFg(dMagenta);
      if(colorMode == statusReport)
         if(rand() % 2)
            colors.setFg(hCyan);
         else
            colors.setFg(dCyan);
      if((i>41) && (i<51) && (colors.getBg() == Black))
         colors.setBg(dBlue);
      if((i == 51) && (colors.getBg() == dBlue))
         colors.setBg(Black);   
      putc(charVec[i-curPos.X], stdout);
      modVec[i] = true;
   }
}
void genericAnsiGui::overlayStringLeft(const std::vector<int> dimVec, int *charVec,
                           int strLength, bool *modVec, int colorMode)
{
   colors_t colors;
   COORD curPos(colors.getCurPos());
   for (int i = 0; i<strLength; ++i)
   {
      if(colorMode == deviceSetup)
         if(rand() % 2)
            colors.setFg(hGreen);
         else
            colors.setFg(dGreen);
      if(colorMode == computeConfig)
         if(rand() % 2)
            colors.setFg(hMagenta);
         else
            colors.setFg(dMagenta);
      if(colorMode == statusReport)
         if(rand() % 2)
            colors.setFg(hCyan);
         else
            colors.setFg(dCyan);
      if((curPos.X+i>41) && (curPos.X+i<51) && (colors.getBg() == Black))
         colors.setBg(dBlue);
      if((curPos.X+i == 51) && (colors.getBg() == dBlue))
         colors.setBg(Black);   
      putc(charVec[i], stdout);
      modVec[curPos.X+i] = true;
   }
}
void initializeGUI()
{
   scAnsiGUILine newLine, nullLine;
   
   newLine.isRepeater = true;
   newLine.dims       = { 60, 20, 2, 14 }; // Line 1
   newLine.repeater   = { 3, 2, 1, 1, 1, 5, 1, 19, 1, 9, 1, 4, 1, 1, 1, 1, 1, 3,
                          2, 1 };
   newLine.data       = {' ', 196 }; // repeater[i], charL1[i mod 2];
   newLine.colSwFg    = {  0, 11, 12, 19, 20, 23, 24, 25, 29, 34, 36, 42, 46,
                          54, 0 };
   newLine.colorFg    = { White, dGray, White, hGray, White, hGray, dGray, White,
                          hGray, White, hGray, White, hGray, White };
   ansiGUILayout.emplace_back(newLine);
   newLine = nullLine;
   newLine.dims       = { 62, 11, 7 }; // Line 2
   newLine.data       = { ' ', ' ', 220, ' ', 223, 220, ' ', 223, 220, 223, 220, 220,
                          219, 220, 219, 219, 223, 219, 176, 177, 178, 219, 177, 219,
                          178, 177, 176, ' ', ' ', 'D', 'D', 'I', 'G', 'E', 'S', 'T',
                          ' ', ' ', 176, 177, 178, 219, 177, 219, 178, 177, 176, 219,
                          219, 219, 223, 219, 219, 220, 219, 219, 220, 220, 220, 223,
                          ' ', 223, };
   newLine.colSwFg    = { 0, 18, 23, 29, 30, 31, 32, 34, 38, 42, 47, 0 };
   newLine.colSwBg    = { 0, 18, 22, 23, 42, 44, 47, 0 };
   newLine.colorFg    = { White, hGray, dGray, dYellow, hYellow, White, hYellow,
                          dYellow, dGray, hGray, White };
   newLine.colorBg    = { Black, White, dGray, Black, dGray, White, Black };
   ansiGUILayout.emplace_back(newLine);
   newLine = nullLine;
   
   //Line 1
   newLine.dims       = { 72, 9, 11 };
   newLine.data       = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', 220, 222, 222, 178, 178, 
                          178, 178, 178, 178, 178, 178, 223, 223, 220, ' ', ' ', ' ',
                          ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
                          ' ', ' ', ' ', ' ', ' ', 219, 219, 219, 177, 177, 177, 177, 
                          177, 177, 177, 177, 177, 177, 219, ' ', ' ', ' ', 178, 178, 
                          178, 178, 178, 178, 178, 178, 178, 178, 178, 178, 178, 178
                         };
   newLine.colSwFg    = { 0, 7, 8, 9, 10, 41, 44, 54, 72, 0 };
   newLine.colorFg    = { hGray, dGray, hRed, hGreen, dGray, White, hCyan, hGray };
   newLine.colSwBg    = { 0, 8, 9, 10, 18, 24, 38, 44, 55, 58, 72, 0 };
   newLine.colorBg    = { Black, hYellow, hCyan, dRed, Black, dBlue, Black, dCyan, Black, 
                          dBlue, Black };
   ansiGUILayout.emplace_back(newLine);
   newLine = nullLine;
   //Line 2
   newLine.dims       = { 72, 21, 17 }; 
   newLine.data       = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', 178, 221, 221, 178, 178, 
                          178, 178, 178, 178, 178, 178, 220, 220, 177, ' ', ' ', ' ',
                          ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 220, 219, 219, 219,
                          ' ', ' ', ' ', ' ', ' ', 219, 219, 220, 220, 219, 219, 219,
                          177, 177, 177, 177, 219, 219, 219, ' ', ' ', ' ', 178, 178,
                          178, 219, 178, 178, 178, 178, 178, 178, 178, 178, 178, 178
                        };
   newLine.colSwFg    = { 0, 7, 8, 9, 10, 18, 20, 32, 41, 42, 43, 45, 46, 47, 48, 52,
                          53, 58, 61, 63, 72, 0 };
   newLine.colorFg    = { hGray, dGray, hRed, hGreen, dGray, dGray, dYellow, White, 
                          hCyan, dCyan, White, hCyan, dCyan, hCyan, White, hCyan,
                          dCyan, hCyan, White, hCyan, hGray };
   newLine.colSwBg    = { 0, 7, 8, 9, 10, 18, 20, 21, 24, 38, 43, 48, 55, 58, 62, 63,
                          73, 0 };
   newLine.colorBg    = { Black, dYellow, hYellow, hCyan, dRed, Black, dGray, Black,
                          dBlue, Black, hCyan, dCyan, Black, hBlue, hCyan, hBlue,
                          Black };
   ansiGUILayout.emplace_back(newLine);
   newLine = nullLine;
   //Line 3
   /*
   newLine.dims       = { 
   newLine.data       = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
   newLine.colSwFg    = { 0, 
   newLine.colorFg    = { hGray, 
   newLine.colSwBg    = { 0, 
   newLine.colorBg    = { Black, 
   */
}
enum ansiGUI
{
   devLine = 6, threadLine = 10, gridLine, blockLine, iterLine = 16,
   compuLine, zeroLine, energyLine, enzALine = 23, enzBLine = 25
};
class doubleDigestGUI : public genericAnsiGui
{
   private:
      //COORD guiBegin, guiEnd;
      bool devInfoMod[60], threadsMod[60], blockDimsMod[60];
      bool gridDimsMod[60], iterationsMod[60], speedMod[60];
      bool zeroesMod[60], energyMod[60], enzymesMod0[60];
      bool enzymesMod1[60], enzymesMod2[60], enzymesMod3[60];

   public:
      doubleDigestGUI();
      void buildGUI();
      void updateGUIElement(int strLen, int begScan, int guiOffset,
                            char *overlayArray, bool *modVec,
                            const scAnsiGUILine guiLine, int colorMode);
      void updateGUIEnz(int strLen, int begScan, int guiOffset,
                            char *overlayArray, bool *modVec,
                            const scAnsiGUILine guiLine, int colorMode);
      void setDeviceInfo(const char *devInfo, int numChar);
      void setDeviceNum(int num);
      void setNumThreads(int numThreads);
      char *dim3ToStr(dim3 cudaDim, int &strLength, int maxLen);
      void setBlockDim(dim3 blockDims);
      void setGridDim(dim3 gridDims);
      void updateIterations(int64 numIterations);
      void updateSpeed(double iterationalSpeed);
      void updateZeroes(int numZeroes);
      void updateEnergy(float optimalEnergy);
      void updateEnzyme(const int *vEnz, const int iSize, int switcher);
      void updateEnzymes(const int *vEnzA, const int iSizeA,
                         const int *vEnzB, const int iSizeB);
};

doubleDigestGUI::doubleDigestGUI()
{
   if(ansiGUILayout.size() == 0)
      initializeGUI();
   buildGUI();
}
void doubleDigestGUI::buildGUI()
{
   printGUIbgkLine();
   printGUIbgkLine();
   for(int i = 0; i < ansiGUILayout.size(); ++i)
      printGUILine(ansiGUILayout.at(i));
   printGUIbgkLine();
   printGUIbgkLine();
  
   colors_t colors;
   guiBegin = colors.getCurPos();
   guiEnd = guiBegin;
   guiBegin.Y -= ansiGUILayout.size()+4;
   for (int i = 0; i<60; ++i)
   {
         devInfoMod    [i] = false;
         threadsMod    [i] = false;
         blockDimsMod  [i] = false;
         gridDimsMod   [i] = false;
         iterationsMod [i] = false;
         speedMod      [i] = false;
         zeroesMod     [i] = false;
         energyMod     [i] = false;
         enzymesMod0   [i] = false;
         enzymesMod1   [i] = false;
         enzymesMod2   [i] = false;
         enzymesMod3   [i] = false;
   }
}
/* Optimization suggestions; updateGUIElement, and updateGUIEnz could
   be merged into one function using conditional code. setDeviceInfo()
   could be set up to use updateGUIElement instead of calling overlay()
   and refreshGUI() directly.
*/
void doubleDigestGUI::updateGUIElement(int strLen, int begScan,
                            int guiOffset, char *overlayArray, bool *modVec,
                            const scAnsiGUILine guiLine, int colorMode)
{
   colors_t colors;
   int numChar(0);
   for (int i = 0; i < strLen; ++i)
      if(overlayArray[i] ==0) {
         numChar = i;
         break;
      }
   int beginUpdStrPos(guiLine.dims.at(0)-numChar-2);
   int *strArray = new int[1+numChar];
   strArray[0] = ' ';
   for(int i = 1; i <= numChar; ++i)
      strArray[i] = static_cast<int>(overlayArray[i-1]);
   int modPos(guiLine.dims.at(0));
   for(int i = begScan; i < guiLine.dims.at(0); ++i)
      if(modVec[i]) {
         modPos = i;
         break;
      }
   if(modPos < beginUpdStrPos) {
      colors.setCurPos(modPos, guiBegin.Y+guiOffset);
      refreshGUILine(guiLine, modPos, beginUpdStrPos, modVec);
   }
   else
      colors.setCurPos(beginUpdStrPos+1, guiBegin.Y+guiOffset);
   overlayString(guiLine.dims, strArray, modVec, colorMode);
   
   delete[] strArray;
}
void doubleDigestGUI::updateGUIEnz(int strLen, int begScan, int guiOffset,
                            char *overlayArray, bool *modVec,
                            const scAnsiGUILine guiLine, int colorMode)
{
   colors_t colors;
   
   int numChar(0);
   for (int i = 0; i < strLen; ++i)
      if(overlayArray[i] == 0) {
         numChar = i;
         break;
      }
   int beginUpdStrPos(12); // This is different from updateGUIElement
   int *strArray = new int[1+numChar];
   strArray[0] = ' ';
   for(int i = 1; i <= numChar; ++i)
      strArray[i] = static_cast<int>(overlayArray[i-1]);
   int modPosHigh(0), modPosLow(guiLine.dims.at(0));
   for(int i = (guiLine.dims.at(0)-1); i > beginUpdStrPos; --i)
      if(modVec[i]) {
         modPosHigh = i;
         break;
      }
   for(int i = beginUpdStrPos;i<guiLine.dims.at(0);++i)
      if(modVec[i]) {
         modPosLow = i;
         break;
      }
   //printf("ModL %d ModH %d", modPosLow, modPosHigh);
   if(modPosHigh > modPosLow) {
      colors.setCurPos(modPosLow, guiBegin.Y+guiOffset);
      refreshGUILine(guiLine, modPosLow, modPosHigh, modVec);
   }

   colors.setCurPos(beginUpdStrPos, guiBegin.Y+guiOffset);
   overlayStringLeft(guiLine.dims, strArray, numChar+1, modVec, colorMode);
   
   delete[] strArray;
}
void doubleDigestGUI::setDeviceInfo(const char *devInfo, int numChar)
{
/* This is how this function should be called
int strSize1(0);
for(int i = 0; i < 256; ++i)
   if(deviceProp.name[i] == 0) {
      strSize1 = i;
      break;
   }
   myGUI.setDeviceInfo(deviceProp.name, strSize1);
*/
   colors_t colors;
   int beginUpdStrPos(ansiGUILayout[4].dims.at(0)-numChar-1);
   int *strArray = new int[1+numChar];
   strArray[0] = ' ';
   for(int i = 1; i <= numChar; ++i)
      strArray[i] = static_cast<int>(devInfo[i-1]);
   int modPos(0);
   for(int i = 23; i < ansiGUILayout[4].dims.at(0); ++i)
      if(devInfoMod[i]) {
         modPos = i;
         break;
      }
   if(modPos < beginUpdStrPos) {
      colors.setCurPos(modPos, guiBegin.Y+6);
      refreshGUILine(ansiGUILayout[4], modPos, beginUpdStrPos,
                     devInfoMod);
   }
   else
      colors.setCurPos(beginUpdStrPos, guiBegin.Y+6);
   overlayString(ansiGUILayout[4].dims, strArray, devInfoMod, deviceSetup);
   
   delete[] strArray;
}
void doubleDigestGUI::setDeviceNum(int num)
{
   colors_t colors;
   colors.setCurPos(18, guiBegin.Y+6);
   char devNum[2];
   itoa(num, devNum, 16);
   colors.setFg(hMagenta);
   putc(devNum[0], stdout);
}
void doubleDigestGUI::setNumThreads(int numThreads)
{
   /*INT_MAX = 2147483647 => maxwidth of int = 10
     INT64_MAX = 9223372036854775807 => maxwidth of int64 = 19*/
   //colors_t colors;
   char numThrString[11];
   itoa(numThreads, numThrString, 10);
   updateGUIElement(11, 31, 10, numThrString, threadsMod,
                    ansiGUILayout[8], computeConfig);
}
char *doubleDigestGUI::dim3ToStr(dim3 cudaDim, int &strLength, int maxLen)
{
   char delimStr[4] = {' ', -98, ' '};
   char *gdDimX = new char[maxLen+1];
   char *gdDimY = new char[maxLen+1];
   char *gdDimZ = new char[maxLen+1];
   int strLenX(0), strLenY(0), strLenZ(0);
   itoa(cudaDim.x, gdDimX, 10);
   itoa(cudaDim.y, gdDimY, 10);
   itoa(cudaDim.z, gdDimZ, 10);
   for(int i = 1; i <= maxLen; ++i)
      if(gdDimX[i] == 0) {
         strLenX = i;
         break;
      }
   for(int i = 1; i <= maxLen; ++i)
      if(gdDimY[i] == 0) {
         strLenY = i;
         break;
      }
   for(int i = 1; i <= maxLen; ++i)
      if(gdDimZ[i] == 0) {
         strLenZ = i;
         break;
      }
   char *gridString = new char[strLenX+strLenY+strLenZ+4];
   int gdStrPos(0);
   for(int i=0;i<strLenX; ++i)
      gridString[i+gdStrPos] = gdDimX[i];
   gdStrPos += strLenX;
   for(int i = 0; i<3; ++i)
      gridString[i+gdStrPos] = delimStr[i];
   gdStrPos += 3;
   for(int i=0;i<strLenY; ++i)
      gridString[i+gdStrPos] = gdDimY[i];
   gdStrPos += strLenY;
   for(int i = 0; i<3; ++i)
      gridString[i+gdStrPos] = delimStr[i];
   gdStrPos += 3;
   for(int i=0;i<strLenZ; ++i)
      gridString[i+gdStrPos] = gdDimZ[i];
   gdStrPos += strLenZ;
   gridString[gdStrPos] = 0;
   strLength = gdStrPos;
   
   delete[] gdDimX, gdDimY, gdDimZ;
   return gridString;
}
void doubleDigestGUI::setGridDim(dim3 gridDims)
{
   /* dim3 struct of unsigned ints; .x, .y, .z */
   // ASCII 158 is 'x' maximum value of dim3 65536 =>
   int maxLen = 5;
   int gdStrPos(0);
   char *gridString;
   gridString = dim3ToStr(gridDims, gdStrPos, maxLen);
   updateGUIElement(gdStrPos+1, 31, gridLine, gridString, gridDimsMod,
                    ansiGUILayout[9], computeConfig);
}
void doubleDigestGUI::setBlockDim(dim3 blockDims)
{
   /* dim3 struct of unsigned ints; .x, .y, .z */
   // ASCII 158 is 'x' 65536
   int maxLen = 5;
   int gdStrPos(0);
   char *blockString;
   blockString = dim3ToStr(blockDims, gdStrPos, maxLen);
   //printf("%s %d", blockString, gdStrPos);
   updateGUIElement(gdStrPos+1, 31, blockLine, blockString, blockDimsMod,
                     ansiGUILayout[10], computeConfig);
}
void doubleDigestGUI::updateIterations(int64 numIterations)
{
   int maxLen = 20;
   //char numThrString[20];
   std::stringstream convertStream;
   convertStream << numIterations;
   std::string convertString(convertStream.str());
   char *numThrString = new char[convertString.size()+1];
   numThrString[convertString.size()]=0;
   memcpy(numThrString, convertString.c_str(), convertString.size());
   //itoa(numIterations, numThrString, 10); // This can only handle int, not int64
   updateGUIElement(maxLen, 35, iterLine, numThrString, iterationsMod,
                    ansiGUILayout[14], statusReport);
   delete[] numThrString;
}
void doubleDigestGUI::updateSpeed(double iterationalSpeed)
{
   std::stringstream convertStream;
   convertStream << iterationalSpeed;
   std::string convertString(convertStream.str());
   char *numThrString = new char[convertString.size()+1];
   numThrString[convertString.size()]=0;
   memcpy(numThrString, convertString.c_str(), convertString.size());
   updateGUIElement(convertString.size()+1, 35, compuLine, numThrString, speedMod,
                    ansiGUILayout[15], statusReport);
}
void doubleDigestGUI::updateZeroes(int numZeroes)
{
   char numString[11];
   itoa(numZeroes, numString, 10);
   updateGUIElement(11, 35, zeroLine, numString, zeroesMod,
                    ansiGUILayout[16], statusReport);
}
void doubleDigestGUI::updateEnergy(float optimalEnergy)
{
   char numString[11];
   snprintf(numString, 11, "%g", optimalEnergy);
   updateGUIElement(11, 35, energyLine, numString, energyMod,
                    ansiGUILayout[17], statusReport);
}
void doubleDigestGUI::updateEnzyme(const int *vEnz, const int iSize,
                                    int switcher)
{
   char **stringArray = new char*[iSize];
   int *strLengths = new int[iSize];
   int bufSize = 12;
   for(int i = 0; i < iSize; ++i) {
      stringArray[i] = new char[bufSize];
      itoa(vEnz[i], stringArray[i], 10);
      for(int j=0; j < bufSize; ++j)
         if(stringArray[i][j] == 0) {
            stringArray[i][j] = ' ';
            stringArray[i][j+1] = 0;
            strLengths[i] = j+1;
            break;
         }
   }
   int lengthChecker1(12), lengthChecker2(12);
   int numelL1(0), numelL2(0);
   for (int i = 0; i < iSize; ++i)
      if((lengthChecker1+strLengths[i]) <= ansiGUILayout[21].dims.at(0)) {
         lengthChecker1 += strLengths[i];
         numelL1 = i+1;
      } else {
         numelL1 = i;
         break;
      }
   char *lineArray1 = new char[lengthChecker1+1];
   int lineArrPos(0);
   for(int i = 0; i < numelL1; ++i) {
      //std::copy(stringArray[i][0], stringArray[i][0]+strLengths[i],
      //     lineArray1 + lineArrPos);
      memcpy(lineArray1 + lineArrPos*sizeof(char), stringArray[i], 
             strLengths[i]*sizeof(char));
      lineArrPos += strLengths[i];
   }
   lineArray1[lineArrPos] = 0;
   if(switcher == 0)
      updateGUIEnz(lengthChecker1+1, 12, enzALine, lineArray1, enzymesMod0,
                   ansiGUILayout[21], statusReport);
   else if(switcher == 1)
      updateGUIEnz(lengthChecker1+1, 12, enzBLine, lineArray1, enzymesMod2,
                   ansiGUILayout[23], statusReport);
   for (int i = numelL1; i < iSize; ++i)
      if((lengthChecker2+strLengths[i]) <= ansiGUILayout[22].dims.at(0)) {
         lengthChecker2 += strLengths[i];
         numelL2 = i-numelL1+1;
      } else {
         numelL2 = i - numelL1;
         break;
      }
   char *lineArray2 = new char[lengthChecker2+1];
   lineArrPos = 0;
   for(int i = numelL1; i < (numelL1+numelL2); ++i) {
      //std::copy(stringArrayA[i][0], stringArrayA[i][0]+strLengths[i],
      //   lineArray2 + lineArrPos);
      memcpy(lineArray2+lineArrPos*sizeof(char), stringArray[i],
             strLengths[i]*sizeof(char));
      lineArrPos += strLengths[i];
   }
   lineArray2[lineArrPos] = 0;
   if(switcher == 0)
      updateGUIEnz(lengthChecker2+1, 12, enzALine+1, lineArray2, enzymesMod1,
                   ansiGUILayout[22], statusReport);
   else if(switcher == 1)
      updateGUIEnz(lengthChecker2+1, 12, enzBLine+1, lineArray2, enzymesMod3,
                   ansiGUILayout[24], statusReport);

   for(int i = 0; i < iSize; ++i)
      delete[] stringArray[i];
   delete[] stringArray, strLengths;
   delete[] lineArray1, lineArray2;
}
void doubleDigestGUI::updateEnzymes(const int *vEnzA, const int iSizeA,
                                    const int *vEnzB, const int iSizeB)
{
   updateEnzyme(vEnzA, iSizeA, 0);
   updateEnzyme(vEnzB, iSizeB, 1);
}

/*
**********        END: ANSI Double Digest Solver Graphical Interface module
******************************************************************************
*/

int main(int argc, char** argv)
{


   srand(cpuSecond());
   char someStr[13] = "Internal CPU";
   std::string someStr2("Internal CPU");
   dim3 testVar1(16, 32, 64);
   dim3 testVar2(128, 256, 512);
   const int enzymeA1Size(6), enzymeB1Size(6);
   const int enzymeA2Size(18), enzymeB2Size(11);
 	const int A1[enzymeA1Size] = { 8479, 4868, 3696, 2646, 169, 142 };
   const int B1[enzymeB1Size] = { 11968, 5026, 1081, 1050, 691, 184 };
   const int A2[enzymeA2Size] = { 9979, 9348, 8022, 4020, 2693,
                                  1892, 1714, 1371, 510, 451,
                                  55536, 8848, 12345, 111, 12441,
                                  333, 123, 34677 };
   const int B2[enzymeB2Size] = { 9492, 8453, 7749, 7365, 2292,
                                  2180, 1023, 959, 278, 124, 85 };

   doubleDigestGUI myGUI;
   for (int i = 128; i<255; ++i)
      putc(i, stdout);
   //setlocale(LC_ALL, "");
   //binmode(STDOUT, ":unix");
   
   HANDLE hStdOut;
   hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
   std::cout << std::endl;
   const int testArray1[3] = { 0x258C, 0x0, 0x0 };
   WriteConsoleW(hStdOut, testArray1, 1, NULL, NULL);
   putc(219, stdout);
/* const int leftbar [2] = { 0x258C, 0x0 };
   const int rightbar[2] = { 0x2590, 0x0 };
   if(char == 221) WriteConsoleW(hStdOut, leftbar, 1, NULL, NULL);
   if(char == 222) WriteConsoleW(hStdOut, rightbar, 1, NULL, NULL);
   else putc(char, stdout);
*/

   return 0;
}

















