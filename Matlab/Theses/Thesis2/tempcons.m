% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% This file analyzes the correlation between electricity consumption
% and outdoor temperature

if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
tmpdaily = mean(reshape(tmp2010.national, 24, 365));
if ~any(strcmp(who,'ELSPOTd2010'))
   load ELSPOTd2010.mat;
end
load ELCONSd2010.mat
load ELCONSh2010.mat

% Eliminate the zero-outlier
zero_el = find(ELCONSh2010.no == 0);
ELCONSh2010.no(zero_el) = (ELCONSh2010.no(zero_el-1) + ...
                            ELCONSh2010.no(zero_el+1))/2;

%
FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

FIGS.fig1 = figure('Color',[1 1 1]);
TMPSh = 20 - tmp2010.national;
CNSh = ELCONSh2010.no;
plot(TMPSh, CNSh/1000, '.', 'MarkerFaceColor', FIGS.LineColors(1,:), ...
     'MarkerEdgeColor', FIGS.LineColors(1,:))
title(['\fontsize{12}Consumption observed by the ' ...
       'hour vs outdoor temperature'])
%
XXh = [ones(length(TMPSh),1) TMPSh];
ELcfh = XXh'*XXh\XXh'*CNSh;
hold on
Xlh =  linspace(0, max(TMPSh)+1, 3);
plot(Xlh, (ELcfh(2)*Xlh + ELcfh(1))/1000, 'r', ...
     'LineWidth', 2.5, 'Color', [153 0 53]/255)
xlabel(['\fontsize{12}\Delta\itt\rm ' ...
                   '(\itt\rm\fontsize{6}indoor\fontsize{12} - \itt' ...
		   '\rm\fontsize{6}outdoor\fontsize{12})'])
ylabel('GW')
xlim([0 max(TMPSh)+1])

FIGS.fig2 = figure('Color',[1 1 1]);
TMPSd = 20-tmpdaily';
CNSd = ELCONSd2010.no;
plot(TMPSd, CNSd/1000, '.', 'MarkerFaceColor', FIGS.LineColors(1,:), ...
     'MarkerEdgeColor', FIGS.LineColors(1,:))
title(['\fontsize{12}Consumption observed by the ' ...
       'day vs outdoor temperature'])
hold on
XXd = [ones(length(TMPSd),1) TMPSd];
ELcfd = XXd'*XXd\XXd'*CNSd;
Xld =  linspace(0, max(TMPSd)+1, 3);
plot(Xld, (ELcfd(2)*Xld + ELcfd(1))/1000, 'r', ...
     'LineWidth', 2, 'Color', [153 0 53]/255)
xlabel(['\fontsize{12}\Delta\itt\rm ' ...
                   '(\itt\rm\fontsize{6}indoor\fontsize{12} - \itt' ...
		   '\rm\fontsize{6}outdoor\fontsize{12})'])
ylabel('GWh/day')
xlim([0 max(TMPSd)+1])
