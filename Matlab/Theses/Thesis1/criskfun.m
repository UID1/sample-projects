%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% April, 2011
%

function Q_model = criskfun(T, beta_CIR)

% Set up the number of time observations and the number of trajectories to
% simulate in the estimations

precision = 50;
iterations = 250;

% We define the market hazard function as an exponentially decreasing function

gamma_h = .375; %mean(mean(rand(10))); % Just some arbitrary value for gamma
Q_market_fun = @(t, gamma) (exp(-gamma.*t));

% The CIR-based intensity model has a closed form expression for the
% default probability which can be found in Alexander's Lecture notes
% from lecture 3 in his Credit risk modelling course (slide 32; eq 33-36) 

gamma_CIR = @(alpha, sigma)(sqrt(alpha.^2+2.*sigma.^2));
CIR_A = @(T, gamma, alpha, mu, nu)(log(2.*gamma.*exp((gamma+alpha).*T.*.5)./((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma)).*2.*alpha.*mu./nu.^2);
CIR_B = @(T, gamma, alpha)(2.*(exp(gamma.*T)-1)./ ((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma));

P_CIR = @(t, kappa, nu, mu, y_0)(exp(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)-CIR_B(t, gamma_CIR(kappa, nu), kappa).*y_0));

% From this we can shape the Psi function as in expression (3.6) in the paper (Brigo,
% Pallavicini 2008). As can be seen Psi has a closed form expression.

Psi = @(t, beta, gamma_h)(log(P_CIR(t, beta(2), beta(4), beta(3), beta(1))./Q_market_fun(t, gamma_h)));

Q_mod = zeros(1, length(T));

for j = 1:length(T)

    % In the next step we work with a CIR process that should be integrated.
    % First we decide upon a set of observation times over which this process
    % is observed and define a vector of time increments dt

    t_CIR = linspace(0, T(j), precision);
    dt_CIR = meshgrid(diff(t_CIR'),1:iterations);
    n_CIR = length(t_CIR);

    % We initialize a vector that starts with the initial value y_0

    y_t = [beta_CIR(1)*ones(iterations,1) nan*dt_CIR];

    % We assume that y_t follows the CIR process
    % dy_t = kappa*(mu_t-y_t)dt + nu*sqrt(mu_t)*dW

    d = 4*beta_CIR(2)*beta_CIR(3)/beta_CIR(4)^2;
    e = exp(-beta_CIR(2)*dt_CIR);
    c = beta_CIR(4)^2.*(1-e)/(4*beta_CIR(2));
    Y_t = zeros(iterations,size(y_t,2));

    % We have to use a for-loop since it is a recursive algorithm
    % Note that each iteration must have its own trajectory

    for i = 1:(n_CIR-1)
        l = y_t(:,i).*e(:,i)./c(:,i); 
        y_t(:,i+1) = c(:,i).*ncx2rnd(d,l);
        Y_t(:,i+1) = Y_t(:,i)+dt_CIR(:,i).*(y_t(:,i)+y_t(:,i+1))/2;
    end
    Q_mod(j) = mean(exp(-Psi(t_CIR, beta_CIR, gamma_h)-mean(Y_t)));

end

Q_model = Q_mod;

