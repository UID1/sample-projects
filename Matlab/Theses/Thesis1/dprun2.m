%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%

fprintf('Note that this should be run AFTER dprun1.m\n\n')

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; [214, 229, 230]; [115, 115, 115]; [165, 169, 176]; [183, 202, 211]]/255;

FIGS.fig2 = figure('Color',[1 1 1]);
plot(MKTSWP.Mt(2:end),100*(DPPHR.antinterp-DPPHR.postinterp)./DPPHR.antinterp, 'LineWidth', 2, 'Color', FIGS.LineColors(3, :))
hold on
plot(MKTSWP.Mt(2:end),100*(DPPMR.antinterp-DPPMR.postinterp)./DPPMR.antinterp, 'LineWidth', 2, 'Color', FIGS.LineColors(2, :))
plot(MKTSWP.Mt(2:end),100*(DPPLR.antinterp-DPPLR.postinterp)./DPPLR.antinterp, 'LineWidth', 2, 'Color', FIGS.LineColors(4, :))
title('\fontsize{12}Relative diffence between DP^A and DP^P estimations')
xlabel('\fontsize{12}Maturity (years)')
ylabel('\fontsize{12}Percent (%)')
legend('Low Risk', 'Medium Risk', 'High Risk')


FIGS.fig3 = figure('Color',[1 1 1]);
plot(MKTSWP.Mt(2:end),10000*(DPPHR.antinterp-DPPHR.postinterp), 'LineWidth', 2, 'Color', FIGS.LineColors(3, :))
hold on
plot(MKTSWP.Mt(2:end),10000*(DPPMR.antinterp-DPPMR.postinterp), 'LineWidth', 2, 'Color', FIGS.LineColors(2, :))
plot(MKTSWP.Mt(2:end),10000*(DPPLR.antinterp-DPPLR.postinterp), 'LineWidth', 2, 'Color', FIGS.LineColors(4, :))
title('\fontsize{12}Absolute diffence between DP^A and DP^P estimations')
xlabel('\fontsize{12}Maturity (years)')
ylabel('\fontsize{12}Basis points (bpp)')
legend('Low Risk', 'Medium Risk', 'High Risk')

R0f_t = @(t, R0T, Mt)((diff([0;R0T])'./diff(Mt))*(max(min(meshgrid(t, Mt(2:end))' , meshgrid(Mt(2:end),t)),meshgrid(Mt(1:end-1),t))-meshgrid(Mt(1:end-1),t))');
R0c_t = @(t, R0T, Mt)([R0T(1);R0T]'*[0*(1:length(t))' diff(meshgrid(t,Mt) <= meshgrid(Mt,t)')']');
P0f_t = @(t, R0T, Mt)(exp(-(diff([0;R0T])'./diff(Mt))*(max(min(meshgrid(t, Mt(2:end))' , meshgrid(Mt(2:end),t)),meshgrid(Mt(1:end-1),t))-meshgrid(Mt(1:end-1),t))'.*t));
P0c_t = @(t, R0T, Mt)(exp(-[R0T(1);R0T]'*[0*(1:length(t))' diff(meshgrid(t,Mt) <= meshgrid(Mt,t)')']'.*t));

FIGS.fig4 = figure('Color',[1 1 1]);
plot(100*R0f_t(1:max(MKTSWP.Mt), DPPLR.R0T, MKTSWP.Mt), 'LineWidth', 2, 'Color', FIGS.LineColors(3, :))
hold on
CVx = 1:30;
CVx(6)=5;CV(11)=10;CV(16)=15;CV(21)=20;CV(26)=25;
CVy = 100*R0c_t(1:max(MKTSWP.Mt), DPPLR.R0T, MKTSWP.Mt);
for i = 1:5:30;
    plot(CVx(i:(i+4)), CVy(i:(i+4)),'LineWidth', 1, 'Color', FIGS.LineColors(2, :))
end
ylim([5.6 6.6]);
legend('Linearly interpolated', 'Piecewise constant')
xlabel('\fontsize{12}Maturity (years)')
ylabel('\fontsize{12}Discount Rate (%)')
title('\fontsize{12}Linearly interpolated vs piecewise constant discount rates')
