function newVelocities = updateVelocities(Particles, c1, c2, deltat, vmax, inertia)
  numParticles = size(Particles.xij, 1);
  numVars = size(Particles.xij, 2);
  deltavij1 = Particles.xijPB - Particles.xij;
  deltavij2 = repmat(Particles.xijSB, [numParticles 1]) - Particles.xij;
  deltavij = (c1*rand*deltavij1 + c2*rand*deltavij2)/deltat;
  velocityUpdates = inertia * Particles.vij + deltavij;
  newSpeeds = sqrt(sum(velocityUpdates.^2,2));
  isWithinBoundaries = repmat(newSpeeds < vmax, [1 numVars]);
  newVelocities = isWithinBoundaries.*velocityUpdates;
end