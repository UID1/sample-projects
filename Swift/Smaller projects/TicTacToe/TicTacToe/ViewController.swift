//
//  ViewController.swift
//  TicTacToe
//
//  Created by Iberius Pred on 25/02/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let winnings: [[Bool]] = [[true, true, true, false, false, false, false, false, false], [false, false, false, true, true, true, false, false, false], [false, false, false, false, false, false, true, true, true], [true, false, false, true, false, false, true, false, false], [false, true, false, false, true, false, false, true, false], [false, false, true, false, false, true, false, false, true], [true, false, false, false, true, false, false, false, true], [false, false, true, false, true, false, true, false, false]]
    
    var playerBoards = [[Bool]]()
    var currentPlayer = 1
    let boardImages = ["cross.png", "nought.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resetBoard()
    }

    @IBOutlet weak var winnerLabel: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func resetGame(_ sender: Any) {
        resetBoard()
        winnerLabel.text = " "
        currentPlayer = 1
        for i in 1...9 {
            if let selectedButton = view.viewWithTag(i) as? UIButton {
                print("Button \(i) cleared")
                selectedButton.setImage(nil, for: [])
            }
        }
    }
    @IBAction func pushButton(_ sender: Any) {
        if let pushedButton = sender as? UIButton {
            print("currentPlayer is \(currentPlayer)")
            // Register the push into checkerboards
            if let pushRegistered = registerPush(pushedButton.tag-1, currentPlayer), pushRegistered {
                if let hasPlayerWon = hasWon(currentPlayer), hasPlayerWon {
                    winnerLabel.text = "Player \(currentPlayer) won."
                }
                if currentPlayer == 2 {
                    currentPlayer = 1
                } else {
                    currentPlayer = 2
                }
                pushedButton.setImage(UIImage(named: boardImages[currentPlayer-1]), for: [])
            }
        }
    }
    
    private func registerPush(_ tag: Int, _ player: Int) -> Bool? {
        if player < 1 || player > 2 {
            return nil
        }
        
        if !playerBoards[0][tag] && !playerBoards[1][tag] {
            playerBoards[player-1][tag] = true
            return true
        } else {
            return false
        }
    }
    
    private func resetBoard() {
        playerBoards = [[false, false, false, false, false, false, false, false, false], [false, false, false, false, false, false, false, false, false]]
    }
    private func hasWon(_ player: Int) -> Bool? {
        if player < 1 || player > 2 {
            return nil
        }
        
        for i in 0..<winnings.count {
            var checkerCount = 0
            for j in 0..<winnings[0].count {
                if winnings[i][j] && playerBoards[player-1][j] {
                    checkerCount += 1
                }
            }
            if checkerCount == 3 {
                return true
            }
        }
        return false
    }
}


