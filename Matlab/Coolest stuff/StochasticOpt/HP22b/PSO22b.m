% Particle Swarm Optimization

% Parameter initialization
maxRuns = 1000;
C1 = 2; C2 = 2;
swarmSize = 400;
vmax = 4;
% Adjust with beta < 1 but close to 1, set w constant when ~.3-.4.
initialInertia = 1.4;
inertiaAdjustment = 0.9995;
inertiaLowLimit = .3;
deltat = .7;
globalMinimum = 1;
machineEpsilon = 1e-4;
alphaVelocity = 1; % [0,1]
varMin = -30; varMax = 30;
varSpan = varMax - varMin;
numVars = 5;
startPoint = ones(1, numVars)*varMax; % Arbitrary value

% Swarm initialization
Particles.xij = zeros(swarmSize, numVars);
Particles.xij = varMin + rand(swarmSize,numVars) * (varSpan);
Particles.vij = alphaVelocity/deltat * ...
                (- varSpan / 2 + rand(swarmSize,numVars) * varSpan);
swarmValues = zeros(swarmSize, 1);

Particles.previouslyBestSwarmValues = ones(swarmSize, 1) * ...
                            PSOFunction(startPoint);
Particles.xijPB = meshgrid(startPoint, 1:swarmSize);
Particles.swarmBestIndex = 1;
Particles.xijSB = Particles.xij(1,:);
fxSB = PSOFunction(Particles.xijSB);

% Plot initialization
numberOfGenerations = 500;
fitnessFigureHandle = figure;
hold on;
set(fitnessFigureHandle, 'Position', [50,50,500,200]);
set(fitnessFigureHandle, 'DoubleBuffer', 'on');
axis([- numberOfGenerations 0 -1000 5000]);
bestPlotHandle = plot(-numberOfGenerations:-1, zeros(1,numberOfGenerations));
grid on;
title('Particle Swarm Optimization (5 vars)', 'FontWeight','Normal');
yTextHandle = ylabel(sprintf('Best: %4.3f', 0.0));
xTextHandle = xlabel('Initializing ...');
hold off;

drawnow;

% PSO Algorithm
numIterations = 0;
currentInertia = initialInertia;

while true %fxSB - globalMinimum > machineEpsilon
  swarmValues = PSOFunction(Particles.xij);
  Particles = updateBestParticles(Particles, swarmValues);
  Particles.vij = updateVelocities(Particles, C1, C2, deltat, vmax, currentInertia);
  Particles.xij = Particles.xij + Particles.vij*deltat;
  numIterations = numIterations + 1;
  if currentInertia > inertiaLowLimit
    currentInertia = currentInertia * inertiaAdjustment;
  end
  
  fxSB = PSOFunction(Particles.xijSB);
  plotvector = get(bestPlotHandle, 'YData');
  plotvector = [plotvector(2:end) fxSB - globalMinimum];
  set(bestPlotHandle, 'YData', plotvector);
  set(yTextHandle, 'String', sprintf('Best: %4.3f', fxSB - globalMinimum));
  roundedXijSB = round(Particles.xijSB);
  xijSBString = ' ';
  for j = 1:numVars
    xijSBString = [xijSBString sprintf('%d ', roundedXijSB(j))];
  end
  xijSBString = sprintf('x_{ij}^{SB} = [%s], Iteration %d', ...
                        xijSBString, numIterations);
  set(xTextHandle, 'String', xijSBString );
  axis([- numberOfGenerations 0 min(plotvector) max(plotvector)+.001]);
  drawnow;

end

disp('Minimum Value');
disp(fxSB);
disp('Point');
disp(Particles.xijSB);


% Found solutions:
%  0  11  22 16  6 ?
% -1 13 23 18 6
% -1 11 21 16 6

























