function r=acf(x,normflg)
%
% r=acf(x,normflg)
%
%  This function computes the acf r(k) k=0:length(x)
%  'acf' means autocorrelation function
%  CAUTION the first value r(1) is hat{gamma}(0) IE the first lag is 0
% x = time series vector (column)
% normflg 0 to divide by nr=length(x)
%         1 to divide by nr*sample variance
%         2 to divide by nr-k
%         3 to divide by (nr-k)*sample variance

[nrows,ncols]=size(x);
if ncols > nrows
    error('x must be a column vector');
end
% conv(x, y) is a discrete convolution between vectors x and y. It is
% defined as C(i) = x(1:i)*flip<lr|ud>(y(1:i))' for the first few
% elements and C(i) = x(i-end:end)*flip<lr|ud>(y(i-end:end))' for the
% rest of them. 1:i turns into i-end:end right after i=end. This is when
% we assume that vectors x and y have equal lengths.
%
% flipud() flips or mirrors the vector vertically, i.e. flipud((1:3)') =
% (3:-1:1)', but flipud(1:3) = 1:3.

r=conv(flipud(x),x);
r=r(nrows:end);
if normflg==0
    r=r/nrows;
elseif normflg==1
    r=r/nrows;
    r=r/r(1);
elseif normflg==2
    den=[nrows:-1:1]';
    r=r./den;
elseif normflg==3
    den=[nrows:-1:1]';
    r=r./den;
    r=r/r(1);
end

