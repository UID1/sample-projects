%
% Counterparty risk and contingent CDS-valuation
%
% R, Axelsson
% Handelshögskolan, Göteborg
% Juni, 2011
%

function DPP = dpcalc(t, T_a, T_b, N_t, sigmaBS)

% +------------------------------------------------------------------+
%
%   Definitions and functions
%


% The intensity volatility parameters as defined in Brigo Pallavicini 2008

beta_CIR = [.0165 .4 .026 .14];

% We are going to use the CIR model for the fair risk-free market price P(t,T) and the risk free rate
% calculated as E[r(t,T)]=-log(P_CIR(t,T))/(T-t)

gamma_CIR = @(alpha, sigma)(sqrt(alpha.^2+2.*sigma.^2));
CIR_A = @(T, gamma, alpha, mu, nu)(log(2.*gamma.*exp((gamma+alpha).*T.*.5)./((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma)).*2.*alpha.*mu./nu.^2);
CIR_B = @(T, gamma, alpha)(2.*(exp(gamma.*T)-1)./ ((gamma+alpha).*(exp(gamma.*T)-1)+2*gamma));

P_CIR = @(t, kappa, nu, mu, y_0)(exp(CIR_A(t, gamma_CIR(kappa, nu), kappa, mu, nu)-CIR_B(t, gamma_CIR(kappa, nu), kappa).*y_0));

% The time period (years) for the duration of the IRS agreement

%T_a =  0; % start of the period
%T_b =  5; % the end
%N_t = 20; % Number of time steps where each step is (T_b - T_a)/N_t

% The function for a fair strike price of a CDS agreement is

S_ab = @(t, T_a, T_b, N_t, beta)(N_t*(P_CIR(T_a-t, beta(2), beta(4), beta(3), beta(1))-P_CIR(T_b-t, beta(2), beta(4), beta(3), beta(1)))/((T_b-T_a)*sum(P_CIR(max((linspace(T_a, T_b, N_t)-t),0), beta(2), beta(4), beta(3), beta(1)).*(linspace(T_a,T_b,N_t)>t))));

% So we need to use the Black-Scholes model for swaptions

d_1= @(V_0, D, sigma, r, T)((log(V_0./D)+(r+0.5*sigma^2).*T) ./(sqrt(T).*sigma));
d_2= @(V_0, D, sigma, r, T)(d_1(V_0, D, sigma, r, T)-sigma.*sqrt(T));

Swaption = @(V_0, D, sigma, r, T) (V_0.*normcdf(d_1(V_0, D, sigma, r, T))-D.*exp(-r.*T).*normcdf(d_2(V_0, D, sigma, r, T)));

% +-------------------------------------------------------------------+
%
%   Computations
%

%t = 1.5;
%sigmaBS = .17;

QmodVect = criskfun(linspace(T_a, T_b, N_t), beta_CIR);
% keyboard
SwapVect = Swaption(max(sabfun(t, linspace(T_a, T_b, N_t), T_b, N_t, beta_CIR),0), S_ab(0, T_a, T_b, N_t, beta_CIR), sigmaBS, -log(P_CIR(t, beta_CIR(2), beta_CIR(4), beta_CIR(3), beta_CIR(1)))/t, T_b-t).*(linspace(T_a,T_b,N_t)>t);
SwapVect(find(isnan(SwapVect)))=0;
DPP = abs(diff([1 QmodVect(2:end)]))*SwapVect(2:end)';

