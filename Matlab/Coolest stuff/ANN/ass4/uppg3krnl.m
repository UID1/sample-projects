% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Assignment 4 - task 3 kernel
% by Robin Axelsson
%

% We import training data for the network
% and set up some parameters

tic % Initialize our clock for performance measurment
wb1 = waitbar(0,'Initializing ...','Color',[1 1 1]);
set(findobj(wb1,'Type','Patch'),'EdgeColor',[.01 .01 .01],'FaceColor',[.4 .1 .1]);
CLwinsize = 1000; % Refresh rate of GUIs during competitive learning
SPwinsize = 10; % Refresh rate when iterating the simple perceptron
UP = 2; % Size of stochastic selection of update patterns
numRuns = 20; % Number of runs
DATA = importdata('data_classify.txt');

movegui(wb1, 'southeast');

% The weights are for 2 input nodes and 5 output nodes and are to be
% initially uniformly distributed over [-1, 1]

M = 2; % # input nodes
%N = 5; % # output nodes/neurons %This parameter is now external
w_ki = 2*rand(M, N);
i_rwin = 1; % The 'winning' index of the best of numRuns
Ewin = 1e15; % Set the winning energy to something big

% The competitive learning process

eta_cl = .02;
sigma_0cl = .6;
sigma_1cl = .1;
tau_cl = 1000/sigma_0cl;
steps_cl = 1e5;  % # Iterations for Kohonen/CL
steps_sp = 3000; % # Iterations for the simple perceptron

% Some further initialization steps

gETRN = ones(steps_sp, numRuns);%, 'gpuArray');
gEVLD = ones(steps_sp, numRuns);%, 'gpuArray');
gCvTRN = ones(steps_sp, numRuns);%, 'gpuArray');
gCvVLD = ones(steps_sp, numRuns);%, 'gpuArray');
O_mu = ones(UP,1);
Ewin = 1e15;

for i_r = 1:numRuns
   set(wb1, 'Name', sprintf('Run %d of %d', i_r, numRuns));
   for i = 1:steps_cl
      sigma_cl = (i <= 1e4)*sigma_0cl + (i >1e4)*sigma_1cl*exp(-(i-1e4+1)/tau_cl);
      rndSel = randi(length(DATA));
      % The following is essentially equation (1)
      g_i = exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2) ...
   		 / sum(exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2));
      i_0 = find(g_i == max(g_i));
      Lambda_ii0 = exp(-((1:N)-i_0).^2/(2*sigma_cl^2));
      deltaw_ki = eta_cl*((ones(M,1)*Lambda_ii0).*(DATA(rndSel, 2:3)'*ones(1,N)-w_ki));
      w_ki = w_ki + deltaw_ki;
      if mod(i, CLwinsize) == 0
         SPC = toc;
		 if ishandle(wb1)
            waitbar(.5*i/steps_cl, wb1, ...
			        sprintf('Computing Kohonen network at %.2f iterations/s', ...
					        CLwinsize/SPC));
         end
   	  tic;
      end
   end
   
   % The simple perceptron training process
   
   W_i = 2*(rand(N,1)-.5);
   beta_0 = .5;
   theta = 0;
   eta_sp = .1;
   % Randomly split the input data into a training and validation matrix
   TRNidx = randperm(length(DATA), floor(length(DATA)*.7));
   TRN = DATA(TRNidx, :);
   VLD = DATA;
   VLD(TRNidx,:) = [];
   diffDwTRN = ones(length(TRN), N, M);%, 'gpuArray');
   diffDwVLD = ones(length(VLD), N, M);%, 'gpuArray');
   CNO_muTRN = ones(length(TRN), N);%, 'gpuArray');
   CNO_muVLD = ones(length(VLD), N);%, 'gpuArray');
   PSO_muTRN = ones(length(TRN), 1);%, 'gpuArray');
   PSO_muVLD = ones(length(VLD), 1);%, 'gpuArray');

   % No performance enhancements with CUDA :(
   gTRN = TRN(:,2:3); %gpuArray(TRN(2:3));
   gVLD = VLD(:,2:3); %gpuArray(VLD(2:3));
   gOTRN = TRN(:,1); %gpuArray(TRN(2:3));
   gOVLD = VLD(:,1); %gpuArray(VLD(2:3));


   
   for i = 1:steps_sp
      IdxIter = randperm(length(TRN), UP);
      for j = 1:UP
         g_i(j,:) = exp(-sum((TRN(IdxIter(j), 2:3)'*ones(1,N)-w_ki).^2)/2) ...
                      / sum(exp(-sum((TRN(IdxIter(j), 2:3)'*ones(1,N)-w_ki).^2)/2));
         O_mu(j) = tanh(beta_0*(g_i(j,:)*W_i-theta));
      end
      derivO_mu = beta_0*(1-O_mu.^2);
      deltaW_i = eta_sp*g_i'*((TRN(IdxIter,1)-O_mu).*derivO_mu);
      W_i = W_i + deltaW_i;
      deltatheta = -eta_sp*sum((TRN(IdxIter,1)-O_mu)'*derivO_mu);
      theta = theta + deltatheta;
      for ii = 1:size(w_ki, 2)
        for kk = 1:size(w_ki, 1)
           diffDwTRN(:,ii,kk) = (gTRN(:,kk)-w_ki(kk,ii)).^2;
           diffDwVLD(:,ii,kk) = (gVLD(:,kk)-w_ki(kk,ii)).^2;
   	    end
      end
      
      % Evaluating classification error and energy level
      CNO_muTRN = exp(-sum(diffDwTRN,3)/2)./repmat(sum(exp(-sum(diffDwTRN,3)/2), 2), 1, N);
      CNO_muVLD = exp(-sum(diffDwVLD,3)/2)./repmat(sum(exp(-sum(diffDwVLD,3)/2), 2), 1, N);
      PSO_muTRN = tanh(beta_0*(CNO_muTRN*W_i-theta));
      PSO_muVLD = tanh(beta_0*(CNO_muVLD*W_i-theta));
   
      gETRN(i,i_r) = .5*sum((gOTRN(:,1)-PSO_muTRN(:)).^2);
      gEVLD(i,i_r) = .5*sum((gOVLD(:,1)-PSO_muVLD(:)).^2);
      gCvTRN(i,i_r) = mean(.5*abs(gOTRN(:,1) - sign(PSO_muTRN)));
      gCvVLD(i,i_r) = mean(.5*abs(gOVLD(:,1) - sign(PSO_muVLD)));
   
      if mod(i, SPwinsize) == 0
         SPC = toc;
         waitbar(.5+.5*i/steps_sp, wb1, sprintf('Training simple perceptron at %.2f iterations/s', SPwinsize/SPC));
   	     tic;
   	  end
   end
   % Choosing a winner for task 1c
   if Ewin > gETRN(end,i_r)
      Ewin = gETRN(end,i_r);
      w_kiwin = w_ki;
	  W_iwin = W_i;
	  i_rwin = i_r;
   end
end
if ishandle(wb1)
   close(wb1);
end
