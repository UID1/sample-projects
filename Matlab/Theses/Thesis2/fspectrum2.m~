%
%  This function returns the frequency spectrum of a series of observations
%
%   Input arguments:
%
%   X_t: The observed time-series
%   F_x: The sample rate in number of samples per time unit
%        (e.g. per second, minute, hour, day, week, ...)
%        1 obs per day is 365.256 obs per year
%        1 obs per hour is 8766.144 obs per year

function fspec = fspectrum2(X_t, F_x)

% First we add a reference signal. Let's say that it goes from
% t = 0 to t = 10 seconds so the sample rate is length(X_t) per
% 10 seconds

% First we determine a reference signal. We know the sample rate
% and we need to find a scale with respect to periodicity. We
% assume that the sample rate is 1 per hour and stick with that
% and let the signal have periodicity 100 hours. Periodicity
% T follows the relationship T = 1/f where a sine wave with
% frequency f follows sin(2*pi*f) = sin(2*pi/T)

refsig1 = 25*sin(F_x/100*2*pi*(1:length(X_t)));
refsig2 = 20*sin(F_x/50*2*pi*(1:length(X_t)));
refsig3 = 15*sin(F_x/25*2*pi*(1:length(X_t)));
refsig4 = 10*sin(F_x/12.5*2*pi*(1:length(X_t)));

% Then we apply the Fast Fourier Transform

Y_t = fftshift(fft(X_t+refsig1'+refsig2'+refsig3'+refsig4'));
Y1_t = fftshift(fft(X_t+refsig1'));
Y2_t = fftshift(fft(X_t+refsig2'));
Y3_t = fftshift(fft(X_t+refsig3'));
Y4_t = fftshift(fft(X_t+refsig4'));
Z_t = fftshift(fft(X_t));

% So we want to find the index of the reference signal in the transform
% we know the periodicity of that signal so that point has periodicity
% T.

refloc1 = find(max(Y1_t(1:floor(end/2))) == Y1_t(1:floor(end/2)));
refloc2 = find(max(Y2_t(1:floor(end/2))) == Y2_t(1:floor(end/2)));
%refloc3 = find(max(Y3_t(1:floor(end/2))) == Y3_t(1:floor(end/2)));
%refloc4 = find(max(Y4_t(1:floor(end/2))) == Y4_t(1:floor(end/2)));


% We also know that the Nyquist frequency is the maximum frequency (or
% minimum period) that can be captured given the sampling rate which is
% half the sampling rate. If the sampling rate is 1 sample per hour then
% the highest frequency that can be captured is 1 period per 2 hours. So
% the lowest period length in the measurements is 2 hours.

% maxpd = F_x*2;

% So we want to build a linear vector with length(X_t) elements with the
% first element fscale(1) = maxpd and the element with index refloc to be
% fscale(refloc) = 100.

% Let's assume that fscale follows the linear equation
%
%     fscale(x) = deltaT*x + f_intercept

deltaT = (1/100-1/50)/(refloc1-refloc2);
f_intercept = 1/50 - deltaT*refloc2;

% Now that we have established the straight line above we know it begins
% with deltaT*1+f_intercept and ends with deltaT*floor(length(X_t)/2).
% We can put a whole vector into the equation.

fspec.pscale = 1./(deltaT*(1:floor(length(X_t)/2)) + f_intercept);
fspec.fscale = deltaT*(1:floor(length(X_t)/2)) + f_intercept;
fspec.spectrum = abs(Z_t(1:floor(end/2)));
fspec.fcoeff = Z_t;
fspec.refspec = abs(Y_t(1:floor(end/2)));
fspec.reflocs = [refloc1 refloc2 ];
end

