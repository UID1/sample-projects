function energy = testDigest(A, B, C)
	CT = sort([cumsum(A(1:(end-1))) cumsum(B)]);
	C_test = sort([CT(1) diff(CT)], 'descend');
	energy = sum(((C-C_test).^2./C));
end
