//
//  ViewController.swift
//  Audio Player
//
//  Created by Iberius Pred on 28/02/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBAction func startPlayback(_ sender: Any) {
        startPlayback()
    }
    @IBAction func pausePlayback(_ sender: Any) {
        pausePlayback()
    }
    @IBAction func volumeSliderMoved(_ sender: Any) {
        player.volume = volumeSlider.value
    }
    @IBAction func stopPlayback(_ sender: Any) {
        pausePlayback()
        positionSlider.value = 0
    }
    @IBAction func positionSliderMoved(_ sender: Any) {
        player.currentTime = Double(positionSlider.value) * player.duration
    }
    
    
    @IBOutlet weak var positionSlider: UISlider!
    @IBOutlet weak var volumeSlider: UISlider!
    var player = AVAudioPlayer()
    var isPlaying = false
    let audioPath = Bundle.main.path(forResource: "sheep", ofType: "mp3")
    var updater: CADisplayLink! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        positionSlider.value = 0
        startPlayback()
    }
    
    private func startPlayback() {
        if !isPlaying {
            do {
                try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
                player.play()
                player.currentTime = Double(positionSlider.value) * player.duration
                // We could also use a timer for the same task
                updater = CADisplayLink(target: self, selector: #selector(ViewController.trackPlayback))
                updater.preferredFramesPerSecond = 1
                updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
                
            } catch {
                // Process any errors
            }
            isPlaying = true
        }
    }
    private func pausePlayback() {
        if isPlaying {
            player.pause()
            updater.invalidate()
            isPlaying = false
        }
    }
    
    @objc private func trackPlayback() {
        positionSlider.value = Float(player.currentTime / player.duration)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

