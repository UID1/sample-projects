#include <cuda_runtime.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/



#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}                                                                             \

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

void sumArraysOnHost (float *A, float *B, float *C, const int N)
{
   for(int idx=0; idx<N;++idx)
      C[idx] = A[idx] + B[idx];
}

__global__ void sumArraysOnGPU(float *A, float *B, float *C, const int N)
{
   int i = blockIdx.x * blockDim.x + threadIdx.x;
   if (i<N) C[i] = A[i] + B[i];
}

void initialData(float *ip, int size)
{
   /* generate different seed for random number */
   time_t t;
   srand((unsigned) time(&t));
   for(int i=0; i<size; ++i)
      ip[i] = (float)(rand() & 0xFF)/10.0f;
   /* the &0xFF operation makes sure that the random value stays
      between 0 and 255
   */
}

__global__ void sumMatrixOnGPU2D(float *MatA, float *MatB, float *MatC, int nx, int ny)
{
   unsigned int ix = threadIdx.x + blockIdx.x*blockDim.x;
   unsigned int iy = threadIdx.y + blockIdx.y*blockDim.y;
   unsigned int idx = iy * nx + ix;
   
   if(ix<nx && iy<ny)
      MatC[idx] = MatA[idx]+MatB[idx];
}

__global__ void sumMatrixOnGPUMix(float *MatA, float *MatB, float *MatC,
                                  int nx, int ny)
{
   unsigned int ix  = threadIdx.x + blockIdx.x * blockDim.x;
   unsigned int iy  = blockIdx.y;
   unsigned int idx = iy * nx + ix;
   
   if(ix<nx && iy<ny)
      MatC[idx] = MatA[idx] + MatB[idx];
}

void checkResult(float *hostRef, float *gpuRef, const int N)
{
   const double epsilon = 1.0E-8;
   bool match = 1;
   for(int i=0; i<N; ++i)
   {
      if(abs(hostRef[i] - gpuRef[i]) > epsilon)
      {
         match = 0;
         printf("Arrays don't match!\n");
         printf("host  %5.2f gpu %5.2f at current %d\n", hostRef[i], gpuRef[i], i);
         break;
      }
   }
   if(match)
      printf("Arrays match!\n");   
}

void sumMatrixOnHost(float *A, float *B, float *C, const int nx, const int ny)
{
   float *ia = A, *ib = B, *ic = C;
   
   for(int iy=0; iy<ny; ++iy)
   {
      for(int ix=0; ix<nx; ++ix)
         ic[ix] = ia[ix]+ib[ix];
      ia += nx;
      ib += nx;
      ic += nx;
   }
   return;
}


int main(int argc, char **argv)
{
   printf("%s starting ...\n", argv[0]);
   
   /* Get device information*/
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   printf("Using Device %d: %s\n", dev, deviceProp.name);
   CHECK(cudaSetDevice(dev));
 
   /* Set up datasize of matrix */
   int nx = 1<<14, ny = 1<<14;
   int nxy = nx*ny;
   int nBytes = nxy*sizeof(float);
   printf("Matrix size: nx = %d ny = %d (%d elements)\n", nx, ny, nx*ny);
   
   /* Allocate memory on the host */
   float *h_A, *h_B, *hostRef, *gpuRef;
   h_A = (float *)malloc(nBytes);
   h_B = (float *)malloc(nBytes);
   hostRef = (float *)malloc(nBytes);
   gpuRef = (float *)malloc(nBytes);
   
   
   /* Initialize data at the host side */
   double iStart = cpuSecond();
   initialData(h_A, nxy);
   initialData(h_B, nxy);
   double iElaps = cpuSecond() - iStart;
   printf("Matrix initialization elapsed in %f seconds\n", iElaps);
   
   memset(hostRef, 0, nBytes);
   memset(gpuRef,  0, nBytes);
   
  /* Add matrix on the host side to check results */ 
  iStart = cpuSecond();
  sumMatrixOnHost(h_A, h_B, hostRef, nx, ny);
  iElaps = cpuSecond() - iStart;
  printf("sumMatrixOnHost(h_A, h_B) elapsed in %f seconds\n", iElaps);
  
  /* Allocate memory on the CUDA device */
  float *d_MatA, *d_MatB, *d_MatC;
  CHECK(cudaMalloc((void **)&d_MatA, nBytes));
  CHECK(cudaMalloc((void **)&d_MatB, nBytes));
  CHECK(cudaMalloc((void **)&d_MatC, nBytes));
  
  /* Transfer data from the host to the device */
  CHECK(cudaMemcpy(d_MatA, h_A, nBytes, cudaMemcpyHostToDevice));
  CHECK(cudaMemcpy(d_MatB, h_B, nBytes, cudaMemcpyHostToDevice));
  
  /* Invoke kernel at the host side */
  int dimx = 1024, dimy = 1;
  dim3 block(dimx, dimy);
  dim3 grid((nx+block.x-1)/block.x, (ny+block.y-1)/block.y);
  
  iStart = cpuSecond();
  sumMatrixOnGPUMix<<<grid, block>>> (d_MatA, d_MatB, d_MatC, nx, ny);
  CHECK(cudaDeviceSynchronize());
  iElaps = cpuSecond() - iStart;
  printf("sumMatrixOnGPU2D<<<(%d, %d), (%d, %d)>>> elapsed in %f seconds\n",
         grid.x, grid.y, block.x, block.y, iElaps);
         
  /* Copy kernel results back to the host */
  CHECK(cudaMemcpy(gpuRef, d_MatC, nBytes, cudaMemcpyDeviceToHost));
  
  /* Check device results */
  checkResult(hostRef, gpuRef, nxy);
  
  /* Free host memory */
  free(h_A);
  free(h_B);
  free(hostRef);
  free(gpuRef);
  
  /* Reset device */
  CHECK(cudaDeviceReset());
  
  return(0);
}
