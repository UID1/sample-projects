#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <ctime>
#include <locale>
#include <cstdlib>
#include <Windows.h>

#define QS_MAX_LEVELS 300
typedef long long int64;
typedef unsigned long long uint64;
#define MAX_ZERO_SOLUTIONS 1000000

/* CUDA grid configuration */

#define GDIMX 2//8
#define GDIMY 2//8
#define GDIMZ 1
#define BDIMX 2//8
#define BDIMY 2//8
#define BDIMZ 1
#define NUM_THREADS 16 //4096

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
      {                                                                       \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
      }                                                                       \
}                                                                             \

/*
**********                            BEGIN: System time measurement module
******************************************************************************
*/

const __int64 DELTA_EPOCH_IN_MICROSECS = 11644473600000000;

struct timezone2
{
	__int32  tz_minuteswest; /* minutes W of Greenwich */
	bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
	__int32    tv_sec;         /* seconds */
	__int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
	FILETIME ft;
	__int64 tmpres = 0;
	TIME_ZONE_INFORMATION tz_winapi;
	int rez = 0;

	ZeroMemory(&ft, sizeof(ft));
	ZeroMemory(&tz_winapi, sizeof(tz_winapi));

	GetSystemTimeAsFileTime(&ft);

	tmpres = ft.dwHighDateTime;
	tmpres <<= 32;
	tmpres |= ft.dwLowDateTime;

	/*converting file time to unix epoch*/
	tmpres /= 10;  /*convert into microseconds*/
	tmpres -= DELTA_EPOCH_IN_MICROSECS;
	tv->tv_sec = (__int32)(tmpres*0.000001);
	tv->tv_usec = (tmpres % 1000000);

	//_tzset(),don't work properly, so we use GetTimeZoneInformation
	rez = GetTimeZoneInformation(&tz_winapi);

	if (tz) /* Check for NULL pointer, AARGH!!!! */
	{
		tz->tz_dsttime = (rez == 2) ? true : false;
		tz->tz_minuteswest = tz_winapi.Bias + ((rez == 2) ? tz_winapi.DaylightBias : 0);
	}
	return 0;
}

double cpuSecond()
{
	struct timeval2 tp;
	gettimeofday(&tp, NULL);
	return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/*
**********                              END: System time measurement module
******************************************************************************
*/

struct scKernelStatus
{
	float currentOptH;
	int currentOptCfgA;
	int currentOptCfgB;
	int numIterations;
	int threadIdx;
	int intCache;
	int64 int64Cache;
	float floatCache;
	bool boolCache;
	bool isAlive;
};

__device__ int generateLehmerCode(const int *vCfg, int iSize)
{
	int *iMask = new int[iSize];
	for (int i = 0; i < iSize; ++i)
		iMask[i] = 1;
	int *lehmerVector = new int[iSize];
	int lehmerValue(0), iFactorial(1);
	for (int i = 0; i < iSize; ++i)
	{
		iMask[vCfg[i]] = 0;
		lehmerVector[i] = 0;
		for (int j = 0; j<=vCfg[i]; ++j) lehmerVector[i] += iMask[j];
	}
   printf("Generated lehmer vector:");
   for (int i = 0; i < iSize; ++i)
      printf(" %d", lehmerVector[i]);
   printf(".\n");
	for (int i = 1; i<=iSize; ++i) {
		iFactorial *= i;
		lehmerValue += lehmerVector[iSize - i] * iFactorial;
	}
   /* Largest factorial is iSize! */
	delete[] iMask;
	delete[] lehmerVector;
	return lehmerValue;
}

__device__ void permuteFromLehmerCode(int *vPermutation,
	const int iSize, const int iLehmerValue)
{
	/* We begin by converting the Lehmer value to a Lehmer code vector */
	int iFactorial(1), iRestValue(iLehmerValue);
	for (int i = 1; i <= iSize; ++i)
		iFactorial *= i;
	int *lehmerVector = new int[iSize];
   for(int i  = 0; i < iSize; ++i)
   {
      lehmerVector[i] = iRestValue / iFactorial;
      iRestValue -= lehmerVector[i]*iFactorial;
      iFactorial /= iSize-i;
   }
	int *iMask = new int[iSize];
	for (int i = 0; i < iSize; ++i)
		iMask[i] = 1;
	/*
	First Lehmer value counts the numbers from the left up to that number
	+ 1. Then we remove that number from the mask, i.e. set that number to
	zero. The second Lehmer value yields the proper number by summing the
	mask elements from the left until the sum equals that number. The
	number of elements needed to yield that sum +1 is the value of the
	second position, again, set that element in the mask to zero. Then take
	the third Lehmer value and count the number of elements required in the
	mask to yield that sum. Take that number +1 set that number in the mask
	to zero and set the third element in the permutation to that value.
	Repeat until you run out of Lehmer values.
	*/
	for (int i = 0; i < iSize; ++i)
	{
		int cumIElements = 0;
		int cumSumIMask = 0;
		while ((cumSumIMask < lehmerVector[i]+1) &&
				(cumIElements<iSize))
			cumSumIMask += iMask[cumIElements++];
      --cumIElements;
		vPermutation[i] = cumIElements;
		iMask[cumIElements] = 0;
		if (cumIElements == iSize)
		{
			printf("Error: Bad Lehmer Code!\n Code: ");
			for (int j = 0; j < iSize; ++j)
				printf("%d ", lehmerVector[j]);
			printf(".\n");
		}
	}
   printf("Permutation from Lehmer:");
   for (int i = 0; i<iSize; ++i)
      printf(" %d", vPermutation[i]);
   printf(".\n");
      printf("iMask:");
   for (int i = 0; i<iSize; ++i)
      printf(" %d", iMask[i]);
   printf(".\n");

	delete[] iMask;
	delete[] lehmerVector;
}

void permuteFromLehmerCodeOnHost(int *vPermutation,
	const int iSize, const int iLehmerValue)
{
	int iFactorial(1), iRestValue(iLehmerValue);
	for (int i = 1; i < iSize; ++i)
		iFactorial *= i;
	int *lehmerVector = new int[iSize];
	lehmerVector[iSize - 1] = iLehmerValue / iFactorial;
	for (int i = iSize - 2; i >= 0; --i)
	{
		iRestValue %= iFactorial;
		if (i>0) iFactorial /= i;
		lehmerVector[i] = iRestValue / iFactorial;
	}
	int *iMask = new int[iSize];
	for (int i = 0; i < iSize; ++i)
		iMask[i] = 1;
	for (int i = 0; i < iSize; ++i)
	{
		int cumIElements = 0;
		int cumSumIMask = 0;
		while (cumSumIMask < lehmerVector[i])
			cumSumIMask += iMask[cumIElements++];
		vPermutation[i] = cumIElements;
		iMask[cumIElements] = 0;
	}
	delete[] iMask;
	delete[] lehmerVector;
}

__global__ void initializeKernel(scKernelStatus *currentKernelStatus, int size, int64 seed)
{
/* Initialize variables for generating permutation */
	int swapIdx(0), swapper(0);
   int otherVector[11] = {5,0,1,7,4,3,6,8,2,9,10};
	curandStateMRG32k3a swpElemState;
	curand_init(seed, 1, 0, &swpElemState);
	/* Initialize vectors to be used for permutation */
	int sizeA(size);
	int *cfgA = new int[sizeA];
   int *lehmerA = new int[sizeA];
	for (int i = 0; i < sizeA; ++i) cfgA[i] = i;
	/* Conduct the permutation */
	for (int i = 0; i < sizeA-1; ++i)
	{
		swapIdx = curand(&swpElemState) % (sizeA - 1 - i);
		swapper = cfgA[i];
		cfgA[i] = cfgA[i + swapIdx];
		cfgA[i + swapIdx] = swapper;
	}
   for (int i = 0; i < sizeA; ++i)
      cfgA[i] = otherVector[i];
	currentKernelStatus->currentOptCfgA
			= generateLehmerCode(cfgA, sizeA);
   printf("Permuted vector: ");
   for (int i = 0; i < sizeA; ++i)
      printf("%d ", cfgA[i]);
   printf("\nLehmer value: %d.\n", currentKernelStatus->currentOptCfgA);
   permuteFromLehmerCode(lehmerA, sizeA, currentKernelStatus->currentOptCfgA);
   printf("Lehmer to vector: ");
   for (int i = 0; i < sizeA; ++i)
      printf("%d ", lehmerA[i]);
   printf(".\n");
   delete[] cfgA, lehmerA;
}

int main (int argc, char** argv)
{
   int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	CHECK(cudaSetDevice(dev));

   scKernelStatus *kernelStatus1;
   CHECK(cudaMalloc((scKernelStatus **)&kernelStatus1, sizeof(scKernelStatus)));
   
   initializeKernel<<<1,1>>> (kernelStatus1, 11, static_cast<int64>(12345/*cpuSecond()*/));   
   
   CHECK(cudaFree(kernelStatus1));
}


/* This for loop is WORKING:

	for (int i = 0; i < iSize; ++i)
	{
		int cumIElements = 0;
		int cumSumIMask = 0;
		while ((cumSumIMask <= lehmerVector[i]+1) &&
				(cumIElements<=iSize))
			cumSumIMask += iMask[cumIElements++];
         
		vPermutation[i] = cumIElements-2;
		if((cumIElements-1) < iSize) iMask[cumIElements-1] = 0;
		if (cumIElements == iSize)
		{
			printf("Error: Bad Lehmer Code!\n Code: ");
			for (int j = 0; j < iSize; ++j)
				printf("%d ", lehmerVector[j]);
			printf(".\n");
		}
	}
   
*/