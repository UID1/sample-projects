function slopeAngle = SlopeFun(x, SlopeParams);
  slopeAngle = interp1((0:length(SlopeParams.angleVector)-1)* ...
                     SlopeParams.deltax, SlopeParams.angleVector, x, 'linear');
end