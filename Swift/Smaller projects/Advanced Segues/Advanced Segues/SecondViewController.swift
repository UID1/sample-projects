//
//  SecondViewController.swift
//  Advanced Segues
//
//  Created by Iberius Pred on 26/02/17.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var username = ""
    var activeRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
