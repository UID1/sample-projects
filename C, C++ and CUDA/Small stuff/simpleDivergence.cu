#include <cuda_runtime.h>
#include <stdio.h>

/*
 * simpleDivergence demonstrates divergent code on the GPU and its impact on
 * performance and CUDA metrics.
 */

/*  
 **********                            BEGIN: System time measurement module
 ******************************************************************************
*/

#include <time.h>
#include <windows.h>

const __int64 DELTA_EPOCH_IN_MICROSECS= 11644473600000000;

struct timezone2 
{
   __int32  tz_minuteswest; /* minutes W of Greenwich */
      bool  tz_dsttime;     /* type of dst correction */
};

struct timeval2
{
   __int32    tv_sec;         /* seconds */
   __int32    tv_usec;        /* microseconds */
};

int gettimeofday(struct timeval2 *tv/*in*/, struct timezone2 *tz/*in*/)
{
   FILETIME ft;
   __int64 tmpres = 0;
   TIME_ZONE_INFORMATION tz_winapi;
   int rez=0;
   
   ZeroMemory(&ft,sizeof(ft));
   ZeroMemory(&tz_winapi,sizeof(tz_winapi));

   GetSystemTimeAsFileTime(&ft);
   
   tmpres = ft.dwHighDateTime;
   tmpres <<= 32;
   tmpres |= ft.dwLowDateTime;
   
   /*converting file time to unix epoch*/
   tmpres /= 10;  /*convert into microseconds*/
   tmpres -= DELTA_EPOCH_IN_MICROSECS; 
   tv->tv_sec = (__int32)(tmpres*0.000001);
   tv->tv_usec =(tmpres%1000000);

   //_tzset(),don't work properly, so we use GetTimeZoneInformation
   rez=GetTimeZoneInformation(&tz_winapi);
   
   if(tz) /* Check for NULL pointer, AARGH!!!! */
   {
      tz->tz_dsttime=(rez==2)?true:false;
      tz->tz_minuteswest = tz_winapi.Bias + ((rez==2)?tz_winapi.DaylightBias:0);
   }
   return 0;
}

double cpuSecond()
{
   struct timeval2 tp;
   gettimeofday(&tp, NULL);
   return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/* 
 **********                              END: System time measurement module
 ******************************************************************************
*/

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
   {                                                                          \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
   }                                                                          \
}                                                                             \

__global__ void mathKernel1(float *c)
{
   int tid = blockIdx.x * blockDim.x + threadIdx.x;
   float ia, ib;
   ia = ib = 0.0f;
   if(tid%2==0) ia = 100.0f;
   else ib = 200.0f;
   c[tid] = ia+ib;
}

__global__ void mathKernel2(float *c)
{
   int tid = blockIdx.x * blockDim.x + threadIdx.x;
   float ia, ib;
   ia = ib = 0.0f;
   if((tid/warpSize)%2==0) ia = 100.0f;
   else ib = 200.0f;
   c[tid] = ia+ib;
}

__global__ void mathKernel3(float *c)
{
   int tid = blockIdx.x * blockDim.x + threadIdx.x;
   float ia, ib;
   ia = ib = 0.0f;
   bool ipred = (tid%2 ==0);
   if(ipred) ia = 100.0f;
   if(!ipred) ib = 200.0f;
   c[tid] = ia+ib;
}

__global__ void mathKernel4(float *c)
{
   int tid = blockIdx.x * blockDim.x + threadIdx.x;
   float ia, ib;
   ia = ib = 0.0f;
   /*
      Here we shift out the binary digits until we hit the
      fifth digit. This digit has significance 32. With a
      bitwise AND operation with 0x01, we can check whether
      this bit is 0 or 1. This operation is less computational
      than a combination of "/" and modulus.
      
   */
   int itid = tid >> 5;
   if(itid & 0x01 == 0) ia = 100.0f;
   else ib = 200.0f;
   c[tid] = ia+ib;
}

__global__ void warmingup(float *c)
{
   int tid = blockIdx.x * blockDim.x + threadIdx.x;
   float ia, ib;
   ia = ib = 0.0f;
   
   if((tid/warpSize)%2 == 0) ia = 100.0f;
   else ib = 200.0f;
   c[tid] = ia+ib;
}

int main (int argc, char **argv)
{
   /* Get device information and set device*/
   int dev = 0;
   cudaDeviceProp deviceProp;
   CHECK(cudaGetDeviceProperties(&deviceProp, dev));
   printf("Using Device %d: %s\n", dev, deviceProp.name);
   CHECK(cudaSetDevice(dev));

   /* Set up dataset and size*/
   int size = 64;
   int blocksize = 64;
   /*
      The function atoi() converts a c-string into an int.
   */
   if(argc > 1) blocksize = atoi(argv[1]);
   if(argc > 2) size = atoi(argv[2]);
   printf("Datasize = %d\n", size);
   
   /* Set up thread configuration */
   dim3 block(blocksize);
   dim3 grid((size+block.x-1)/block.x);
   printf("Thread configuration (numblocks = %d gridsize = %d)\n", block.x, grid.x);

   /* Allocate GPU memory */
   float *d_C;
   size_t nBytes = size * sizeof(float);
   CHECK(cudaMalloc((float**)&d_C, nBytes));
   
   /* We run a "warmup" kernel to remove initial overhead */
   size_t iStart, iElaps;
   CHECK(cudaDeviceSynchronize());
   iStart = cpuSecond();
   warmingup<<<grid, block>>>(d_C);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;
   printf("warmup     <<<%d, %d>>> completed in %f seconds.\n",
          grid.x, block.x, iElaps );
   CHECK(cudaGetLastError());
   
   /* Run kernel 1*/
   iStart = cpuSecond();
   mathKernel1<<<grid, block>>>(d_C);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;
   printf("mathKernel1<<<%d, %d>>> completed in %f seconds.\n",
          grid.x, block.x, iElaps);
   CHECK(cudaGetLastError());
   
   /* Run kernel 2 */
   iStart = cpuSecond();
   mathKernel2<<<grid, block>>>(d_C);
   CHECK(cudaDeviceSynchronize());
   iElaps = cpuSecond() - iStart;
   printf("mathKernel2<<<%d, %d>>> completed in %f seconds.\n",
          grid.x, block.x, iElaps);
   CHECK(cudaGetLastError());
   
   /* Run kernel 3 */
   iStart = cpuSecond();
   mathKernel3<<<grid, block>>>(d_C);
   CHECK(cudaDeviceSynchronize());
   printf("mathKernel3<<<%d, %d>>> completed in %f seconds.\n",
          grid.x, block.x, iElaps);
   CHECK(cudaGetLastError());
   
   /* Run kernel 4 */
   iStart = cpuSecond();
   mathKernel4<<<grid, block>>>(d_C);
   CHECK(cudaDeviceSynchronize());
   printf("mathKernel4<<<%d, %d>>> completed in %f seconds.\n",
          grid.x, block.x, iElaps);
   CHECK(cudaGetLastError());
   
   /* Free GPU memory and reset device */
   CHECK(cudaFree(d_C));
   CHECK(cudaDeviceReset());
   return EXIT_SUCCESS;
}

   
   
   