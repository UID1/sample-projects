#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <ctime>
#include <locale>
#include <cstdlib>
#include <Windows.h>

int main(int argc, char **argv)
{
   int vecA[11] = { 8479, 4167, 2646, 1081, 881, 859, 701, 691, 184, 169, 142 };
   int vecB[11] = { 8479, 3960, 2630, 1081, 1066, 908, 859, 691, 169, 142, 15 };
   std::cout << "Diff squared:";
   for(int i = 0; i<11; ++i)
      std::cout << (vecA[i]-vecB[i])*(vecA[i]-vecB[i]) << " ";
   std::cout << "." << std::endl;
   std::cout << "Diff squared (float):";
   for(int i = 0; i<11; ++i)
      std::cout << (float)((vecA[i]-vecB[i])*(vecA[i]-vecB[i])) << " ";
   std::cout << "." << std::endl;
   std::cout << "Diffsquarediv (float):";
   for(int i = 0; i<11; ++i)
      std::cout << (float)((vecA[i]-vecB[i])*(vecA[i]-vecB[i]))/(float)vecA[i] << " ";
   std::cout << "." << std::endl;
   
   printf("Address of vecA: %p\n", &vecA);
   std::cout << "Address of vecB:" << &vecB << std::endl;
}
