% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% Test the contribution  from temperature data to  the electricity price
% data using fourier methods. I intend to try with d = 0,1,2

% This code works:

% (d=2)
%A0 = [ELSPOTh2010(3:end-1) ELSPOTh2010(2:end-2) ...
%      ELSPOTh2010(1:end-3)];
%elspot_coef = (A0'*A0)\A0'*tmp2010.national(4:end)
%ELSPOTh2010t = ELSPOTh2010(4:end) - A0*elspot_coef;
%ELSPEC2010t2 = fspectrum3(ELSPOTh2010t,1)

% (d=1)
%A0 = [ELSPOTh2010(2:end-1) ELSPOTh2010(1:end-2)];
%elspot_coef = (A0'*A0)\A0'*tmp2010.national(3:end)
%ELSPOTh2010t = ELSPOTh2010(3:end) - A0*elspot_coef;
%ELSPEC2010t2 = fspectrum3(ELSPOTh2010t,1)

A0 = [ELSPOTh2010(1:end-2)];
elspot_coef = (A0'*A0)\A0'*tmp2010.national(3:end)
ELSPOTh2010t = ELSPOTh2010(3:end) - A0*elspot_coef;
ELSPEC2010t2 = fspectrum3(ELSPOTh2010t,1)


% The fspectrum is unable to get a proper pscale for the data but fortu-
% nately I can use the pscale from the original data so I have the fol-
% lowing plot:

% This code works:
% (d=2)
%plot(ELSPECh2010.pscale(3:end), ELSPEC2010t2.spectrum')
%hold on
%plot(ELSPECh2010.pscale(3:end), ELSPECh2010.spectrum(3:end)', 'r')
% (d=1)
%plot(ELSPECh2010.pscale(2:end), ELSPEC2010t2.spectrum')
%hold on
%plot(ELSPECh2010.pscale(2:end), ELSPECh2010.spectrum(2:end)', 'r')

plot(ELSPECh2010.pscale(2:end), ELSPEC2010t2.spectrum')
hold on
plot(ELSPECh2010.pscale(2:end), ELSPECh2010.spectrum(2:end)', 'r')

