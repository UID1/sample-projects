%
%
%  Laboration i mekanik 2 2013
%  Stekroppssimulering av
%  asteroiden Toutatis
%
%

global I_p

w_0 = [20, 32, 98]*pi/180/24; % Vi r�knar i radianer per timme
m = 5*10^13;

% Kropp 1 - tv� sf�rer

I_sym = 2/5*m*([3.5 3.5 1]*(2500^2+1500^2));

I_p = I_sym;

options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4 1e-5]);
[T_s,w_s] = ode45(@wdot,[0 240],w_0,options); % R�relse under 5-dygn

%Kropp 2 - assymetrisk kropp

I_ass = 2/5*m*([4.5 2.5 1]*(2500^2+1500^2));

I_p = I_ass;

options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4 1e-5]);
[T_ass,w_ass] = ode45(@wdot,[0 240],w_0,options); % R�relse under 5-dygn

plot3(w_ass(:,1),w_ass(:,2), w_ass(:,3))
figure
plot3(w_s(:,1),w_s(:,2), w_s(:,3))
