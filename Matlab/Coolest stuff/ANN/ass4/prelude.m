% Artificial Neural Networks FFR135
% Examinator: Bernhard Mehlig 2015
% Chalmers University of Technology
%
% Assignment 4
% by Robin Axelsson
%

% We import training data for the network
% and normalize the inputs

tic
wb1 = waitbar(0,'Initializing ...');
CLwinsize = 1000;
SPwinsize = 10;
UP = 2;
DATA = importdata('data_classify.txt');

FIGS.fig1 = figure('Color', [1 1 1]);
FIGS.LineColors = [[123, 0, 0]; [138, 36, 55]; [199, 138, 127]; ...
                   [230, 229, 214]; [115, 115, 115]; ...
				   [176, 169, 165]; [211, 202, 183]]/255;

plot(DATA(find(DATA(:,1)==-1),2), DATA(find(DATA(:,1)==-1),3), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(DATA(find(DATA(:,1)==1),2), DATA(find(DATA(:,1)==1),3), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Locations of data points')
movegui(wb1, 'southeast');

% The weights are for 2 input nodes and 5 output nodes and are to be
% initially uniformly distributed over [-1, 1]

M = 2; % # input nodes
N = 5; % # output nodes/neurons
w_ki = 2*rand(M, N);
% Distance definitions for the neurons
%alpha_i = [ linspace(min(DATA(:,2)), max(DATA(:,2)), N); ...
%            linspace(max(DATA(:,3)), min(DATA(:,3)), N)];

% The competitive learning process

eta_cl = .02;
sigma_0cl = .6;
sigma_1cl = .1;
tau_cl = 1000/sigma_0cl;
steps_cl = 1e5;
steps_sp = 3000;

for i = 1:steps_cl
   sigma_cl = (i <= 1e4)*sigma_0cl + (i >1e4)*sigma_1cl*exp(-(i-1e4+1)/tau_cl);
   rndSel = randi(length(DATA));
   % The following is essentially equation (1)
   g_i = exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2) ...
		 / sum(exp(-sum((DATA(rndSel, 2:3)'*ones(1,N)-w_ki).^2)/2));
   i_0 = find(g_i == max(g_i));
   Lambda_ii0 = exp(-((1:N)-i_0).^2/(2*sigma_cl^2));
%   if isnan(prod(prod(w_ki))) || abs(prod(prod(w_ki))) == Inf
%      w_ki
%	  g_i
%	  i_0
%      keyboard
%   end
   deltaw_ki = eta_cl*((ones(M,1)*Lambda_ii0).*(DATA(rndSel, 2:3)'*ones(1,N)-w_ki));
   w_ki = w_ki + deltaw_ki;
   if mod(i, CLwinsize) == 0
      SPC = toc;
      waitbar(i/steps_cl, wb1, sprintf('Computing Hakonen network at %.2f iterations/s', CLwinsize/SPC));
	  tic;
   end
end

% The simple perceptron training process

W_i = 2*(rand(N,1)-.5);
beta_0 = .5;
theta = 0;
eta_sp = .1;
% Randomly split the input data into a training and validation matrix
TRNidx = randperm(length(DATA), floor(length(DATA)*.7));
TRN = DATA(TRNidx, :);
VLD = DATA;
VLD(TRNidx,:) = [];
% No performance enhancements with CUDA :(
gTRN = TRN(:,2:3); %gpuArray(TRN(2:3));
gVLD = VLD(:,2:3); %gpuArray(VLD(2:3));
gOTRN = TRN(:,1); %gpuArray(TRN(2:3));
gOVLD = VLD(:,1); %gpuArray(VLD(2:3));

diffDwTRN = ones(length(TRN), N, M);%, 'gpuArray');
diffDwVLD = ones(length(VLD), N, M);%, 'gpuArray');
CNO_muTRN = ones(length(TRN), N);%, 'gpuArray');
CNO_muVLD = ones(length(VLD), N);%, 'gpuArray');
PSO_muTRN = ones(length(TRN), 1);%, 'gpuArray');
PSO_muVLD = ones(length(VLD), 1);%, 'gpuArray');
gETRN = ones(steps_sp, 1);%, 'gpuArray');
gEVLD = ones(steps_sp, 1);%, 'gpuArray');
gCvTRN = ones(steps_sp, 1);%, 'gpuArray');
gCvVLD = ones(steps_sp, 1);%, 'gpuArray');
O_mu = ones(UP,1);
%ETRN = ones(steps_sp, 1);
%EVLD = ones(steps_sp, 1);
%CvTRN = ones(steps_sp, 1);
%CvVLD = ones(steps_sp, 1);
%O_muTRN = ones(length(TRN),1);
%O_muVLD = ones(length(VLD),1);


for i = 1:steps_sp
   IdxIter = randperm(length(TRN), UP);
   for j = 1:UP
      g_i(j,:) = exp(-sum((TRN(IdxIter(j), 2:3)'*ones(1,N)-w_ki).^2)/2) ...
                   / sum(exp(-sum((TRN(IdxIter(j), 2:3)'*ones(1,N)-w_ki).^2)/2));
      O_mu(j) = tanh(beta_0*(g_i(j,:)*W_i-theta));
   end
   derivO_mu = beta_0*(1-O_mu.^2);
   deltaW_i = eta_sp*g_i'*((TRN(IdxIter,1)-O_mu).*derivO_mu);
   W_i = W_i + deltaW_i;
   deltatheta = -eta_sp*sum((TRN(IdxIter,1)-O_mu)'*derivO_mu);
   theta = theta + deltatheta;
%   if mod(i, SPwinsize) == 0
   for ii = 1:size(w_ki, 2)
     for kk = 1:size(w_ki, 1)
        diffDwTRN(:,ii,kk) = (gTRN(:,kk)-w_ki(kk,ii)).^2;
        diffDwVLD(:,ii,kk) = (gVLD(:,kk)-w_ki(kk,ii)).^2;
	 end
   end
   CNO_muTRN = exp(-sum(diffDwTRN,3)/2)./repmat(sum(exp(-sum(diffDwTRN,3)/2), 2), 1, N);
   CNO_muVLD = exp(-sum(diffDwVLD,3)/2)./repmat(sum(exp(-sum(diffDwVLD,3)/2), 2), 1, N);
   PSO_muTRN = tanh(beta_0*(CNO_muTRN*W_i-theta));
   PSO_muVLD = tanh(beta_0*(CNO_muVLD*W_i-theta));
% The old fashioned but well tested way of computations   
%   for j = 1:length(TRN)
%      g_iTRN = exp(-sum((TRN(j,2:3)'*ones(1,N)-w_ki).^2)/2) ...
%	                     / sum(exp(-sum((TRN(j,2:3)'*ones(1,N)-w_ki).^2)/2));
%      O_muTRN(j) = tanh(beta_0*(g_iTRN*W_i-theta));
%   end
%   for j = 1:length(VLD)
%      g_iVLD = exp(-sum((VLD(j, 2:3)'*ones(1,N)-w_ki).^2)/2) ...
%	                     / sum(exp(-sum((VLD(j,2:3)'*ones(1,N)-w_ki).^2)/2));
%      O_muVLD(j) = tanh(beta_0*(g_iVLD*W_i-theta));
%   end
%   ETRN(i) = .5*sum((TRN(:,1)-O_muTRN(:)).^2);
%   EVLD(i) = .5*sum((VLD(:,1)-O_muVLD(:)).^2);
%   CvTRN(i) = mean(abs(TRN(:,1) - sign(O_muTRN)));
%   CvVLD(i) = mean(abs(VLD(:,1) - sign(O_muVLD)));

      gETRN(i) = .5*sum((gOTRN(:,1)-PSO_muTRN(:)).^2);
      gEVLD(i) = .5*sum((gOVLD(:,1)-PSO_muVLD(:)).^2);
      gCvTRN(i) = mean(abs(gOTRN(:,1) - sign(PSO_muTRN)));
      gCvVLD(i) = mean(abs(gOVLD(:,1) - sign(PSO_muVLD)));

	  if mod(i, SPwinsize) == 0
         SPC = toc;
         waitbar(i/steps_sp, wb1, sprintf('Training simple perceptron at %.2f iterations/s', SPwinsize/SPC));
	     tic;
	  end
%   end
end
close(wb1);

% Provoke the system to return values given that the weights are set
% in a Monte-Carlo simulation

SimSize = 1000;
O_MC = ones(SimSize,1);
INP = [(max(DATA(:,2))-min(DATA(:,2)))*rand(SimSize,1)+min(DATA(:,2)) ...
           (max(DATA(:,3))-min(DATA(:,3)))*rand(SimSize,1)+min(DATA(:,3)) ];
for i = 1:SimSize
   g_i = exp(-sum((INP(i,:)'*ones(1,N)-w_ki).^2)/2) ...
		 / sum(exp(-sum((INP(i,:)'*ones(1,N)-w_ki).^2)/2));
   O_MC(i) = tanh(beta_0*(g_i*W_i-theta));
end

TST = [INP(:,1) INP(:,2) sign(O_MC)];
FIGS.fig7 = figure('Color', [1 1 1]);
plot(TST(find(TST(:,3)==-1),1), TST(find(TST(:,3)==-1),2), 'o', 'color', FIGS.LineColors(1,:))
hold on
plot(TST(find(TST(:,3)==1),1), TST(find(TST(:,3)==1),2), 'o', 'color', FIGS.LineColors(4,:))
title('\fontsize{12}Returned values from Monte-Carlo simulation')
