% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% This Matlab script  is intended to review  the results of the computa-
% tions in the 'main3.m' script.

% We start by loading the data:

load amraxdata001.mat

% Get the temperature data for year 2010

tmp2010 = temp_proc(8760, 'cubic');

% We set up the configuration for our plots

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;


FIGS.fig1 = figure('Color',[1 1 1]);
plot(20-tmp2010.national, 'Color', FIGS.LineColors(2,:))
set(gca, 'XTick', 0:3*730:8760)
set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
                    'April', 'July', ...
                    'October', 'December'})
xlim([0 8760])
ylabel(['\fontsize{12}\Delta\itt\rm ' ...
                   '(\itt\rm\fontsize{6}indoor\fontsize{12} - \itt' ...
		   '\rm\fontsize{6}outdoor\fontsize{12})'])
title(['\fontsize{12}Average national temperature ' ...
       'vs. room temperature 2010'])

FIGS.fig2 = figure('Color',[1 1 1]);
surf(par1a.aicmtx(:,:,1))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on 1 year data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)


FIGS.fig3 = figure('Color',[1 1 1]);
surf(par1a.aicmtx(:,:,2))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on 1 year data at \itr\rm = 2'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig4 = figure('Color',[1 1 1]);
surf(par1a.aicmtx(:,:,3))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on 1 year data at \itr\rm = 3'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig5 = figure('Color',[1 1 1]);
surf(par1c.aicmtx(:,:,1))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on deseasonalized 1 year data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig6 = figure('Color',[1 1 1]);
surf(par1c.aicmtx(:,:,2))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       'on deseasonalized 1 year data at \itr\rm = 2'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig7 = figure('Color',[1 1 1]);
surf(par1c.aicmtx(:,:,3))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on deseasonalized 1 year data at \itr\rm = 3'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)





FIGS.fig8 = figure('Color',[1 1 1]);
surf(par2a.aicmtx(:,:,1))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on first 3 months of data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig9 = figure('Color',[1 1 1]);
surf(par2a.aicmtx(:,:,2))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on first 3 months of data at \itr\rm = 2'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig10 = figure('Color',[1 1 1]);
surf(par2a.aicmtx(:,:,3))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on first 3 months of data at \itr\rm = 3'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig11 = figure('Color',[1 1 1]);
surf(par2c.aicmtx(:,:,1))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on first 3 months of deseasonalized data at \itr\rm = 1'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig12 = figure('Color',[1 1 1]);
surf(par2c.aicmtx(:,:,2))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on first 3 months of deseasonalized data at \itr\rm = 2'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

FIGS.fig13 = figure('Color',[1 1 1]);
surf(par2c.aicmtx(:,:,3))
view(227.5, 30)
title(['\fontsize{12}AIC surface for ARMAX(\itp\rm,\itq\rm,\itr\rm)' ...
       ' on first 3 months of deseasonalized data at \itr\rm = 3'])
xlabel('\itp', 'FontSize', 12)
ylabel('\itq', 'FontSize', 12)

