function newPopulation = InsertBestIndividual(population, bestChromosome, numberOfCopies)
  newPopulation = population;
  newPopulation(1:numberOfCopies, :) = ...
                            meshgrid(bestChromosome, 1:numberOfCopies);
end
