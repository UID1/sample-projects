% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% This matlab script evaluates and compares moving average models of
% different orders.

%
% /----------- : BEGIN : Preparations --------------------------------/
%
%

if ~any(strcmp(who,'ELSPOTh2010'))
   fid1 = fopen('elspoth2010.bin', 'r');
   ELSPOTh2010 = fread(fid1, 8760, 'double');
   fclose(fid1);
end
if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
deltaELmin = 1e7;
for k = 1:length(ELSPOTh2010)-90*24
	deltaEL = max(ELSPOTh2010(k:k+90*24)) - ...
	          min(ELSPOTh2010(k:k+90*24));
	if deltaEL < deltaELmin
		deltaELmin = deltaEL;
		kmin = k;
	end
end
min90 = (kmin:kmin+90*24);

ELSPEC1 = fspectrum3(ELSPOTh2010(min90), 1);
ELTREND = est_ma(ELSPOTh2010(min90), 500);
ELSPOTdt = ELSPOTh2010(min90)-ELTREND.removable;
ELSPECdt = fspectrum3(ELSPOTdt, 1);
TMPSPEC = fspectrum3(tmp2010.national(min90), 1);

[filt.n1, filt.d1] = butter(2, [450 750]/5e4, 'stop');
[filt.n2, filt.d2] = butter(2, [4100 4300]/5e4, 'stop'); % Partial
[filt.n3, filt.d3] = butter(2, [8350 8420]/5e4, 'stop'); % Max partial
[filt.n4, filt.d4] = butter(2, [1150 1250]/5e4, 'stop');

ELhdes = filter(filt.n1, filt.d1, ...
	               filter(filt.n2, filt.d2, ...
		                ELSPOTh2010(kmin-300:kmin+90*24)));
for i = 1:2
   ELhdes = filter(filt.n3, filt.d3, ...
                        filter(filt.n1, filt.d1, ...
                                ELhdes));
   ELhdes = filter(filt.n3, filt.d3, ELhdes);
end
ELSPECdes = fspectrum3(ELhdes, 1);
ELSPOTds = ELhdes(301:end);
ELTRENDds = est_ma(ELSPOTds, 500);
ELSPOTdsdt = ELhdes(301:end)-ELTRENDds.removable;
ELSPECdsdt = fspectrum3(ELSPOTdsdt, 1);

ELloghds = (ELSPOTds);
ELTRENDdslog = est_ma(ELloghds, 500);
ELloghdsdt = (ELloghds - ELTRENDdslog.removable);
ELloghdsdt = ELloghdsdt/1000;

%
% /----------- :  END  : Preparations --------------------------------/
%
%


%m1a = armax(ELSPOTdsdt, [0, 1]);
%m1b = armax(ELSPOTdsdt, [0, 5]);
%m1c = armax(ELSPOTdsdt, [0, 10]);
%m1d = armax(ELSPOTdsdt, [0, 15]);
m1a = [1 1*ones(1, 1)];
m1b = [1 1*ones(1, 5)];
m1c = [1 1*ones(1, 25)];
m1d = [1 1*ones(1, 100)];

edata = cumsum(randn(1, 10000));

sim1a = armaxsiman2(m1a, 'nil', edata, 10000, 1, 0);
sim1b = armaxsiman2(m1b, 'nil', edata, 10000, 1, 0);
sim1c = armaxsiman2(m1c, 'nil', edata, 10000, 1, 0);
sim1d = armaxsiman2(m1d, 'nil', edata, 10000, 1, 0);

% We set up the configuration for our plots

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

% We begin with plotting the Random walk iterary

FIGS.fig1 = figure('Color',[1 1 1]);
plot(sim1a(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(1)')
set(gca, 'FontSize', 24)
set(gca, 'YTick', [])
set(gca, 'XTick', [])

FIGS.fig2 = figure('Color',[1 1 1]);
plot(sim1b(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(5)')
set(gca, 'FontSize', 24)
set(gca, 'YTick', [])
set(gca, 'XTick', [])

FIGS.fig3 = figure('Color',[1 1 1]);
plot(sim1c(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(25)')
set(gca, 'FontSize', 24)
set(gca, 'YTick', [])
set(gca, 'XTick', [])

FIGS.fig4 = figure('Color',[1 1 1]);
plot(sim1d(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(100)')
set(gca, 'FontSize', 24)
set(gca, 'YTick', [])
set(gca, 'XTick', [])

% We move on to plot the white noise

edata2 = randn(1, 10000);

sim2a = armaxsiman2(m1a, 'nil', edata2, 10000, 1, 0);
sim2b = armaxsiman2(m1b, 'nil', edata2, 10000, 1, 0);
sim2c = armaxsiman2(m1c, 'nil', edata2, 10000, 1, 0);
sim2d = armaxsiman2(m1d, 'nil', edata2, 10000, 1, 0);

FIGS.fig5 = figure('Color',[1 1 1]);
plot(sim2a(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(1)')
set(gca, 'FontSize', 24)
ylim([min(sim2a(100:end)) max(sim2a(100:end))])
set(gca, 'YTick', [])
set(gca, 'XTick', [])

FIGS.fig6 = figure('Color',[1 1 1]);
plot(sim2b(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(5)')
set(gca, 'FontSize', 24)
ylim([min(sim2b(100:end)) max(sim2b(100:end))])
set(gca, 'YTick', [])
set(gca, 'XTick', [])

FIGS.fig7 = figure('Color',[1 1 1]);
plot(sim2c(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(25)')
set(gca, 'FontSize', 24)
ylim([min(sim2c(100:end)) max(sim2c(100:end))])
set(gca, 'YTick', [])
set(gca, 'XTick', [])

FIGS.fig8 = figure('Color',[1 1 1]);
plot(sim2d(100:end), 'Color', FIGS.LineColors(2,:))
title('\fontsize{18}\fontname{Arial}MA(100)')
set(gca, 'FontSize', 24)
ylim([min(sim2d(100:end)) max(sim2d(100:end))])
set(gca, 'YTick', [])
set(gca, 'XTick', [])

