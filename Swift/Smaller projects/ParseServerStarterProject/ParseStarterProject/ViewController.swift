/**
* Copyright (c) 2015-present, Parse, LLC.
* All rights reserved.
*
* This source code is licensed under the BSD-style license found in the
* LICENSE file in the root directory of this source tree. An additional grant
* of patent rights can be found in the PATENTS file in the same directory.
*/

import UIKit
import Parse

class ViewController: UIViewController {
    var signupMode = true
    var activityIndicator = UIActivityIndicatorView()
    
    @IBOutlet weak var emailTextField: UITextField!

    @IBOutlet weak var passwordTextField: UITextField!
    
    private func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func signupOrLogin(_ sender: Any) {
        if emailTextField.text == "" || passwordTextField.text == "" {
            createAlert(title: "Error in form", message: "Please enter a proper email and password")
        } else {
            activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            if signupMode {
                // Signup
                let user = PFUser()
                user.username = emailTextField.text
                user.email = emailTextField.text
                user.password = passwordTextField.text
                user.signUpInBackground(block: { (success, error) in
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if error != nil {
                        if let errorContainer = error as? NSError {
                            self.createAlert(title: "Submission failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                        } else {
                            self.createAlert(title: "Submission failure", message: "Please try again later.")
                        }
                    } else {
                        print("User signed up")
                        self.performSegue(withIdentifier: "showUserTable", sender: self)
                        
                    }
                    
                })
                
            } else {
                // Login
                PFUser.logInWithUsername(inBackground: emailTextField.text!, password: passwordTextField.text!, block: { (user, error) in
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if error != nil {
                        if let errorContainer = error as? NSError {
                            self.createAlert(title: "Login failure", message: "Code \(errorContainer.code): \(errorContainer.localizedDescription)")
                        } else {
                            self.createAlert(title: "Login failure", message: "Please try again later.")
                        }
                    } else {
                        print("User logged in")
                        self.performSegue(withIdentifier: "showUserTable", sender: self)
                    }
                })
            }
        }
    }
    @IBOutlet weak var signupOrLogin: UIButton!
    @IBAction func changeSignupMode(_ sender: Any) {
        if signupMode {
            // Change to login mode
            
            signupOrLogin.setTitle("Log In", for: [])
            changeSignupButton.setTitle("Sign Up", for: [])
            messageLabel.text = "Don't have an account?"
            signupMode = false
        } else {
            signupOrLogin.setTitle("Sign Up", for: [])
            changeSignupButton.setTitle("Log In", for: [])
            messageLabel.text = "Already have an account?"
            signupMode = true

        }
    }
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var changeSignupButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Save an entry into the parse server
        /*
        let userObject = PFObject(className: "Users")
        userObject["User"] = "Haaplo"
        userObject.saveInBackground {
            (success, error) -> Void in
            
            if success {
                print("Object has been saved to parse server")
            } else {
                if error != nil {
                    print(error ?? "Error: (no error description)")
                } else {
                    print("Object failed to save but no error was detected")
                }
            }
        }*/
        
        // Fetch the data from the parse server
        /*let query = PFQuery(className: "Users")
        query.getObjectInBackground(withId: "CHh28BJEik") { (object, error) in
                if error != nil {
                    print(error ?? "Error: (no error description)")
                } else {
                    if let userObject = object {
                        print(userObject)
                        print(userObject["User"])
                        
                        userObject["User"] = "Haaplu"
                        userObject.saveInBackground(block: { (success, error) in
                            if success {
                                print("Object has been saved to parse server")
                            } else {
                                if error != nil {
                                    print(error ?? "Error: (no error description)")
                                } else {
                                    print("Object failed to save but no error was detected")
                                }
                            }
                        })
                    } else {
                        print("Failed to retrieve object but no error was reported")
                }
            }
        }*/
    }
    override func viewDidAppear(_ animated: Bool) {
        if PFUser.current()?.username != nil {
            performSegue(withIdentifier: "showUserTable", sender: self)
        }
        // Hide the navigation bar at the top of the screen
        self.navigationController?.navigationBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
