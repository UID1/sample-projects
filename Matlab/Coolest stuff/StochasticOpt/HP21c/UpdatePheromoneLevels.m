function newPheromoneLevels = UpdatePheromoneLevels(oldPheromoneLevels,deltaPheromoneLevel,rho)
  newPheromoneLevels = (1 - rho) * oldPheromoneLevels + deltaPheromoneLevel;
end