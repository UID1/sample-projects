function initialPheromoneLevels = InitializePheromoneLevels(numberOfCities, tau0)
  initialPheromoneLevels = tau0*~eye(numberOfCities);
end