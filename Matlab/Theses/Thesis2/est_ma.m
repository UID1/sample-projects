function output = est_ma(X_t, ma_order)
% SYNOPSIS
% 
%  output = EST_MA(X_t, ma_order)
%
% DESCRIPTION
%
%  This is a function that estimates the moving average of a time-series
%
% INPUT
% 
%   X_t      : input data
%   ma_order : the desired order of the moving average to be estimated
%
% OUTPUT
%
%  output           : the output data of the moving average estimation
%  output.precise   : the moving average is made as precise as
%                     possible by allowing the MA window to shrink at
%                     both ends of X_t
%  output.removable : the moving average estimated using a rigid window
%                     so the MA is be constant at the endpoints
%  output.leftprec  : the moving average is 'precise' only at the
%                     left endpoint, i.e. at the beginning of X_t
%  output.rightprec : the moving average is 'precise' only at the
%                     right endpoint, i.e. at the end of X_t
%
% NOTES
%
%  Designed for the final master thesis project in Physics to study
%  model estimation of electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011

SZ = size(X_t);
if SZ(2) > 1
	X_t=X_t';
end

if mod(ma_order, 2) == 0
	m1_vect = [.5 ones(1, ma_order-1) .5]';
else
	m1_vect = ones(ma_order, 1);
end
m1 = ones(1,length(X_t)-ma_order);
q1 = fix(ma_order/2);
for i = q1+1:length(X_t)-q1
	m1(i-q1) = X_t(i-q1:i+q1)'*m1_vect/ma_order;
end

ma_beg = cumsum(X_t(1:q1)')./(1:q1);
ma_end = fliplr(cumsum(X_t(end:-1:end-q1+1)')./(1:q1));

output.precise = [ma_beg m1 ma_end]';
output.removable = [m1(1)*ones(1,q1) m1 m1(end)*ones(1,q1)]';
output.leftprec = [ma_beg m1 m1(end)*ones(1,q1)]';
output.rightprec = [m1(1)*ones(1,q1) m1 ma_end]';

end % est_ma
