function [data_out,coef] = harvey_pierse( data1, data2, freq, p, q, flow )

% HARVEY_PIERSE.M 
% function [data_out,coefs] = harvey_pierse(data1,data2,freq,p,q,flow)
%
% DESCRIPTION:
% This program uses Harvey and Pierse (1984) method to estimate missing
% observations in economic time series based on an ARMA model. 
% The function finds the MLE estimates of the parameters of an ARMA model
% when some observations are missing or subject to temporal aggregation.
% The function also produces estimates of the missing observations.
% 
% The input series has to be divided in two parts: 1) the section of the
% series that does not contain missing observation, which is the last part
% of the sample (data2), and 2) the first part of the series that contains 
% missing observations or temporally aggregated data.
% It is assumed that the missing data are at the beginning of the sample.
% For example, macro series are now available at a quarterly frequency;
% however, some time ago these series were available only at an annual
% frequency. The idea is to estimate quarterly observations to attach to
% the initial periods.
% EXAMPLE: Suppose we have 5 years of data of a single time series as 
% follows: for years 1 and 2 we only have annual observations (2 points), 
% but for years 3, 4, and 5 we have quarterly observations (12 points):
%
%
%    y_t  -----|-----------|-----|--|--|--|--|--|--|--|--|--|--|--|--
%         \________/  \________/ \________/  \________/  \________/ 
%           year 1      year 2      year 3     year 4      year 5
%
% The idea is to view the quarterly observations in years 1 and 2 as
% missing observations, or subject to temporal aggregation. The aim of this
% program is to estimate values for the quarterly observations in year 1
% and 2.
%
% IMPORTANT: This function assumes that the data is stationary and both
% stock variables and aggregate data is allocated to the last subperiod of
% a given period (e.g. if want to find quarterly observations and we have
% yearly data, I attach the stock value (or the aggregate data) to the last
% quarter.
% 
% INPUTS:
% 1) data1 = data at the beginning of the sample in the lower frequency.
%            (In the example above: 2 data points for years 1 and 2.)
% 2) data2 = data at the end of the sample, in higher frequency.
%            (In the example above: 12 data points for years 3, 4 and 5.)
% 3) freq  = how many periods in frequency of data 2 has each period in
%            frequency of data 1. In the case of a flow variable, this is 
%            the number of periods over which the variable is aggregated.
%            (In the example above freq=4 for both flow and stock vars.)
% 4) p     = order of autoregressive part of ARMA.
% 5) q     = order of moving average part of ARMA.
% 6) flow  = determines whether the time series is a flow or a stock.
%            flow > 0, the series is a flow, flow=0, the series is a stock. 
% 
% OUTPUTS:
% 1) data_out : the data with the estimated missing observations included.
%               (In the example above, this is a time series with 20
%               observations that include the quarterly estimates for years
%               1 and 2.)
% 2) coefs : Structure with the estimated coefficients of the ARMA model.
%            coefs.ar contains the estimated autoregressive coefficients; 
%            coefs.ma, the estimated moving average coefficients; coefs.sdev, 
%            the estimated standard deviation of the innovations, and coefs.L
%            the value of the log-likelihood.
%
% Written by Constantino Hevia, March 17, 2008.
%
% REFERENCES: 
% 1) E.J. Hannan and L. Kavalieris. "A Method for Autoregressive-Moving
%      Average Estimation" Biometrika, Vol 71, No2 Aug 1984.
% 2) A.C. Harvey and R.G. Pierse. "Estimating Missing Observations in 
%      Economic Time Series". Journal of the American Statistical
%      Association, March 1984, Volume 79, Number 385.
% 3) R.H. Jones. "Maximum Likelihood Fitting of ARMA Models to Time Series
%      With Missing Observations". Technometrics, VOL 22, No3 August 1980.
% 

T1 = max(size(data1));  % Number of observation in first sample
T2 = max(size(data2));  % Number of observations in second sample
T = T1*freq+T2; % Total number of observations (including missing values)
r = max(p,q+1);

% Stack all series in 'y'. Put NaN to missing values
y = NaN(T,1);
y(freq*T1+1:T)=data2;
for t=1:T1
    y(freq*t) = data1(t);   % assign values of data1 to y
end

% -------------------------------------------------------------
% INITIALIZE PARAMETERS OF ARMA MODEL USING DISSAGREGATED DATA
% AND THE METHOD OF HANNAN AND KAVALIERIS
coefs = initialize_arma;

% Check if need to invert sample for ML estimation
if flow > 0     %invert sample
    temp = y; y=zeros(T,1); 
    for t=1:T 
        y(t) = temp(T-t+1);
    end
    clear temp
end

% Create vector storing information about missing values
observ = (1-isnan(y));      % observ(i)=1 if there is data, =0 if not

% ---------------------------------------------------------------------
% FIND MLE ESTIMATES
%options = optimset('LargeScale','off','Display','iter');
options = optimset('LargeScale','off','Display','off');
coefs1 = fminunc( @log_likelihood, coefs, options );

% Estimate standard deviation of ARMA
[ L sigmahat] = log_likelihood( coefs1 );

coef.ar     = coefs1(1:p);
coef.ma     = coefs1(p+1:p+q);
coef.sdev   = sigmahat;
coef.loglik = -L;

% ---------------------------------------------------------------------
% FIND SMOOTHED ESTIMATES OF MISSING OR AGGREGATED OBSERVATIONS
data_out = fixed_point_smoothing( coefs1 );


% Check if need to invert sample again
if flow > 0     %invert sample
    temp = data_out; data_out=zeros(T,1); 
    for t=1:T 
        data_out(t) = temp(T-t+1);
    end
end

% -------------------------------------------------------------------------
% INTERNAL FUNCTIONS

    function coefs = initialize_arma
        % This function uses Hannan and McDougall . " Regression Procedures
        % for ARMA estimation". Journal of the American  Statistical 
        % Association. June 1988, Vol 83, No 402.

        % Step 1: Choose order of autoregression.
        best = 1e+10;
        for h = p : ceil(log(T2)^1.5)
            Y = data2(h+1:T2);
            X = [];
            for j=1:h
                X = [X data2(h+1-j : T2-j)];
            end
            bet = (X'*X)\X'*Y;
            res = Y - X*bet;
            sigma2 = (res'*res)/length(res);
            BIC = log(sigma2) + h*log(T2)/T2;   % information criterium
            if BIC < best
                horder=h; best = BIC; residuals = res;
            end
        end
        
        % Step2: Run regression on lagged values and residuals
        nlag = max(p,q);
        Y = data2(horder+nlag+1:T2);
        X = [];
        for i=1:p 
            X = [X data2(horder+nlag+1-i:T2-i)];
        end
        for i=1:q
            X = [X residuals(nlag+1-i:T2-horder-i)];
        end
        bet = (X'*X)\X'*Y;
        coefs(1:p) = bet(1:p);
        coefs(p+1:p+q) = bet(p+1:p+q);
        coefs = coefs';
    end

            % --------------------------------------------

    function [ L, sigmahat] = log_likelihood( coefs )
        % This function computes the log-likelihood of the ARMA model when
        % there are missing observations in a stock variable or temporal
        % aggregation in a flow variable.
        
        phi   = coefs(1:p);
        theta = coefs(p+1:p+q);

        % Build Matrices of state space representation
        %
        %      x[t+1] = PHI x[t] + THETA eps[t+1]
        %      y[t]   = Z'x[t]
        
        PHI = zeros(r,r); THETA = zeros(r,1); Z = zeros(r,1);
        PHI(1:p,1) = phi; PHI(1:r-1,2:r) = eye(r-1);
        THETA(1) = 1; THETA(2:q+1) = theta;
        Z(1) = 1; Q = THETA*THETA';    
        
        % Initialize state and covariance matrix
        xhat_tt  = zeros(r,1);      % E( x(t) | y(1),...,y(t), x(0))
        Sigma_tt = reshape( (eye(r^2)-kron(PHI,PHI) ) \ Q(:), r, r );
        logsum = 0;
        sumsq  = 0;
 
        if flow == 0        % For stock variables
            
            for i = 0 : T-1
                xhat_t1t  = PHI*xhat_tt;    % E( x(t+1) | y(1),...,y(t), x(0))
                Sigma_t1t = PHI*Sigma_tt*PHI' + Q;
                yhat_t1t  = Z'* xhat_t1t;
                if observ(i+1)            % If there is data in i+1
                    omega     = Z'*Sigma_t1t*Z;
                    delta_t1  = Sigma_t1t*Z/omega;
                    innov     = y(i+1) - yhat_t1t;
                    xhat_t1t1 = xhat_t1t + delta_t1*innov;
                    Sigma_t1t1= Sigma_t1t - delta_t1*Z'*Sigma_t1t;
                    % Add likelihood terms:
                    logsum = logsum + log(omega);
                    sumsq  = sumsq  + innov^2/omega;                    
                    % Update estimates;
                    xhat_tt  = xhat_t1t1; 
                    Sigma_tt = Sigma_t1t1;
                else    % if there is missing (stock) data
                    % Update only xhat and Sigma
                    xhat_tt  = xhat_t1t; 
                    Sigma_tt = Sigma_t1t;
                end
            end
                        
        else    % flow > 0. IF WE OBSERVE AGGREGATED DATA
            
            % In this case we run the estimation backwards until T2
            % arrives, then we move to the transformed model. See Harvey
            % and Pierse (1984).
        
            % I) DISSAGREGATED SUBSAMPLE (no missing observations here)
            for i = 0 : T2-1
                xhat_t1t  = PHI*xhat_tt;
                Sigma_t1t = PHI*Sigma_tt*PHI' + Q;
                yhat_t1t  = Z'* xhat_t1t;
                omega     = Z'*Sigma_t1t*Z;
                delta_t1  = Sigma_t1t*Z/omega;
                innov     = y(i+1) - yhat_t1t;
                xhat_t1t1 = xhat_t1t + delta_t1*innov;
                Sigma_t1t1= Sigma_t1t - delta_t1*Z'*Sigma_t1t;
               
                % Add likelihood terms:
                logsum = logsum + log(omega);
                sumsq  = sumsq  + innov^2/omega;                    
                
                % Update estimates;
                xhat_tt  = xhat_t1t1; 
                Sigma_tt = Sigma_t1t1;
            end
           
            % II) NOW FOCUS ON THE SUBSAMPLE WITH AGGREGATED DATA
            % Construct augmented state-space model
            PHI2   = zeros(r+freq-1); THETA2 = zeros(r+freq-1,1); 
            Z2     = zeros(r+freq-1,1);
            PHI2(1:r,1:r) = PHI; PHI2(r+1,1)=1; 
            PHI2( r+2:r+freq-1, r+1:r+freq-2 ) = eye(freq-2);
            THETA2(1:r) = THETA; 
            Q2 = THETA2*THETA2';
            Z2(1)=1; Z2(r+1:end)=1;
            
            % Initialize augmented state xhat2_T2T2:
            xhat2_tt  = zeros(r+freq-1,1);
            xhat2_tt(1:r) = xhat_tt;     % First part is forecast T2|T2
            for j=1:freq-1
                xhat2_tt( j + r ) = y( T2-j );
            end
            
            % Initialize Sigma2_T2T2
            Sigma2_tt =  zeros(r+freq-1);
            Sigma2_tt(1:r,1:r) = Sigma_t1t1;    % Initialize Sigma2_T2T2
            
            % Compute Kalman Filter Recursion
            for j = 0 : freq*T1-1
                xhat2_t1t  = PHI2*xhat2_tt;
                Sigma2_t1t = PHI2*Sigma2_tt*PHI2' + Q2;
                yhat2_t1t  = Z2'* xhat2_t1t;                
                if observ(T2+j+1)            % If there is data in T2+j+1
                    omega     = Z2'*Sigma2_t1t*Z2;
                    delta_t1  = Sigma2_t1t*Z2/omega;
                    innov     = y(T2+j+1) - yhat2_t1t;
                    xhat2_t1t1 = xhat2_t1t + delta_t1*innov;
                    Sigma2_t1t1= Sigma2_t1t - delta_t1*Z2'*Sigma2_t1t;
                    % Add likelihood terms:
                    logsum = logsum + log(omega);
                    sumsq  = sumsq  + innov^2/omega;                    
                    % Update estimates and MSE matrix
                    xhat2_tt  = xhat2_t1t1; 
                    Sigma2_tt = Sigma2_t1t1;
                else    % if there is no observable data
                    % Update only xhat2 and Sigma2
                    xhat2_tt  = xhat2_t1t; 
                    Sigma2_tt = Sigma2_t1t;
                end
            end
        end
        
        L = logsum + (T1+T2)*log(sumsq);  % Concentrated likelihood
        sigmahat = sqrt(sumsq/(T1+T2));   % Estimate of standard deviation

    end  % end function log_likelihood

                % --------------------------------------------
                
    function y_out = fixed_point_smoothing( coefs )
        % This function computes the smoothed estimated of the missing or
        % aggregated variables. See Harvey and Pierse (1984) pp. 127-128.
        
        phi   = coefs(1:p);
        theta = coefs(p+1:p+q);

        % Build Matrices of state space representation
        %
        %      x[t+1] = PHI x[t] + THETA eps[t+1]
        %      y[t]   = Z'x[t]
        
        PHI = zeros(r,r); THETA = zeros(r,1); Z = zeros(r,1);
        PHI(1:p,1) = phi; PHI(1:r-1,2:r) = eye(r-1);
        THETA(1) = 1; THETA(2:q+1) = theta;
        Z(1) = 1; Q = THETA*THETA';    
        
        % Initialize state and covariance matrix
        xhat_tt  = zeros(r,1);      % E( x(t) | y(1),...,y(t), x(0))
        Sigma_tt = reshape( (eye(r^2)-kron(PHI,PHI) ) \ Q(:), r, r );
       
        if flow == 0        % IF MISSING OBSERVATION IS A STOCK VARIABLE
            
            xhat_t1t  = zeros(r,T);     % Store here kalman estimates
            Sigma_t1t = zeros(r,r,T);   % Store here MSE
            omega     = zeros(1,T);     % mse innovation
            innov     = zeros(1,T);     % Array with innovations

            % COMPUTE KALMAN FILTER MATRICES
            for i = 0 : T-1
                   
                xhat_t1t(:,i+1)    = PHI*xhat_tt;
                Sigma_t1t(:,:,i+1) = PHI*Sigma_tt*PHI' + Q;
                yhat_t1t  = Z'* xhat_t1t(:,i+1);
                
                if observ(i+1)            % If there is data in i+1
                    omega(i+1) = Z'*Sigma_t1t(:,:,i+1)*Z;
                    delta_t1   = Sigma_t1t(:,:,i+1)*Z/omega(i+1);
                    innov(i+1) = y(i+1)-yhat_t1t;
                    xhat_t1t1  = xhat_t1t(:,i+1) + delta_t1*innov(i+1);
                    Sigma_t1t1 = Sigma_t1t(:,:,i+1) - delta_t1*Z'*Sigma_t1t(:,:,i+1);         
                    % Update estimates;
                    xhat_tt  = xhat_t1t1; 
                    Sigma_tt = Sigma_t1t1;
                else    % if there is missing (stock) data
                    % Update only xhat and Sigma
                    xhat_tt  = xhat_t1t(:,i+1); 
                    Sigma_tt = Sigma_t1t(:,:,i+1);
                    
                end
            end
            % COMPUTE SMOOTHED ESTIMATES
            y_out = y;
            for j=1:T
                if ~observ(j)
                    xsmooth = xhat_t1t(:,j);  % Initialize for smoothing
                    Sigmaa = Sigma_t1t(:,:,j);  % initialize Sigma augmented
                    for k = j:T
                        if observ(k)
                            kgaina  = Sigmaa*Z/omega(k);
                            xsmooth = xsmooth + kgaina*innov(k);
                            Sigmaa  = Sigmaa*( PHI - Sigma_t1t(:,:,k)*Z*Z'/ omega(k) );
                        else
                            Sigmaa  = Sigmaa*PHI;
                        end
                    end
                    y_out(j) = xsmooth(1);      % Fixed point smoothing
                end
            end      
            
        else       % IF WE HAVE PROBLEM OF AGGREGATED VARIABLES
            
            
            % COMPUTE KALMAN RECURSION FOR DISSAGREGATED SAMPLE
            for i=0:T2-1
                xhat_t1t  = PHI*xhat_tt;
                Sigma_t1t = PHI*Sigma_tt*PHI' + Q;
                yhat_t1t  = Z'*xhat_t1t;
                omega     = Z'*Sigma_t1t*Z;
                delta_t1  = Sigma_t1t*Z/omega;
                innov     = y(i+1) - yhat_t1t;
                xhat_t1t1 = xhat_t1t + delta_t1*innov;
                Sigma_t1t1= Sigma_t1t - delta_t1*Z'*Sigma_t1t;
               
                % Update estimates;
                xhat_tt  = xhat_t1t1; 
                Sigma_tt = Sigma_t1t1;
            end
            
            % Construct augmented state-space model
            PHI2   = zeros(r+freq-1); THETA2 = zeros(r+freq-1,1); 
            Z2     = zeros(r+freq-1,1);
            PHI2(1:r,1:r) = PHI; PHI2(r+1,1)=1; 
            PHI2( r+2:r+freq-1, r+1:r+freq-2 ) = eye(freq-2);
            THETA2(1:r) = THETA; 
            Q2 = THETA2*THETA2';
            Z2(1)=1; Z2(r+1:end)=1;
            
            % Initialize augmented state xhat2_T2T2:
            for j=1:freq-1
                xhat_tt(j+r) = y(T2-j);
            end
            
            % Initialize Sigma_T2T2 for augmented model
            %Sigma_tt =  zeros(r+freq-1);
            %Sigma_tt(1:r,1:r) = Sigma_t1t1;    % Initialize Sigma2_T2T2
            
            % Seems to work better at the break point with this initial condition:
            Sigma_tt = reshape( (eye((r+freq-1)^2)-kron(PHI2,PHI2) ) \ Q2(:), r+freq-1, r+freq-1 );
            
            % Now store arrays with forecasts, since we will need them for
            % the part of the sample with aggregated data.
            xhat_t1t  = zeros(r+freq-1,freq*T1 );           % Store here kalman estimates
            Sigma_t1t = zeros(r+freq-1,r+freq-1,freq*T1);   % Store here MSE
            omega     = zeros( 1, freq*T1 );  % MSE innovation
            innov     = zeros(1,freq*T1);     % Array with innovations
            
            for j = 0 : freq*T1-1
                xhat_t1t(:,j+1) = PHI2*xhat_tt;
                Sigma_t1t(:,:,j+1) = PHI2*Sigma_tt*PHI2'+Q2;
                yhat_t1t = Z2'*xhat_t1t(:,j+1);
               if observ(T2+j+1)    % There is data in T2+j+1
                   omega(j+1) = Z2'*Sigma_t1t(:,:,j+1)*Z2;
                   delta_t1 = Sigma_t1t(:,:,j+1)*Z2/omega(j+1);
                   innov(j+1) = y(T2+j+1)-yhat_t1t;
                   xhat_t1t1  = xhat_t1t(:,j+1) + delta_t1*innov(j+1);
                   Sigma_t1t1 = Sigma_t1t(:,:,j+1) - delta_t1*Z2'*Sigma_t1t(:,:,j+1);
                   % Update estimates and MSE matrix
                   xhat_tt = xhat_t1t1;
                   Sigma_tt = Sigma_t1t1;
               else                 % If there is no observations
                   % Only update xhat and Sigma
                   xhat_tt = xhat_t1t(:,j+1);
                   Sigma_tt = Sigma_t1t(:,:,j+1);
               end
            end
            
            % COMPUTE SMOOTHED ESTIMATES (Recall here that we need to
            % estimate all terms in the first subsample
            
            y_out(T1+1:T) = y(T1+1:T);
            for j = 1:T1*freq
                xsmooth = xhat_t1t(:,j);    % Initialize smoothing
                Sigmaa = Sigma_t1t(:,:,j);  % Initialize Sigma augmented
                for k = j:T1*freq;
                    if observ( T2 + k )
                        kgain = Sigmaa * Z2/omega(k);
                        xsmooth = xsmooth + kgain*innov(k);
                        Sigmaa = Sigmaa*( PHI2-Sigma_t1t(:,:,k)*Z2*Z2'/omega(k) );
                    else
                        Sigmaa = Sigmaa*PHI2;
                    end
                end
                y_out(T2+j) = xsmooth(1);       % Fixed point estimate
                %y_out(T2+j) = xhat_t1t(1,j);    % Kalman filter estimate
            end
                
        end
                
    end

end     % end function harvey_pierse.m