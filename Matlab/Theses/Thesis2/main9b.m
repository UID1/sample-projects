% 
%  Final master thesis project in Physics to study model estimation of
%  electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011
%
% In this script  we evaluate the results from  the calculations done in
% main9.m

%
% /----------- : BEGIN : Preparations --------------------------------/
%
%

if ~any(strcmp(who,'ELSPOTh2010'))
   fid1 = fopen('elspoth2010.bin', 'r');
   ELSPOTh2010 = fread(fid1, 8760, 'double');
   fclose(fid1);
end
if ~any(strcmp(who,'tmp2010'))
   tmp2010 = temp_proc(8760, 'cubic');
end
deltaELmin = 1e7;
for k = 1:length(ELSPOTh2010)-90*24
	deltaEL = max(ELSPOTh2010(k:k+90*24)) - ...
	          min(ELSPOTh2010(k:k+90*24));
	if deltaEL < deltaELmin
		deltaELmin = deltaEL;
		kmin = k;
	end
end
min90 = (kmin:kmin+90*24);

ELSPEC1 = fspectrum3(ELSPOTh2010(min90), 1);
ELTREND = est_ma(ELSPOTh2010(min90), 500);
ELSPOTdt = ELSPOTh2010(min90)-ELTREND.removable;
ELSPECdt = fspectrum3(ELSPOTdt, 1);
TMPSPEC = fspectrum3(tmp2010.national(min90), 1);

[filt.n1, filt.d1] = butter(2, [450 750]/5e4, 'stop');
[filt.n2, filt.d2] = butter(2, [4100 4300]/5e4, 'stop'); % Partial
[filt.n3, filt.d3] = butter(2, [8350 8420]/5e4, 'stop'); % Max partial
[filt.n4, filt.d4] = butter(2, [1150 1250]/5e4, 'stop');

ELhdes = filter(filt.n1, filt.d1, ...
	               filter(filt.n2, filt.d2, ...
		                ELSPOTh2010(kmin-300:kmin+90*24)));
for i = 1:2
   ELhdes = filter(filt.n3, filt.d3, ...
                        filter(filt.n1, filt.d1, ...
                                ELhdes));
   ELhdes = filter(filt.n3, filt.d3, ELhdes);
end
ELSPECdes = fspectrum3(ELhdes, 1);
ELSPOTds = ELhdes(301:end);
ELTRENDds = est_ma(ELSPOTds, 500);
ELSPOTdsdt = ELhdes(301:end)-ELTRENDds.removable;
ELSPECdsdt = fspectrum3(ELSPOTdsdt, 1);

ELloghds = (ELSPOTds);
ELTRENDdslog = est_ma(ELloghds, 500);
ELloghdsdt = (ELloghds - ELTRENDdslog.removable);
ELloghdsdt = ELloghdsdt/1000;

load ARMAX2est(Mar-05_01-54.761).mat

%
% /----------- :  END  : Preparations --------------------------------/
%
%

% We set up the configuration for our plots

FIGS.LineColors = [[0, 0, 123]; [75, 56, 138]; [127, 138, 199]; ...
                   [214, 229, 230]; [115, 115, 115]; ...
		   [165, 169, 176]; [183, 202, 211]]/255;

% Extract the seasonalities
ELs = (ELSPOTh2010(min90)-ELSPOTds);
FIGS.fig1 = figure('Color',[1 1 1]);
plot(min90', armaxsim(m1b.data, tmp2010.national(min90), ...
       length(min90), 1, 0) + ELTRENDds.removable + ELs, ...
       'Color', FIGS.LineColors(4,:))
hold on
plot(min90, ELSPOTh2010(min90), 'Color', FIGS.LineColors(2,:))
title(['\fontsize{12}Simulated estimations (bright) vs ' ...
       'real observations (dark)'])
set(gca, 'XTick', [5808 6528 7272])
set(gca, 'FontSize', 12, 'XTickLabel', {'August', ...
                    'September', 'October'})
xlim([min90(1) min90(end)])
ylabel('\fontsize{12}SEK/MWh')

FIGS.fig2 = figure('Color',[1 1 1]);
plot(armaxsim(m1a.data, tmp2010.national(min90), 2500, 1, 0), ...
     'Color', FIGS.LineColors(1,:))
title(['\fontsize{12}Illustration of a non-stationary ' ...
      'autoregressive process'])

% The following plot is commented out because it takes time
%
%FIGS.fig3 = figure('Color',[1 1 1]);
%hold on
%for i=1:100
%   ELTREND = est_ma(ELSPOTh2010, 10*i);
%   plot(ELTREND.removable, 'color', [230-2*i 0 230-i]/255, ...
%         'LineWidth', 1)
%end
%set(gca, 'XTick', 0:3*730:8760)
%set(gca, 'FontSize', 12, 'XTickLabel', {'January', ...
%                    'April', 'July', ...
%                    'October', 'December'})
%xlim([0 8760])
%ylabel('\fontsize{12}SEK/MWh')
%title(['\fontsize{12}Estimated trend (100 - 1000 hours) ' ...
%       'for hourly spot prices in 2010'])

FIGS.fig4 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m1b.data, ...
                     tmp2010.national(min90), ...
                     8760, 1, 0));
[f_empir x_empir] = ksdensity(ELSPOTdsdt, 'kernel', 'box');

plot(x_model, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_model, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMAX(3, 17, 13)/1000', '\fontsize{12}Empirical', ...
       'Location', 'NorthWest')

FIGS.fig5 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m1a.data, ...
                     tmp2010.national(min90), ...
                     8760, 1, 0));
plot(x_model, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_model, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMAX(24, 10, 13)', '\fontsize{12}Empirical', ...
       'Location', 'NorthWest')

FIGS.fig6 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m2a.data, ...
                     tmp2010.national(min90), ...
                     8760, 1, 0));
plot(x_model, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_model, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMA(1, 8)', '\fontsize{12}Empirical', ...
       'Location', 'NorthWest')

FIGS.fig7 = figure('Color',[1 1 1]);
[f_model x_model] = ksdensity(armaxsim(m2b.data, ...
                     tmp2010.national(min90), ...
                     8760, 1, 0));
plot(x_model, cumsum(f_model)/sum(f_model), 'Color', FIGS.LineColors(1,:), ...
      'LineWidth', 1.5);
hold on
plot(x_model, cumsum(f_empir)/sum(f_empir), 'Color', FIGS.LineColors(4,:), ...
      'LineWidth', 2.5);
title(['\fontsize{12}Empirical distribution vs. the ' ...
       'distribution of the estimation'])
legend('\fontsize{12}ARMA(2, 7)/1000', '\fontsize{12}Empirical', ...
       'Location', 'NorthWest')

