function [mbest,bestpar]=armabat3(x, pvec, qvec, msg)
% SYNOPSIS
% 
%  [mbest, bestpar] = ARMABAT3(x, pvec, qvec, msg)
%
% DESCRIPTION
%
%  This functions computes ARMA(p,q) models for (p,q) in (x, pvec, qvec)
%  data; it returns the best according to AIC where AIC has been
%  modified to account for fixed parameters.
%
% INPUT
%
%   x      : input data
%   pvec   : vector of p's ; set pvec=[0] for no AR
%   qvec   : vector of q's; set qvec=[0] for no MA
%   msg    : a message or tag to use to identify the run process and
%            monitor progress when running multiple batches
%
% OUTPUT
%
%   mbest             : matlab model structure for the best one found
%                       using the matlab aic the model parameters can be
%                       retreived from mbest
%   bestpar.minaic    : value of the aic at the min
%   bestpar.pbest     : best value of p found
%   bestpar.qbest     : best value of q found
%   bestpar.aicmtx    : a matrix of all computed AIC values
%   bestpar.fepmtx    = a matrix of all computed FPE values
%   bestpar.newaicmtx = a matrix of modified AIC values
%
% NOTES
%
%  Designed for the final master thesis project in Physics to study
%  model estimation of electricity price data. 
%
%  R, Axelsson
%  Chalmers CTH, G�teborg
%  H�sten, 2011


nx=length(x);
np=length(pvec);
nq=length(qvec);
aicsave=-99*ones(np,nq);
newaicsave=-99*ones(np,nq);
fpesave=-99*ones(np,nq);
minaic=1e+6;
for pp=1:np 
    p=pvec(pp);
    for qq=1:nq
        q=qvec(qq);
        if p+q ~=0 % ~= is 'not' equal, or '!='
            orders=[p q]; % Is this really necessary?
            m=armax(x,orders);      % m is a structure with the model stuff in it
            resids=pe(m,x);         % pe returns the prediction errors
            nres=length(resids);
            rhores=acf(resids,1); % this returns the acf normalized by the variance
            nrho=length(rhores);  
            % next compute the Ljung - Box statistic and P-values
            deltak=floor(nrho/10)+1;
            kvec=[p+q+1:deltak:p+q+1+4*deltak];
            for kk=1:5
                Qsum=0;
                for j=2:kvec(kk)+1
                    Qsum=Qsum+(rhores(j).^2)/(nx-j);
                end
                Qsum=nx*(nx-1)*Qsum;
                ljpv(kk)=1-chi2cdf(Qsum,kvec(kk)-p-q);  % df=kvec(kk)-p-q
            end
            aicsave(pp,qq)=aic(m);
            fpesave(pp,qq)=fpe(m);
            AICTEST=1;
            if AICTEST
                nval = m.EstimationInfo.DataLength;
                vval = m.EstimationInfo.LossFcn;
                totalpars=m.na+m.nc;
                netpars=totalpars-length(m.fix);
                aicval=log(vval)+2*netpars/nval;
                disp(sprintf('AIC : %g  NEWAIC : %g',aicsave(pp,qq),aicval));
		newaicsave(pp, qq) = aicval;
            end
            if aicsave(pp,qq) < minaic
                bestpar.minaic=aicsave(pp,qq); % save the min
                bestpar.pbest=p;
                bestpar.qbest=q;
                mbest=m;
            end
            disp(sprintf('(p,q)=(%d,%d) aic=%g  fpe=%g',p,q,aicsave(pp,qq),fpesave(pp,qq)));
            QQ=[kvec;ljpv];
            disp(sprintf('Ljung-Box P-values (%s): ', msg));
            disp(sprintf('  K=%d P-v=%6.3e \n',QQ(:)));

        end
    end
bestpar.aicmtx = aicsave;
bestpar.fepmtx = fpesave;
bestpar.newaicmtx = newaicsave;
end % armabat3.m
