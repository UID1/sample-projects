%
%  Vinkelaccelerationsfunktionen vilken vi vill
%  l�sa genom exempelvis ode45 solvern.
%
%

function dw = wdot(t, w)

global I_p

dw = zeros(3,1);
dw(1) = w(2)*w(3)*(I_p(2)-I_p(3))/I_p(1);
dw(2) = w(3)*w(1)*(I_p(3)-I_p(1))/I_p(2);
dw(3) = w(1)*w(2)*(I_p(1)-I_p(2))/I_p(3);

