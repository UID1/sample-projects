#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <ctime>
#include <locale>
#include <cstdlib>
#include <Windows.h>

/* 8bitsIsOneByte! 
*/

#define QS_MAX_LEVELS 300
typedef long long int64;
typedef unsigned long long uint64;
#define MAX_ZERO_SOLUTIONS 1000000

/* CUDA grid configuration */

#define GDIMX 2//8
#define GDIMY 2//8
#define GDIMZ 1
#define BDIMX 2//8
#define BDIMY 2//8
#define BDIMZ 1
#define NUM_THREADS 16 //4096

#define CHECK(call)                                                           \
{                                                                             \
   const cudaError_t error = call;                                            \
   if (error != cudaSuccess)                                                  \
      {                                                                       \
      printf("Error: %s:%d, ", __FILE__, __LINE__);                           \
      printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));     \
      exit(1);                                                                \
      }                                                                       \
}                                                                             \

struct scEnzConfig
{
	int *vEnzA;  // Vector of elements of enzyme A
	int *vEnzB;  // and B respectively
	int *vDigest; // Vector of resulting double digest of A and B
	int iSizeA; // Vector sizes
	int iSizeB;
	int iSizeDigest;
};

__device__ int getGlobalThreadIdx()
{
	int blockId = blockIdx.x
		+ gridDim.x * blockIdx.y
		+ gridDim.x * gridDim.y * blockIdx.z;
	int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z)
		+ (threadIdx.z * (blockDim.x * blockDim.y))
		+ (threadIdx.y * blockDim.x)
		+ threadIdx.x;

	return threadId;
}

__global__ void testSetup(scEnzConfig *inpCfg)
{
   int sizeA(inpCfg->iSizeA);
   int sizeB(inpCfg->iSizeB);
   int sizeDigest(inpCfg->iSizeDigest);
   int *enzA = new int[sizeA];
   int *enzB = new int[sizeB];
   int *realDigest = new int[sizeDigest];
   int threadId = getGlobalThreadIdx();
   for (int i = 0; i < sizeA; ++i) {
      printf("Thread %d, iteration: %d\n", threadId, i);
      enzA[i] = inpCfg->vEnzA[i];
   }
   for (int i = 0; i < sizeB; ++i)
      enzB[i] = inpCfg->vEnzB[i];
   printf("Enzyme A: ");
   for (int i = 0; i < sizeDigest; ++i)
      realDigest[i] = inpCfg->vDigest[i];
   for (int i = 0; i < sizeA; ++i) {
      printf("%d ", enzA[i]);
   }
   printf("\n\nEnzyme B: ");
   for (int i = 0; i < sizeB; ++i) {
      printf("%d ", enzB[i]);
   }
   printf("\n\nDigested protein: ");
   for (int i = 0; i < sizeDigest; ++i) {
      printf("%d ", realDigest[i]);
   }

	printf("ThreadID %d,%d,%d ", threadIdx.x, threadIdx.y, threadIdx.z);
	printf("BlockID %d,%d,%d ", blockIdx.x, blockIdx.y, blockIdx.z);
	printf("BlockDim %d,%d,%d ", blockDim.x, blockDim.y, blockDim.z);
	printf("GridDim %d,%d,%d ", gridDim.x, gridDim.y, gridDim.z);
}

int main(int argc, char** argv)
{
	int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	CHECK(cudaSetDevice(dev));


	const int enzymeA1Size(6), enzymeB1Size(6), digestedProtein1Size(11);
	const int A1[enzymeA1Size] = { 8479, 4868, 3696, 2646, 169, 142 };
	const int B1[enzymeB1Size] = { 11968, 5026, 1081, 1050, 691, 184 };
	const int C1[digestedProtein1Size] = { 8479, 4167, 2646, 1081, 881,
		859, 701, 691, 184, 169, 142 };

	scEnzConfig *enzConfig1 = new scEnzConfig;
	enzConfig1->iSizeA = enzymeA1Size;
	enzConfig1->iSizeB = enzymeB1Size;
	enzConfig1->iSizeDigest = digestedProtein1Size;
	enzConfig1->vEnzA = new int[enzymeA1Size];
	enzConfig1->vEnzB = new int[enzymeB1Size];
	enzConfig1->vDigest = new int[digestedProtein1Size];
	for (int i = 0; i < enzymeA1Size; ++i)
		enzConfig1->vEnzA[i] = A1[i];
	for (int i = 0; i < enzymeB1Size; ++i)
		enzConfig1->vEnzB[i] = B1[i];
	for (int i = 0; i < digestedProtein1Size; ++i)
		enzConfig1->vDigest[i] = C1[i];

	/* Let's transfer the enzyme parameters to the device */
	scEnzConfig *dEnzConfig1;
   int *vEnzA1;
   /* A POINTER is a variable that holds MEMORY ADDRESS as a value,
      here, vEnzA is a pointer to an integer value. If an address is assigned to
      vEnzA 'std::cout << vEnzA' will print the address and 'std::cout << *vEnzA' will
      print out the contents of that address */
   std::cout << "Got to line: 129" << std::endl;
   //CHECK(cudaMalloc((int **)&enzConfig1->vEnzA,enzConfig1->iSizeA*sizeof(int)));   
   //std::cout << "Got to line: 11..." << std::endl;
   /* Allocate memory of struct scEnzConfig and store the location in the address of dEnzConfig1 */
	CHECK(cudaMalloc((scEnzConfig **)&dEnzConfig1, sizeof(scEnzConfig)));
   /* Transfer/copy struct data on host to VRAM */
   CHECK(cudaMemcpy(dEnzConfig1, enzConfig1, sizeof(scEnzConfig), cudaMemcpyHostToDevice));
   /* Allocate memory for a free vector in VRAM and set the address of it to vEnzA */   
   CHECK(cudaMalloc(&vEnzA1, enzymeA1Size*sizeof(int)));
   /* Transfer the location of that free vector to the pointer inside the struct */
   CHECK(cudaMemcpy(&dEnzConfig1->vEnzA, &vEnzA1, sizeof(int *), cudaMemcpyHostToDevice));
   /* Copy data on the host vector to the struct via the free vector pointer */
   CHECK(cudaMemcpy(vEnzA1, enzConfig1->vEnzA, enzymeA1Size*sizeof(int), cudaMemcpyHostToDevice));

   /* We do the same thing for enzyme B */
   int *vEnzB1; // In fact, we could recycle the int pointer above
   /* The struct above is already defined and allocated so we just keep in adding onto it */
   CHECK(cudaMalloc(&vEnzB1, enzymeB1Size*sizeof(int)));
   CHECK(cudaMemcpy(&dEnzConfig1->vEnzB, &vEnzB1, sizeof(int *), cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(vEnzB1, enzConfig1->vEnzB, enzymeB1Size*sizeof(int), cudaMemcpyHostToDevice));
   int *vDigest1;
   CHECK(cudaMalloc(&vDigest1, digestedProtein1Size*sizeof(int)));
   CHECK(cudaMemcpy(&dEnzConfig1->vDigest, &vDigest1, sizeof(int *), cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(vDigest1, enzConfig1->vDigest, digestedProtein1Size*sizeof(int), cudaMemcpyHostToDevice));
   
   std::cout << "Got to line: 154" << std::endl;
	//CHECK(cudaMemcpy(dEnzConfig1, enzConfig1, 
	//	sizeof(scEnzConfig), cudaMemcpyHostToDevice));
	
	/* Set up grid and block for execution */
	dim3 grid(GDIMX, GDIMY, GDIMZ);
	dim3 block(BDIMX, BDIMY, BDIMZ);

   /* Test print some results */
   for (int i=0; i < enzConfig1->iSizeA; i++)
   {
      std::cout << enzConfig1->vEnzA[i] << " ";
   }
   std::cout << std::endl;
	/* Launch kernel */

	testSetup <<< grid, block >>> (dEnzConfig1);

   CHECK(cudaDeviceSynchronize());
   CHECK(cudaFree(dEnzConfig1));
   CHECK(cudaFree(vEnzA1));
   CHECK(cudaFree(vEnzB1));
   CHECK(cudaFree(vDigest1));
	delete[] enzConfig1;
}
